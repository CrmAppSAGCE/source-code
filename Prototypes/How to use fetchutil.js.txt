/// <reference path="library/fetchUtil.js" />
var _oService;
var _sOrgName = "";
var _sServerUrl = "http://172.19.4.33" + Xrm.Page.context.prependOrgName("");

function doStuff()
{
            var result = queryUserRoles();
            var isCashbookController = false;
            var isShop = result.length > 0;
                for (var i = 0; i < result.length; i++) {
                if (result[i].attributes.isp_usertype.value == 1 || result[i].attributes.isp_usertype.value == 863300001)
                    return true;
                else
                    return false;
            }
            return false;
}
					
var ur = null;
function queryUserRoles() {
    if (ur != null)
        return ur;

    var ownerId = Xrm.Page.getAttribute('ownerid').getValue()[0].id;

    var xml = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">' +
              '   <entity name="systemuser">' +
              '     <attribute name="systemuserid" />' +
              '     <attribute name="parentsystemuserid" />' +
              '     <attribute name="isp_usertype" />' +
              '     <order attribute="isp_usertype" descending="false" />' +
              '     <filter type="and">' +
              '       <filter type="or">' +
              '         <condition attribute="systemuserid" operator="eq-userid" />' +
              '         <condition attribute="systemuserid" operator="in">' +
              '           <value uitype="systemuser">' + ownerId + '</value>' +
              '         </condition>' +
              '       </filter>' +
              '     </filter>' +
   '     <link-entity name="systemuserroles" from="systemuserid" to="systemuserid" visible="false" intersect="true" link-type="outer">' +
              '       <link-entity name="role" from="roleid" to="roleid" link-type="outer" alias="ab">' +
              '         <attribute name="name" />' +
              '         <filter type="and">' +
              '           <condition attribute="name" operator="eq" value="Cashbook Controller" />' +
              '         </filter>' +
              '       </link-entity>' +
              '     </link-entity>' +
              '   </entity>' +
              ' </fetch>';

    _oService = new FetchUtil(_sOrgName, _sServerUrl);
    ur = _oService.Fetch(xml);
    // ur = $crm.data.Fetch(xml);
    return ur;
}