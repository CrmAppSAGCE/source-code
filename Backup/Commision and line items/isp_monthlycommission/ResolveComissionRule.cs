﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Scoin.Crm.Plugins.isp_monthlycommission
{
    public class ResolveComissionRule :IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext) serviceProvider.GetService(typeof (IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory) serviceProvider.GetService(typeof (IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            var target = new EntityHelper(context, service);

            if (target.HasChanged("isp_totalsalesamount"))
            {
                var user = service.Retrieve("systemuser", target.GetAttributeValue<EntityReference>("ownerid").Id, new ColumnSet(new[] { "businessunitid" }));
                var salesamount = target.GetAttributeValue<Money>("isp_totalsalesamount").Value;

                var query = new QueryExpression("isp_commissionrule") { ColumnSet = new ColumnSet(new[] { "isp_defaultcommissionpercentage", "isp_defaultcommissionamount" }) };
                query.Criteria.AddCondition("isp_isaggregaterule", ConditionOperator.Equal, true);
                query.Criteria.AddCondition("statecode", ConditionOperator.Equal, "Active");
                query.PageInfo = new PagingInfo { Count = 1, PageNumber = 1 };

                var ismanagerfilter = query.Criteria.AddFilter(LogicalOperator.Or);
                ismanagerfilter.AddCondition("isp_ismanager", ConditionOperator.Null);
                ismanagerfilter.AddCondition("isp_ismanager", ConditionOperator.Equal, target.Contains("isp_businessunitlevel") && target.GetAttributeValue<int?>("isp_businessunitlevel").HasValue);

                var businessunitFilter = query.Criteria.AddFilter(LogicalOperator.Or);
                businessunitFilter.AddCondition("isp_businessunitid", ConditionOperator.Null);
                businessunitFilter.AddCondition("isp_businessunitid", ConditionOperator.Equal, user.GetAttributeValue<EntityReference>("businessunitid").Id);

                var targetLink = query.AddLink("isp_commissionruletarget", "isp_commissionruleid", "isp_commissionruleid", JoinOperator.LeftOuter);
                targetLink.EntityAlias = "target";
                targetLink.Columns = new ColumnSet(new[] { "isp_commissionpercentage", "isp_commissionamount" });
                var minFilter = targetLink.LinkCriteria.AddFilter(LogicalOperator.Or);
                minFilter.AddCondition("isp_targetminvalue", ConditionOperator.Null);
                minFilter.AddCondition("isp_targetminvalue", ConditionOperator.LessEqual, salesamount);

                var maxFilter = targetLink.LinkCriteria.AddFilter(LogicalOperator.Or);
                maxFilter.AddCondition("isp_targetmaxvalue", ConditionOperator.Null);
                maxFilter.AddCondition("isp_targetmaxvalue", ConditionOperator.GreaterThan, salesamount);

                query.AddOrder("isp_businessunitid", OrderType.Descending);
                query.AddOrder("isp_producttype", OrderType.Descending);

                var results = service.RetrieveMultiple(query);
                if (results.Entities.Count > 0)
                {
                    target.GetAttributeValue<Money>("isp_calculatedcommission").Value = 0;
                    target["isp_commissionruleid"] = results[0].ToEntityReference();

                    if (results.Entities[0].Contains("target.isp_commissionpercentage"))
                        target.GetAttributeValue<Money>("isp_calculatedcommission").Value += ((Decimal)results[0].GetAttributeValue<AliasedValue>("target.isp_commissionpercentage").Value) * salesamount / 100;
                    else if (results.Entities[0].Contains("isp_defaultcommissionpercentage"))
                        target.GetAttributeValue<Money>("isp_calculatedcommission").Value += results[0].GetAttributeValue<Decimal>("isp_defaultcommissionpercentage") * salesamount / 100;

                    if (results[0].Attributes.Contains("target.isp_commissionamount"))
                        target.GetAttributeValue<Money>("isp_calculatedcommission").Value += ((Money)results[0].GetAttributeValue<AliasedValue>("target.isp_commissionamount").Value).Value;
                    else if (results[0].Attributes.Contains("isp_defaultcommissionamount"))
                        target.GetAttributeValue<Money>("isp_calculatedcommission").Value += results[0].GetAttributeValue<Money>("isp_defaultcommissionamount").Value;
                }
            }

            if (target.HasChanged("isp_calculatedcommission") || target.HasChanged("isp_aggregatedcommission") || target.HasChanged("isp_overwrittencommission") || target.HasChanged("isp_overwrittenby"))
                target.GetAttributeValue<Money>("isp_totalcommission").Value = ((target.Contains("isp_overwrittencommission") && target.Contains("isp_overwrittenby")) ? target.GetAttributeValue<Money>("isp_overwrittencommission").Value : target.GetAttributeValue<Money>("isp_calculatedcommission").Value) + target.GetAttributeValue<Money>("isp_aggregatedcommission").Value;
        }
    }
}
