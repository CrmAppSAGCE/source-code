/// <reference path="library/crm.js" />
/// <reference path="library/crm.data.js" />
/// <reference path="library/XrmPage-vsdoc.js" />


function isp_invoicelineitemcommission_onload() {
PrintPopUp();
    if (Xrm.Page.ui.getFormType() == 1) {
        Xrm.Page.getControl('isp_producttype').setDisabled(false);
        Xrm.Page.getControl('isp_invoiceid').setDisabled(false);
    }
    else {
        Xrm.Page.getControl('isp_producttype').setDisabled(true);
        Xrm.Page.getControl('isp_invoiceid').setDisabled(true);
    }
    var OwnerId = Xrm.Page.data.entity.attributes.get("ownerid").getValue()[0].id.substring(1, 37).toLowerCase();
    var owner = $crm.data.Retrieve('systemuser', OwnerId, ['businessunitid', 'parentsystemuserid']);
    var CurrentUserId = Xrm.Page.context.getUserId().toString().substring(1, 37).toLowerCase();
    var currentuser = $crm.data.Retrieve('systemuser', CurrentUserId, ['businessunitid', 'isp_usertype']);
    var currentuserbusinessunit = $crm.data.Retrieve('businessunit', currentuser.attributes.businessunitid.value.substring(1, 37), ['isp_businessunitlevel']);
    if (currentuser.attributes.isp_usertype.value == 1 || currentuser.attributes.isp_usertype.value == 2) {
        Xrm.Page.ui.tabs.get(2).setVisible(false);
    }
    Xrm.Page.getAttribute('isp_producttype').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_invoiceid').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_splitpercentage').setSubmitMode("always");
    Xrm.Page.getControl('isp_overwrittenamount').setDisabled(true);

    // Determine if the overriding amount is available
    if (CurrentUserId != OwnerId) {

        if (Xrm.Page.getAttribute('isp_bulevelofoverwritinguser').getValue() == null) {

            if (owner.attributes.parentsystemuserid != null) {
                if (CurrentUserId == owner.attributes.parentsystemuserid.value.substring(1, 37).toLowerCase())
                    Xrm.Page.getControl('isp_overwrittenamount').setDisabled(false);
                else {
                    var ownermanager = $crm.data.Retrieve('systemuser', owner.attributes.parentsystemuserid.value.substring(1, 37), ['businessunitid']);
                    var ownermanagerbusinessunit = $crm.data.Retrieve('businessunit', ownermanager.attributes.businessunitid.value.substring(1, 37), ['isp_businessunitlevel']);
                    if (ownermanagerbusinessunit.attributes.isp_businessunitlevel != null && currentuserbusinessunit.attributes.isp_businessunitlevel != null && ownermanagerbusinessunit.attributes.isp_businessunitlevel.value >= currentuserbusinessunit.attributes.isp_businessunitlevel.value)
                        Xrm.Page.getControl('isp_overwrittenamount').setDisabled(false);
                }
            }
        }
        else {
            if (currentuserbusinessunit.attributes.isp_businessunitlevel != null && Xrm.Page.getAttribute('isp_bulevelofoverwritinguser').getValue() >= currentuserbusinessunit.attributes.isp_businessunitlevel.value)
                Xrm.Page.getControl('isp_overwrittenamount').setDisabled(false);
        }

    }

}

function isp_invoicelineitemcommission_isp_onsave() {
   // debugger;
    if (Xrm.Page.getAttribute('isp_overwrittenamount').getIsDirty()) {
        var CurrentUserId = Xrm.Page.context.getUserId().toString().substring(1, 37).toLowerCase();
        var currentuser = $crm.data.Retrieve('systemuser', CurrentUserId, ['businessunitid', 'fullname']);
        var currentuserbusinessunit = $crm.data.Retrieve('businessunit', currentuser.attributes.businessunitid.value.substring(1, 37), ['isp_businessunitlevel']);

        if (currentuserbusinessunit.attributes.isp_businessunitlevel != null) {
            Xrm.Page.getAttribute('isp_bulevelofoverwritinguser').setValue(eval(currentuserbusinessunit.attributes.isp_businessunitlevel.value));
            Xrm.Page.getAttribute('isp_bulevelofoverwritinguser').setSubmitMode('always');
        }
        Xrm.Page.getAttribute('isp_overwrittenby').setValue([{ id: CurrentUserId, name: currentuser.attributes.fullname ? currentuser.attributes.fullname.value : '', entityType: 'systemuser'}]);
        Xrm.Page.getAttribute('isp_overwrittenby').setSubmitMode('always');
    }
}

function isp_invoicelineitemcommission_isp_overwrittenamount_onchange() {



}
function PrintPopUp()
{
var InvoiceNumber = 'IN125242';
var windowName = "printElementWindow";
windowName = windowName + (Math.round(Math.random() * 99999)).toString(); 

var newWin = window.open('http://sagcesql/ReportServer/Pages/ReportViewer.aspx?%2fCoinnect_MSCRM%2fCustomReports%2fInvoice+Copy+Report&rs:Command=Render&invoicenumber=IN125242&rc:Parameters=false', windowName , 'width=350,height=350');
			
	debugger;		
//newWin.document.write( //var URL = "http://172.19.4.14/ReportServer/Pages/ReportViewer.aspx?%2fCoinnect_MSCRM%2fCustomReports%2fInvoice+Copy+Report&rs:Command=Render&invoicenumber="+InvoiceNumber+"&rc:Parameters=false"; );

newWin.focus();                     // not sure if this line is necessary
newWin.print();
}