﻿/// <reference path="library/crm.js" />
/// <reference path="library/crm.data.js" />
/// <reference path="library/XrmPage-vsdoc.js" />



function ensureDataIntegrity() {
    if (Xrm.Page.getAttribute('isp_targetminvalue') == null || Xrm.Page.getAttribute('isp_targetmaxvalue') == null)
        return;
    if (Xrm.Page.getAttribute('isp_targetminvalue').getValue() > Xrm.Page.getAttribute('isp_targetmaxvalue').getValue()) {
        alert("Target Min Value cannot be greater than Target Max Value");
        window.event.returnValue = false;
    }
}

function isp_commissionruletarget_isp_targetminvalue_onchange() {

}

function isp_commissionruletarget_isp_targetmaxvalue_onchange() {

}
function isp_commissionruletarget_onsave() {
    ensureDataIntegrity();
}
