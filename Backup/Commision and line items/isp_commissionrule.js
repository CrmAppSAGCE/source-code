﻿/// <reference path="library/crm.js" />
/// <reference path="library/crm.data.js" />
/// <reference path="library/XrmPage-vsdoc.js" />

function isp_commissionrule_onload() {

    EnsureAggregateTargets();
    if (Xrm.Page.getAttribute('isp_producttype') != null)
        Xrm.Page.getAttribute('isp_producttype').setSubmitMode('always');

}

function isp_commissionrule_isp_isaggregaterule_onchange() {
    EnsureAggregateTargets();
}

function EnsureAggregateTargets() {

    Xrm.Page.ui.navigation.items.get('nav_isp_isp_CommissionRuleTarget_commissionruleid').setVisible(!(Xrm.Page.getAttribute('isp_isaggregaterule').getValue() == "0"));
    if (Xrm.Page.getAttribute('isp_isaggregaterule').getValue() == "0") {
        if (Xrm.Page.getAttribute('isp_ismanager') != null) {
            Xrm.Page.getAttribute('isp_ismanager').setValue(0);
            Xrm.Page.getControl('isp_ismanager').setDisabled(true);
        }
          if (Xrm.Page.getAttribute('isp_saletype') != null)
              Xrm.Page.getControl('isp_saletype').setDisabled(false);
          if (Xrm.Page.getAttribute('isp_producttype') != null)
              Xrm.Page.getControl('isp_producttype').setDisabled(false);
    }
    else {
        if (Xrm.Page.getAttribute('isp_ismanager') != null) {
            Xrm.Page.getControl('isp_ismanager').setDisabled(false);
            Xrm.Page.getAttribute('isp_ismanager').setValue(null);
        }
            if (Xrm.Page.getAttribute('isp_saletype') != null) 
            {
                Xrm.Page.getAttribute('isp_saletype').setValue(100000000)//sale
                Xrm.Page.getControl('isp_saletype').setDisabled(true);
            }
            if (Xrm.Page.getAttribute('isp_producttype') != null) {
                Xrm.Page.getAttribute('isp_producttype').setValue(2)//rare
                Xrm.Page.getControl('isp_producttype').setDisabled(true);
            }
    }
}
