﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
namespace Scoin.Crm.Plugins.isp_invoicelineitemcommission
{
    public class Update : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            var target = new EntityHelper(context, service);
            var Target = (Entity)context.InputParameters["Target"];
            if (target.GetAttributeValue<OptionSetValue>("statuscode").Value == 863300004 && !target.HasChanged("statuscode"))
            {
                throw new Exception("You cannot update the commission as it has already been accepted.");
            }

            //if (Target.GetAttributeValue<OptionSetValue>("statuscode").Value != 863300005 )
            //{
            //   Target.Attributes["statecode"] = 1;
            //   Target.Attributes["statuscode"] = 863300005;
                
            //}


        }
    }

}
