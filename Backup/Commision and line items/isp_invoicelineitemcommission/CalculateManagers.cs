﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Scoin.Crm.Plugins.isp_invoicelineitemcommission
{
    public class CalculateManagers : IPlugin
    {
        private static Dictionary<int, string> _map;
        public static Dictionary<int, string> Map
        {
            get
            {
                if (_map == null)
                {
                    _map = new Dictionary<int, string>();
                    _map.Add(3, "isp_shopmonthlycommissionid");
                    _map.Add(2, "isp_regionalmonthlycommissionid");
                    _map.Add(1, "isp_channelmonthlycommissionid");
                }
                return _map;
            }   
        }

        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            var moniker = (EntityReference)context.InputParameters["EntityMoniker"];
            var target = service.Retrieve(moniker.LogicalName, moniker.Id, new ColumnSet(true)); //new EntityHelper(context, service);
            var statuscode = (OptionSetValue)context.InputParameters["Status"];
//Commented out the check as the commission now needs to be auto accepted
          //  if (statuscode.Value == 863300005)
          //  {
                var userEntity = service.Retrieve("systemuser", target.GetAttributeValue<EntityReference>("owninguser").Id, new ColumnSet("fullname", "businessunitid"));
                Entity bu = service.Retrieve("businessunit", userEntity.GetAttributeValue<EntityReference>("businessunitid").Id, new ColumnSet(true));
                Entity invoice = service.Retrieve("invoice", target.GetAttributeValue<EntityReference>("isp_invoiceid").Id, new ColumnSet(new [] {"isp_saletype"}));
                bool aggregateSales = invoice.GetAttributeValue<OptionSetValue>("isp_saletype").Value == 100000000 && target.GetAttributeValue<OptionSetValue>("isp_producttype").Value == 2;  // Sale && Rare

                Entity MonthlyComm = GetMonthlyCommEntry(target, target.GetAttributeValue<EntityReference>("owninguser"), bu, factory, true, aggregateSales);

                target["isp_brokermonthlycommissionid"] = MonthlyComm.ToEntityReference();
                while (bu != null)
                {
                    if (bu.Contains("isp_businessunitlevel"))
                    {
                        var buLevel = bu.GetAttributeValue<int>("isp_businessunitlevel");
                        if (Map.ContainsKey(buLevel) && bu.Contains("isp_managerid"))
                        {
                            var field = Map[buLevel];
                            MonthlyComm = GetMonthlyCommEntry(target, bu.GetAttributeValue<EntityReference>("isp_managerid"), bu, factory, false, aggregateSales);
                            target[field] = MonthlyComm.ToEntityReference();
                            //service.Update(MonthlyComm);
                        }

                        if (buLevel <= 1)
                            bu = null;
                    }

                    if (bu != null && bu.Contains("parentbusinessunitid"))
                        bu = service.Retrieve("businessunit", bu.GetAttributeValue<EntityReference>("parentbusinessunitid").Id, new ColumnSet(true));
                    else
                        bu = null;
                }
                service.Update(target);
           // }
        }

        private Entity GetMonthlyCommEntry(EntityHelper target, EntityReference User, Entity BU, IOrganizationServiceFactory factory, bool isBroker, bool aggregateSales)
        {
            var service = factory.CreateOrganizationService(User.Id);

            var monthlyCommQuery = new QueryExpression("isp_monthlycommission") { Criteria = new FilterExpression(), ColumnSet = new ColumnSet(new [] {"isp_calculatedcommission", "isp_totalsalesamount", "isp_aggregatedcommission"}) };
            //if (User != null)
            monthlyCommQuery.Criteria.AddCondition("ownerid", ConditionOperator.Equal, User.Id);
            monthlyCommQuery.Criteria.AddCondition("isp_businessunitid", ConditionOperator.Equal, BU.Id);
            monthlyCommQuery.Criteria.AddCondition("isp_cyclestartdate", ConditionOperator.LessEqual, DateTime.Now.Date);
            monthlyCommQuery.Criteria.AddCondition("isp_cycleenddate", ConditionOperator.GreaterEqual, DateTime.Now.Date);
            monthlyCommQuery.Criteria.AddCondition("statecode", ConditionOperator.Equal, "Active");
            if (isBroker)
                monthlyCommQuery.Criteria.AddCondition("isp_businessunitlevel", ConditionOperator.Null);
            else
                monthlyCommQuery.Criteria.AddCondition("isp_businessunitlevel", ConditionOperator.Equal, BU.GetAttributeValue<int>("isp_businessunitlevel"));

            var monthlyCommResult = service.RetrieveMultiple(monthlyCommQuery);

            EntityHelper comm;
            bool create = false;

            if (monthlyCommResult.Entities.Count == 0)
            {
                comm = new Entity("isp_monthlycommission");
                create = true;

                //comm["isp_userid"] = User;
                comm["isp_businessunitid"] = BU.ToEntityReference();
                if (!isBroker)
                    comm["isp_businessunitlevel"] = BU["isp_businessunitlevel"];

                CommissionPeriod newCommPeriod = CalculatePeriod(BU);
                comm["isp_cyclestartdate"] = newCommPeriod.StartDate;
                comm["isp_cycleenddate"] = newCommPeriod.EndDate;

                comm["isp_name"] = string.Format("{0}{1} ({2}-{3})", isBroker ? "" : "Manager: ", User.Name, newCommPeriod.StartDate.ToShortDateString(), newCommPeriod.EndDate.ToShortDateString());
                
            } else
                comm = monthlyCommResult.Entities[0];

            if (aggregateSales)
                comm.GetAttributeValue<Money>("isp_totalsalesamount").Value += target.GetAttributeValue<Money>("isp_totalamount").Value;

            if (isBroker)
                comm.GetAttributeValue<Money>("isp_aggregatedcommission").Value += (target.Contains("isp_overwrittenamount") && target.Contains("isp_overwrittenby") ) ? target.GetAttributeValue<Money>("isp_overwrittenamount").Value : target.GetAttributeValue<Money>("isp_commissionamount").Value;

            if (create)
            {
                comm.Id = service.Create(comm); 
                /*comm.Id = service.Create(comm);
                service.Execute(new Microsoft.Crm.Sdk.Messages.AssignRequest { Assignee = User, Target = ((Entity)comm).ToEntityReference() });*/
            }
            else if (isBroker || aggregateSales)
                service.Update(comm);

            return comm;
        }

        private CommissionPeriod CalculatePeriod(Entity BU)
        {
            //var result = Service.Retrieve("systemuser", Guid.Parse(UserID), new ColumnSet(new[] { "isp_cycleclosedate" }));

            int cycleCloseDay = 25;
            if (BU.Contains("isp_cycleenddate") && BU["isp_cycleenddate"] != null)
                cycleCloseDay = BU.GetAttributeValue<int>("isp_cycleenddate");

            CommissionPeriod CycleDates = new CommissionPeriod();

            if (cycleCloseDay >= DateTime.Now.Day)
            {
                CycleDates.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, cycleCloseDay);
                CycleDates.StartDate = CycleDates.EndDate.AddMonths(-1).AddDays(1);
            }
            else
            {
                CycleDates.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, cycleCloseDay + 1);
                CycleDates.EndDate = CycleDates.StartDate.AddMonths(1).AddDays(-1);
            }

            return CycleDates;
        }

        private struct CommissionPeriod
        {
            public DateTime StartDate;
            public DateTime EndDate;
        }

    }
}
