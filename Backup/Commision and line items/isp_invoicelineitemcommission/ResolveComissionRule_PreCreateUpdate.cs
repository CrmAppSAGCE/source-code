﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Scoin.Crm.Plugins.isp_invoicelineitemcommission
{
    public class ResolveComissionRule_PreCreateUpdate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext) serviceProvider.GetService(typeof (IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory) serviceProvider.GetService(typeof (IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            var target = new EntityHelper(context, service);

            if (target.HasChanged("isp_totalamount") || target.HasChanged("isp_totalquantity") || target.HasChanged("isp_splitpercentage"))
            {
                var user = service.Retrieve("systemuser", target.GetAttributeValue<EntityReference>("ownerid").Id, new ColumnSet(new [] {"businessunitid"}));
                var invoice = service.Retrieve("invoice", target.GetAttributeValue<EntityReference>("isp_invoiceid").Id, new ColumnSet(new [] { "isp_saletype"}));

                var query = new QueryExpression("isp_commissionrule") { ColumnSet = new ColumnSet(new[] { "isp_defaultcommissionpercentage", "isp_defaultcommissionamount" }) };
                query.Criteria.AddCondition("isp_isaggregaterule", ConditionOperator.Equal, false);
                query.Criteria.AddCondition("isp_saletype", ConditionOperator.Equal, invoice.GetAttributeValue<OptionSetValue>("isp_saletype").Value);
                query.Criteria.AddCondition("statecode", ConditionOperator.Equal, "Active");
                
                query.PageInfo = new PagingInfo {Count = 1, PageNumber = 1};

                var ismanagerfilter = query.Criteria.AddFilter(LogicalOperator.Or);
                ismanagerfilter.AddCondition("isp_ismanager", ConditionOperator.Null);
                ismanagerfilter.AddCondition("isp_ismanager", ConditionOperator.Equal, false);
                
                var productFilter = query.Criteria.AddFilter(LogicalOperator.Or);
                productFilter.AddCondition("isp_producttype", ConditionOperator.Null);
                productFilter.AddCondition("isp_producttype", ConditionOperator.Equal, target.GetAttributeValue<OptionSetValue>("isp_producttype").Value);

                var businessunitFilter = query.Criteria.AddFilter(LogicalOperator.Or);
                businessunitFilter.AddCondition("isp_businessunitid", ConditionOperator.Null);
                businessunitFilter.AddCondition("isp_businessunitid", ConditionOperator.Equal, user.GetAttributeValue<EntityReference>("businessunitid").Id);

                query.AddOrder("isp_businessunitid", OrderType.Descending);
                query.AddOrder("isp_producttype", OrderType.Descending);

                var results = service.RetrieveMultiple(query);
if (results.Entities.Count > 0)
                {
                    target.GetAttributeValue<Money>("isp_commissionamount").Value = 0;
                    target["isp_commissionruleid"] = results[0].ToEntityReference();

                    if ((target.Contains("isp_splitpercentage")))
                    {
                        if (results[0].Attributes.Contains("isp_defaultcommissionpercentage"))
                            target.GetAttributeValue<Money>("isp_commissionamount").Value += results[0].GetAttributeValue<decimal>("isp_defaultcommissionpercentage") * target.GetAttributeValue<Money>("isp_totalamount").Value / 100 * target.GetAttributeValue<Decimal>("isp_splitpercentage") / 100;
                        if (results[0].Attributes.Contains("isp_defaultcommissionamount"))
                            target.GetAttributeValue<Money>("isp_commissionamount").Value  += results[0].GetAttributeValue<Money>("isp_defaultcommissionamount").Value * target.GetAttributeValue<decimal>("isp_totalquantity") * target.GetAttributeValue<Decimal>("isp_splitpercentage") / 100;
                    }
                    else
                    {
                        if (results[0].Attributes.Contains("isp_defaultcommissionpercentage"))
                            target.GetAttributeValue<Money>("isp_commissionamount").Value += results[0].GetAttributeValue<decimal>("isp_defaultcommissionpercentage") * target.GetAttributeValue<Money>("isp_totalamount").Value / 100;
                        if (results[0].Attributes.Contains("isp_defaultcommissionamount"))
                            target.GetAttributeValue<Money>("isp_commissionamount").Value += results[0].GetAttributeValue<Money>("isp_defaultcommissionamount").Value * target.GetAttributeValue<decimal>("isp_totalquantity");
                    }
                }

                else
                    throw new Exception(string.Format("No commission rule exist for this commission split (Rule Type: Broker, Business Unit: {0}, Product Type: {1}, Sale Type: {2}) ", user.GetAttributeValue<EntityReference>("businessunitid").Name, target.GetAttributeValue<OptionSetValue>("isp_producttype").Value, invoice.GetAttributeValue<OptionSetValue>("isp_saletype").Value));

            }


            
            

        }

        
    }
}
