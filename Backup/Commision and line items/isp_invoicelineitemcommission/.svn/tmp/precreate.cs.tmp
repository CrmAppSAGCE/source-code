﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
namespace Scoin.Crm.Plugins.isp_invoicelineitemcommission
{
    public class precreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            var InvoiceId = target.GetAttributeValue<EntityReference>("isp_invoiceid").Id;

            var InvoiceQuery = new QueryExpression("isp_invoicelineitemcommission") { Criteria = new FilterExpression(), ColumnSet = new ColumnSet(new[] { "isp_splitpercentage" }) };
            InvoiceQuery.Criteria.AddCondition("isp_invoiceid", ConditionOperator.Equal, InvoiceId.ToString());
            InvoiceQuery.Criteria.AddCondition("isp_producttype", ConditionOperator.Equal, target.GetAttributeValue<OptionSetValue>("isp_producttype"));
            //check to see if the entity message = Update and add condition not to include the entityid

            if (context.MessageName.ToLower() == "update")
            {
                InvoiceQuery.Criteria.AddCondition("isp_invoicelinecommissionid", ConditionOperator.NotEqual, target.Id);
            }

            var RetrieveResult = service.RetrieveMultiple(InvoiceQuery);
            Decimal SplitPercentageLeft = 100;
            
            for (int i = 0; i < RetrieveResult.Entities.Count; i++)
            {
                if (RetrieveResult.Entities[i].Attributes["isp_splitpercentage"] != null )
                {
                    SplitPercentageLeft -= RetrieveResult.Entities[i].GetAttributeValue<Decimal>("isp_splitpercentage");
                }
            }

            if (target.GetAttributeValue<Decimal>("isp_splitpercentage") > SplitPercentageLeft)
            {
                throw new Exception("Highest Split Percentage that can be supplied is "+SplitPercentageLeft.ToString() +".");
            }

        }

        private Money GetCommission(BusinessUnit BU, int ProductType, bool IsBuyBack, Decimal SaleAmount, Decimal CommModifier, IOrganizationService Service)
        {
            var commissionRuleQuery = new QueryExpression("isp_CommissionRule") { Criteria = new FilterExpression(), ColumnSet = new ColumnSet(true) };
            commissionRuleQuery.Criteria.AddCondition("isp_businessunitid", ConditionOperator.Equal, BU.ToEntityReference());
            commissionRuleQuery.Criteria.AddCondition("isp_producttype", ConditionOperator.Equal, ProductType);
            commissionRuleQuery.Criteria.AddCondition("isp_isbuyback", ConditionOperator.Equal, IsBuyBack);
            commissionRuleQuery.Criteria.AddCondition("isp_isaggregaterule", ConditionOperator.NotEqual, true);

            var result = Service.RetrieveMultiple(commissionRuleQuery);
            if (result.Entities.Count > 0)
            {
                isp_CommissionRule rule = (isp_CommissionRule)result.Entities[0];
                if (rule.isp_defaultcommissionpercentage.HasValue)
                    return new Money(rule.isp_defaultcommissionpercentage.Value * SaleAmount * CommModifier);

                return new Money(rule.isp_defaultcommissionamount.Value * CommModifier);
            }

            commissionRuleQuery.Criteria.Conditions.Clear();
            commissionRuleQuery.Criteria.AddCondition("isp_businessunitid", ConditionOperator.Equal, BU.ToEntityReference());
            commissionRuleQuery.Criteria.AddCondition("isp_producttype", ConditionOperator.Null);
            commissionRuleQuery.Criteria.AddCondition("isp_isbuyback", ConditionOperator.Equal, IsBuyBack);
            commissionRuleQuery.Criteria.AddCondition("isp_isaggregaterule", ConditionOperator.NotEqual, true);

            result = Service.RetrieveMultiple(commissionRuleQuery);
            if (result.Entities.Count > 0)
            {
                isp_CommissionRule rule = (isp_CommissionRule)result.Entities[0];
                if (rule.isp_defaultcommissionpercentage.HasValue)
                    return new Money(rule.isp_defaultcommissionpercentage.Value * SaleAmount * CommModifier);

                return new Money(rule.isp_defaultcommissionamount.Value * CommModifier);
            }

            commissionRuleQuery.Criteria.Conditions.Clear();
            commissionRuleQuery.Criteria.AddCondition("isp_businessunitid", ConditionOperator.Null);
            commissionRuleQuery.Criteria.AddCondition("isp_producttype", ConditionOperator.Equal, ProductType);
            commissionRuleQuery.Criteria.AddCondition("isp_isbuyback", ConditionOperator.Equal, IsBuyBack);
            commissionRuleQuery.Criteria.AddCondition("isp_isaggregaterule", ConditionOperator.NotEqual, true);

            result = Service.RetrieveMultiple(commissionRuleQuery);
            if (result.Entities.Count > 0)
            {
                isp_CommissionRule rule = (isp_CommissionRule)result.Entities[0];
                if (rule.isp_defaultcommissionpercentage.HasValue)
                    return new Money(rule.isp_defaultcommissionpercentage.Value * SaleAmount * CommModifier);

                return new Money(rule.isp_defaultcommissionamount.Value * CommModifier);
            }

            return new Money(0);
        }
    }
}
