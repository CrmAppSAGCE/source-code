﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Scoin.Crm.Plugins.isp_invoicelineitemcommission
{
    public class CalculateTotalPerProductType_PreCreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);


            var invoiceQuery = new QueryExpression("invoicedetail") { Criteria = new FilterExpression(), ColumnSet = new ColumnSet(true) };
            var productLink = invoiceQuery.AddLink("product", "productid", "productid", JoinOperator.Inner);
            productLink.LinkCriteria.AddCondition("isp_producttype", ConditionOperator.Equal, target.GetAttributeValue<OptionSetValue>("isp_producttype").Value);
            invoiceQuery.Criteria.AddCondition("invoiceid", ConditionOperator.Equal, target.GetAttributeValue<EntityReference>("isp_invoiceid").Id);
           
            //'Product' entity doesn't contain attribute with Name = 'invoiceid'.
            var result = service.RetrieveMultiple(invoiceQuery);

            Money TotalAmount = new Money(0);
            Decimal TotalQuantity = new Decimal(0);

            if (result.Entities.Count > 0)
            {
                for (int i = 0; i < result.Entities.Count; i++)
                {
                    var quantity = result.Entities[i].GetAttributeValue<Decimal?>("quantity") ?? 0;
                    TotalQuantity += quantity;

                    //if (result.Entities[i].Attributes.ContainsKey("BaseAmount"))
                    if (result.Entities[i].Attributes.Contains("isp_vatexcl"))
                        TotalAmount.Value += result.Entities[i].GetAttributeValue<Money>("isp_vatexcl").Value;
                }
            }

            if (target.Attributes.ContainsKey("isp_totalquantity"))
                target.Attributes["isp_totalquantity"] = TotalQuantity;
            else
                target.Attributes.Add("isp_totalquantity", TotalQuantity);

            if (target.Attributes.ContainsKey("isp_totalamount"))
                target.Attributes["isp_totalamount"] = TotalAmount;
            else
                target.Attributes.Add("isp_totalamount", TotalAmount);

        }


    }
}
