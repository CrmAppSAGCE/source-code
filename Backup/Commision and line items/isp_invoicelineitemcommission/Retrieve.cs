﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Scoin.Crm.Plugins.isp_invoicelineitemcommission
{
    public class Retrieve : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.Depth == 1)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);

                var Query = (QueryExpression)context.InputParameters["Query"];

                if (Query.Criteria.Conditions.Count == 1 && Query.Criteria.Conditions[0].Values.Count == 1 && Query.LinkEntities.Count == 0 && Query.Criteria.Conditions[0].AttributeName == "isp_brokermonthlycommissionid")
                {
                    Query.Criteria.FilterOperator = LogicalOperator.Or;

                    foreach (var item in CalculateManagers.Map.Values)
                        Query.Criteria.AddCondition(item, Query.Criteria.Conditions[0].Operator, Query.Criteria.Conditions[0].Values[0]);
                }
            }
            
        }
    }
}