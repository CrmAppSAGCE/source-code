﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
namespace Scoin.Crm.Plugins.isp_invoicelineitemcommission
{
    public class precreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {

            //SPLIT PERCENTAGE PLUGIN - ONLY USED FOR CREATING COMMISSION WITH A SPLIT PERCENTAGE
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            var target = new EntityHelper(context, service);
            var Target = (Entity)context.InputParameters["Target"];
            EntityReference InvoiceId = target.GetAttributeValue<EntityReference>("isp_invoiceid");

            var InvoiceQuery = new QueryExpression("isp_invoicelineitemcommission") { Criteria = new FilterExpression(), ColumnSet = new ColumnSet(new[] { "isp_splitpercentage" }) };
            InvoiceQuery.Criteria.AddCondition("isp_invoiceid", ConditionOperator.Equal, InvoiceId.Id);
            if (target.Contains("isp_producttype"))
                InvoiceQuery.Criteria.AddCondition("isp_producttype", ConditionOperator.Equal, target.GetAttributeValue<OptionSetValue>("isp_producttype").Value);
            if (context.MessageName.ToLower() == "update")
                InvoiceQuery.Criteria.AddCondition("isp_invoicelineitemcommissionid", ConditionOperator.NotEqual, target.Id);

            var RetrieveResult = service.RetrieveMultiple(InvoiceQuery);
            Decimal SplitPercentageLeft = 100;
            
            for (int i = 0; i < RetrieveResult.Entities.Count; i++)
                if (RetrieveResult.Entities[i].Contains("isp_splitpercentage") && RetrieveResult.Entities[i].Attributes["isp_splitpercentage"] != null)
                    SplitPercentageLeft -= RetrieveResult.Entities[i].GetAttributeValue<Decimal>("isp_splitpercentage");

            if (!target.Contains("isp_splitpercentage") && context.MessageName.ToLower() == "create")
                target["isp_splitpercentage"] = SplitPercentageLeft;
            else if (target.GetAttributeValue<Decimal>("isp_splitpercentage") > SplitPercentageLeft)
                throw new Exception("Highest Split Percentage that can be supplied is "+ SplitPercentageLeft.ToString() +".");
            
        }

      
    }
}
