﻿using System;
using System.Activities;
using System.IO;
using System.Net;
using System.ServiceModel;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

namespace CustomWorkflow
{
    public sealed class RenderReportSendEmail : CodeActivity
    {
        /// <summary>
        /// Executes the workflow activity.
        /// </summary>
        /// <param name="executionContext">The execution context.</param>
        protected override void Execute(CodeActivityContext executionContext)
        {
            // Create the tracing service
            ITracingService trace = executionContext.GetExtension<ITracingService>();

            if (trace == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve tracing service.");
            }

            trace.Trace("Entered RenderReportSendEmail.Execute(), Activity Instance Id: {0}, Workflow Instance Id: {1}",
                executionContext.ActivityInstanceId,
                executionContext.WorkflowInstanceId);

            // Create the context
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();

            if (context == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve workflow context.");
            }

            trace.Trace("RenderReportSendEmail.Execute(), Correlation Id: {0}, Initiating User: {1}",
                context.CorrelationId,
                context.InitiatingUserId);

            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            try
            {
                //Getting of email to attach the pdf file
                Guid createdEmailGuid = EmailReference.Get(executionContext).Id;

                #region Example Code
                ////Getting Reporting Services Proxy and do some default settings
                //ReportExecutionService.ReportExecutionService rs = new ReportExecutionService.ReportExecutionService();

                //rs.Credentials = CredentialCache.DefaultCredentials;
                //string historyID = null;
                //string devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
                //byte[] result;
                //string encoding;
                //string mimeType;
                //string extension;
                //Warning[] warnings = null;
                //string[] streamIDs = null;

                //ExecutionInfo execInfo = new ExecutionInfo();
                //ExecutionHeader execHeader = new ExecutionHeader();

                //rs.ExecutionHeaderValue = execHeader;

                ////Setting the path of the report
                //execInfo = rs.LoadReport("/xyz_MSCRM/" + ReportName.Get(executionContext), historyID);

                ////Setting the Guid of the current document we run the process for
                //ParameterValue[] parameterListe = new ParameterValue[1];
                //ParameterValue value = new ParameterValue();
                //value.Name = "Uid";
                //value.Value = context.PrimaryEntityId.ToString();
                //parameterListe[0] = value;
                //rs.SetExecutionParameters(parameterListe, "en-us");

                ////Render the report as byte array
                //result = rs.Render("pdf", devInfo, out extension, out encoding, out mimeType, out warnings, out streamIDs);

                #endregion

                var invoiceGuid = Parameter1Value.Get(executionContext).Trim();

                string path = "http://172.19.4.20/ReportServer/Pages/ReportViewer.aspx?%2fCoinnect_MSCRM%2fCustomReports%2f" + ReportName.Get(executionContext) + "&rs:Command=Render&rs:Format=PDF&" + Parameter1.Get(executionContext) + "=" + invoiceGuid;
                var WebReq = HttpWebRequest.Create(path);

                WebReq.Credentials = CredentialCache.DefaultCredentials;
                WebReq.ImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                trace.Trace("retrieved the web response");
                BinaryReader sr = new BinaryReader(WebResp.GetResponseStream());
                MemoryStream ms = new MemoryStream();
                trace.Trace("opened memory stream");
                var buffer = new byte[4096];

                var length = sr.Read(buffer, 0, buffer.Length);
                trace.Trace("got length");
                while (length > 0)
                {
                    ms.Write(buffer, 0, length);
                    length = sr.Read(buffer, 0, buffer.Length);
                }
                var result = ms.GetBuffer();
                string fileName = string.Format("C:\\test\\{0}", AttachmentName.Get(executionContext) + ".pdf");
                FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);
                trace.Trace("opened file");
                ms.Seek(0, SeekOrigin.Begin);
                fs.Write(ms.GetBuffer(), 0, Int32.Parse(ms.Position.ToString()));
                ms.Close();
                fs.Close();

                //Attach Report to Email
                ActivityMimeAttachment attach = new ActivityMimeAttachment();
                attach.ObjectId = EmailReference.Get(executionContext);
                attach.ObjectTypeCode = EmailReference.Get(executionContext).LogicalName;
                attach.Body = System.Convert.ToBase64String(result);
                attach.Subject = AttachmentName.Get(executionContext);
                attach.FileName = AttachmentName.Get(executionContext) + ".pdf";
                attach.MimeType = @"application/pdf";
                trace.Trace("attached file");

                service.Create(attach);

                //Send Email
                SendEmailRequest sendrequest = new SendEmailRequest();
                sendrequest.EmailId = createdEmailGuid;
                sendrequest.TrackingToken = "";
                sendrequest.IssueSend = true;
                service.Execute(sendrequest);

            }
            catch (FaultException<OrganizationServiceFault> e)
            {
                trace.Trace("Exception: {0}", e.ToString());

                // Handle the exception.
                throw;
            }

            trace.Trace("Exiting RenderReportSendEmail.Execute(), Correlation Id: {0}", context.CorrelationId);
        }

        [RequiredArgument]
        [Input("Parameter1")]
        public InArgument<string> Parameter1 { get; set; }

        [RequiredArgument]
        [Input("Parameter2")]
        public InArgument<string> Parameter2 { get; set; }

        [RequiredArgument]
        [Input("Parameter1Value")]
        public InArgument<string> Parameter1Value { get; set; }

        [RequiredArgument]
        [Input("Parameter2Value")]
        public InArgument<string> Parameter2Value { get; set; }

        [RequiredArgument]
        [Input("Reportname")]
        public InArgument<string> ReportName { get; set; }

        [RequiredArgument]
        [Input("AttachmentName")]
        public InArgument<string> AttachmentName { get; set; }

        [RequiredArgument]
        [Input("Email")]
        [ReferenceTarget("email")]
        public InArgument<EntityReference> EmailReference { get; set; }

        [RequiredArgument]
        [Input("FileLocation")]
        [ReferenceTarget("email")]
        public InArgument<string> FileLocation { get; set; }
    }
}


