﻿using System;
using System.Activities;
using System.Data;
using System.Data.SqlClient;
using System.ServiceModel;
using Microsoft.Win32;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

namespace CustomWorkflow
{
    public class DupCheck : CodeActivity
    {
        [RequiredArgument]
        [Input("UserGuid")]
        [Default("")]
        public InArgument<string> UserGuid { get; set; }

        /// <summary>
        /// Executes the workflow activity.
        /// </summary>
        /// <param name="executionContext">The execution context.</param>
        protected override void Execute(CodeActivityContext executionContext)
        {
            // Create the tracing service
            ITracingService trace = executionContext.GetExtension<ITracingService>();

            if (trace == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve tracing service.");
            }
            trace.Trace("Entered DupCheck.Execute(), Activity Instance Id: {0}, Workflow Instance Id: {1}",
                executionContext.ActivityInstanceId,
                executionContext.WorkflowInstanceId);

            // Create the context
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();

            if (context == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve workflow context.");
            }

            trace.Trace("DupCheck.Execute(), Correlation Id: {0}, Initiating User: {1}",
                context.CorrelationId,
                context.InitiatingUserId);

            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\MSCRM");
                string configDBConnectionString = key.GetValue("configdb").ToString();
                DataSet clientDBConnectionData = new DataSet();

                using (SqlConnection connection = new SqlConnection(configDBConnectionString))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = connection;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = string.Format("Select SqlServerName, DatabaseName From Organization Where UniqueName = '{0}'", "Coinnect");

                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                            adapter.Fill(clientDBConnectionData);
                    }
                    connection.Close();
                }

                if (clientDBConnectionData.Tables.Count == 0 || clientDBConnectionData.Tables[0].Rows.Count == 0)
                    throw new Exception("Check your config parameters!");

                string connectionString = string.Format("Data Source={0};Initial Catalog={1};Integrated Security=SSPI",
                        new object[] { (string)clientDBConnectionData.Tables[0].Rows[0]["SqlServerName"], 
											   (string)clientDBConnectionData.Tables[0].Rows[0]["DatabaseName"]});
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlParameter userGuid = new SqlParameter("@userid", SqlDbType.UniqueIdentifier);
                    userGuid.Value = new Guid(UserGuid.Get(executionContext));

                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = connection;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "sql_rundupchecks";
                        cmd.Parameters.Add(userGuid);
                        cmd.ExecuteNonQuery();
                    }
                    connection.Close();
                }
            }
            catch (FaultException<OrganizationServiceFault> e)
            {
                trace.Trace("Exception: {0}", e.ToString());

                // Handle the exception.
                throw;
            }
            trace.Trace("DupCheck.Execute(), Correlation Id: {0}", context.CorrelationId);
        }
    }
}
