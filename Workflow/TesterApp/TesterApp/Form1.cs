﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace TesterApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CRMWS.CrmService MyService = new CRMWS.CrmService();
            MyService.Credentials = new System.Net.NetworkCredential("crmapp", "i$Partner$", "sagoldcoin");
            CRMWS.CrmAuthenticationToken token = new CRMWS.CrmAuthenticationToken();
            token.AuthenticationType = 0;
            token.OrganizationName = "coinnect";
            MyService.CrmAuthenticationTokenValue = token;

            CRMWS.QueryExpression stockTransferRequestQuery = new QueryExpression("isp_stocktransferrequest") { Criteria = new FilterExpression(), ColumnSet = new ColumnSet(new[] { "isp_stocktransferrequestname" }), PageInfo = new PagingInfo { Count = 1, PageNumber = 1 } };
            stockTransferRequestQuery.Criteria.AddCondition("isp_stocktransferrequestname", ConditionOperator.NotNull);

            int maxRefNum = 1000;

            var RetrieveResult = MyService.RetrieveMultiple(stockTransferRequestQuery);
            if (RetrieveResult.Entities.Count > 0 && RetrieveResult.Entities[0].Attributes.ContainsKey("isp_stocktransferrequestname"))
                maxRefNum = Convert.ToInt32(RetrieveResult.Entities[0].GetAttributeValue<string>("isp_stocktransferrequestname").Substring(4, 5));
            maxRefNum++;
        }
    }
}
