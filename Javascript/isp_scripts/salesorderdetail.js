/// <reference path="library/crm.js" />
/// <reference path="library/crm.odata.js" />

function salesorderdetail_onload() {
    if (Xrm.Page.getAttribute("isp_serviceitem").getValue() != null) {
        Xrm.Page.getAttribute('isp_stock').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_serviceitem').setRequiredLevel('required');
        Xrm.Page.getControl('isp_stock').setVisible(false);
        Xrm.Page.getControl('isp_serviceitem').setVisible(true);
        return;
    }
    else if (Xrm.Page.getAttribute("isp_stock").getValue() != null) {
        Xrm.Page.getAttribute('isp_stock').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_serviceitem').setRequiredLevel('none');
        Xrm.Page.getControl('isp_serviceitem').setVisible(false);
        Xrm.Page.getControl('isp_stock').setVisible(true);
        CalculateStockLevels();
        return;
    }
    else if (Xrm.Page.getAttribute("isp_stock").getValue() == null) {
        return;
    }
    else {
        Xrm.Page.getAttribute("isp_vatamount").setValue(eval(0));
        Xrm.Page.getAttribute("isp_vattotalamount").setValue(eval(0));
        Xrm.Page.getAttribute("isp_vatexcludedamount").setValue(eval(0));
        Xrm.Page.getAttribute("isp_vatexcludedtotalamount").setValue(eval(0));
        return;
    }
    Xrm.Page.getAttribute('quantity').setSubmitMode('always');
    Xrm.Page.getAttribute("isp_sellablestock").setSubmitMode('always');
}

var onsave = null;
function salesorderdetail_onsave() {
    salesorderdetail_KrugerRandFica();
    if (!onsave) {
        onsave = Mscrm.Form_onsave;
        Mscrm.Form_onsave = function () { };
    }
}

function CalculateStockLevels() {
    if (Xrm.Page.getAttribute("productid").getValue() != null && Xrm.Page.getAttribute("productid").getValue().length > 0) {
        //Getting the Global Stock Level
        var ProductGuid = Xrm.Page.data.entity.attributes.get("productid").getValue()[0].id;
        ProductGuid = ProductGuid.substring(1, 37);
        var StockLevel = $crm.odata.retrieve("Product", ProductGuid, "?$select=ProductId,QuantityOnHand", null, null);
        if (StockLevel.QuantityOnHand != null)
            Xrm.Page.getAttribute('isp_globalstockonhand').setValue(eval(StockLevel.QuantityOnHand));

        var LocalProductGuid = Xrm.Page.data.entity.attributes.get("isp_stock").getValue()[0].id;
        var LocalStockLevel = $crm.odata.retrieve("isp_stockcheck", LocalProductGuid, "", null, null);
        //Sellable Stock Level 
        var SellableStock = LocalStockLevel.isp_StockLevel - Xrm.Page.getAttribute('isp_stockordered').getValue();
        Xrm.Page.getAttribute("isp_sellablestock").setValue(SellableStock);
        if (Xrm.Page.getAttribute('isp_sellablestock').getValue() < eval(Xrm.Page.getAttribute('quantity').getValue())) {
            alert("There is not enough Stock in the local Pastel warehouse");
            //return false;
        }
        if (LocalStockLevel.isp_StockLevel != null) {
            Xrm.Page.getAttribute('isp_localstockonhand').setValue(eval(LocalStockLevel.isp_StockLevel));
        }
    }
}

function salesorderdetail_KrugerRandFica() {
    var Price = Xrm.Page.getAttribute('isp_vatexcludedtotalamount').getValue();
    if (Price > 25000) {
        var product = $crm.odata.retrieve("Product", Xrm.Page.data.entity.attributes.get("productid").getValue()[0].id, "?$select=ProductId,isp_ProductType", null, null);
        if (product.isp_ProductType.Value == 1) {
            alert('FICA Documents need to be completed and necessary documents obtained from the customer: \r\n \r\n \b FICA Form - 2 Pages (typed and attached as Microsoft Excel Documents) \r\n \r\n \b Copy of Invoices \r\n \r\n \b Copy of ID \r\n \r\n \b Copy of Deposit Slip \r\n \r\n \b Clients Details must be completed \r\n \r\n \b All Documentation must be scanned and attached as PDF Documents');
        }
    }
}

function salesorderdetail_isp_productswithstock_onchange() {
    if (Xrm.Page.getAttribute('isp_stock').getValue() == null) {
        Xrm.Page.getAttribute('isp_stock').setRequiredLevel('none');
        Xrm.Page.getControl("isp_serviceitem").setVisible(true);
        Xrm.Page.data.entity.save();

        return;
    }
    else {
        Xrm.Page.getControl("isp_serviceitem").setVisible(false);
    }
    //get the default unit
    Xrm.Page.getAttribute('uomid').setValue([{ entityType: "uom", name: "Primary Unit", id: "06A0E1CF-913B-44E9-BD3E-A6C04FB08DBC"}]);
    if (Xrm.Page.getAttribute("isp_stock").getValue() != null && Xrm.Page.getAttribute("isp_stock").getValue().length > 0) {
        var product = $crm.odata.retrieve("isp_stockcheck", Xrm.Page.getAttribute("isp_stock").getValue()[0].id, "", null, null);
        //Crash for Courier Charge on Product.Type
        Xrm.Page.getAttribute('productid').setValue([{ entityType: product.isp_Product.LogicalName, name: product.isp_Product.Name, id: product.isp_Product.Id}]);
        var product2 = $crm.odata.retrieve("Product", Xrm.Page.getAttribute("productid").getValue()[0].id, "?$select=ProductId,Price", null, null);
        Xrm.Page.getAttribute('priceperunit').setValue(product2.Price ? eval(product2.Price.Value) : 0);
    } else
        Xrm.Page.getAttribute('productid').setValue([]);

    if (Xrm.Page.getAttribute('quantity').getValue() == null)
        Xrm.Page.getAttribute('quantity').setValue(new Number(1).valueOf());
    CalculateTotals();
    Xrm.Page.data.entity.save();
}

function CalculateTotals() {
    if (Xrm.Page.data.entity.attributes.get("productid") == null)
        return;
    var Product = $crm.odata.retrieve("Product", Xrm.Page.data.entity.attributes.get("productid").getValue()[0].id, "?$select=ProductId,isp_vatcode", null, null);
    var Quantity = Xrm.Page.getAttribute('quantity').getValue();
    Xrm.Page.getAttribute('isp_vatamount').setSubmitMode('always');
    Xrm.Page.getAttribute('isp_vattotalamount').setSubmitMode('always');
    Xrm.Page.getAttribute('isp_vatexcludedamount').setSubmitMode('always');
    Xrm.Page.getAttribute('isp_vatexcludedtotalamount').setSubmitMode('always');
    Xrm.Page.getAttribute('extendedamount').setSubmitMode('always');
    if (Product.isp_vatcode == 1) {
        Xrm.Page.getAttribute('isp_vatexcludedamount').setValue(Xrm.Page.getAttribute('priceperunit').getValue() * 100 / 114);
        Xrm.Page.getAttribute('isp_vatexcludedtotalamount').setValue(Xrm.Page.getAttribute('isp_vatexcludedamount').getValue() * Quantity);
        Xrm.Page.getAttribute('isp_vatamount').setValue(Xrm.Page.getAttribute('priceperunit').getValue() - Xrm.Page.getAttribute('isp_vatexcludedamount').getValue());
        Xrm.Page.getAttribute('isp_vattotalamount').setValue(Xrm.Page.getAttribute('isp_vatamount').getValue() * Quantity);
    }
    else if (Product.isp_vatcode == 2) {
        Xrm.Page.getAttribute('isp_vatamount').setValue(eval(0));
        Xrm.Page.getAttribute('isp_vattotalamount').setValue(0);
        Xrm.Page.getAttribute('isp_vatexcludedamount').setValue(Xrm.Page.getAttribute('priceperunit').getValue());
        Xrm.Page.getAttribute('isp_vatexcludedtotalamount').setValue(Xrm.Page.getAttribute('isp_vatexcludedamount').getValue() * Quantity);

    }
    else if (Product.isp_vatcode == 6) {
        Xrm.Page.getAttribute('isp_vatamount').setValue(Xrm.Page.getAttribute('priceperunit').getValue());
        Xrm.Page.getAttribute('isp_vattotalamount').setValue(Xrm.Page.getAttribute('priceperunit').getValue() * Quantity);
        Xrm.Page.getAttribute('isp_vatexcludedamount').setValue(eval(0));
        Xrm.Page.getAttribute('isp_vatexcludedtotalamount').setValue(eval(0));
    }
    Xrm.Page.getAttribute('extendedamount').setValue(Xrm.Page.getAttribute('priceperunit').getValue() * Quantity);
}

function salesorderdetail_isp_serviceitem_onchange() {
    if (Xrm.Page.getAttribute('isp_serviceitem').getValue() == null) {
        Xrm.Page.getAttribute('isp_serviceitem').setRequiredLevel('none');
        Xrm.Page.getControl("isp_stock").setVisible(true);
        Xrm.Page.data.entity.save();
        return;
    }
    else {
        Xrm.Page.getControl("isp_stock").setVisible(false);
    }

    //get the default unit
    Xrm.Page.getAttribute('uomid').setValue([{ entityType: "uom", name: "Primary Unit", id: "06A0E1CF-913B-44E9-BD3E-A6C04FB08DBC"}]);
    if (Xrm.Page.getAttribute("isp_serviceitem").getValue() != null && Xrm.Page.getAttribute("isp_serviceitem").getValue().length > 0) {
        var product = Xrm.Page.getAttribute('isp_serviceitem').getValue();
        Xrm.Page.getAttribute('productid').setValue(product);

        var product = $crm.odata.retrieve("Product", Xrm.Page.data.entity.attributes.get("productid").getValue()[0].id, "?$select=ProductId,isp_vat,isp_vatexcl,Price", null, null);
        Xrm.Page.getAttribute('isp_vatamount').setSubmitMode('always');
        Xrm.Page.getAttribute('isp_vattotalamount').setSubmitMode('always');
        Xrm.Page.getAttribute('isp_vatexcludedamount').setSubmitMode('always');
        Xrm.Page.getAttribute('isp_vatexcludedtotalamount').setSubmitMode('always');
        Xrm.Page.getAttribute("isp_vatamount").setValue(eval(product.isp_vat.Value));
        Xrm.Page.getAttribute('isp_vatexcludedamount').setValue(eval(product.isp_vatexcl.Value));
        Xrm.Page.getAttribute('priceperunit').setValue(product.Price ? eval(product.Price.Value) : 0);
    } else
        Xrm.Page.getAttribute('isp_serviceitem').setValue([]);
    var quantity = 0;
    if (Xrm.Page.getAttribute('quantity').getValue() == null) {
        Xrm.Page.getAttribute('quantity').setValue(new Number(1).valueOf());
        quantity = 1;
    }
    else {
        quantity = Xrm.Page.getAttribute('quantity').getValue();
    }
    Xrm.Page.getAttribute('isp_vatexcludedtotalamount').setValue(product.isp_vatexcl.Value * quantity);
    Xrm.Page.getAttribute('isp_vattotalamount').setValue(product.isp_vat.Value * quantity);
    Xrm.Page.getAttribute('isp_stock').setRequiredLevel('none');
    Xrm.Page.getAttribute('isp_serviceitem').setRequiredLevel('required');
    Xrm.Page.data.entity.save();
    //  CalculateTotals();
}

function salesorderdetail_productid_onchange() {
}


function salesorderdetail_priceperunit_onchange() {
    CalculateTotals();
}

function salesorderdetail_quantity_onchange() {
    var ATPType = 0;
    if (Xrm.Page.getAttribute('salesorderid') != null) {
        if (Xrm.Page.getAttribute('salesorderid').getValue() != null && Xrm.Page.getAttribute('salesorderid').getValue().length > 0)
            ATPType = $crm.odata.retrieve("SalesOrder", Xrm.Page.getAttribute('salesorderid').getValue(), "?$select=SalesOrderId,isp_ordertype", null, null);
    }
    if (Xrm.Page.getAttribute("isp_serviceitem").getValue() == null) {
        if (Xrm.Page.getAttribute('isp_sellablestock').getValue() < Xrm.Page.getAttribute('quantity').getValue() && ATPType.isp_ordertype.Value == 100000000) {
            {
                var NeededStock = Xrm.Page.getAttribute('quantity').getValue() - Xrm.Page.getAttribute('isp_sellablestock').getValue();
                alert('Quantity requested for sale is greater than the sellable stock on hand, place a stock transfer request for ' + NeededStock + ' items.');
            }
        }
        else {
            CalculateTotals();
        }
    }
    if (Xrm.Page.getAttribute("isp_serviceitem").getValue() != null) {
        CalculateTotals();
    }
}