/// <reference path="library/crm.js" />
/// <reference path="library/helper.js" />

function salesorder_onload() {
    attachEvents();
    if ($helper.hasRole("System Administrator", Xrm.Page.context)) {
        var tab = Xrm.Page.ui.tabs.get('tab_8');
        tab.sections.get('management_approval').setVisible(true);
    }
    if (Xrm.Page.ui.getFormType() == 1) { // create
        Xrm.Page.getControl('invoice_line_items').setVisible(false);
        Xrm.Page.getControl('isp_contactid').setDisabled(false);
        ownerid_onchange();
        var userType = $crm.odata.retrieve("SystemUser", Xrm.Page.context.getUserId(), "?$select=SystemUserId,isp_usertype", null, null);
        if (userType.isp_usertype.Value == 2) {
            Xrm.Page.getAttribute('isp_scoinshopbroker').setRequiredLevel('required');
        }
        if (Xrm.Page.getAttribute('isp_contactid').getValue() != null) {
            Xrm.Page.getAttribute('isp_contactid').fireOnChange();
        }

        // get the Pastel Pricelist
        var Filter = "?$filter=Name eq 'SA Gold Coin Pastel Integrated Pricelist'";
        var pricelevels = $crm.odata.retrieveMultiple('PriceLevel', Filter, null, null, true);
        Xrm.Page.getAttribute('pricelevelid').setValue([{ entityType: 'pricelevel', name: pricelevels[0].Name, id: pricelevels[0].PriceLevelId}]);

    } else {
        if (Xrm.Page.getAttribute('isp_contactid').getValue() != null) {
            salesorder_isp_contact_onchange();
        }

        ownerid_onchange();
        Xrm.Page.getControl('ownerid').setDisabled(true);
    }
}

function attachEvents() {
    Xrm.Page.getAttribute('isp_contactid').addOnChange(salesorder_isp_contact_onchange);
    Xrm.Page.getAttribute('isp_contactid').addOnChange(contactChanged);
    Xrm.Page.getAttribute('isp_contactid').addOnChange(addAccount);
    Xrm.Page.getAttribute('isp_datepaymentconfirmed').addOnChange(checkDates);
}

function contactChanged() {
    Xrm.Page.getAttribute('isp_verifydetails').setValue(null);
}

function addAccount() {
    var ContactGuid = Xrm.Page.getAttribute('isp_contactid').getValue()[0].id.substring(1, 37);

    var Contact = $crm.odata.retrieveMultiple("Contact", "?$filter=ContactId eq guid'" + ContactGuid + "'", null, null, true);
    Xrm.Page.getAttribute("isp_accountid").setValue([{ id: Contact[0].ParentCustomerId.Id, name: Contact[0].ParentCustomerId.Name, entityType: Contact[0].ParentCustomerId.LogicalName}]);
}

function checkDates() {
    var time = Xrm.Page.getAttribute('isp_datepaymentconfirmed').getValue();
    var minDate = new Date();
    var maxDate = new Date();
    minDate.setDate(minDate.getDate() - 7);
    maxDate.setDate(maxDate.getDate() + 7);

    if (time < minDate || time > maxDate) {
        alert("Please enter the date in a correct format and try again or click on the calendar item and choose the correct date");
        Xrm.Page.getAttribute('isp_datepaymentconfirmed').setValue(null);
    }
}

function salesorder_isp_contact_onchange() {
    if (Xrm.Page.getAttribute('isp_contactid').getValue() != null) {
        Xrm.Page.getAttribute('customerid').setValue(Xrm.Page.getAttribute('isp_contactid').getValue());
    }

    if (Xrm.Page.getAttribute('customerid').getValue() == null || Xrm.Page.getAttribute('isp_contactid').getValue() == null) {
        return;
    }
    else {
        var parentContactID = Xrm.Page.data.entity.attributes.get("isp_contactid").getValue()[0].id;
        parentContactID = parentContactID.substring(1, 37);

        // var columnSet = ["ownerid", "isp_customercode", "emailaddress1", "isp_idnumber", "isp_salutation", "firstname", "lastname", "address1_line1", "address1_line2", "address1_line3", "address1_city", "isp_province", "address1_postalcode", "address1_country", "mobilephone", "fax", "isp_detailshavebeenverified", "isp_vatnumber", "isp_companyname"]
        var parentContact = $crm.odata.retrieve("Contact", parentContactID, "?$select=OwnerId,isp_customercode,EMailAddress1,isp_idnumber,isp_salutation,FirstName,LastName,Address1_Line1,Address1_Line2,Address1_Line3,Address1_City,isp_province,Address1_Country,Address1_PostalCode,Address1_Name,MobilePhone,Fax,isp_Detailshavebeenverified,isp_VATNumber,isp_CompanyName", null, null);
    }
    var CurrentFormType = Xrm.Page.getAttribute('isp_ordertype').getValue();
    if (CurrentFormType == "100000000") {
        var parentContactID = Xrm.Page.data.entity.attributes.get("isp_contactid").getValue()[0].id;
        parentContactID = parentContactID.substring(1, 37);

        if (parentContact.OwnerId != null) {
            var BrokerShop = $crm.odata.retrieve("SystemUser", parentContact.OwnerId.Id, "?$select=SystemUserId,FirstName,LastName", null, null);
            if (BrokerShop.FirstName != null && BrokerShop.LastName != null) {
                Xrm.Page.getAttribute('isp_customermasterbrokershop').setValue(BrokerShop.FirstName + " " + BrokerShop.LastName);
            }
        }
        Xrm.Page.getAttribute('isp_customercode').setValue(parentContact.isp_customercode ? parentContact.isp_customercode : '');
        Xrm.Page.getAttribute('isp_emailaddress').setValue(parentContact.EMailAddress1 ? parentContact.EMailAddress1 : '');
        if (Xrm.Page.getAttribute('isp_emailaddress').getValue() != null && Xrm.Page.getAttribute('isp_sendemail') != null) {
            Xrm.Page.getAttribute('isp_sendemail').setValue(1);
        }
        else {
            Xrm.Page.getAttribute('isp_sendemail').setValue(0);
        }
        Xrm.Page.getAttribute('isp_idnumber').setValue(parentContact.isp_idnumber ? parentContact.isp_idnumber : '');
        var Title = ""
        if (parentContact.isp_salutation != null) {
            switch (parentContact.isp_salutation.Value) {
                case 1:
                    Title = "Mr";
                    break;
                case 2:
                    Title = "Mrs";
                    break;
                case 3:
                    Title = "Miss";
                    break;
                case 863300000:
                    Title = "Doctor";
                    break;
                case 863300001:
                    Title = "Professor";
                    break;
                case 863300006:
                    Title = "";
                    break;
                default:
                    Title = "";
            }
        }
        var province = ""
        if (parentContact.Address1_County != null) {
            province = parentContact.Address1_County.Value;
        }
        var FullName = "";
        if (parentContact.isp_salutation != null) {
            FullName = Title;
        }
        if (parentContact.FirstName != null) {
            FullName += ' ' + parentContact.FirstName;
        }
        else {
            FullName += ' First Name';
        }
        if (parentContact.LastName != null && parentContact.LastName != null) {

            FullName += ' ' + parentContact.LastName;
        }
        else {
            FullName += ' Last Name';
        }
        Xrm.Page.getAttribute('isp_companyname').setValue(parentContact.isp_CompanyName ? parentContact.isp_CompanyName : '');
        Xrm.Page.getAttribute('isp_vatnumber').setValue(parentContact.isp_VATNumber ? parentContact.isp_VATNumber : '');
        Xrm.Page.getAttribute('shipto_contactname').setValue(FullName);
        Xrm.Page.getAttribute('shipto_line1').setValue(parentContact.Address1_Line1 ? parentContact.Address1_Line1 : '');
        Xrm.Page.getAttribute('shipto_line2').setValue(parentContact.Address1_Line2 ? parentContact.Address1_Line2 : '');
        Xrm.Page.getAttribute('shipto_line3').setValue(parentContact.Address1_Line3 ? parentContact.Address1_Line3 : '');
        Xrm.Page.getAttribute('shipto_city').setValue(parentContact.Address1_City ? parentContact.Address1_City : '');

        Xrm.Page.getAttribute('shipto_postalcode').setValue(parentContact.Address1_PostalCode ? parentContact.Address1_PostalCode : '');
        Xrm.Page.getAttribute('shipto_country').setValue(parentContact.Address1_Country ? parentContact.Address1_Country : '');
        Xrm.Page.getAttribute('shipto_telephone').setValue(parentContact.MobilePhone ? parentContact.MobilePhone : '');
        Xrm.Page.getAttribute('shipto_fax').setValue(parentContact.Fax ? parentContact.Fax : '');
        if ((Xrm.Page.getAttribute('isp_idnumber').getValue() != null || Xrm.Page.getAttribute('isp_idnumber').getValue()) != "") {
            Xrm.Page.getAttribute('isp_idnumber').setSubmitMode('always');
        }
    }

    if (CurrentFormType == "100000001") {
        var parentContact = $crm.odata.retrieve("Contact", parentContactID, "?$select=ContactId,isp_idnumber", null, null);
        Xrm.Page.getAttribute('isp_idnumber').setSubmitMode('always');
        Xrm.Page.getAttribute('isp_idnumber').setValue(parentContact.isp_idnumber ? parentContact.isp_idnumber : '');
    }
}

function ownerid_onchange() {
    var pastelwarehouse = null;
    var pastelCompanyId = null;
    if (Xrm.Page.getAttribute('ownerid') != null && Xrm.Page.getAttribute('isp_pastelwarehousecode') != null) {
        if (Xrm.Page.getAttribute('ownerid').getValue() != null && Xrm.Page.getAttribute('ownerid').getValue().length > 0) {
            var user = $crm.odata.retrieve("SystemUser", Xrm.Page.getAttribute('ownerid').getValue()[0].id.toString(), "?$select=SystemUserId,isp_pastelwarehouse", null, null);
            if (user != null && user.isp_pastelwarehouse != null) {
                pastelwarehouse = $crm.odata.retrieve("isp_pastelwarehouse", user.isp_pastelwarehouse.Id.toString(), "", null, null);
                if (pastelwarehouse.isp_pastelcompanyid != null)
                    pastelCompanyId = pastelwarehouse.isp_pastelcompanyid.Id;
            }
        }
        Xrm.Page.getAttribute('isp_pastelwarehousecode').setValue(pastelwarehouse ? [{ entityType: 'isp_pastelwarehouse', name: pastelwarehouse.isp_pastelwarehousename, id: pastelwarehouse.isp_pastelwarehouseId}] : []);

        var fetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical'>" +
                            "<entity name='isp_pastelwarehouse'>" +
                            "<attribute name='isp_pastelwarehousecode' />" +
                            "<attribute name='isp_pastelcompanyid' />" +
                            "<order attribute='isp_pastelwarehousename' descending='false' />" +
                        ((pastelCompanyId != null) ?
                            "<filter type='and'>" +
                                 "<condition attribute='isp_pastelcompanyid' operator='eq' value='" + pastelCompanyId + "' />" +
                            "</filter>" : "") +
                            "</entity>" +
                        "</fetch>";

        var layoutXML = "<grid name='resultset' object='1' jump='isp_pastelwarehouse' select='1' icon='1' preview='1'>" +
                                "<row name='result' id='isp_pastelwarehouseid'>" +
                                "<cell name='isp_pastelwarehousename' width='300' />" +
                                "<cell name='isp_pastelwarehousecode' width='50' />" +
                                "<cell name='isp_pastelcompanyid' width='300' />" +
                                "</row>" +
                            "</grid>";

        Xrm.Page.ui.controls.get("isp_pastelwarehousecode").addCustomView("{3F2504E0-4F89-11D3-9A0C-0305E82C3301}", "isp_pastelwarehouse", "Available Pastel Warehouses", fetchXml, layoutXML, true);
        Xrm.Page.ui.controls.get("isp_pastelwarehousecode").setDefaultView("{3F2504E0-4F89-11D3-9A0C-0305E82C3301}");
    }
}

function salesorder_onsave() {
    if (Xrm.Page.data.entity.attributes.get("isp_idnumber").getValue() == null || Xrm.Page.data.entity.attributes.get("isp_idnumber").getValue() == "") {
        alert("Please note: There is no ID number. You will not be allowed to process an invoice until the ID number is populated. Please enter an ID number, CK number or Passport number on a customerand save");
    }
    if ((Xrm.Page.getAttribute('isp_idnumber').getValue() != null || Xrm.Page.getAttribute('isp_idnumber').getValue()) != "" && Xrm.Page.ui.getFormType() == 1) {
        Xrm.Page.getAttribute('isp_idnumber').setSubmitMode('always');
    }
    var contact = new Object();
    contact.isp_CompanyName = Xrm.Page.getAttribute('isp_companyname').getValue() != null ? Xrm.Page.getAttribute('isp_companyname').getValue() : null;
    contact.Address1_Line1 = Xrm.Page.getAttribute('shipto_line1').getValue() != null ? Xrm.Page.getAttribute('shipto_line1').getValue() : null;
    contact.Address1_Line2 = Xrm.Page.getAttribute('shipto_line2').getValue() != null ? Xrm.Page.getAttribute('shipto_line2').getValue() : null;
    contact.Address1_Line3 = Xrm.Page.getAttribute('shipto_line3').getValue() != null ? Xrm.Page.getAttribute('shipto_line3').getValue() : null;
    contact.Address1_PostalCode = Xrm.Page.getAttribute('shipto_postalcode').getValue() != null ? Xrm.Page.getAttribute('shipto_postalcode').getValue() : null;
    contact.isp_VATNumber = Xrm.Page.getAttribute('isp_vatnumber').getValue() != null ? Xrm.Page.getAttribute('isp_vatnumber').getValue() : null;
    contact.Fax = Xrm.Page.getAttribute('shipto_fax').getValue() != null ? Xrm.Page.getAttribute('shipto_fax').getValue() : null;
    contact.Address1_City = Xrm.Page.getAttribute('shipto_city').getValue() != null ? Xrm.Page.getAttribute('shipto_city').getValue() : null;
    contact.MobilePhone = Xrm.Page.getAttribute('shipto_telephone').getValue() != null ? Xrm.Page.getAttribute('shipto_telephone').getValue() : null;
    contact.EMailAddress1 = Xrm.Page.getAttribute('isp_emailaddress').getValue() != null ? Xrm.Page.getAttribute('isp_emailaddress').getValue() : null;

    if (contact != null) {
        $crm.odata.Update("Contact", Xrm.Page.getAttribute("isp_contactid").getValue()[0].id, contact);
    }
}

function salesorder_amountpaid_onchange() {
    Xrm.Page.getAttribute("isp_amountpaid").setSubmitMode("always");
}
