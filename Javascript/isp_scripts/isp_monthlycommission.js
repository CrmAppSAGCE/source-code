﻿/// <reference path="library/crm.js" />
/// <reference path="library/crm.data.js" />
/// <reference path="library/XrmPage-vsdoc.js" />

function isp_monthlycommission_onload() {
    //debugger;
    var OwnerId = Xrm.Page.data.entity.attributes.get("ownerid").getValue()[0].id.substring(1, 37).toLowerCase();
    var owner = $crm.data.Retrieve('systemuser', OwnerId, ['businessunitid', 'parentsystemuserid']);
    var CurrentUserId = Xrm.Page.context.getUserId().toString().substring(1, 37).toLowerCase();
    var currentuser = $crm.data.Retrieve('systemuser', CurrentUserId, ['businessunitid']);
    var currentuserbusinessunit = $crm.data.Retrieve('businessunit', currentuser.attributes.businessunitid.value.substring(1, 37), ['isp_businessunitlevel']);

    Xrm.Page.getControl('isp_overwrittencommission').setDisabled(true);

    // Determine if the overriding amount is available
    if (CurrentUserId != OwnerId) {

        if (Xrm.Page.getAttribute('isp_bulevelofoverwritinguser').getValue() == null) {

            if (owner.attributes.parentsystemuserid != null) {
                if (CurrentUserId == owner.attributes.parentsystemuserid.value.substring(1, 37).toLowerCase())
                    Xrm.Page.getControl('isp_overwrittencommission').setDisabled(false);
                else {
                    var ownermanager = $crm.data.Retrieve('systemuser', owner.attributes.parentsystemuserid.value.substring(1, 37), ['businessunitid']);
                    var ownermanagerbusinessunit = $crm.data.Retrieve('businessunit', ownermanager.attributes.businessunitid.value.substring(1, 37), ['isp_businessunitlevel']);
                    if (ownermanagerbusinessunit.attributes.isp_businessunitlevel != null && currentuserbusinessunit.attributes.isp_businessunitlevel != null && ownermanagerbusinessunit.attributes.isp_businessunitlevel.value >= currentuserbusinessunit.attributes.isp_businessunitlevel.value)
                        Xrm.Page.getControl('isp_overwrittencommission').setDisabled(false);
                }
            }
        }
        else {
            if (currentuserbusinessunit.attributes.isp_businessunitlevel != null && Xrm.Page.getAttribute('isp_bulevelofoverwritinguser').getValue() >= currentuserbusinessunit.attributes.isp_businessunitlevel.value)
                Xrm.Page.getControl('isp_overwrittencommission').setDisabled(false);
        }

    }

}


function isp_monthlycommission_isp_onsave() {
    // debugger;
    if (Xrm.Page.getAttribute('isp_overwrittencommission').getIsDirty()) {
        var CurrentUserId = Xrm.Page.context.getUserId().toString().substring(1, 37).toLowerCase();
        var currentuser = $crm.data.Retrieve('systemuser', CurrentUserId, ['businessunitid', 'fullname']);
        var currentuserbusinessunit = $crm.data.Retrieve('businessunit', currentuser.attributes.businessunitid.value.substring(1, 37), ['isp_businessunitlevel']);

        if (currentuserbusinessunit.attributes.isp_businessunitlevel != null) {
            Xrm.Page.getAttribute('isp_bulevelofoverwritinguser').setValue(eval(currentuserbusinessunit.attributes.isp_businessunitlevel.value));
            Xrm.Page.getAttribute('isp_bulevelofoverwritinguser').setSubmitMode('always');
        }
        Xrm.Page.getAttribute('isp_overwrittenby').setValue([{ id: CurrentUserId, name: currentuser.attributes.fullname ? currentuser.attributes.fullname.value : '', entityType: 'systemuser'}]);
        Xrm.Page.getAttribute('isp_overwrittenby').setSubmitMode('always');
    }
}

