function isp_brokercommission_onload() {
   setOwnerDisabled();
}

function setOwnerDisabled() {
    Xrm.Page.getControl('ownerid').setDisabled(true);
}