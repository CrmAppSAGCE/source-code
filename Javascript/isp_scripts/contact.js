/// <reference path="library/crm.js" />
/// <reference path="library/helper.js" />

function contact_onload() {
    if ($helper.hasRole('Special Shop Rights', Xrm.Page.context)) {
        Xrm.Page.getAttribute('isp_scoinshopbroker').setRequiredLevel('required');
    }
    if (Xrm.Page.ui.getFormType() == 1) { //create
        Xrm.Page.getControl('address1_country').setVisible(false);
        Xrm.Page.getControl('isp_idnumber').setVisible(false);
        if (Xrm.Page.getAttribute('emailaddress1') != null)
            Xrm.Page.getAttribute('emailaddress1').setRequiredLevel('required');
        //Home Phone
        if (Xrm.Page.getAttribute('telephone2') != null)
            Xrm.Page.getAttribute('telephone2').setRequiredLevel('required');
        if (Xrm.Page.getAttribute('mobilephone') != null)
            Xrm.Page.getAttribute('mobilephone').setRequiredLevel('required');
    }
}

function contact_contactRequirements() {
    if (Xrm.Page.getAttribute('emailaddress1') != null && Xrm.Page.getAttribute('telephone2') != null && Xrm.Page.getAttribute('mobilephone') != null) {
        if (Xrm.Page.getAttribute('emailaddress1').getValue() == null && Xrm.Page.getAttribute('mobilephone').getValue() == null && Xrm.Page.getAttribute('telephone2').getValue() == null) {
            Xrm.Page.getAttribute('emailaddress1').setRequiredLevel('required');
            Xrm.Page.getAttribute('telephone2').setRequiredLevel('required');
            Xrm.Page.getAttribute('mobilephone').setRequiredLevel('required');
        }
        else if (Xrm.Page.getAttribute('emailaddress1').getValue() != null) {
            Xrm.Page.getAttribute('emailaddress1').setRequiredLevel('required');
            Xrm.Page.getAttribute('telephone2').setRequiredLevel('none');
            Xrm.Page.getAttribute('mobilephone').setRequiredLevel('none');
        }
        else if (Xrm.Page.getAttribute('telephone2').getValue() != null) {
            Xrm.Page.getAttribute('emailaddress1').setRequiredLevel('none');
            Xrm.Page.getAttribute('telephone2').setRequiredLevel('required');
            Xrm.Page.getAttribute('mobilephone').setRequiredLevel('none');
        }
        else if (Xrm.Page.getAttribute('mobilephone').getValue() != null) {
            Xrm.Page.getAttribute('emailaddress1').setRequiredLevel('none');
            Xrm.Page.getAttribute('telephone2').setRequiredLevel('none');
            Xrm.Page.getAttribute('mobilephone').setRequiredLevel('required');
        }

    }
}
function contact_emailaddress1_onchange() {
    contact_contactRequirements();
    var value = Xrm.Page.getAttribute('emailaddress1').getValue();
    CheckForDupes(value, "email");
}
function contact_telephone2_onchange() {
    contact_contactRequirements();
    var value = Xrm.Page.getAttribute('telephone2').getValue();
    CheckForDupes(value, "phone");
}
function contact_mobilephone_onchange() {
    contact_contactRequirements();
    var value = Xrm.Page.getAttribute('mobilephone').getValue();
    CheckForDupes(value, "phone");
}

function contact_isp_detailshavebeenverified_onchange() {
}

function contact_isp_postal_country_onchange() {
    if (Xrm.Page.ui.getFormType() == 1) { // create
        if (Xrm.Page.getAttribute('isp_postal_country').getValue() == '7') {  // if OTHER
            Xrm.Page.getAttribute('address2_country').setValue(null);
            Xrm.Page.getControl('address2_country').setVisible(true);
        }
        else {
            Xrm.Page.getControl('address2_country').setVisible(false);
        }
    }
    if (Xrm.Page.ui.getFormType() == 2) { // edit
        if (Xrm.Page.getAttribute('isp_postal_country').getValue() == '7') {  // if OTHER
            Xrm.Page.getAttribute('address2_country').setValue(null);
            Xrm.Page.getControl('address2_country').setVisible(true);
        }
        else {
            Xrm.Page.getControl('address2_country').setVisible(false);
        }
    }
    if (Xrm.Page.getAttribute('isp_postal_country').getValue() != 0)
        Xrm.Page.getAttribute('isp_postal_province').setRequiredLevel('none');
    else
        Xrm.Page.getAttribute('isp_postal_province').setRequiredLevel('required');
}

function contact_isp_contactidtype_onchange() {
    if (Xrm.Page.getAttribute('isp_contactidtype').getValue() == null) { // if blank
        Xrm.Page.getControl('isp_idnumber').setVisible(false);
    }
    if (Xrm.Page.getAttribute('isp_contactidtype').getValue() == '0') { // if ID
        Xrm.Page.getControl('isp_idnumber').setLabel('ID Number');
        Xrm.Page.getControl('isp_idnumber').setVisible(true);
    }
    else if (Xrm.Page.getAttribute('isp_contactidtype').getValue() == '1') { // if Passport
        Xrm.Page.getControl('isp_idnumber').setLabel('Passport Number');
        Xrm.Page.getControl('isp_idnumber').setVisible(true);
    }
    else if (Xrm.Page.getAttribute('isp_contactidtype').getValue() == '863300000') { // if CK Number
        Xrm.Page.getControl('isp_idnumber').setLabel('CK Number');
        Xrm.Page.getControl('isp_idnumber').setVisible(true);
    }
}
function contact_isp_idnumber_onchange() {
    var IDNumber = Xrm.Page.getAttribute('isp_idnumber').getValue();
    CheckForDupes(IDNumber, "id");
    if ((Xrm.Page.getAttribute('isp_contactidtype').getValue() == 0) && (!ValidateIDnumber(IDNumber))) {
        alert("Invalid ID Number");
        Xrm.Page.getAttribute('isp_idnumber').setValue(null)
        window.event.returnValue = false;
    }
    if (IDNumber.length == 13 && Xrm.Page.getAttribute('isp_contactidtype').getValue() == 0) {
        var day = IDNumber.toString().substring(4, 6);
        var month = IDNumber.toString().substring(2, 4);
        var year = IDNumber.toString().substring(0, 2);

        if (day >= '01' && day <= '31')
            if (month >= '01' && month <= '12')
                if (year >= '00' && year < '99')
                    Xrm.Page.getAttribute('birthdate').setValue(new Date(year, month - 1, day));
    }
}

function contact_onsave() {
    if (Xrm.Page.getAttribute('isp_contactidtype') != null) {
        if (Xrm.Page.getAttribute('isp_contactidtype').getValue() != null) {
            var ContactIdTypeVal = Xrm.Page.getAttribute('isp_contactidtype').getValue();
            var IDNumber = Xrm.Page.getAttribute('isp_idnumber').getValue();
            if ((Xrm.Page.getAttribute('isp_contactidtype').getValue() == 0) && (!ValidateIDnumber(IDNumber))) {
                alert("Invalid ID Number");
                window.event.returnValue = false;
            }
        }
    }
}

function contact_isp_streetcode_onchange() {
    Xrm.Page.getAttribute('address1_postalcode').setValue(Xrm.Page.getAttribute('isp_streetcode').getValue()[0].name);
}

function CheckForDupes(value, type) {
    var serverUrl = Xrm.Page.context.getServerUrl();
    // Creating the Odata Endpoint
    var oDataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";
    var retrieveReq = new XMLHttpRequest();
    if (type == "phone") {
        var Odata = oDataPath + "/ContactSet?$filter=OwnerId/Id eq guid'" + Xrm.Page.context.getUserId() + "' and (MobilePhone eq '" + value + "' or Telephone1 eq '" + value + "' or Telephone2 eq '" + value + "')";
    }
    if (type == "email")
        var Odata = oDataPath + "/ContactSet?$filter=OwnerId/Id eq guid'" + Xrm.Page.context.getUserId() + "' and EMailAddress1 eq '" + value + "'";
    if (type == "id") {
        var Odata = oDataPath + "/ContactSet?$filter=OwnerId/Id eq guid'" + Xrm.Page.context.getUserId() + "' and isp_idnumber eq '" + value + "'";
    }
    retrieveReq.open("GET", Odata, false);
    retrieveReq.setRequestHeader("Accept", "application/json");
    retrieveReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    retrieveReq.onreadystatechange = function () { retrieveReqCallBack(this); };
    retrieveReq.send();
}

function retrieveReqCallBack(retrieveReq) {
    if (retrieveReq.readyState == 4) {
        // this.parent.JSON.parse
        var retrieved = JSON.parse(retrieveReq.responseText).d;
        var Contact = retrieved.results[0];
        if (Contact != null) {
            alert('You already have a duplicate customer in the system called ' + Contact.FullName);
        }
    }
}

function ValidateIDnumber(idnumber) {
    //1. numeric and 13 digits
    if (isNaN(idnumber) || (idnumber.length != 13)) { return false; }
    //2. first 6 numbers is a valid date
    var tempDate = new Date(idnumber.substring(0, 2), idnumber.substring(2, 4) - 1, idnumber.substring(4, 6));
    if (!((tempDate.getYear() == idnumber.substring(0, 2)) && (tempDate.getMonth() == idnumber.substring(2, 4) - 1) && (tempDate.getDate() == idnumber.substring(4, 6)))) { return false; }
    //3. luhn formula
    var tempTotal = 0; var checkSum = 0; var multiplier = 1;
    for (var i = 0; i < 13; ++i) {
        tempTotal = parseInt(idnumber.charAt(i)) * multiplier;
        if (tempTotal > 9) { tempTotal = parseInt(tempTotal.toString().charAt(0)) + parseInt(tempTotal.toString().charAt(1)); }
        checkSum = checkSum + tempTotal;
        multiplier = (multiplier % 2 == 0) ? 1 : 2;
    }
    if ((checkSum % 10) == 0) { return true };
    return false;
}

function Bindevents() {
    // Binding the events to crm field, which needs to be validated
    crmForm.all.telephone1.onkeypress = isvalidPhone;
    crmForm.all.telephone1.onblur = function () { formatPhoneNumber("telephone1"); };
}

// Allows to enter only Numbers & upto 10 char's
function isvalidPhone() {
    var charCode = event.keyCode;
    if (charCode == 43)
        return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    var phnum = event.srcElement.value.replace(/[^0-9]/g, "");
    if (phnum.length >= 13)
        return false;
    return true;
}