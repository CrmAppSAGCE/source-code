/// <reference path="library/helper.js" />
function lead_onload() {
    if ($helper.hasRole('Special Shop Rights', Xrm.Page.context)) {
        Xrm.Page.getAttribute('isp_scoinshopbroker').setRequiredLevel('required');
    }

    Xrm.Page.getAttribute('firstname').setRequiredLevel('required');
    Xrm.Page.getAttribute('lastname').setRequiredLevel('required');
    Xrm.Page.getAttribute('emailaddress1').setRequiredLevel('required');
    Xrm.Page.getAttribute('telephone2').setRequiredLevel('required');
    Xrm.Page.getAttribute('mobilephone').setRequiredLevel('required');
    setNameRequirement();
    lead_contactRequirements();
    if (Xrm.Page.getAttribute('isp_atpguid').getValue() != "Created" && Xrm.Page.getAttribute('isp_createatp').getValue()) {
        lead_atpguid_onchange();
    }
    if (Xrm.Page.getAttribute('isp_customerguid').getValue() != "Created" && !Xrm.Page.getAttribute('isp_createatp').getValue() && Xrm.Page.getAttribute('isp_createcustomer').getValue()) {
        lead_customerguid_onchange();
    }
    if (Xrm.Page.getAttribute('isp_buybackguid').getValue() != "Created" && Xrm.Page.getAttribute('isp_createbuyback').getValue()) {
        lead_buybackguid_onchange();
    }
    //    if (Xrm.Page.getAttribute('isp_customerguid').getValue() != null && Xrm.Page.getAttribute('isp_firsttimecustomeropen').getValue() == 1) {
    //        Xrm.Utility.openEntityForm("contact", Xrm.Page.getAttribute('isp_customerguid').getValue());
    //    }
    //    if (Xrm.Page.getAttribute('isp_firsttimecustomeropen').getValue() == 1) {
    //        Xrm.Page.getAttribute('isp_firsttimecustomeropen').setSubmitMode("always");
    //        Xrm.Page.getAttribute('isp_firsttimecustomeropen').setValue(2);
    //        Xrm.Page.data.entity.save("saveandclose")
    //    }
}

function setNameRequirement() {
    if (Xrm.Page.getAttribute('firstname') != null && Xrm.Page.getAttribute('lastname') != null) {
        if (Xrm.Page.getAttribute('firstname').getValue() == null && Xrm.Page.getAttribute('lastname').getValue() == null) {
            Xrm.Page.getAttribute('firstname').setRequiredLevel('required');
            Xrm.Page.getAttribute('lastname').setRequiredLevel('required');
        }
        else if (Xrm.Page.getAttribute('firstname').getValue() != null) {
            Xrm.Page.getAttribute('firstname').setRequiredLevel('required');
            Xrm.Page.getAttribute('lastname').setRequiredLevel('none');
        }
        else if (Xrm.Page.getAttribute('lastname').getValue() != null) {
            Xrm.Page.getAttribute('firstname').setRequiredLevel('none');
            Xrm.Page.getAttribute('lastname').setRequiredLevel('required');
        }
        else if (Xrm.Page.getAttribute('firstname').getValue != null && Xrm.Page.getAttribute('lastname').getValue() != null) {
            Xrm.Page.getAttribute('firstname').setRequiredLevel('required');
            Xrm.Page.getAttribute('lastname').setRequiredLevel('required');
        }
    }
}

function lead_firstname_onchange() {
    setNameRequirement();
}

function lead_lastname_onchange() {
    setNameRequirement();
}

function lead_contactRequirements() {
    if (Xrm.Page.getAttribute('emailaddress1') != null && Xrm.Page.getAttribute('telephone2') != null && Xrm.Page.getAttribute('mobilephone') != null) {
        if (Xrm.Page.getAttribute('emailaddress1').getValue() == null && Xrm.Page.getAttribute('mobilephone').getValue() == null && Xrm.Page.getAttribute('telephone2').getValue() == null) {
            Xrm.Page.getAttribute('emailaddress1').setRequiredLevel('required');
            Xrm.Page.getAttribute('telephone2').setRequiredLevel('required');
            Xrm.Page.getAttribute('mobilephone').setRequiredLevel('required');
        }
        else if (Xrm.Page.getAttribute('emailaddress1').getValue() != null) {
            Xrm.Page.getAttribute('emailaddress1').setRequiredLevel('required');
            Xrm.Page.getAttribute('telephone2').setRequiredLevel('none');
            Xrm.Page.getAttribute('mobilephone').setRequiredLevel('none');
        }
        else if (Xrm.Page.getAttribute('telephone2').getValue() != null) {
            Xrm.Page.getAttribute('emailaddress1').setRequiredLevel('none');
            Xrm.Page.getAttribute('telephone2').setRequiredLevel('required');
            Xrm.Page.getAttribute('mobilephone').setRequiredLevel('none');
        }
        else if (Xrm.Page.getAttribute('mobilephone').getValue() != null) {
            Xrm.Page.getAttribute('emailaddress1').setRequiredLevel('none');
            Xrm.Page.getAttribute('telephone2').setRequiredLevel('none');
            Xrm.Page.getAttribute('mobilephone').setRequiredLevel('required');
        }

    }
}
function lead_emailaddress1_onchange() {
    lead_contactRequirements();
    var value = Xrm.Page.getAttribute('emailaddress1').getValue();
    CheckForDupes(value, "email");
}
function lead_telephone2_onchange() {
    lead_contactRequirements();
    var value = Xrm.Page.getAttribute('telephone2').getValue();
    CheckForDupes(value, "phone");
}
function lead_mobilephone_onchange() {
    lead_contactRequirements();
    var value = Xrm.Page.getAttribute('mobilephone').getValue();
    CheckForDupes(value, "phone");
}
function lead_telephone1_onchange() {
    var value = Xrm.Page.getAttribute('telephone1').getValue();
    CheckForDupes(value, "phone");
}

function leadcreateatp_onchange() {
    Xrm.Page.data.entity.save();
}

function lead_customerguid_onchange() {
    Xrm.Utility.openEntityForm("contact", Xrm.Page.getAttribute('isp_customerguid').getValue());
    Xrm.Page.getAttribute('isp_customerguid').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_customerguid').setValue("Created");
    Xrm.Page.data.entity.save('saveandclose');
}

function lead_atpguid_onchange() {
    Xrm.Utility.openEntityForm("salesorder", Xrm.Page.getAttribute('isp_atpguid').getValue());
    Xrm.Page.getAttribute('isp_atpguid').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_atpguid').setValue("Created");
    Xrm.Page.data.entity.save('saveandclose');
}

function lead_buybackguid_onchange() {
    Xrm.Utility.openEntityForm("isp_buyback", Xrm.Page.getAttribute('isp_buybackguid').getValue());
    Xrm.Page.getAttribute('isp_buybackguid').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_buybackguid').setValue("Created");
    Xrm.Page.data.entity.save('saveandclose');
}

function lead_onsave() {
}

function OtherSelected() {
    if (Xrm.Page.getAttribute('isp_disqualificationreason').getValue() == 863300005) {
        Xrm.Page.getControl('isp_disqualificationother').setVisible(true);
    }
    else
        Xrm.Page.getControl('isp_disqualificationother').setVisible(false);
}

function CheckForDupes(value, type) {
    var serverUrl = "/" + Xrm.Page.context.getOrgUniqueName();
    // Creating the Odata Endpoint
    var oDataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";
    var retrieveReq = new XMLHttpRequest();
    if (type == "phone") {
        var Odata = oDataPath + "/ContactSet?$filter=OwnerId/Id eq guid'" + Xrm.Page.context.getUserId() + "' and (MobilePhone eq '" + value + "' or Telephone1 eq '" + value + "' or Telephone2 eq '" + value + "')";
    }
    if (type == "email")
        var Odata = oDataPath + "/ContactSet?$filter=OwnerId/Id eq guid'" + Xrm.Page.context.getUserId() + "' and EMailAddress1 eq '" + value + "'";
    retrieveReq.open("GET", Odata, false);
    retrieveReq.setRequestHeader("Accept", "application/json");
    retrieveReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    retrieveReq.onreadystatechange = function () { retrieveReqCallBack(this); };
    retrieveReq.send();

}

function retrieveReqCallBack(retrieveReq) {

    if (retrieveReq.readyState == 4) {
        var retrieved = this.parent.JSON.parse(retrieveReq.responseText).d;
        var Contact = retrieved.results[0];
        if (Contact != null) {
            alert('You already have a duplicate customer in the system called ' + Contact.FullName);
        }
    }
}

function ShowWarning(message, mode) {
    var boxId = "warning"; // DOM ID of new DIV
    if (document.getElementById(boxId)) {
        document.getElementById("crmFormTabContainer").removeChild(document.getElementById(boxId));
    }

    if (!message || message == "") { return; }
    var warning = document.createElement("DIV");
    warning.style.padding = "10px 10px 10px 50px";
    warning.style.border = "1px solid";
    warning.style.height = "50px";
    warning.style.display = "table-cell";
    warning.style.verticalAlign = "middle";
    warning.style.margin = "15px 15px 5px 15px";

    var colour = "";
    switch (mode) {

        case "information":
        case "info":
            warning.style.background = "#BDE5F8 url('/_imgs/error/notif_icn_info.png') 10px 55% no-repeat";
            warning.style.borderColor = "#00529B";
            colour = "#00529B";
            break;
        case "success":
        case "ok":
            warning.style.background = "#DFF2BF url('/_imgs/importwizard_greentick.gif') 20px 55% no-repeat";
            warning.style.borderColor = "#4F8A10";
            colour = "#4F8A10";
            break;
        case "warning":
        case "warn":
            warning.style.background = "#FEEFB3 url('/_imgs/error/notif_icn_warn.png') 10px 55% no-repeat";
            warning.style.borderColor = "#9F6000";
            colour = "#9F6000";
            break;
        default:
            warning.style.background = "#FFBABA url('/_imgs/error/notif_icn_critical.png') 10px 55% no-repeat";
            warning.style.borderColor = "#D8000C";
            colour = "#D8000C";
            break;
    }
    warning.innerHTML = "<table cellpadding=0 cellspacing=0 border=0 width=100% height=100%><tr><td valign=middle style=\"font-size: 13px; color: " + colour + ";\">" + message + "</td></tr></table>";
    document.getElementById("crmFormTabContainer").insertBefore(warning, document.getElementById("crmFormTabContainer").firstChild);
}