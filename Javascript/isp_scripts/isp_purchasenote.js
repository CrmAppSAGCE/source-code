﻿function getReport() {
    var serverUrl = Xrm.Page.context.getServerUrl();
}

function purchasenote_onload() {
    if (Xrm.Page.getAttribute('isp_paymentoption').getValue() == 863300002) {
        Xrm.Page.getControl('isp_atp').setVisible(true);
    }
    Xrm.Page.getControl("isp_paymentoption").setDisabled(true);
    Xrm.Page.getControl("isp_purchasenotenumber").setDisabled(true);
    Xrm.Page.getControl("isp_idnumber").setDisabled(true);
    Xrm.Page.getControl("isp_contactname").setDisabled(true);
    Xrm.Page.getControl("isp_buybacknumber").setDisabled(true);
    Xrm.Page.getControl("isp_total").setDisabled(true);

    var PurchaseNoteid = Xrm.Page.data.entity.getId();

    var reportURL = Xrm.Page.context.getServerUrl() + "/crmreports/viewer/viewer.aspx?action=run&helpID=PurchaseNote.rdl&id=%7bEF5B0ADE-4FAB-E211-9572-00155D04F907%7d&p:purchasenoteid=" + PurchaseNoteid;
      Xrm.Page.getControl("IFRAME_PurchaseNote").setSrc(reportURL);

    if (Xrm.Page.ui.getFormType() == 2 && Xrm.Page.getAttribute('isp_purchasenoteguid').getValue() == null) {
        Xrm.Page.getAttribute('isp_purchasenoteguid').setValue(Xrm.Page.data.entity.getId().toString().substring(1, 37));
    }
}