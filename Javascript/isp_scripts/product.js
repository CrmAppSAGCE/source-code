﻿function product_onload() {
    var currentForm = Xrm.Page.ui.formSelector.getCurrentItem().getLabel();
    if (currentForm.toString() == "Catalogue") {
        var ProductNumber = Xrm.Page.getAttribute("productnumber").getValue();
        var reportURL = Xrm.Page.context.getServerUrl() + "/crmreports/viewer/viewer.aspx?action=filter&helpID=ProductPriceHistory.rdl&id=%7b3FB410A8-9994-E311-8D27-00155D041F17%7d&p:productnumber=" + ProductNumber;
        Xrm.Page.getControl("IFRAME_productpricehistory").setSrc(reportURL);
    }
}