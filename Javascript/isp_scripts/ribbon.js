/// <reference path="library/crm.js" />
/// <reference path="library/crm.odata.js" />
/// <reference path="library/helper.js" />

function atp_submit_enabledrule() {
    var status = Xrm.Page.getAttribute('statuscode').getValue();
    if (status == 1 || status == 863300001 || status == 863300013) { //added 863300013 for "credit noted" statuscode for ATPs done through workflow rollback     
        var userType = $crm.odata.retrieve("SystemUser", Xrm.Page.context.getUserId(), "?$select=SystemUserId,isp_usertype", null, null);
        if (userType.isp_usertype.Value == 2 || userType.isp_usertype.Value == 863300000 || userType.isp_usertype.Value == 863300001)
            return false
        else
            return true;
    }
}

function atp_approvedecline_enabledrule() {
    return $helper.hasRole('ATP Approver', Xrm.Page.context);
}

function atp_changeStateCode(desiredstatus) {
    if (desiredstatus == 2) // submit for approval
    {
        Xrm.Page.getAttribute('isp_idnumber').setRequiredLevel('required');
        Xrm.Page.getAttribute('shipto_line1').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_verifydetails').setRequiredLevel('required');
        Xrm.Page.getAttribute('shippingmethodcode').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_paymentmethods').setRequiredLevel('required');
        if (Xrm.Page.getAttribute('isp_idnumber').getValue() == null) {
            alert("Please enter an ID number, CK or Passport number on the customer, save the customer, refresh the ATP and try again");
            Xrm.Page.context.getEventArgs().preventDefault();
        }
        if (Xrm.Page.getAttribute('isp_paymentmethods') != null) {
            if (Xrm.Page.getAttribute('isp_paymentmethod').getValue() == 100000004 || Xrm.Page.getAttribute('isp_paymentmethods').getValue() == 863300004) {
                if (confirm("Go to VCS Website to confirm authorisation?")) {
                    window.open('http://www.vcs.co.za', '', 'scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
                }
            }
        }
    }
    Xrm.Page.getAttribute('statuscode').setSubmitMode('always');
    Xrm.Page.getAttribute('statuscode').setValue(desiredstatus);

    if (desiredstatus == 863300000) {
        //lockSalesOrder();
    }
    Xrm.Page.data.entity.save('saveandclose');
}

function isp_stocktransferrequest_enabledrule() {
    return $helper.hasRole('Stock Controller', Xrm.Page.context);
}

function createInvoice() {
    if (Xrm.Page.getAttribute('isp_idnumber').getValue() == null || Xrm.Page.getAttribute('isp_idnumber').getValue() == "") {
        alert("Please enter an ID number, CK or Passport number on the customer, save the customer, refresh the ATP and try again");
        Xrm.Page.context.getEventArgs().preventDefault();
    }
    if ((Xrm.Page.getAttribute('isp_idnumber').getValue() != null || Xrm.Page.getAttribute('isp_idnumber').getValue()) != "" && Xrm.Page.ui.getFormType() == 1) {
        Xrm.Page.getAttribute('isp_idnumber').setSubmitMode('always');
    }

    var orderType = Xrm.Page.getAttribute('isp_ordertype');
    if (orderType.getValue() == 100000000) {
        //    Xrm.Page.getAttribute('isp_atpverified').setRequiredLevel('required');
        //        if (Xrm.Page.getAttribute('isp_atpverified').getValue() != 1) {
        //            alert('Please ensure that the ATP has been verified.');
        //            //return false;
        //        }
        //test change 1
        Xrm.Page.getAttribute('isp_idnumber').setRequiredLevel('required');
        Xrm.Page.getAttribute('shipto_line1').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_verifydetails').setRequiredLevel('required');
        Xrm.Page.getAttribute('shippingmethodcode').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_paymentmethods').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_amountpaid').setRequiredLevel('required');

        if (Xrm.Page.getAttribute('isp_amountpaid').getValue() == 0) {
            var answer = confirm("Amount Paid entered is R0.00. \r\nClick OK to continue or Click Cancel to return back to the form");
            if (!answer) {
                Xrm.Page.context.getEventArgs().preventDefault();
            }
        }
        Xrm.Page.getAttribute('isp_paymentauthorisationnumber').setRequiredLevel('required');

        if (Xrm.Page.getAttribute('isp_paymentmethods') != null) {
            if (Xrm.Page.getAttribute('isp_paymentmethods').getValue() == 100000004) {
                if (confirm("Go to VCS Website to confirm authorisation?")) {
                    window.open('http://www.vcs.co.za', '', 'scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
                }
            }
        }
    }

    if (orderType.getValue() == 100000000 && eval(Xrm.Page.getAttribute('totalamount').getValue()) < 15000 && Xrm.Page.getAttribute('shippingmethodcode').getValue() == 100000000) {
        var answer2 = confirm("Press OK if you do not need to add the courier fee where necessary.");
        if (!answer2) {
            Xrm.Page.context.getEventArgs().preventDefault();
        }
    }
    processOrder();
}


function isActive() {
    var status = Xrm.Page.getAttribute('statuscode').getValue();
    return status == 1 || status == 2 || status == 863300000 || status == 863300001;
}
// This method takes SecurityRole Name and context as parameters
function LeadCreateCustomer() {
    Xrm.Page.getAttribute('isp_createcustomer').setValue(true);
    Xrm.Page.data.entity.save();
}

function LeadCreateATP() {
    Xrm.Page.getAttribute('isp_createatp').setValue(true);
    Xrm.Page.data.entity.save();
}

function LeadCreateBuyBack() {
    Xrm.Page.getAttribute('isp_createbuyback').setValue(true);
    Xrm.Page.data.entity.save();
}

function lead_deactivate() {
    Xrm.Page.getAttribute('isp_deactivateon').setValue(new Date());
    Xrm.Page.getAttribute('isp_disqualificationreason').setRequiredLevel('required');
    Xrm.Page.getAttribute('statuscode').setSubmitMode("always");
    Xrm.Page.getAttribute('statuscode').setValue(863300005);
    Xrm.Page.data.entity.save("saveandclose");
}

function InvoiceCreateCreditNote() {
    Xrm.Page.getAttribute('isp_creditnotedate').setValue(new Date());
    Xrm.Page.data.entity.save();
}

function sendcreditnote() {
    Xrm.Page.getAttribute('isp_daterequestsent').setValue(new Date());
    if (Xrm.Page.getAttribute('isp_moneyneedstoberefunded').getValue() == 0)
        Xrm.Page.getAttribute('statuscode').setValue(863300000);
    if (Xrm.Page.getAttribute('isp_moneyneedstoberefunded').getValue() == 1)
        Xrm.Page.getAttribute('statuscode').setValue(863300001);

    Xrm.Page.data.entity.save();
}
function ApproveCreditNote() {
    Xrm.Page.getAttribute('isp_approvedon').setValue(new Date());

    var setUservalue = new Array();
    setUservalue[0] = new Object();
    setUservalue[0].id = Xrm.Page.context.getUserId();
    setUservalue[0].entityType = 'systemuser';
    Xrm.Page.getAttribute('isp_approvedby').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_approvedby').setValue(setUservalue);
    Xrm.Page.getAttribute('statuscode').setValue(863300002);
    Xrm.Page.data.entity.save();
}
function ProcessCreditNote() {
    var setUservalue = new Array();
    setUservalue[0] = new Object();
    setUservalue[0].id = Xrm.Page.context.getUserId();
    setUservalue[0].entityType = 'systemuser';

    Xrm.Page.getAttribute('isp_processedon').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_processedby').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_processedby').setValue(setUservalue);
    Xrm.Page.getAttribute('isp_processedon').setValue(new Date());

    Xrm.Page.getAttribute('isp_actualprocessdate').setValue(new Date());
    Xrm.Page.data.entity.save('saveandclose');
}

function creditnote_approval_enabledrule() {
    return $helper.hasRole('Credit Note Approver', Xrm.Page.context);
}

function creditnote_supervisorapproval_enabledrule() {
    return $helper.hasRole('Credit Note Supervisor Approver', Xrm.Page.context);
}

function creditnote_process_enabledrule() {
    return $helper.hasRole('Credit Note Creator', Xrm.Page.context);
}

function creditnote_canrequestnote() {
    if (Xrm.Page.getAttribute('isp_creditnote') != null && Xrm.Page.getAttribute('isp_creditnote').getValue() != null)
        return false;
    else
        return true;
}

function atp_processpartialpayment() {
    var context = Xrm.Page.context;
    Xrm.Page.getAttribute('isp_amountpaid').setSubmitMode('always');
    Xrm.Page.getAttribute('isp_paymentauthorisationnumber').setSubmitMode('always');
    Xrm.Page.getAttribute('isp_amountpaid').setRequiredLevel('required');
    Xrm.Page.getAttribute('isp_paymentauthorisationnumber').setRequiredLevel('required');
    if (Xrm.Page.getAttribute('isp_amountpaid').getValue() == 0) {
        var answer = confirm("Amount Paid entered is R0.00 or the user has not tabbed from the Amount Paid field. \r\nClick OK to continue or Click Cancel to return back to the form");
        if (!answer) {
            context.getEventArgs().preventDefault();
        }
    }
    Xrm.Page.getAttribute('isp_systempartialpaymenttemp').setValue(guidGenerator());
    Xrm.Page.data.entity.save();
}

function guidGenerator() { var S4 = function () { return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1); }; return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4()); }

function GenerateStatement() {
    //    var SONumber = Xrm.Page.getAttribute("ordernumber").getValue();
    //    var reportURL = "http://" + window.location.host + "/" + Xrm.Page.context.getOrgUniqueName() + "/crmreports/viewer/viewer.aspx?action=filter&helpID=Document1.rdl&id=%7bD91EC7DA-67DA-E111-B41A-78ACC08839C9%7d&p:sonumber=" + SONumber;
    //    window.open(reportURL);
}

function GenerateOriginalInvoice() {
    var INumber = Xrm.Page.data.entity.getId();
    var reportURL = Xrm.Page.context.getServerUrl() + "/crmreports/viewer/viewer.aspx?action=run&helpID=TaxInvoice.rdl&id=%7bB3BDF00C-C46E-E211-8287-00155D04F907%7d&p:invoiceid=" + INumber;
    window.open(reportURL);
}

function buyback_submit_enablerule() {
    if (!$helper.hasRole('Buy Back Approver', Xrm.Page.context)) {
        return false;
    }

    var status = Xrm.Page.getAttribute('statuscode').getValue();
    if (status == 863300000 || status == 863300008)
        return true;
    else
        return false;
}

function buyback_changeStateCode(desiredstatus) {
    if (desiredstatus == 863300000) // submit for approval
    {
        if (Xrm.Page.getAttribute('isp_questioncheck').getValue() == null || !Xrm.Page.getAttribute('isp_questioncheck').getValue()) {
            alert("Please ask the customer the questions and update the question check before requesting approval");
            return;
        }
        var bool = Xrm.Page.getAttribute('isp_paymentoptions').getValue() == 863300002;
        if (bool) {
            Xrm.Page.getAttribute('isp_atp').setRequiredLevel('required');
        }
        else {
            Xrm.Page.getAttribute('isp_atp').setRequiredLevel('none');
            Xrm.Page.getAttribute('isp_atp').setValue(null);
        }
        if (Xrm.Page.getAttribute('isp_idnumber').getValue() == null || Xrm.Page.getAttribute('isp_idnumber').getValue() == "") {
            alert("Please note: There is no ID number. You will not be allowed to request approval until the ID number is populated. Please enter an ID number, CK number or Passport number on a customer, save the customer, refresh the buy back and try again");
            Xrm.Page.context.getEventArgs().preventDefault();
        }
        if (Xrm.Page.getAttribute('isp_bankingdetailsverified').getValue() != true) {
            Xrm.Page.getAttribute('isp_bankingdetailsverified').setRequiredLevel('required');
            alert("Please ensure that the banking details have been verified");
            return false;
        }
        if (Xrm.Page.getAttribute('isp_collectionpreference').getValue() == null) {
            Xrm.Page.getAttribute('isp_collectionpreference').setRequiredLevel('required');
            alert("Collection Preference is required");
            return false;
        }
        if (Xrm.Page.getAttribute('isp_paymentoptions').getValue() == null) {
            Xrm.Page.getAttribute('isp_paymentoptions').setRequiredLevel('required');
            alert("A Payment option is required");
            return false;
        }
        Xrm.Page.getAttribute('isp_approvalrequestsenton').setSubmitMode("always");
        Xrm.Page.getAttribute('isp_approvalrequestsenton').setValue(new Date());
        var setUservalue = new Array();
        setUservalue[0] = new Object();
        setUservalue[0].id = Xrm.Page.context.getUserId();
        setUservalue[0].entityType = 'systemuser';
        Xrm.Page.getAttribute('isp_approvalrequestsentby').setSubmitMode("always");
        Xrm.Page.getAttribute('isp_approvalrequestsentby').setValue(setUservalue);
        Xrm.Page.getAttribute('statuscode').setSubmitMode('always');
        Xrm.Page.getAttribute('statuscode').setValue(desiredstatus);

    }
    else if (desiredstatus == 863300001)//Approve
    {
        var BuyBackGuid = Xrm.Page.data.entity.getId().substring(1, 37);
        // var Filter = "?$filter=isp_BuyBackProductsIdRelationship/Id eq guid'" + BuyBackGuid + "' and (statecode/Value ne 1) and (( statuscode/Value ne 863300005) or ( statuscode/Value ne 863300001))";
        var Filter = "?$filter=(isp_BuyBackProductsIdRelationship/Id eq guid'" + BuyBackGuid + "' and statuscode/Value eq 1 and isp_productname ne null) or (isp_BuyBackProductsIdRelationship/Id eq guid'" + BuyBackGuid + "' and statuscode/Value eq 863300000 and isp_productname ne null) or (isp_BuyBackProductsIdRelationship/Id eq guid'" + BuyBackGuid + "' and statuscode/Value eq 863300002 and isp_productname ne null) or (isp_BuyBackProductsIdRelationship/Id eq guid'" + BuyBackGuid + "' and statuscode/Value eq 863300003 and isp_productname ne null) or (isp_BuyBackProductsIdRelationship/Id eq guid'" + BuyBackGuid + "' and statuscode/Value eq 863300004 and isp_productname ne null)";
        var NonActive = $crm.odata.retrieveMultiple("isp_buybackproducts", Filter, null, null, true);

        var buyBackFilter = "?$filter=statecode/Value ne 1 and isp_BuyBackProductsIdRelationship/Id eq guid'" + BuyBackGuid + "'";
        var products = $crm.odata.retrieveMultiple("isp_buybackproducts", buyBackFilter, null, null, true);

        if (products.length == 0) {
            alert('You cannot approve without a product.');
            return;
        }

        if (NonActive.length > 0) {
            alert('Please ensure that All buy back products have been approved.');
            return;
        }
        Xrm.Page.getAttribute('isp_approvedon').setSubmitMode("always");
        Xrm.Page.getAttribute('isp_approvedon').setValue(new Date());

        var setUservalue = new Array();
        setUservalue[0] = new Object();
        setUservalue[0].id = Xrm.Page.context.getUserId();
        setUservalue[0].entityType = 'systemuser';
        Xrm.Page.getAttribute('isp_approvedby').setSubmitMode("always");
        Xrm.Page.getAttribute('isp_approvedby').setValue(setUservalue);
        Xrm.Page.getAttribute('statuscode').setSubmitMode('always');
        Xrm.Page.getAttribute('statuscode').setValue(desiredstatus);

    }

    else if (desiredstatus == 863300003)//Process Purchase Note
    {
        if ((Xrm.Page.getAttribute('isp_safecustodyapproval').getValue() == null || !Xrm.Page.getAttribute('isp_safecustodyapproval').getValue()) && Xrm.Page.getAttribute('isp_collectionpreference').getValue() == 863300002) {
            alert('Safe Custody has been chosen. Please verify the confirm safe custody approval first');
            return
        }
        var BuyBackGuid = Xrm.Page.data.entity.getId().substring(1, 37);
        //863300004 New Product Requested    | 863300001 approved	|	863300000 awaiting approval	|	1 new
        var Filter = "?$filter=isp_BuyBackProductsIdRelationship/Id eq guid'" + BuyBackGuid + "' and (statecode/Value ne 1) and ( statuscode/Value ne 863300005)";
        var NonActive = $crm.odata.retrieveMultiple("isp_buybackproducts", Filter, null, null, true);

        if (NonActive.length > 0) {
            alert('Please ensure that All buy back products have been approved and verified.');
            return;
        }
        else {
            Xrm.Page.getAttribute('isp_purchasenotecreatedon').setSubmitMode("always");
            Xrm.Page.getAttribute('isp_purchasenotecreatedon').setValue(new Date());
            Xrm.Page.getAttribute('isp_purchasenotecreated').setValue(true);
        }
    }

    else if (desiredstatus == 863300007)//Decline 
    {
        Xrm.Page.getAttribute('statuscode').setSubmitMode('always');
        Xrm.Page.getAttribute('statuscode').setValue(desiredstatus);

    }
    else if (desiredstatus == 863300008)//Refer To Committee 
    {
        Xrm.Page.getAttribute('statuscode').setSubmitMode('always');
        Xrm.Page.getAttribute('statuscode').setValue(desiredstatus);
    }
    else if (desiredstatus != 863300003) {
        Xrm.Page.getAttribute('statuscode').setSubmitMode('always');
        Xrm.Page.getAttribute('statuscode').setValue(desiredstatus);
    }
    Xrm.Page.data.entity.save();
}



function BuyBackProductsApprove(SelectedControl, SelectedControlSelectedItemReferences, SelectedEntityTypeCode, SelectedEntityName) {
    // if (confirm('Approve all selected Buy Back Products?')) {
    var NewStatus = new Object();
    NewStatus.isp_approvaldate = new Date();

    for (var i = 0; i < SelectedControlSelectedItemReferences.length; i++) {
        $crm.odata.Update("isp_buybackproducts", SelectedControlSelectedItemReferences[i].Id.substring(1, 37), NewStatus, null, null);
        //$crm.odata.Update("isp_buybackproducts", SelectedControlSelectedItemReferences[i].Id.substring(1, 37), setUservalue, null, null);
    }
}

function BuyBackProductsDecline(SelectedControl, SelectedControlSelectedItemReferences, SelectedEntityTypeCode, SelectedEntityName) {
    //  if (confirm('Decline all selected Buy Back Products?')) {
    for (var i = 0; i < SelectedControlSelectedItemReferences.length; i++)
        $crm.data.SetState(SelectedEntityName, SelectedControlSelectedItemReferences[i].Id, 'Active', 863300002);
}

function BuyBackProductApproveSingle() {
    Xrm.Page.getAttribute('isp_productsbuybackprice').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_productsbuybackprice').setRequiredLevel('required');
    Xrm.Page.getAttribute('isp_approvaldate').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_approvaldate').setValue(new Date());

    var setUservalue = new Array();
    setUservalue[0] = new Object();
    setUservalue[0].id = Xrm.Page.context.getUserId();
    setUservalue[0].entityType = 'systemuser';
    Xrm.Page.getAttribute('isp_approvedby').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_approvedby').setValue(setUservalue);

    Xrm.Page.getAttribute('isp_approved').setSubmitMode('always');
    Xrm.Page.getAttribute('isp_approved').setValue(true);
    Xrm.Page.getAttribute('statuscode').setSubmitMode('always');
    Xrm.Page.getAttribute('statuscode').setValue(863300001);
    Xrm.Page.data.entity.save('saveandclose');
}

function BuyBackProductDeclineSingle() {
    if (Xrm.Page.getAttribute('isp_declinedreason').getValue() == null) {
        Xrm.Page.getAttribute('isp_declinedreason').setRequiredLevel('required');
    }
    Xrm.Page.getAttribute('isp_approvaldate').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_approvaldate').setValue(new Date());

    var setUservalue = new Array();
    setUservalue[0] = new Object();
    setUservalue[0].id = Xrm.Page.context.getUserId();
    setUservalue[0].entityType = 'systemuser';
    Xrm.Page.getAttribute('isp_approvedby').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_approvedby').setValue(setUservalue);

    Xrm.Page.getAttribute('statuscode').setSubmitMode('always');
    Xrm.Page.getAttribute('statuscode').setValue(863300002);

    Xrm.Page.data.entity.save('saveandclose');
}

function BuyBackProduct_Next() {
    var BuyBackGuid = Xrm.Page.getAttribute('isp_buyback').getValue()[0].id.substring(1, 37);
    var Filter = "?$filter=isp_BuyBack/Id eq guid'" + BuyBackGuid + "'";
    var BBP = $crm.odata.retrieveMultiple("isp_buybackproducts", Filter, null, null, true);

    var CurrentI = 0;
    for (var i = 0; i < BBP.length; i++) {
        //find current Index
        if (BBP[i].isp_buybackproductsId == Xrm.Page.data.entity.getId().substring(1, 37).toLowerCase())
            CurrentI = i;
    }
    if (CurrentI == BBP.length - 1)
        CurrentI = 0;
    else
        CurrentI++;
    var NextGUID = BBP[CurrentI].isp_buybackproductsId;
    var URL = Xrm.Page.context.getServerUrl() + "/main.aspx?etc=10066&extraqs=%3f_CreateFromId%3d%257bf1194cc4-77fe-e111-9c59-78acc08839c9%257d%26_CreateFromType%3d10064%26_gridType%3d10066%26etc%3d10066%26id%3d%257b" + NextGUID + "%257d%26pagemode%3diframe%26preloadcache%3d1347634893504%26rskey%3d921120760&pagetype=entityrecord";
    window.open(URL);
}

function BuyBackProduct_Previous() {
    var BuyBackGuid = Xrm.Page.getAttribute('isp_buyback').getValue()[0].id.substring(1, 37);
    var Filter = "?$filter=isp_BuyBack/Id eq guid'" + BuyBackGuid + "'";
    var BBP = $crm.odata.retrieveMultiple("isp_buybackproducts", Filter, null, null, true);

    var CurrentI = 0;
    for (var i = 0; i < BBP.length; i++) {
        //find current Index
        if (BBP[i].isp_buybackproductsId == Xrm.Page.data.entity.getId().substring(1, 37).toLowerCase())
            CurrentI = i;
    }
    if (CurrentI == 0)
        CurrentI = BBP.length - 1;
    else
        CurrentI--;
    var NextGUID = BBP[CurrentI].isp_buybackproductsId;
    var URL = Xrm.Page.context.getServerUrl() + "/main.aspx?etc=10066&extraqs=%3f_CreateFromId%3d%257bf1194cc4-77fe-e111-9c59-78acc08839c9%257d%26_CreateFromType%3d10064%26_gridType%3d10066%26etc%3d10066%26id%3d%257b" + NextGUID + "%257d%26pagemode%3diframe%26preloadcache%3d1347634893504%26rskey%3d921120760&pagetype=entityrecord";
    window.open(URL, "_top");
}

function buyback_openNewProduct() {
    var parameters = {};
    parameters["isp_buybackproductsidrelationship"] = Xrm.Page.data.entity.getId();
    // parameters["isp_buybackproductsidrelationship[0].name"] = Xrm.Page.getAttribute('isp_buybacknumber').getValue();
    Xrm.Utility.openEntityForm("isp_buybackproducts", null, parameters);
    Xrm.Page.getAttribute('isp_buybackproductgenerate').setValue('createGuid' + new Date());
}

function buyback_approvedecline_enabledrule() {
    return $helper.hasRole('Buy Back Approver', Xrm.Page.context);
}

function BuyBackProduct_RequestProductCreation() {
    var setUservalue = new Array();
    setUservalue[0] = new Object();
    setUservalue[0].id = Xrm.Page.context.getUserId();
    setUservalue[0].entityType = 'systemuser';
    Xrm.Page.getAttribute('isp_newproductrequestsentby').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_newproductrequestsentby').setValue(setUservalue);
    Xrm.Page.getAttribute('isp_newproductrequestsenton').setSubmitMode("always");
    Xrm.Page.getAttribute('isp_newproductrequestsenton').setValue(new Date());
    Xrm.Page.getAttribute('statuscode').setSubmitMode('always');
    Xrm.Page.getAttribute('statuscode').setValue(863300004);
    Xrm.Page.data.entity.save('saveandclose');
}

function ATPRequestClearance() {
    Xrm.Page.getAttribute('isp_clearancerequestedon').setValue(new Date());
    Xrm.Page.getAttribute('statuscode').setValue(863300011);
    Xrm.Page.getAttribute('shippingmethodcode').setRequiredLevel('required');
    Xrm.Page.getAttribute('isp_paymentmethods').setRequiredLevel('required');
    //Xrm.Page.getAttribute('ispricelocked').setValue(1);
    Xrm.Page.data.entity.save('saveandclose');
}

function ATPRequestClearanceEnable() {
    var userType = $crm.odata.retrieve("SystemUser", Xrm.Page.context.getUserId(), "?$select=SystemUserId,isp_usertype", null, null);
    if (userType.isp_usertype.Value == 2) {
        if (Xrm.Page.getAttribute('statuscode').getValue() == 1 || Xrm.Page.getAttribute('statuscode').getValue() == 863300009 || Xrm.Page.getAttribute('statuscode').getValue() == 863300013) {
            if (Xrm.Page.getAttribute('isp_paymentmethods').getValue() == 863300003 || Xrm.Page.getAttribute('isp_paymentmethods').getValue() == 863300001 || Xrm.Page.getAttribute('isp_paymentmethods').getValue() == 863300005) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}

function ATPLockPricingEnabled() {
    var userType = $crm.odata.retrieve("SystemUser", Xrm.Page.context.getUserId(), "?$select=SystemUserId,isp_usertype", null, null);
    if (userType.isp_usertype.Value == 1 || userType.isp_usertype.Value == 2)
        return false
    else
        return true;
}

function ATPEnableInvoice() {
    if ($helper.hasRole('System Administrator', Xrm.Page.context) && Xrm.Page.getAttribute('statuscode').getValue() != 1)
        return true;
    if (Xrm.Page.getAttribute('isp_partialpaymentdate') != null && Xrm.Page.getAttribute('isp_partialpaymentdate').getValue() != null && Xrm.Page.getAttribute('statuscode').getValue() != 863300013)
        return false;
    //If it is a partial Payment, do not show the process invoice button
    if (Xrm.Page.getAttribute('statuscode').getValue() == 863300009)
        return false;
    //    //Check to see if the user has Cashbook Controller Role
    //    if ($helper.hasRole('Cashbook Controller', Xrm.Page.context) && Xrm.Page.getAttribute('statuscode').getValue() != 1)
    //        return true;
    var userType = $crm.odata.retrieve("SystemUser", Xrm.Page.context.getUserId(), "?$select=SystemUserId,isp_usertype", null, null);
    if (Xrm.Page.getAttribute('isp_paymentmethods').getValue() == 863300005 && userType.isp_usertype.Value != 3)
        return false;
    if (Xrm.Page.getAttribute('isp_paymentmethods').getValue() == 863300005 && userType.isp_usertype.Value == 3)
        return true;
    if (userType.isp_usertype.Value == 3 && Xrm.Page.getAttribute('statuscode').getValue() != 1)
        return true;
    //If broker return false
    if (userType.isp_usertype.Value == 1)
        return false;
    if (userType.isp_usertype.Value == 2 && (Xrm.Page.getAttribute('isp_paymentmethods').getValue() != eval(863300003)) && (Xrm.Page.getAttribute('isp_paymentmethods').getValue() != eval(863300001)))//IF shop show process invoice button
        return true;
}

function ATPEnableProcessPartialPayment() {
    var userType = $crm.odata.retrieve("SystemUser", Xrm.Page.context.getUserId(), "?$select=SystemUserId,isp_usertype", null, null);
    if (Xrm.Page.getAttribute('statuscode').getValue() == 863300013)
        return false;
    if (userType.isp_usertype.Value == 1)
        return false;
    if (Xrm.Page.getAttribute('statuscode').getValue() == 100003)
        return false;
    if (Xrm.Page.getAttribute('isp_partialpaymentdate') != null && Xrm.Page.getAttribute('isp_partialpaymentdate').getValue() != null && userType.isp_usertype.Value == 3)
        return true;
    if (userType.isp_usertype.Value == 2 && Xrm.Page.getAttribute('isp_partialpaymentdate').getValue() != null && Xrm.Page.getAttribute('isp_paymentmethods').getValue() != 863300003)
        return true;
    return false;
}

function setPurchaseNoteAsPaid() {
    Xrm.Page.getAttribute('statuscode').setSubmitMode('always');
    Xrm.Page.getAttribute('statuscode').setValue(863300000);
    Xrm.Page.data.entity.save();
}
function buyback_paid_enabledrule() {
    return $helper.hasRole('Purchase Note Creator', Xrm.Page.context);
}

function buyBackEnablePurchaseNote() {
    return $helper.hasRole('Purchase Note Creator', Xrm.Page.context);
}

function buyBack_saveAndNew() {
    var buyBack = Xrm.Page.getAttribute('isp_buybackproductsidrelationship').getValue();
    var parameters = {};
    parameters["isp_buybackproductsidrelationship"] = buyBack[0].id;
    parameters["isp_buybackproductsidrelationshipname"] = buyBack[0].name;
    parameters["isp_buyback"] = buyBack[0].id;
    parameters["isp_buybackname"] = buyBack[0].name;

    Xrm.Utility.openEntityForm("isp_buybackproducts", null, parameters);
    Xrm.Page.data.entity.save("saveandclose");
}

function isp_stocktransferrequest_markComplete() {
    Xrm.Page.getAttribute("isp_pastelcommandsent").setValue("sent");
}

function isp_stockCheck_allocateStock() {
    var alloc = new Object();
    alloc.isp_masterstock = { Id: Xrm.Page.data.entity.getId().toString().substring(1, 37), LogicalName: 'isp_stockcheck', Name: Xrm.Page.getAttribute('isp_name').getValue() };
    alloc.isp_remainingstock = Xrm.Page.getAttribute('isp_stocklevel').getValue();
    alloc.isp_name = "Unallocated";
    var allocId = $crm.odata.Create('isp_stockallocation', alloc);
    Xrm.Utility.openEntityForm('isp_stockallocation', allocId.isp_stockallocationId);
}

function isp_stockCheck_unAllocateStock() {
    if (Xrm.Page.getAttribute('isp_parentstock').getValue() == null) {
        alert('You cannot release the stock of a Primary Item');
        return false;
    }
    var origStock = $crm.odata.retrieve("isp_stockcheck", Xrm.Page.getAttribute('isp_parentstock').getValue()[0].id, "?$select=isp_stockcheckid,isp_unallocatedstock", null, null);
    var StockLevel = new Object;
    StockLevel.isp_unallocatedstock = origStock.isp_unallocatedstock + Xrm.Page.getAttribute('isp_stocklevel').getValue();

    $crm.odata.Update("isp_stockcheck", Xrm.Page.getAttribute('isp_parentstock').getValue()[0].id, StockLevel, null, null);
    var currentEntity = new Object();
    currentEntity.isp_StockLevel = 0;
    currentEntity.isp_unallocatedstock = 0;
    Xrm.Page.getAttribute("isp_stocklevel").setValue(0);
    Xrm.Page.getAttribute("isp_unallocatedstock").setValue(0);
    $crm.odata.Update("isp_stockcheck", Xrm.Page.data.entity.getId(), currentEntity);
}

function isp_paymentsWindowRefresh() {
    window.location.reload(true);
}

function BuyBackProductVerified() {
    if ((Xrm.Page.getAttribute('isp_correctweight').getValue() != null || Xrm.Page.getAttribute('isp_correctdiameterincludingcapsule').getValue() != null || Xrm.Page.getAttribute('isp_correctthicknessincludingcapsule').getValue() != null || Xrm.Page.getAttribute('isp_correctdesign').getValue() != null || Xrm.Page.getAttribute('isp_conditionofcoin').getValue() != null || Xrm.Page.getAttribute('isp_conditionofcapsule').getValue() != null || Xrm.Page.getAttribute('isp_conditionofcertificate').getValue() != null || Xrm.Page.getAttribute('isp_conditionofbox').getValue() != null)) {
        var coinVeriError = "";
        if (!Xrm.Page.getAttribute('isp_correctweight').getValue())
            coinVeriError += "Correct Weight \r\n";
        if (!Xrm.Page.getAttribute('isp_correctdiameterincludingcapsule').getValue())
            coinVeriError += "Correct Diameter \r\n";
        if (!Xrm.Page.getAttribute('isp_correctthicknessincludingcapsule').getValue())
            coinVeriError += "Correct Thickness \r\n";
        if (!Xrm.Page.getAttribute('isp_correctdesign').getValue())
            coinVeriError += "Correct Design \r\n";
        if (!Xrm.Page.getAttribute('isp_conditionofcoin').getValue())
            coinVeriError += "Condition of Coin \r\n";
        if (!Xrm.Page.getAttribute('isp_conditionofcapsule').getValue())
            coinVeriError += "Condition of Capsule \r\n";
        if (!Xrm.Page.getAttribute('isp_conditionofcertificate').getValue())
            coinVeriError += "Condition of Certificate \r\n";
        if (!Xrm.Page.getAttribute('isp_conditionofbox').getValue())
            coinVeriError += "Condition of Box";

        if (coinVeriError != "") {
            alert("The Following Coin Verifications are missing:\r\n" + coinVeriError + "\nYou need all verifications to be complete before the status will change to the verification stage");
        }
        if (coinVeriError == "") {
            Xrm.Page.getAttribute('isp_verified').setSubmitMode('always');
            Xrm.Page.getAttribute('isp_verified').setValue(true);
            Xrm.Page.getAttribute('statuscode').setSubmitMode('always');
            alert("The product will now enter the verified stage");
            Xrm.Page.getAttribute('statuscode').setValue(863300003);
            Xrm.Page.data.entity.save('save');
        }
    }
}

function buyback_verifyrule() {
    if (Xrm.Page.ui.getFormType() == 1) {
        return false;
    }
    if (Xrm.Page.getAttribute('statuscode').getValue() == 1 /*New*/ || Xrm.Page.getAttribute('statuscode').getValue() == 863300000 /*Awaiting Approval*/ || Xrm.Page.getAttribute('statuscode').getValue() == 863300001 /*Approved*/) {
        var userType = $crm.odata.retrieve("SystemUser", Xrm.Page.context.getUserId(), "?$select=SystemUserId,isp_usertype", null, null);
        if (userType.isp_usertype.Value == 1) {
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return false;
    }
}

function buyback_approve_enabledrule() {
    if (Xrm.Page.ui.getFormType() == 1) {
        return false;
    }
    if (Xrm.Page.getAttribute('statuscode').getValue() == 863300005 /*Approved + Verified*/ || Xrm.Page.getAttribute('statuscode').getValue() == 863300001 /*Approved*/) {
        return false;
    }
    var userType = $crm.odata.retrieve("SystemUser", Xrm.Page.context.getUserId(), "?$select=SystemUserId,isp_usertype", null, null);
    if (userType.isp_usertype.Value == 1 || userType.isp_usertype.Value == 2) {
        return false
    }
    else {
        return true;
    }
}

function buyback_decline_enabledrule() {
    if (Xrm.Page.ui.getFormType() == 1) {
        return false;
    }
    var userType = $crm.odata.retrieve("SystemUser", Xrm.Page.context.getUserId(), "?$select=SystemUserId,isp_usertype", null, null);
    if (userType.isp_usertype.Value == 1 || userType.isp_usertype.Value == 2) {
        return false
    }
    else {
        return true;
    }
    if (Xrm.Page.getAttribute('statuscode').getValue() == 863300005) {
        return false;
    }
    else {
        return true;
    }
}

function RefreshUsersADInfo(selectedusers) {
    var orgserviceurl = Xrm.Page.context.prependOrgName("/XRMServices/2011/Organization.svc/web");

    for (var i = 0; i < selectedusers.length; i++) {
        var domainname = GetDomainName(selectedusers[i], orgserviceurl);
        UpdateUserADInfo(selectedusers[i], domainname, orgserviceurl);
    }

    document.all['crmGrid'].control.refresh();
}

function UpdateUserADInfo(userid, domainname, orgserviceurl) {
    var oCommand = new RemoteCommand("UserManager", "RetrieveADUserProperties");
    if (oCommand != null) {
        oCommand.SetParameter("domainAccountName", domainname);
        var oResult = oCommand.Execute();

        var request = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
          "<s:Body>" +
            "<Update xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">" +
              "<entity xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">" +
                "<a:Attributes xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";

        if (oResult.Success && !IsNull(oResult.ReturnValue) && oResult.ReturnValue.length > 0)
            for (var oUserXmlDoc = loadXmlDocument(oResult.ReturnValue), oNodeList = oUserXmlDoc.documentElement.childNodes, i = 0; i < oNodeList.length; i++) {
                var oNode = oNodeList.item(i);

                request += "<a:KeyValuePairOfstringanyType>" +
            "<b:key>" + oNode.tagName + "</b:key>" +
            "<b:value i:type=\"c:string\" xmlns:c=\"http://www.w3.org/2001/XMLSchema\">" + oNode.text + "</b:value>" +
            "</a:KeyValuePairOfstringanyType>";
            }

    request += "</a:Attributes>" +
        "<a:EntityState i:nil=\"true\" />" +
        "<a:FormattedValues xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\" />" +
        "<a:Id>" + userid + "</a:Id>" +
        "<a:LogicalName>systemuser</a:LogicalName>" +
        "<a:RelatedEntities xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\" />" +
        "</entity>" +
        "</Update>" +
        "</s:Body>" +
        "</s:Envelope>";

    var req = new XMLHttpRequest();
    req.open("POST", orgserviceurl, false);
    req.setRequestHeader("Accept", "application/xml, text/xml, */*");
    req.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
    req.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Update");
    req.send(request);
}
}

function GetDomainName(userid, orgserviceurl) {
    var request = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
     "<s:Body>" +
       "<Retrieve xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">" +
         "<entityName>systemuser</entityName>" +
         "<id>" + userid + "</id>" +
         "<columnSet xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">" +
           "<a:AllColumns>false</a:AllColumns>" +
           "<a:Columns xmlns:b=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">" +
             "<b:string>domainname</b:string>" +
           "</a:Columns>" +
         "</columnSet>" +
       "</Retrieve>" +
     "</s:Body>" +
    "</s:Envelope>";

    var req = new XMLHttpRequest();
    req.open("POST", orgserviceurl, false);
    req.setRequestHeader("Accept", "application/xml, text/xml, */*");
    req.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
    req.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Retrieve");
    req.send(request);

    var attributes = req.responseXML.getElementsByTagName("a:KeyValuePairOfstringanyType");
    if (attributes == null || attributes.length == 0)
        return null;

    for (var i = 0; i < attributes.length; i++)
        if (attributes[i].selectSingleNode("./b:key").text == "domainname") {
            var node = attributes[i].selectSingleNode("./b:value");
            if (node != null)
                return node.nodeTypedValue;
        }

return null;
}
