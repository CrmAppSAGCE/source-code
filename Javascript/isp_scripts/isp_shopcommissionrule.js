function isp_shopcommissionrule_onload() {
    showHideFields();
    attachEvents();
}

function attachEvents() {
    Xrm.Page.getAttribute('isp_producttype').addOnChange(showHideFields);
}

function showHideFields() {
    if (Xrm.Page.getAttribute('isp_producttype').getValue() == null) {
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Targets").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_PurchaseNote").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Bullion").setVisible(false);
        Xrm.Page.getControl('isp_target').setVisible(false);
        Xrm.Page.getAttribute('isp_bullionvalue').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_target').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_adminmanagerbelowtarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_adminmanagerabovetarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_salesmanagerbelowtarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_salesmanagerabovetarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_salesconsultantbelowtarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_salesconsultantabovetarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_buybackcommissionvalue').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_buybackbullioncommissionvalue').setRequiredLevel('none');
    }
    if (Xrm.Page.getAttribute('isp_producttype').getValue() == 1) {
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Targets").setVisible(true);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_PurchaseNote").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Bullion").setVisible(false);
        Xrm.Page.getControl('isp_target').setVisible(true);
        Xrm.Page.getAttribute('isp_target').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_adminmanagerbelowtarget').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_adminmanagerabovetarget').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_salesmanagerbelowtarget').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_salesmanagerabovetarget').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_salesconsultantbelowtarget').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_salesconsultantabovetarget').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_bullionvalue').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_buybackcommissionvalue').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_buybackbullioncommissionvalue').setRequiredLevel('none');
    }
    if (Xrm.Page.getAttribute('isp_producttype').getValue() == 2) {
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Targets").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_PurchaseNote").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Bullion").setVisible(true);
        Xrm.Page.getAttribute('isp_bullionvalue').setRequiredLevel('required');
        Xrm.Page.getControl('isp_target').setVisible(false);
        Xrm.Page.getAttribute('isp_target').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_adminmanagerbelowtarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_adminmanagerabovetarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_salesmanagerbelowtarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_salesmanagerabovetarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_salesconsultantbelowtarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_salesconsultantabovetarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_buybackcommissionvalue').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_buybackbullioncommissionvalue').setRequiredLevel('none');
    }
    if (Xrm.Page.getAttribute('isp_producttype').getValue() == 3) {
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_PurchaseNote").setVisible(true);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Targets").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Bullion").setVisible(false);
        Xrm.Page.getControl('isp_target').setVisible(false);
        Xrm.Page.getAttribute('isp_target').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_buybackcommissionvalue').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_buybackbullioncommissionvalue').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_adminmanagerbelowtarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_adminmanagerabovetarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_salesmanagerbelowtarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_salesmanagerabovetarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_salesconsultantbelowtarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_salesconsultantabovetarget').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_bullionvalue').setRequiredLevel('none');
    }
}