/// <reference path="library/isp_jquery.js" />

$helper = {
    /// <summary>The helper library</summary>
    namespace: true
}

$helper.hasRole = function (roleName, context) {
    var serverUrl = "/" + Xrm.Page.context.getOrgUniqueName();
    var oDataEndpointUrl = serverUrl + "/XRMServices/2011/OrganizationData.svc/";
    oDataEndpointUrl += "RoleSet?$filter=Name eq '" + roleName + "'&$select=RoleId";
    var service = GetRequestObject();
    if (service != null) {
        service.open("GET", oDataEndpointUrl, false);
        service.setRequestHeader("X-Requested-Width", "XMLHttpRequest");
        service.setRequestHeader("Accept", "application/json, text/javascript, */*");
        service.send(null);
        var requestResults = eval('(' + service.responseText + ')').d;
        if (requestResults != null && requestResults.results.length > 0) {
            for (var i = 0; i < requestResults.results.length; i++) {
                var role = requestResults.results[i];
                var id = role.RoleId;
                var currentUserRoles = Xrm.Page.context.getUserRoles();
                for (var j = 0; j < currentUserRoles.length; j++) {
                    var userRole = currentUserRoles[j];
                    if (GuidsAreEqual(userRole, id)) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}


function GetRequestObject() {
    if (window.XMLHttpRequest) {
        return new window.XMLHttpRequest;
    }
    else {
        try {
            return new ActiveXObject("MSXML2.XMLHTTP.3.0");
        }
        catch (ex) {
            return null;
        }
    }
}

function GuidsAreEqual(guid1, guid2) {
    var isEqual = false;
    if (guid1 == null || guid2 == null) {
        isEqual = false;
    }

    else {
        isEqual = (guid1.replace(/[{}]/g, "").toLowerCase() == guid2.replace(/[{}]/g, "").toLowerCase());
    }
    return isEqual;
}
