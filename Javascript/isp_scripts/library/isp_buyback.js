﻿function isp_buyback_onchange() {
    var customerGuid = Xrm.Page.getAttribute("isp_customer").getValue();
    var reportURL = Xrm.Page.context.getServerUrl() + "/main.aspx?etc=2&extraqs=%3f_gridType%3d2%26etc%3d2%26id%3d%257b" + customerGuid + "%257d%26pagemode%3diframe%26preloadcache%3d1346837970760%26rskey%3d656151560&pagetype=entityrecord";
    Xrm.Page.getControl("IFRAME_Customer").setSrc(reportURL);
}