/// <reference path="crm.js" />
/// <reference path="jquery.js" />
/// <reference path="json2.js" />
$crm.odata = {
    url: $crm.Server()
         , ajaxGetType: "GET"
         , ajaxPostType: "POST"

    // ==============================================================================================================
    // Private Utility Functions - Note: Private methods are denoted by an underscore "_"
    // ==============================================================================================================
    // ==============================================================================================================

        , _ExecuteRequest: function (odataUri, successCallBack, errorCallBack, beforeFunction, data, type) {
            var results;
            $.ajax({
                type: type,
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                data: data,
                url: odataUri,
                beforeSend: beforeFunction,
                async: (successCallBack ? true : false),
                success: function (data, textStatus, XmlHttpRequest) {
                    //Check if call back and update __next flag correctly on result set
                    if (successCallBack) {
                        if (data && data.d && data.d.results) {
                            if (data.d.__next) {
                                data.d.results.__next = data.d.__next;
                            } else {
                                data.d.results.__next = "none";
                            }
                            successCallBack(data.d.results, textStatus, XmlHttpRequest);
                        }
                        else if (data && data.d) {
                            if (data.__next) {
                                data.d.__next = data.__next;
                            } else {
                                data.d.__next = "none";
                            }
                            successCallBack(data.d, textStatus, XmlHttpRequest);
                        }
                        else {
                            if (!data.__next) {
                                data.__next = "none";
                            }
                            successCallBack(data, textStatus, XmlHttpRequest);
                        }
                    } else {
                        if (data && data.d && data.d.results) {
                            results = data.d.results;
                            if (data.d.__next) {
                                results.__next = data.d.__next;
                            } else {
                                results.__next = "none";
                            }
                        } else if (data && data.d) {
                            results = data.d;
                            if (data.__next) {
                                results.__next = data.__next;
                            } else {
                                results.__next = "none";
                            }
                        } else {
                            results = data;
                            if (!data.__next) {
                                results.__next = "none";
                            }
                        }
                    }
                },
                error: function (XmlHttpRequest, textStatus, errorThrown) {
                    if (errorCallBack)
                        errorCallBack(XmlHttpRequest, textStatus, errorThrown);
                    else $crm._errorHandler(XmlHttpRequest, textStatus, errorThrown);
                }
            });

            if (results || results == "")
                return results;
        }

        , _GetBeforeSend: function (XMLHttpRequest) {
            //Specifying this header ensures that the results will be returned as JSON.             
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        }

        , _UpdateBeforeSend: function (XMLHttpRequest) {
            //Specifying this header ensures that the results will be returned as JSON.             
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            //Specify the HTTP method MERGE to update just the changes you are submitting.
            XMLHttpRequest.setRequestHeader("X-HTTP-Method", "MERGE");
        }

        , _DeleteBeforeSend: function (XMLHttpRequest) {
            //Specifying this header ensures that the results will be returned as JSON.             
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            //Specify the HTTP method MERGE to update just the changes you are submitting.
            XMLHttpRequest.setRequestHeader("X-HTTP-Method", "DELETE");
        }


    // ==============================================================================================================
    // ==============================================================================================================
    // Public Method
    // ==============================================================================================================
    // ==============================================================================================================


        , retrieveMultiple: function (targetEntityName, filter, successCallBack, errorCallBack, retrieveAll) {
            /// <summary>Retrieve multiple entites by default max of 50 returned. If results.__next == "none" there are no more results to process otherwise call 
            ///           retrieveNextResult(results) with the same results set to get the next page of results</summary>
            /// <param name="targetEntityName">Entity name. e.g., account, contact, etc.</param>
            /// <param name="filter">Filter for the entity</param>
            /// <param name="successCallBack" optional=true type="function(data, textStatus, XmlHttpRequest)">A call back function if the request is successfull. Will be passed 3 params data(result data), textStatus("success") and XmlHttpRequest(the ajax request object)</param>
            /// <param name="errorCallBack" optional=true type="function(XmlHttpRequest, textStatus, errorThrown)">A call back function if there is an error with the request. If not provided the default $crm._errorHandler will be called</param>
            /// <param name="retrieveAll" optional=true type-"Boolean">If set to true will make repeated calls to get all of the data back. Default is false</param>
            /// <returns type="Array">Returns results if any are recieved (if being called synchronously)</returns> 

            //odataSetName is required, i.e. "AccountSet"
            if (!targetEntityName) {
                alert("targetEntityName is required.");
                return;
            }

            //Build the URI
            var odataUri = this.url + "/" + targetEntityName + 'Set()';

            //If a filter is supplied, append it to the OData URI
            if (filter)
                odataUri += filter;

            //Execute the AJAX function to Retrieve CRM records using OData
            var results = this._ExecuteRequest(odataUri, successCallBack, errorCallBack, this._GetBeforeSend, null, this.ajaxGetType);

            //Return results if available
            if (results) {
                //If we are to get all results then need to call until there are no more
                if (retrieveAll) {
                    while (results.__next != "none") {
                        var newResults = this.retrieveNextResult(results);
                        //Add the newResults to the original result set 
                        for (var i = 0; i < newResults.length; i++) {
                            results.push(newResults[i]);
                        }
                        results.__next = newResults.__next;
                    }
                }
                return results;
            }

        }

        , retrieve: function (targetEntityName, guid, filter, successCallBack, errorCallBack) {
            /// <summary>Retrieve a single entity with filter</summary>
            /// <param name="targetEntityName">Entity name. e.g., account, contact, etc.</param>
            /// <param name="guid">The entity guid to return</param>
            /// <param name="filter">Filter for the entity</param>
            /// <param name="successCallBack" optional=true type="function(data, textStatus, XmlHttpRequest)">A call back function if the request is successfull. Will be passed 3 params data(result data), textStatus("success") and XmlHttpRequest(the ajax request object)</param>
            /// <param name="errorCallBack" optional=true type="function(XmlHttpRequest, textStatus, errorThrown)">A call back function if there is an error with the request. If not provided the default $crm._errorHandler will be called</param>
            /// <returns type="Array">Array containing the the result (if being called synchronously)</returns>

            //odataSetName is required
            if (!targetEntityName) {
                alert("targetEntityName is required.");
                return;
            }

            //guid is required
            if (!guid) {
                alert("guid is required");
                return;
            }

            //Build up the URL to fecth back a single instance
            var odataUri = this.url + "/" + targetEntityName + "Set(guid'" + guid + "')";

            //If a filter is supplied, append it to the OData URI
            if (filter)
                odataUri += filter;

            var results = this._ExecuteRequest(odataUri, successCallBack, errorCallBack, this._GetBeforeSend, null, this.ajaxGetType);

            if (results) {
                return results;
            }
        }

        , retrieveNextResult: function (results, successCallBack, errorCallBack) {
            /// <summary>Retrieve the next set of results from a previous odata call</summary>
            /// <param name="results">The previous result set</param>
            /// <param name="errorCallBack" optional=true type="function(XmlHttpRequest, textStatus, errorThrown)">A call back function if there is an error with the request. If not provided the default $crm._errorHandler will be called</param>
            /// <returns type="Array">Array containing the next set of results (if being called synchronously)</returns>

            if (!results) {
                return;
            }

            if (!results.__next) {
                return;
            }

            var newResults = this._ExecuteRequest(results.__next, successCallBack, errorCallBack, this._GetBeforeSend, null, this.ajaxGetType);

            if (newResults) {
                return newResults;
            }

        }

        , Update: function (targetEntityName, targetEntityId, updateObject, successCallBack, errorCallBack) {
            /// <summary>Update an entity</summary>
            /// <param name="targetEntityName">The name of the target entity</param>
            /// <param name="targetEntityId">The Guid of the entity you want to update</param>
            /// <param name="updateObject">An object with the attributes to update e.g. If you wanted to update some details on a Contact
            /// then the update object could look like:- 
            /// 
            /// var changes = new Object;
            /// changes.FullName = "Joe Bloggs";
            /// changes.Telephone1 = "0112354637"
            ///
            ///</param>
            /// <param name="successCallBack" optional=true type="function(data, textStatus, XmlHttpRequest)">A call back function if the request is successfull. Will be passed 3 params data(result data), textStatus("success") and XmlHttpRequest(the ajax request object)</param>
            /// <param name="errorCallBack" optional=true type="function(XmlHttpRequest, textStatus, errorThrown)">A call back function if there is an error with the request. If not provided the default $crm._errorHandler will be called</param>
            /// <returns type="Boolean">False on failure, true on success (if being called synchronously)</returns> 

            //odataSetName is required
            if (!targetEntityName) {
                alert("targetEntityName is required.");
                return false;
            }

            //update object is required
            if (!targetEntityId) {
                alert("targetEntityName is required.");
                return false;
            }

            //guid is required
            if (!updateObject) {
                alert("An update Object is required");
                return false;
            }

            //Build up the URL to update the object
            var odataUri = this.url + "/" + targetEntityName + "Set(guid'" + targetEntityId + "')";

            var data = JSON.stringify(updateObject);

            var results = this._ExecuteRequest(odataUri, successCallBack, errorCallBack, this._UpdateBeforeSend, data, this.ajaxPostType);

            if (results == "") {
                return true;
            }
        }


        , Create: function (targetEntityName, createObject, successCallBack, errorCallBack) {
            /// <summary>Create an entity</summary>
            /// <param name="targetEntityName">The name of the target entity</param>
            /// <param name="createObject">An object with the attributes to create for the entity specified in targetEntityName e.g. If you wanted to create a new Contact with some details
            /// then the create object could look like:- 
            /// 
            /// var create = new Object;
            /// create.FullName = "Joe Bloggs";
            /// create.Telephone1 = "0112354637"
            ///
            ///</param>
            /// <param name="successCallBack" optional=true type="function(data, textStatus, XmlHttpRequest)">A call back function if the request is successfull. Will be passed 3 params data(result data), textStatus("success") and XmlHttpRequest(the ajax request object)</param>
            /// <param name="errorCallBack" optional=true type="function(XmlHttpRequest, textStatus, errorThrown)">A call back function if there is an error with the request. If not provided the default $crm._errorHandler will be called</param>
            /// <returns type="Array">Array containing the newly created entity (if being called synchronously)</returns> 

            //odataSetName is required
            if (!targetEntityName) {
                alert("targetEntityName is required.");
                return null;
            }

            //create object is required
            if (!createObject) {
                alert("create Object is required");
                return null;
            }

            //Build up the URL to create the object
            var odataUri = this.url + "/" + targetEntityName + "Set()";

            var data = JSON.stringify(createObject);

            var results = this._ExecuteRequest(odataUri, successCallBack, errorCallBack, this._GetBeforeSend, data, this.ajaxPostType);

            if (results) {
                return results;
            }

        }


        , Delete: function (targetEntityName, guid, successCallBack, errorCallBack) {
            /// <summary>Delete an entity</summary>
            /// <param name="targetEntityName">The name of the target entity</param>
            /// <param name="guid">The Guid of the entity to delete</param>
            /// <param name="successCallBack" optional=true type="function(data, textStatus, XmlHttpRequest)">A call back function if the request is successfull. Will be passed 3 params data(result data), textStatus("success") and XmlHttpRequest(the ajax request object)</param>
            /// <param name="errorCallBack" optional=true type="function(XmlHttpRequest, textStatus, errorThrown)">A call back function if there is an error with the request. If not provided the default $crm._errorHandler will be called</param>
            /// <returns type="Boolean">False on failure, true on success (if being called synchronously)</returns> 

            //odataSetName is required
            if (!targetEntityName) {
                alert("targetEntityName is required.");
                return false;
            }

            //
            if (!guid) {
                alert("Guid of object to delete is required");
                return false;
            }

            //Build up the URL to create the object
            var odataUri = this.url + "/" + targetEntityName + "Set(guid'" + guid + "')";

            var results = this._ExecuteRequest(odataUri, successCallBack, errorCallBack, this._DeleteBeforeSend, null, this.ajaxPostType);

            if (results == "") {
                return true;
            }
        }

        , Associate: function (entityid1, entity1Name, entityid2, entity2Name, relationshipFieldName, successCallBack, errorCallBack) {
            /// <summary>Associate two records via the 1:N or N:1 relationship field</summary>
            /// <param name="entityid1">Entity id of the first record being associated</param>
            /// <param name="entity1Name">Entity name of first record being associated</param>
            /// <param name="entityid2">Entity id of the first record being associated</param>
            /// <param name="entity2Name">Entity name of first record being associated</param>
            /// <param name="relationFieldName">SchemaName of the field on entity 1 to associate entity 2</param>
            /// <param name="successCallBack" optional=true type="function(data, textStatus, XmlHttpRequest)">A call back function if the request is successfull. Will be passed 3 params data(result data), textStatus("success") and XmlHttpRequest(the ajax request object)</param>
            /// <param name="errorCallBack" optional=true type="function(XmlHttpRequest, textStatus, errorThrown)">A call back function if there is an error with the request. If not provided the default $crm._errorHandler will be called</param>
            /// <returns type="Boolean">False on failure, true on success (if being called synchronously)</returns> 

            if (!entityid1) {
                alert("Entity 1 Id is required");
                return false;
            }

            if (!entity1Name) {
                alert("Entity 1 Name is required");
                return false;
            }

            if (!entityid2) {
                alert("Entity 2 Id is required");
                return false;
            }

            if (!entity2Name) {
                alert("Entity 2 Name is required");
                return false;
            }

            if (!relationshipFieldName) {
                alert("Relationship Field Name is required");
                return false;
            }

            var entityObject = new Object();
            entityObject.__metadata = { type: "Microsoft.Crm.Sdk.Data.Services.EntityReference" };
            entityObject.Id = entityid2;
            entityObject.LogicalName = entity2Name.toLowerCase();

            var updateObject = new Object();
            updateObject["" + relationshipFieldName] = entityObject;

            var results = this.Update(entity1Name, entityid1, updateObject, successCallBack, errorCallBack);

            if (results)
                return results;
        }


        , Disassociate: function (entityid1, entity1Name, relationshipFieldName, successCallBack, errorCallBack) {
            /// <summary>Disassociate two records that have a 1:N N:1 relationship field</summary>
            /// <param name="entityid1">Entity id of the first record being associated</param>
            /// <param name="entity1Name">Entity name of first record being associated</param>
            /// <param name="relationFieldName">SchemaName of the field on entity 1 to disassociate</param>
            /// <param name="successCallBack" optional=true type="function(data, textStatus, XmlHttpRequest)">A call back function if the request is successfull. Will be passed 3 params data(result data), textStatus("success") and XmlHttpRequest(the ajax request object)</param>
            /// <param name="errorCallBack" optional=true type="function(XmlHttpRequest, textStatus, errorThrown)">A call back function if there is an error with the request. If not provided the default $crm._errorHandler will be called</param>
            /// <returns type="Boolean">False on failure, true on success (if being called synchronously)</returns> 

            if (!entityid1) {
                alert("Entity 1 Id is required");
                return false;
            }

            if (!entity1Name) {
                alert("Entity 1 Name is required");
                return false;
            }

            if (!relationshipFieldName) {
                alert("Relationship Field Name is required");
                return false;
            }

            var updateObject = new Object();
            updateObject["" + relationshipFieldName] = null;

            var results = this.Update(entity1Name, entityid1, updateObject, successCallBack, errorCallBack);

            if (results)
                return results;
        }

        , AssociateN2N: function (entityid1, entity1Name, entityid2, entity2Name, relationshipName, successCallBack, errorCallBack) {
            /// <summary>Associate two records via the 1:N or N:1 relationship field</summary>
            /// <param name="entityid1">Entity id of the first record being associated</param>
            /// <param name="entity1Name">Entity name of first record being associated</param>
            /// <param name="entityid2">Entity id of the first record being associated</param>
            /// <param name="entity2Name">Entity name of first record being associated</param>
            /// <param name="relationshipName">SchemaName of the field on entity 1 to associate entity 2</param>
            /// <param name="successCallBack" optional=true type="function(data, textStatus, XmlHttpRequest)">A call back function if the request is successfull. Will be passed 3 params data(result data), textStatus("success") and XmlHttpRequest(the ajax request object)</param>
            /// <param name="errorCallBack" optional=true type="function(XmlHttpRequest, textStatus, errorThrown)">A call back function if there is an error with the request. If not provided the default $crm._errorHandler will be called</param>
            /// <returns type="Boolean">False on failure, true on success (if being called synchronously)</returns>

            if (!entityid1) {
                alert("Entity 1 Id is required");
                return false;
            }

            if (!entity1Name) {
                alert("Entity 1 Name is required");
                return false;
            }

            if (!entityid2) {
                alert("Entity 2 Id is required");
                return false;
            }

            if (!entity2Name) {
                alert("Entity 2 Name is required");
                return false;
            }

            if (!relationshipName) {
                alert("Relationship Field is required");
                return false;
            }

            var newEntity = {};
            newEntity.uri = this.url + "/" + entity2Name + "Set(guid'" + entityid2 + "')";
            var jsonData = JSON.stringify(newEntity);
            var odatauri = this.url + "/" + entity1Name + "Set(guid'" + entityid1 + "')/$links/" + relationshipName;

            var results = this._ExecuteRequest(odatauri, successCallBack, errorCallBack, this._GetBeforeSend, jsonData, this.ajaxPostType);

            if (results == "") {
                return true;
            }
        }


        , DisassociateN2N: function (entityid1, entity1Name, entityid2, relationshipName, successCallBack, errorCallBack) {
            /// <summary>Disassociate two records via the N:N relationship name</summary>
            /// <param name="entityid1">Entity id of the first record being associated</param>
            /// <param name="entity1Name">Entity name of first record being associated</param>
            /// <param name="entityid2">Entity id of the first record being associated</param>
            /// <param name="entity2Name">Entity name of first record being associated</param>
            /// <param name="relationshipName">SchemaName of the field on entity 1 to associate entity 2</param>
            /// <param name="successCallBack" optional=true type="function(data, textStatus, XmlHttpRequest)">A call back function if the request is successfull. Will be passed 3 params data(result data), textStatus("success") and XmlHttpRequest(the ajax request object)</param>
            /// <param name="errorCallBack" optional=true type="function(XmlHttpRequest, textStatus, errorThrown)">A call back function if there is an error with the request. If not provided the default $crm._errorHandler will be called</param>
            /// <returns type="Boolean">False on failure, true on success (if being called synchronously)</returns> 

            if (!entityid1) {
                alert("Entity 1 Id is required");
                return false;
            }

            if (!entity1Name) {
                alert("Entity 1 Name is required");
                return false;
            }

            if (!entityid2) {
                alert("Entity 2 Id is required");
                return false;
            }

            if (!relationshipName) {
                alert("Relationship Field is required");
                return false;
            }

            var odatauri = this.url + "/" + entity1Name + "Set(guid'" + entityid1 + "')/$links/" + relationshipName + "(guid'" + entityid2 + "')";

            var results = this._ExecuteRequest(odatauri, successCallBack, errorCallBack, this._DeleteBeforeSend, null, this.ajaxPostType);

            if (results == "") {
                return true;
            }
        }
}

