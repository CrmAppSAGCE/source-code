var $crm = {
    _getContext: function () {
        var errorMessage = "Context is not available.";
        if (typeof GetGlobalContext != "undefined")
        { return GetGlobalContext(); }
        else {
            if (typeof Xrm != "undefined") {
                return Xrm.Page.context;
            }
            else { throw new Error(errorMessage); }
        }
    },

    Server: function () {///<summary>
        /// Private function used to establish the path to the SOAP endpoint based on context
        /// provided by the Xrm.Page object or the context object returned by the GlobalContext object.
        ///</summary>
        var ServerUrl = this._getContext().getServerUrl();
        if (ServerUrl.match(/\/$/)) {
            ServerUrl = ServerUrl.substring(0, ServerUrl.length - 1);
        }
        return "/" + Xrm.Page.context.getOrgUniqueName() + "/XRMServices/2011/OrganizationData.svc";
        //return ServerUrl + "/XRMServices/2011/OrganizationData.svc";
    }

    ///<summary>
    ///A function that will display the error results of an AJAX operation
    ///</summary>
    , _errorHandler: function (xmlHttpRequest, textStatus, errorThrow) {
        if (xmlHttpRequest && xmlHttpRequest.responseText)
            alert(xmlHttpRequest.responseText);
        else
            alert("Error : " + textStatus + ": " + xmlHttpRequest.statusText);
    }
}