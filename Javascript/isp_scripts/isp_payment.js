function isp_payment_onload() {
    showHideSections();
    filteredShopCommissionLookUp();
    filteredBrokerCommissionLookUp();
}

function showHideSections() {
    if (Xrm.Page.getAttribute('isp_brokercommissionid').getValue() != null) {
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_ShopCommission").setVisible(false);    
    }
    if (Xrm.Page.getAttribute('isp_shopcommissionid').getValue() != null) {
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_BrokerCommission").setVisible(false);      
    }
}

function filteredShopCommissionLookUp() {
    var viewId = "{1DFB2B35-B07C-44D1-868D-258DEEAB88E1}";
    var entityName = "isp_shopcommission";
    var viewDisplayName = "Filtered Shop Commissions";

    defaultViewId = Xrm.Page.getControl("isp_shopcommissionid").getDefaultView();
    if (Xrm.Page.getAttribute('ownerid').getValue() != null) {
        var owner = Xrm.Page.getAttribute('ownerid').getValue()[0].id;
        var fetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
        "  <entity name='isp_shopcommission'>" +
        "    <attribute name='isp_name' />" +
        "    <order attribute='isp_name' descending='false' />" +
              "    <filter type='and'>" +
                "      <condition attribute='ownerid' operator='eq' value='" + owner.toString() + "' />" +
              "    </filter>" +
        "  </entity>" +
        "</fetch>";

        var layoutXml = "<grid name='resultset' object='1' jump='isp_shopcommissionid' select='1' icon='0' preview='0'>" +
        "  <row name='result' id='isp_shopcommissionid'>" +
        "    <cell name='isp_name' width='300' />" +
        "  </row>" +
        "</grid>";
        Xrm.Page.getControl("isp_shopcommissionid").addCustomView(viewId, entityName, viewDisplayName, fetchXml, layoutXml, true);
    }

    else {
        Xrm.Page.getControl("isp_shopcommissionid").setDefaultView(defaultViewId);
    }
}
function filteredBrokerCommissionLookUp() {
    var viewId = "{1DFB2B35-B07C-44D1-868D-258DEEAB88E2}";
    var entityName = "isp_brokercommission";
    var viewDisplayName = "Filtered Broker Commissions";

    defaultViewId = Xrm.Page.getControl("isp_brokercommissionid").getDefaultView();
    if (Xrm.Page.getAttribute('ownerid').getValue() != null) {
        var owner = Xrm.Page.getAttribute('ownerid').getValue()[0].id;
        var fetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
        "  <entity name='isp_brokercommission'>" +
        "    <attribute name='isp_name' />" +
        "    <order attribute='isp_name' descending='false' />" +
              "    <filter type='and'>" +
                "      <condition attribute='ownerid' operator='eq' value='" + owner.toString() + "' />" +
              "    </filter>" +
        "  </entity>" +
        "</fetch>";

        var layoutXml = "<grid name='resultset' object='1' jump='isp_brokercommissionid' select='1' icon='0' preview='0'>" +
        "  <row name='result' id='isp_brokercommissionid'>" +
        "    <cell name='isp_name' width='300' />" +
        "  </row>" +
        "</grid>";
        Xrm.Page.getControl("isp_brokercommissionid").addCustomView(viewId, entityName, viewDisplayName, fetchXml, layoutXml, true);
    }

    else {
        Xrm.Page.getControl("isp_brokercommissionid").setDefaultView(defaultViewId);
    }
}