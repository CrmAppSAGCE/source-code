function getReport() {
    var serverUrl = Xrm.Page.context.getServerUrl();

}
function invoice_onload() {
    Xrm.Page.getControl("shipto_name").setDisabled(true);
    Xrm.Page.getControl("shipto_stateorprovince").setDisabled(true);
    Xrm.Page.getControl("shipto_line1").setDisabled(true);
    Xrm.Page.getControl("shipto_postalcode").setDisabled(true);
    Xrm.Page.getControl("shipto_line2").setDisabled(true);
    Xrm.Page.getControl("shipto_country").setDisabled(true);
    Xrm.Page.getControl("shipto_line3").setDisabled(true);
    Xrm.Page.getControl("shipto_telephone").setDisabled(true);
    Xrm.Page.getControl("shipto_city").setDisabled(true);
    Xrm.Page.getControl("shipto_fax").setDisabled(true);
    Xrm.Page.getControl("isp_pastelinvoicenumber").setDisabled(true);
    shippingMethodCode_onchange();
    collectFrom_onchange();
    var Invoiceid = Xrm.Page.data.entity.getId();
    var reportURL = Xrm.Page.context.getServerUrl() + "/crmreports/viewer/viewer.aspx?action=run&helpID=Invoice%20Copy%20Report.rdl&id=%7bC522658F-C16E-E211-8287-00155D04F907%7d&p:invoiceid=" + Invoiceid;
    Xrm.Page.getControl("IFRAME_Invoice").setSrc(reportURL);

    if (Xrm.Page.getAttribute('ispricelocked').getValue() == false) {

        Xrm.Page.getAttribute('isp_invoiceupdate').setValue(guidGenerator());
        invoiceupdate_onchange();
    }

    if (Xrm.Page.getAttribute('isp_creditnotenumber').getValue() == 'PendingCreditNoteCreation') {
        if (Xrm.Page.getAttribute('ispricelocked').getValue() == false) {
            Xrm.Page.getAttribute('isp_invoiceupdate').setValue(guidGenerator());
            invoiceupdate_onchange();
        }
        Xrm.Page.getAttribute('isp_creditnotenumber').setValue('');
        Xrm.Page.data.entity.save();
        window.open(Xrm.Page.context.getServerUrl() + '/main.aspx?etc=10059&extraqs=%3f_gridType%3d10059%26etc%3d10059%26id%3d%257b' + Xrm.Page.getAttribute('isp_creditnote').getValue()[0].id.substring(1, 37) + '%257d%26pagemode%3diframe%26preloadcache%3d1341398805381&pagetype=entityrecord');

    }
    if (Xrm.Page.ui.getFormType() == 2) {
        Xrm.Page.getControl('isp_invoiceupdate').setVisible(false);
    }
}

function invoiceupdate_onchange() {
    Xrm.Page.data.entity.save();
}

function shippingMethodCode_onchange() {
    //Collect
    if (Xrm.Page.getAttribute('shippingmethodcode').getValue() == 100000001) {
        Xrm.Page.getControl('isp_collectfrom').setVisible(true);
        Xrm.Page.getControl('isp_expecteddeliverycollection').setVisible(false);
        Xrm.Page.getControl('isp_deliverystatus').setVisible(false);
        Xrm.Page.getControl('isp_deliverymessage').setVisible(false);
        // Xrm.Page.getAttribute('isp_collectfrom').setRequiredLevel("required");
    }
    //Courier
    if (Xrm.Page.getAttribute('shippingmethodcode').getValue() == 100000000) {
        Xrm.Page.getAttribute('isp_collectfrom').setValue(null);
        Xrm.Page.getControl('isp_collectfrom').setVisible(false);
        Xrm.Page.getControl('isp_expecteddeliverycollection').setVisible(true);
        Xrm.Page.getControl('isp_deliverystatus').setVisible(true);
        Xrm.Page.getControl('isp_deliverymessage').setVisible(true);
        //   Xrm.Page.getAttribute('isp_collectfrom').setRequiredLevel("none");
    }
    else if (Xrm.Page.getAttribute('shippingmethodcode').getValue() == 100000002 || Xrm.Page.getAttribute('shippingmethodcode').getValue() == null) {
        Xrm.Page.getAttribute('isp_collectfrom').setValue(null);
        Xrm.Page.getControl('isp_collectfrom').setVisible(false);
        Xrm.Page.getControl('isp_expecteddeliverycollection').setVisible(false);
        Xrm.Page.getAttribute('isp_shoptocollectfrom').setValue(null);
        Xrm.Page.getAttribute('isp_expecteddeliverycollection').setValue(null);
        Xrm.Page.getControl('isp_deliverystatus').setVisible(false);
        Xrm.Page.getControl('isp_deliverymessage').setVisible(false);
        // Xrm.Page.getAttribute('isp_collectfrom').setRequiredLevel("none");
    }
}

function collectFrom_onchange() {
    //Head Offices
    if (Xrm.Page.getAttribute('isp_collectfrom').getValue() == 100000000) {
        Xrm.Page.getControl('isp_expecteddeliverycollection').setVisible(true);
        Xrm.Page.getAttribute('isp_expecteddeliverycollection').setRequiredLevel("none");
        Xrm.Page.getControl('isp_shoptocollectfrom').setVisible(false);
    }
    //Shop
    if (Xrm.Page.getAttribute('isp_collectfrom').getValue() == 100000001) {
        Xrm.Page.getControl('isp_expecteddeliverycollection').setVisible(true);
        Xrm.Page.getAttribute('isp_expecteddeliverycollection').setRequiredLevel("none");
        Xrm.Page.getControl('isp_shoptocollectfrom').setVisible(true);
    }
    //Regionals
    else if (Xrm.Page.getAttribute('isp_collectfrom').getValue() == 863300001 || Xrm.Page.getAttribute('isp_collectfrom').getValue() == 863300000) {
        Xrm.Page.getControl('isp_expecteddeliverycollection').setVisible(false);
        Xrm.Page.getAttribute('isp_expecteddeliverycollection').setRequiredLevel("none");
        Xrm.Page.getControl('isp_shoptocollectfrom').setVisible(false);
    }
}

function guidGenerator() { var S4 = function () { return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1); }; return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4()); }