﻿function isp_buyback_onload() {
    attachEvents();
    showHideATP();
    PopulateContactDetails();
    checkCollectionPreference();
    if ((Xrm.Page.getAttribute('statuscode').getValue() == 863300001) && (!$helper.hasRole('System Administrator', Xrm.Page.context))) {
        var attributes = Xrm.Page.data.entity.attributes.get();
        for (var i in attributes) {
            Xrm.Page.getControl(attributes[i].getName()).setDisabled(true);
        }
        Xrm.Page.getControl("isp_safecustodyapproval").setDisabled(false);
        Xrm.Page.getControl("isp_safecustodycode").setDisabled(false);
    }

    var user = $crm.odata.retrieve("SystemUser", Xrm.Page.context.getUserId(), "", null, null);
    var userType = user.isp_usertype;
    var show = userType.Value == 2;
    if (show) {
        Xrm.Page.getAttribute('isp_scoinshopbroker').setRequiredLevel('required');
        Xrm.Page.getControl('isp_scoinshopbroker').setVisible(show);
    }
    else {
        Xrm.Page.getAttribute('isp_scoinshopbroker').setRequiredLevel('none');
        Xrm.Page.getControl('isp_scoinshopbroker').setVisible(show);
    }
    if (Xrm.Page.ui.getFormType() != 1) {
        if (Xrm.Page.getAttribute('isp_purchasenotecreated').getValue() && Xrm.Page.getAttribute('isp_purchasenote').getValue() != null) {

            Xrm.Page.getAttribute('isp_purchasenotecreated').setValue(false);
            Xrm.Page.getAttribute('isp_purchasenotecreated').setSubmitMode('always');
            Xrm.Page.data.entity.save('saveandclose');
            Xrm.Utility.openEntityForm('isp_purchasenote', Xrm.Page.getAttribute('isp_purchasenote').getValue()[0].id);
        }
        var BuyBackGuid = Xrm.Page.data.entity.getId().substring(1, 37);
        var reportURL = Xrm.Page.context.getServerUrl() + "/crmreports/viewer/viewer.aspx?action=run&helpID=BuyBack.rdl&id=%7b17632DA0-49B6-E211-83EC-00155D04F907%7d&p:buyback=" + Xrm.Page.data.entity.getId();
        Xrm.Page.getControl("IFRAME_BuyBack").setSrc(reportURL);
    }
    var interval = null;
    interval = setInterval("SubGridRefresh();", 15000);
}

function getReport() {
    var serverUrl = Xrm.Page.context.getServerUrl();
}

function SubGridRefresh() {
    var grid = document.getElementById("isp_buybackproducts");
    if (grid) {
        grid.attachEvent("onrefresh", ReLoadForm);
        if (interval != null) {
            clearInterval(interval);
        }
    }
}

function attachEvents() {
    Xrm.Page.getAttribute('isp_paymentoptions').addOnChange(showHideATP, saveATP);
    Xrm.Page.getAttribute('isp_bankingdetailsverified').addOnChange(saveDetails);
    Xrm.Page.getAttribute('isp_collectionpreference').addOnChange(CollectionPreferenceOnchange);
}

function showHideATP() {
    var bool = Xrm.Page.getAttribute('isp_paymentoptions').getValue() == 863300002;
    Xrm.Page.getControl('isp_atp').setVisible(bool);
}

function saveATP() {
    Xrm.Page.data.entity.save();
}

function ReLoadForm() {
    window.location.reload(true);
}
function saveDetails() {
    Xrm.Page.data.entity.save();
}

function isp_customer_onchange() {
    PopulateContactDetails();
}

function PopulateProductDetails() {
}

function CollectionPreferenceOnchange() {
    Xrm.Page.data.entity.save();
}

function checkCollectionPreference() {
    var bool = Xrm.Page.getAttribute('isp_collectionpreference').getValue() == 863300002;
    Xrm.Page.getControl('isp_safecustodyapproval').setVisible(bool);
    Xrm.Page.getControl('isp_safecustodycode').setVisible(bool);
}
function isp_buyback_onsave() {
    if (Xrm.Page.getAttribute('isp_idnumber').getValue() == null || Xrm.Page.getAttribute('isp_idnumber').getValue() == "") {
        alert("Please note: There is no ID number. You will not be allowed to request approval until the ID number is populated. Please update this on the customer.");
    }
    if ((Xrm.Page.getAttribute('isp_idnumber').getValue() != null || Xrm.Page.getAttribute('isp_idnumber').getValue()) != "" && Xrm.Page.ui.getFormType() == 1) {
        Xrm.Page.getAttribute('isp_idnumber').setSubmitMode('always');
    }
    var contact = new Object();
    contact.isp_CompanyName = Xrm.Page.getAttribute('isp_companyname').getValue() != null ? Xrm.Page.getAttribute('isp_companyname').getValue() : null;
    contact.Address1_Line1 = Xrm.Page.getAttribute('isp_line1').getValue() != null ? Xrm.Page.getAttribute('isp_line1').getValue() : null;
    contact.Address1_Line2 = Xrm.Page.getAttribute('isp_line2').getValue() != null ? Xrm.Page.getAttribute('isp_line2').getValue() : null;
    contact.Address1_Line3 = Xrm.Page.getAttribute('isp_line3').getValue() != null ? Xrm.Page.getAttribute('isp_line3').getValue() : null;
    contact.Address1_PostalCode = Xrm.Page.getAttribute('isp_postalcode').getValue() != null ? Xrm.Page.getAttribute('isp_postalcode').getValue() : null;
    contact.isp_VATNumber = Xrm.Page.getAttribute('isp_vatnumber').getValue() != null ? Xrm.Page.getAttribute('isp_vatnumber').getValue() : null;
    contact.Address1_City = Xrm.Page.getAttribute('isp_city').getValue() != null ? Xrm.Page.getAttribute('isp_city').getValue() : null;
    contact.MobilePhone = Xrm.Page.getAttribute('isp_telephone').getValue() != null ? Xrm.Page.getAttribute('isp_telephone').getValue() : null;
    contact.isp_Bank = Xrm.Page.getAttribute('isp_bank').getValue() != null ? Xrm.Page.getAttribute('isp_bank').getValue() : null;
    contact.isp_BankAccountType = Xrm.Page.getAttribute('isp_bankaccounttype').getValue() != null ? Xrm.Page.getAttribute('isp_bankaccounttype').getValue() : null;
    contact.isp_BranchCode = Xrm.Page.getAttribute('isp_branchnumber').getValue() != null ? Xrm.Page.getAttribute('isp_branchnumber').getValue() : null;
    contact.isp_BranchName = Xrm.Page.getAttribute('isp_branchname').getValue() != null ? Xrm.Page.getAttribute('isp_branchname').getValue() : null;
    contact.isp_AccountNumber = Xrm.Page.getAttribute('isp_accountnumber').getValue() != null ? Xrm.Page.getAttribute('isp_accountnumber').getValue() : null;
    contact.isp_AccountHolderName = Xrm.Page.getAttribute('isp_accountholdername').getValue() != null ? Xrm.Page.getAttribute('isp_accountholdername').getValue() : null;
    contact.isp_customercode = Xrm.Page.getAttribute('isp_customercode').getValue() != null ? Xrm.Page.getAttribute('isp_customercode').getValue() : null;

    if (contact != null) {
        $crm.odata.Update("Contact", Xrm.Page.getAttribute("isp_customer").getValue()[0].id, contact);
    }
}

function PopulateContactDetails() {
    if (Xrm.Page.getAttribute('isp_customer').getValue() != null) {
        var ContactGuid = Xrm.Page.getAttribute('isp_customer').getValue()[0].id.substring(1, 37);

        var Contact = $crm.odata.retrieveMultiple("Contact", "?$filter=ContactId eq guid'" + ContactGuid + "'", null, null, true);
        //update Customer Details
        Xrm.Page.getAttribute('isp_customercode').setValue(Contact[0].isp_customercode ? Contact[0].isp_customercode : '');
        Xrm.Page.getAttribute('isp_idnumber').setValue(Contact[0].isp_idnumber ? Contact[0].isp_idnumber : '');
        //banking details
        Xrm.Page.getAttribute('isp_bank').setValue(Contact[0].isp_Bank ? Contact[0].isp_Bank : '');
        Xrm.Page.getAttribute('isp_bankaccounttype').setValue(Contact[0].isp_BankAccountType ? Contact[0].isp_BankAccountType : '');
        Xrm.Page.getAttribute('isp_branchnumber').setValue(Contact[0].isp_BranchCode ? Contact[0].isp_BranchCode : '');
        Xrm.Page.getAttribute('isp_branchname').setValue(Contact[0].isp_BranchName ? Contact[0].isp_BranchName : '');
        Xrm.Page.getAttribute('isp_accountholdername').setValue(Contact[0].isp_AccountHolderName ? Contact[0].isp_AccountHolderName : '');
        Xrm.Page.getAttribute('isp_accountnumber').setValue(Contact[0].isp_AccountNumber ? Contact[0].isp_AccountNumber : '');
        //Address Info
        Xrm.Page.getAttribute('isp_companyname').setValue(Contact[0].isp_CompanyName ? Contact[0].isp_CompanyName : '');
        Xrm.Page.getAttribute('isp_vatnumber').setValue(Contact[0].isp_VATNumber ? Contact[0].isp_VATNumber : '');
        Xrm.Page.getAttribute('isp_line1').setValue(Contact[0].Address1_Line1 ? Contact[0].Address1_Line1 : '');
        Xrm.Page.getAttribute('isp_line2').setValue(Contact[0].Address1_Line2 ? Contact[0].Address1_Line2 : '');
        Xrm.Page.getAttribute('isp_line3').setValue(Contact[0].Address1_Line3 ? Contact[0].Address1_Line3 : '');
        Xrm.Page.getAttribute('isp_city').setValue(Contact[0].Address1_City ? Contact[0].Address1_City : '');

        Xrm.Page.getAttribute('isp_postalcode').setValue(Contact[0].Address1_PostalCode ? Contact[0].Address1_PostalCode : '');
        Xrm.Page.getAttribute('isp_country').setValue(Contact[0].Address1_Country ? Contact[0].Address1_Country : '');
        Xrm.Page.getAttribute('isp_telephone').setValue(Contact[0].MobilePhone ? Contact[0].MobilePhone : '');
    }
}