function systemuser_onload() {
    if (Xrm.Page.getAttribute('isp_usertype').getValue() == null)
        Xrm.Page.getAttribute('isp_usertype').setValue(0);
    systemuser_isp_usertype_onchange(true);
    Xrm.Page.ui.tabs.get(1).setVisible(false);
    if (Xrm.Page.getAttribute('isp_usertype').getValue() == 1) {
        Xrm.Page.getAttribute('parentsystemuserid').setRequiredLevel('required');
        Xrm.Page.ui.tabs.get(1).setVisible(true);
    }
    if (Xrm.Page.getAttribute('isp_usertype').getValue() == 2) {
        Xrm.Page.ui.tabs.get(1).setVisible(true);
        Xrm.Page.getAttribute('parentsystemuserid').setRequiredLevel('none');
    }
    if (Xrm.Page.getAttribute('isp_brokerlinkedtoshop') != null)
        setRequirementForBrokerLinkedToShop();
}

function systemuser_businessunitid_onchange() {
    setRequirementForBrokerLinkedToShop();
}

function setRequirementForBrokerLinkedToShop() {
    var lookupItem = Xrm.Page.getAttribute('businessunitid').getValue();

    if (lookupItem[0] != null) {
        if (lookupItem[0].name == "Outbound Brokers")
            Xrm.Page.getAttribute('isp_brokerlinkedtoshop').setRequiredLevel('required');
        else
            Xrm.Page.getAttribute('isp_brokerlinkedtoshop').setRequiredLevel('none');
    }
}
function systemuser_isp_usertype_onchange(isOnLoad) {
    if (!isOnLoad) {
        Xrm.Page.getAttribute('isp_brokerlinkedtoshop').setValue([]);
        Xrm.Page.getAttribute('parentsystemuserid').setValue([]);
    }

    var userType = Xrm.Page.getAttribute('isp_usertype').getValue();
    // 0 = External
    // 1 = Broker
    // 2 = Shop
    Xrm.Page.getAttribute('parentsystemuserid').setRequiredLevel(userType == 1 ? 'required' : 'none');
    Xrm.Page.getAttribute('isp_pastelwarehouse').setRequiredLevel(userType == 2 ? 'required' : 'none');
    Xrm.Page.getAttribute('isp_salescode').setRequiredLevel('required');
    Xrm.Page.getAttribute('businessunitid').setRequiredLevel('required');

    Xrm.Page.getControl('isp_salescode').setDisabled(userType == 0);
    Xrm.Page.getControl('isp_brokerlinkedtoshop').setDisabled(userType != 1);
    Xrm.Page.getControl('parentsystemuserid').setDisabled(userType == 1);
    Xrm.Page.ui.tabs.get(1).setVisible(false);
    if (Xrm.Page.getAttribute('isp_usertype').getValue() == 1) {
        Xrm.Page.getAttribute('parentsystemuserid').setRequiredLevel('required');
        Xrm.Page.ui.tabs.get(1).setVisible(true);
    }
    if (Xrm.Page.getAttribute('isp_usertype').getValue() == 2) {
        Xrm.Page.ui.tabs.get(1).setVisible(true);
    }

    if (userType == 8633000000 || userType == 863300001)
        Xrm.Page.getControl('isp_salescode').setDisabled(true);
}