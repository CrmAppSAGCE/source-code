/// <reference path="library/crm.js" />
/// <reference path="library/isp_json2.js" />
/// <reference path="library/crm.odata.js" />
/// <reference path="library/helper.js" />
/// <reference path="salesorder.js" />

function CalculateTotals() {
    var SalesOrderGUID = Xrm.Page.data.entity.getId();
    SalesOrderGUID = SalesOrderGUID.substring(1, 37);
    var serverUrl = Xrm.Page.context.getServerUrl();
    var oDataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";
    var filter = "/SalesOrderDetailSet?$select=ExtendedAmount&$filter=SalesOrderId/Id ne guid'" + SalesOrderGUID + "'";
    var retrieveTotals = new XMLHttpRequest();
    retrieveTotals.open("GET", oDataPath + filter, false);
    retrieveTotals.setRequestHeader("Accept", "application/json");
    retrieveTotals.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    retrieveTotals.onreadystatechange = function () {
        retrieveTotalsCallBack(this);
    };
    retrieveTotals.send();
}

function retrieveTotalsCallBack(retrieveTotals) {
    if (retrieveTotals.readyState == 4) {
        if (retrieveTotals.status == 200) {
            var retrievedTotals = JSON.parse(retrieveTotals.responseText).d;
            if (retrievedTotals.FullName != null)
                var setUservalue = new Array();
            setUservalue[0] = new Object();
            setUservalue[0].id = Xrm.Page.context.getUserId();
            setUservalue[0].entityType = 'systemuser';
            setUservalue[0].name = retrievedTotals.FullName;

            Xrm.Page.getAttribute("isp_paymentreceiptconfirmedby").setValue(setUservalue)
        }
        else {
            //Do nothing?
        }
    }
}

function salesorder_atp_onload() {
    shippingmethodcode_onchange();
    isp_collectfrom_onchange();
    var SONumber = Xrm.Page.getAttribute("ordernumber").getValue();
    var reportURL = Xrm.Page.context.getServerUrl() + "/crmreports/viewer/viewer.aspx?action=filter&helpID=Document1.rdl&id=%7b4B6F25B0-49C7-E111-B3D6-78ACC08839C9%7d&p:sonumber=" + SONumber;
    Xrm.Page.getControl("IFRAME_ATP").setSrc(reportURL);

    if (Xrm.Page.ui.getFormType() == 1) { // create 
        Xrm.Page.getAttribute('isp_ordertype').setValue(100000000);
        Xrm.Page.getControl('isp_paymentauthorisationnumber').setVisible(false);
        Xrm.Page.getControl('isp_datepaymentconfirmed').setVisible(false);
        Xrm.Page.getControl('isp_paymentreceiptconfirmedby').setVisible(false);
    }
    if (Xrm.Page.getAttribute('isp_systempartialpaymentguid').getValue() != null && Xrm.Page.getAttribute('isp_systempartialpaymentguid').getValue() != "NEW") {
        var Guid = Xrm.Page.getAttribute('isp_systempartialpaymentguid').getValue();
        Xrm.Page.getAttribute('isp_systempartialpaymentguid').setValue('');
        Xrm.Page.getAttribute('isp_systempartialpaymenttemp').setValue('');
        var URL = Xrm.Page.context.getServerUrl() + "/main.aspx?etc=10062&extraqs=%3f_gridType%3d10062%26etc%3d10062%26id%3d%257b" + Guid + "%257d%26pagemode%3diframe%26preloadcache%3d1342609965328%26rskey%3d661832463&pagetype=entityrecord";
        window.open(URL);
    }
    Xrm.Page.getAttribute('ordernumber').setRequiredLevel('none');
    if (Xrm.Page.getAttribute('statuscode').getValue() == 863300009) {
        Xrm.Page.getAttribute('isp_amountpaid').setSubmitMode('always');
        Xrm.Page.getAttribute('isp_paymentauthorisationnumber').setSubmitMode('always');
    }
}


function isp_companyname_onchange() {
    if (Xrm.Page.getAttribute('isp_companyname') != null && Xrm.Page.getAttribute('isp_companyname').getValue() != null)
        Xrm.Page.getAttribute('isp_vatnumber').setRequiredLevel('required');
    else
        Xrm.Page.getAttribute('isp_vatnumber').setRequiredLevel('none');
}

function shippingmethodcode_onchange() {
    if (Xrm.Page.getAttribute('shippingmethodcode').getValue() == '100000001') {
        Xrm.Page.getControl('isp_collectfrom').setVisible(true);
        Xrm.Page.getControl('isp_expecteddeliverycollectiondate').setVisible(false);
        Xrm.Page.getControl('isp_shoptocollectfrom').setVisible(false);
        Xrm.Page.getAttribute('isp_collectfrom').setRequiredLevel("required");
    }
    else if (Xrm.Page.getAttribute('shippingmethodcode').getValue() == '100000000') {
        Xrm.Page.getControl('isp_collectfrom').setVisible(false);
        Xrm.Page.getControl('isp_expecteddeliverycollectiondate').setVisible(true);
        Xrm.Page.getControl('isp_shoptocollectfrom').setVisible(false);
        Xrm.Page.getAttribute('shipto_telephone').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_collectfrom').setRequiredLevel("none");
    }
    else {
        Xrm.Page.getControl('isp_collectfrom').setVisible(false);
        Xrm.Page.getControl('isp_expecteddeliverycollectiondate').setVisible(false);
        Xrm.Page.getControl('isp_shoptocollectfrom').setVisible(false);
    }
    if (Xrm.Page.getAttribute('shippingmethodcode').getValue() == '100000000')
        Xrm.Page.getAttribute('shipto_telephone').setRequiredLevel('required');
    else
        Xrm.Page.getAttribute('shipto_telephone').setRequiredLevel('none');
}

function isp_collectfrom_onchange() {
    if (Xrm.Page.getAttribute('isp_collectfrom').getValue() == '100000001') {
        Xrm.Page.getControl('isp_shoptocollectfrom').setVisible(true);
        Xrm.Page.getControl('isp_expecteddeliverycollectiondate').setVisible(true);
    }
    else if (Xrm.Page.getAttribute('isp_collectfrom').getValue() == '100000000') {
        Xrm.Page.getControl('isp_shoptocollectfrom').setVisible(false);
        Xrm.Page.getControl('isp_expecteddeliverycollectiondate').setVisible(true);
    }
    else {
        Xrm.Page.getControl('isp_shoptocollectfrom').setVisible(false);
        Xrm.Page.getControl('isp_expecteddeliverycollectiondate').setVisible(false);
    }
}

function salesorder_atp_isp_paymentmethod_onchange() {
    if (Xrm.Page.getAttribute('isp_paymentmethod').getValue() == '100000003' || Xrm.Page.getAttribute('isp_paymentmethod').getValue() == '863300003') {
        Xrm.Page.getControl('isp_paymentauthorisationnumber').setVisible(true);
        Xrm.Page.getControl('isp_datepaymentconfirmed').setVisible(false);
        Xrm.Page.getControl('isp_paymentreceiptconfirmedby').setVisible(false);
    }
    //if it's cash
    else if (Xrm.Page.getAttribute('isp_paymentmethod').getValue() == '100000002' || Xrm.Page.getAttribute('isp_paymentmethod').getValue() == '863300000') {
        Xrm.Page.getControl('isp_paymentauthorisationnumber').setVisible(true);
        Xrm.Page.getControl('isp_datepaymentconfirmed').setVisible(true);
    }
    else if (Xrm.Page.getAttribute('isp_paymentmethod').getValue() == null) {
        Xrm.Page.getControl('isp_paymentauthorisationnumber').setVisible(false);
        Xrm.Page.getControl('isp_datepaymentconfirmed').setVisible(false);
        Xrm.Page.getControl('isp_paymentreceiptconfirmedby').setVisible(false);
    }
    else {
        Xrm.Page.getControl('isp_paymentauthorisationnumber').setVisible(true);
        Xrm.Page.getControl('isp_datepaymentconfirmed').setVisible(true);
        Xrm.Page.getControl('isp_paymentreceiptconfirmedby').setVisible(true);
    }
}

function salesorder_atp_isp_paymentmethods_onchange() {
    var SalesOrderGUID = Xrm.Page.data.entity.getId();
    if (SalesOrderGUID != null) {
        Xrm.Page.data.entity.save();
    }
    Xrm.Page.ui.refreshRibbon();
    //EFT
    if (Xrm.Page.getAttribute('isp_paymentmethods').getValue() == '100000003' || Xrm.Page.getAttribute('isp_paymentmethods').getValue() == '863300003') {
        Xrm.Page.getControl('isp_paymentauthorisationnumber').setVisible(true);
        Xrm.Page.getControl('isp_datepaymentconfirmed').setVisible(false);
        Xrm.Page.getControl('isp_paymentreceiptconfirmedby').setVisible(false);
    }
    //if it's cash
    else if (Xrm.Page.getAttribute('isp_paymentmethods').getValue() == '100000002' || Xrm.Page.getAttribute('isp_paymentmethods').getValue() == '863300000') {
        Xrm.Page.getControl('isp_paymentauthorisationnumber').setVisible(true);
        Xrm.Page.getControl('isp_datepaymentconfirmed').setVisible(true);
    }
    else if (Xrm.Page.getAttribute('isp_paymentmethods').getValue() == null) {
        Xrm.Page.getControl('isp_paymentauthorisationnumber').setVisible(false);
        Xrm.Page.getControl('isp_datepaymentconfirmed').setVisible(false);
        Xrm.Page.getControl('isp_paymentreceiptconfirmedby').setVisible(false);
    }
    else {
        Xrm.Page.getControl('isp_paymentauthorisationnumber').setVisible(true);
        Xrm.Page.getControl('isp_datepaymentconfirmed').setVisible(true);
        Xrm.Page.getControl('isp_paymentreceiptconfirmedby').setVisible(true);
    }
}

function isp_paymentauthorisationnumber_onchange() {
    if (Xrm.Page.getAttribute('isp_paymentauthorisationnumber').getValue() != null) {
        Xrm.Page.getAttribute('isp_datepaymentconfirmed').setRequiredLevel('required');
    }
    if (Xrm.Page.getAttribute('isp_paymentauthorisationnumber').getValue() == null) {
        Xrm.Page.getAttribute('isp_datepaymentconfirmed').setRequiredLevel('none');
    }
    PopulatePaymentReceiptUser();
}


function salesorder_atp_onsave() {
    var CellPhone = Xrm.Page.getAttribute('shipto_telephone').getValue();
    var Email = Xrm.Page.getAttribute('isp_emailaddress').getValue();
}

function PopulatePaymentReceiptUser() {
    //Get GUID of logged user
    var user = Xrm.Page.context.getUserId();
    var userId = user.substring(1, 37);
    // Read the CRM Context to determine the CRM URL
    var serverUrl = Xrm.Page.context.getServerUrl();
    var ODATA_ENDPOINT = "/XRMServices/2011/OrganizationData.svc";
    var ODATA_EntityCollection = "/SystemUserSet";
    // Specify the ODATA Query
    var ODATA_Query = "(guid\'" + userId + "')";
    // Combine into the final URL
    var ODATA_Final_url = serverUrl + ODATA_ENDPOINT + ODATA_EntityCollection + ODATA_Query;

    //Calls the REST endpoint to retrieve data
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        url: ODATA_Final_url,
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        success: function (data, textStatus, XmlHttpRequest) {
            var userName = data.d.FullName;
            var setUservalue = new Array();
            setUservalue[0] = new Object();
            setUservalue[0].id = Xrm.Page.context.getUserId();
            setUservalue[0].entityType = 'systemuser';
            setUservalue[0].name = data.d.FullName;

            Xrm.Page.getAttribute('isp_paymentreceiptconfirmedby').setSubmitMode('always');
            Xrm.Page.getAttribute("isp_paymentreceiptconfirmedby").setValue(setUservalue)

        },
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            alert('Error: ' + ODATA_Final_url);
        }
    });
}

function CheckForDupes(value, type) {
    var serverUrl = Xrm.Page.context.getServerUrl();
    // Creating the Odata Endpoint
    var oDataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";
    var retrieveReq = new XMLHttpRequest();
    if (type == "phone") {
        var Odata = oDataPath + "/ContactSet?$filter=OwnerId/Id ne guid'" + Xrm.Page.context.getUserId() + "' and (MobilePhone eq '" + value + "' or Telephone1 eq '" + value + "' or Telephone2 eq '" + value + "')"
    }
    if (type == "email")
        var Odata = oDataPath + "/ContactSet?$filter=OwnerId/Id ne guid'" + Xrm.Page.context.getUserId() + "' and EMailAddress1 eq '" + value + "'";
    if (type == "id")
        var Odata = oDataPath + "/ContactSet?$filter=OwnerId/Id ne guid'" + Xrm.Page.context.getUserId() + "' and isp_idnumber eq '" + value + "'";

    retrieveReq.open("GET", Odata, false);
    retrieveReq.setRequestHeader("Accept", "application/json");
    retrieveReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    retrieveReq.onreadystatechange = function () { retrieveReqCallBack(this); };
    retrieveReq.send();
}

function retrieveReqCallBack(retrieveReq) {
    if (retrieveReq.readyState == 4) {
        var retrieved = JSON.parse(retrieveReq.responseText).d;
        var Contact = retrieved.results[0];
        if (Contact != null) {
            Xrm.Page.getAttribute('isp_managementnotification').setValue(true);
            Xrm.Page.getAttribute("isp_duplicateowner").setValue([{ id: Contact.OwnerId.Id, name: Contact.OwnerId.Name, entityType: 'systemuser'}]);
        }
    }
}

function salesorder_atp_isp_contact_onchange() {
    var CellPhone = Xrm.Page.getAttribute('shipto_telephone').getValue();
    var Email = Xrm.Page.getAttribute('isp_emailaddress').getValue();
    if (Xrm.Page.getAttribute('isp_idnumber') != null)
        var IDNumber = Xrm.Page.getAttribute('isp_idnumber').getValue();
    CheckForDupes(CellPhone, 'phone');
    CheckForDupes(Email, 'email');
    CheckForDupes(IDNumber, 'id');
}

function salesorder_atp_isp_atpverified_onchange() {
    if (Xrm.Page.getAttribute('isp_atpverified').getValue() == 1) {
        Xrm.Page.getControl('isp_paymentmethods').setDisabled(true);
        Xrm.Page.getControl('shippingmethodcode').setDisabled(true);
        Xrm.Page.getControl('isp_contactid').setDisabled(true);
    }
    else {
        Xrm.Page.getControl('isp_paymentmethods').setDisabled(false);
        Xrm.Page.getControl('shippingmethodcode').setDisabled(false);
        Xrm.Page.getControl('isp_contactid').setDisabled(false);
    }
    Xrm.Page.ui.refreshRibbon();
} 
 