/// <reference path="library/crm.js" />
/// <reference path="library/crm.odata.js" />
/// <reference path="library/helper.js" />

function isp_stocktransferrequest_onload() {
    var OwnerId = Xrm.Page.data.entity.attributes.get("ownerid").getValue();
    if (Xrm.Page.ui.getFormType() == 1) {  // create 
        Xrm.Page.getAttribute('isp_requestingbrokershop').setValue(OwnerId);
    }
    if (Xrm.Page.getAttribute('isp_movetostore') != null) {
        if ($helper.hasRole('Stock Controller', Xrm.Page.context))
            Xrm.Page.getControl('isp_movetostore').setDisabled(false);
        else
            Xrm.Page.getControl('isp_movetostore').setDisabled(true);
    }
    var PastelWarehouse = $crm.odata.retrieve("SystemUser", OwnerId[0].id.substring(1, 37), "", null, null);
    Xrm.Page.getAttribute('isp_pastelwarehouse').setValue([{ id: PastelWarehouse.isp_pastelwarehouse.Id, name: PastelWarehouse.isp_pastelwarehouse.Name, entityType: PastelWarehouse.isp_pastelwarehouse.LogicalName}]);
}

function isp_stocktransferrequest_isp_atpnumber_onchange() {
    if (Xrm.Page.getAttribute('isp_atpnumber') != null) {
        if (Xrm.Page.getAttribute('isp_atpnumber').getValue() != null) {
            var parentATPID = Xrm.Page.getAttribute("isp_atpnumber").getValue();
            parentATPID = parentATPID[0].id.substring(1, 37);

            var parentATP = $crm.odata.retrieve("SalesOrder", parentATPID, "", null, null);
            Xrm.Page.getAttribute('isp_requestingcustomer').setValue([{ id: parentATP.isp_ContactId.Id, name: parentATP.isp_ContactId.Name, entityType: parentATP.isp_ContactId.LogicalName}]);
        }
    }
}
