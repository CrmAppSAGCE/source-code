﻿function isp_commissionrule_onload() {
    showHideFields();
    attachEvents();
}

function attachEvents() {
    Xrm.Page.getAttribute('isp_producttype').addOnChange(showHideFields);
}

function showHideFields() {
    if (Xrm.Page.getAttribute('isp_producttype').getValue() == null) {
        Xrm.Page.getAttribute('isp_bullioncommissionvalue').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_minimumvalue').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_tier').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_buybackcommissionvalue').setRequiredLevel('none');
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Rare").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Bullion").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_BuyBack").setVisible(false);
    }
    if (Xrm.Page.getAttribute('isp_producttype').getValue() == 1) {
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Rare").setVisible(true);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Bullion").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_BuyBack").setVisible(false);
        Xrm.Page.getAttribute('isp_bullioncommissionvalue').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_minimumvalue').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_tier').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_buybackcommissionvalue').setRequiredLevel('none');
    }
    if (Xrm.Page.getAttribute('isp_producttype').getValue() == 2) {
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Bullion").setVisible(true);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Rare").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_BuyBack").setVisible(false);
        Xrm.Page.getAttribute('isp_bullioncommissionvalue').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_minimumvalue').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_tier').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_buybackcommissionvalue').setRequiredLevel('none');
    }
    if (Xrm.Page.getAttribute('isp_producttype').getValue() == 3) {
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_BuyBack").setVisible(true);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Rare").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_General").sections.get("sec_Bullion").setVisible(false);
        Xrm.Page.getAttribute('isp_buybackcommissionvalue').setRequiredLevel('required');
        Xrm.Page.getAttribute('isp_bullioncommissionvalue').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_minimumvalue').setRequiredLevel('none');
        Xrm.Page.getAttribute('isp_tier').setRequiredLevel('none');
    }
}