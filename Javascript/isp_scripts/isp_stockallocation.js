/// <reference path="library/crm.js" />
/// <reference path="library/crm.odata.js" />

function onload() {
    if (Xrm.Page.getAttribute('isp_masterstock').getValue() != null) {
        //Retrieve the Remaining Stock value for the Master Stock
        var remainingQuantity = $crm.odata.retrieve("isp_stockcheck", Xrm.Page.getAttribute('isp_masterstock').getValue()[0].id.toString().substring(1, 37), "", null, null);
        Xrm.Page.getAttribute('isp_remainingstock').setValue(remainingQuantity.isp_StockLevel);
    }
}

function onsave() {
    // Check they not too greedy
    if (Xrm.Page.getAttribute('isp_quantity').getValue() > Xrm.Page.getAttribute('isp_remainingstock').getValue()) {
        alert('You cannot allocate more stock than what is currently available to allocate');
        return false;
    }
    if (Xrm.Page.getAttribute('isp_quantity').getValue() != null) {
        // Check if there is already a stock check item for this (owner or team) and product        
        var remainingQuantity = $crm.odata.retrieve("isp_stockcheck", Xrm.Page.getAttribute('isp_masterstock').getValue()[0].id.toString().substring(1, 37), "", null, null);
        var origStock = $crm.odata.retrieve("isp_stockcheck", Xrm.Page.getAttribute('isp_masterstock').getValue()[0].id, "", null, null);

        //Create a NEW Stock Check item, Owner = isp_allocateto, isp_stocklevel = isp_quantity
        var stock = new Object();
        if (Xrm.Page.getAttribute('isp_allocateto').getValue() != null) {
            Xrm.Page.getAttribute('isp_name').setValue(Xrm.Page.getAttribute('isp_allocateto').getValue()[0].name + ' : ' + Xrm.Page.getAttribute('isp_quantity').getValue());
            stock.OwnerId = { Id: Xrm.Page.getAttribute('isp_allocateto').getValue()[0].id.toString().substring(1, 37), LogicalName: Xrm.Page.getAttribute('isp_allocateto').getValue()[0].typename, Name: Xrm.Page.getAttribute('isp_allocateto').getValue()[0].name };
        }
        else if (Xrm.Page.getAttribute('isp_allocatetoteam').getValue() != null) {
            Xrm.Page.getAttribute('isp_name').setValue(Xrm.Page.getAttribute('isp_allocatetoteam').getValue()[0].name + ' : ' + Xrm.Page.getAttribute('isp_quantity').getValue());
            stock.OwnerId = { Id: Xrm.Page.getAttribute('isp_allocatetoteam').getValue()[0].id.toString().substring(1, 37), LogicalName: Xrm.Page.getAttribute('isp_allocatetoteam').getValue()[0].typename, Name: Xrm.Page.getAttribute('isp_allocatetoteam').getValue()[0].name };
        }
        //stock.isp_unallocatedstock = Xrm.Page.getAttribute('isp_quantity').getValue();.
        stock.isp_StockLevel = Xrm.Page.getAttribute('isp_quantity').getValue();
        stock.isp_name = Xrm.Page.getAttribute('isp_masterstock').getValue()[0].name;
        stock.isp_parentstock = { Id: Xrm.Page.getAttribute('isp_masterstock').getValue()[0].id, LogicalName: 'isp_stockcheck', Name: Xrm.Page.getAttribute('isp_name').getValue() };
        stock.isp_vat = origStock.isp_vat;
        stock.isp_ProductCode = origStock.isp_ProductCode;
        stock.isp_productstock = origStock.isp_productstock;
        stock.isp_PastelWarehouse = { Id: origStock.isp_PastelWarehouse.Id, LogicalName: origStock.isp_PastelWarehouse.LogicalName, Name: origStock.isp_PastelWarehouse.Name };
        stock.isp_Product = { Id: origStock.isp_Product.Id, LogicalName: origStock.isp_Product.LogicalName, Name: origStock.isp_Product.Name };
        stock.TransactionCurrencyId = origStock.TransactionCurrencyId;
        stock.isp_price = { Value: origStock.isp_price.Value };
        stock.isp_GlobalStockLevel = origStock.isp_GlobalStockLevel;

        var NewStockCheckId = "";
        // Check if this owner already has stock
        var sFilter = "?$filter=OwnerId/Id eq guid'" + stock.OwnerId.Id.toString() + "' and isp_ProductCode eq '" + stock.isp_ProductCode + "' and isp_PastelWarehouse/Id eq guid'" + stock.isp_PastelWarehouse.Id.toString() + "'";
        var MyResults = $crm.odata.retrieveMultiple("isp_stockcheck", sFilter, null, null, true);
        if (MyResults.length > 0) {
            // Take the first one and increase it and grab the guid.
            NewStockCheckId = MyResults[0].isp_stockcheckId;
            var oStock = new Object();
            var iStockLevel = MyResults[0].isp_StockLevel + Xrm.Page.getAttribute('isp_quantity').getValue();
            oStock.isp_StockLevel = iStockLevel;
            oStock.TransactionCurrencyId = origStock.TransactionCurrencyId;
            oStock.isp_price = { Value: origStock.isp_price.Value };
            $crm.odata.Update('isp_stockcheck', NewStockCheckId, oStock, null, null);
        }
        else {
            var stockId = $crm.odata.Create('isp_stockcheck', stock);
            NewStockCheckId = stockId.isp_stockcheckId;
        }
        // Update old parent
        var ParentStock = origStock.isp_StockLevel - Xrm.Page.getAttribute('isp_quantity').getValue();
        var newStock = Object();
        newStock.isp_StockLevel = ParentStock;
        $crm.odata.Update('isp_stockcheck', Xrm.Page.getAttribute('isp_masterstock').getValue()[0].id, newStock, null, null);

        // Open the new record
        Xrm.Utility.openEntityForm('isp_stockcheck', NewStockCheckId);
    }
}

function isp_quantity_onchange() {
    if (Xrm.Page.getAttribute('isp_quantity').getValue() > Xrm.Page.getAttribute('isp_remainingstock').getValue()) {
        alert("You cannot allocate more stock than what is currently available to allocate");
        return false;
    }
}

function isp_allocateto_onchange() {
}

function unallocateStock() {
    var currentUnallocatedStock = origStock.isp_unallocatedstock - Xrm.Page.getAttribute('isp_quantity').getValue();
    var newStock = Object();
    newStock.isp_unallocatedstock = currentUnallocatedStock;
    $crm.odata.Update('isp_stockcheck', Xrm.Page.getAttribute('isp_masterstock').getValue()[0].id, newStock, null, null);
}