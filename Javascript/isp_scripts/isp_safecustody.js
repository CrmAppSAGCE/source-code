/// <reference path="library/crm.js" />

function safecustody_onload() {
    if (Xrm.Page.getControl('isp_atpnumber') != null) {

        Xrm.Page.getControl('isp_atpnumber').addCustomView('{00000000-0000-0000-0000-000000000001}', 'salesorder', 'Safe Custody ATPs',
        '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">' +
        '  <entity name="salesorder">' +
        '    <attribute name="ordernumber" />' +
        '    <order attribute="ordernumber" descending="false" />' +
        '    <filter type="and">' +
        '      <condition attribute="shippingmethodcode" operator="eq" value="100000002" />' +
        '    </filter>' +
        '  </entity>' +
        '</fetch>',
        '<grid name="resultset" object="2" jump="name" select="1" icon="1" preview="1">' +
        '<row name="result" id="salesorderid">' +
        '   <cell name="ordernumber" width="200" />' +
        '</row>' +
        '</grid>');

        Xrm.Page.getControl('isp_atpnumber').setDefaultView('{00000000-0000-0000-0000-000000000001}');
    }
}

function safecustody_isp_atpnumber_onchange() {
    var parentATPID = Xrm.Page.data.entity.attributes.get("isp_atpnumber").getValue()[0].id;
    parentATPID = parentATPID.substring(1, 37);
    var parentATP = $crm.odata.retrieve("SalesOrder", parentATPID, "", null, null);
    Xrm.Page.getAttribute('isp_customer').setValue([{ id: parentATP.isp_ContactId.Id, name: parentATP.isp_ContactId.Name, entityType: parentATP.isp_ContactId.LogicalName}]);
}

