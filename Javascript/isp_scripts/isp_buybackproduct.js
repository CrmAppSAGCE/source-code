﻿/// <reference path="library/crm.js" />
/// <reference path="library/crm.odata.js" />

function isp_newproduct_onchange() {
    if (Xrm.Page.getAttribute('isp_newproduct')) {//if it is a new Product
        if (Xrm.Page.getAttribute('isp_newproduct').getValue()) {
            Xrm.Page.getControl('isp_existingproducts').setDisabled(true);
            Xrm.Page.getAttribute('isp_existingproducts').setValue(null);
            Xrm.Page.getControl('isp_productdescription').setDisabled(false);
        }
        else { //if the product needs to be selected from the lookup
            Xrm.Page.getControl('isp_existingproducts').setDisabled(false);
            Xrm.Page.getAttribute('isp_productdescription').setValue('');
            Xrm.Page.getControl('isp_productdescription').setDisabled(true);
        }
    }
}

function isp_existingproducts_onchange() {
    if (Xrm.Page.getAttribute('isp_existingproducts').getValue() != null) {
        Xrm.Page.getAttribute('isp_productname').setValue(Xrm.Page.getAttribute('isp_existingproducts').getValue()[0].name);

        getProductDetails();
        Xrm.Page.data.entity.save();
    }
}

function isp_productdescription_onchange() {
    if (Xrm.Page.getAttribute('isp_productdescription').getValue() != null) {
        Xrm.Page.getAttribute('isp_productname').setValue(Xrm.Page.getAttribute('isp_productdescription').getValue());
    }
}

function buybackproduct_onsave(event) {
    if (Xrm.Page.ui.getFormType() == 1) {
        Xrm.Page.getAttribute('isp_buybackproductsidrelationship').setSubmitMode('always');
    }
}

function buybackproduct_onload() {
    if (Xrm.Page.ui.getFormType() == 1) {
        Xrm.Page.getControl('isp_correctweight').setDisabled(true);
        Xrm.Page.getControl('isp_conditionofcoin').setDisabled(true);
        Xrm.Page.getControl('isp_correctdiameterincludingcapsule').setDisabled(true);
        Xrm.Page.getControl('isp_conditionofcapsule').setDisabled(true);
        Xrm.Page.getControl('isp_correctthicknessincludingcapsule').setDisabled(true);
        Xrm.Page.getControl('isp_conditionofcertificate').setDisabled(true);
        Xrm.Page.getControl('isp_correctdesign').setDisabled(true);
        Xrm.Page.getControl('isp_conditionofbox').setDisabled(true);
    }
    if (Xrm.Page.getAttribute('isp_correctweight').getValue() && Xrm.Page.getAttribute('isp_correctdiameterincludingcapsule').getValue() && Xrm.Page.getAttribute('isp_correctthicknessincludingcapsule').getValue() && Xrm.Page.getAttribute('isp_correctdesign').getValue() && Xrm.Page.getAttribute('isp_conditionofcoin').getValue() && Xrm.Page.getAttribute('isp_conditionofcapsule').getValue() && Xrm.Page.getAttribute('isp_conditionofcapsule').getValue() && Xrm.Page.getAttribute('isp_conditionofcertificate').getValue() && Xrm.Page.getAttribute('isp_conditionofbox').getValue() && (Xrm.Page.getAttribute('statuscode').getValue() == 863300003 || Xrm.Page.getAttribute('statuscode').getValue() == 863300005)) {
        Xrm.Page.getControl('isp_correctweight').setDisabled(true);
        Xrm.Page.getControl('isp_conditionofcoin').setDisabled(true);
        Xrm.Page.getControl('isp_correctdiameterincludingcapsule').setDisabled(true);
        Xrm.Page.getControl('isp_conditionofcapsule').setDisabled(true);
        Xrm.Page.getControl('isp_correctthicknessincludingcapsule').setDisabled(true);
        Xrm.Page.getControl('isp_conditionofcertificate').setDisabled(true);
        Xrm.Page.getControl('isp_correctdesign').setDisabled(true);
        Xrm.Page.getControl('isp_conditionofbox').setDisabled(true);
    }
    Xrm.Page.getAttribute("isp_buybackproductsidrelationship").setSubmitMode("always");
    if (Xrm.Page.getAttribute('statuscode').getValue() == 863300001) {
        Xrm.Page.getControl('isp_productsbuybackprice').setDisabled(true); //the buy back price should be disabled at all times
    }
    else {
        Xrm.Page.getControl('isp_productsbuybackprice').setDisabled(false);
    }
    if (Xrm.Page.getAttribute('isp_quantity').getValue() == null) {
        Xrm.Page.getAttribute('isp_quantity').setValue(eval(1));
    }
    if (Xrm.Page.getAttribute('isp_newproduct').getValue()) {
        Xrm.Page.getControl('isp_existingproducts').setDisabled(true);
        Xrm.Page.getAttribute('isp_existingproducts').setValue(null);
        Xrm.Page.getControl('isp_productdescription').setDisabled(false);
    }
    else {
        Xrm.Page.getControl('isp_existingproducts').setDisabled(false);
        Xrm.Page.getAttribute('isp_productdescription').setValue('');
        Xrm.Page.getControl('isp_productdescription').setDisabled(true);
    }
    disableApproved();
}

function disableApproved() {
    if (Xrm.Page.getAttribute('statuscode').getValue() == 863300001 || Xrm.Page.getAttribute('statuscode').getValue() == 863300005) {
        Xrm.Page.getControl('isp_existingproducts').setDisabled(true);
        Xrm.Page.getControl('isp_productsbuybackprice').setDisabled(true);
        Xrm.Page.getControl('isp_quantity').setDisabled(true);
        Xrm.Page.getControl('isp_newproduct').setDisabled(true);
    }
}

function getProductDetails() {
    var ProductGuid = Xrm.Page.getAttribute('isp_existingproducts').getValue()[0].id.substring(1, 37);
    var Product = $crm.odata.retrieveMultiple("Product", "?$filter=ProductId eq guid'" + ProductGuid + "'", null, null, true);
    Xrm.Page.getAttribute('isp_productsbuybackprice').setValue(eval(Product[0].isp_BuyBackPrice.Value));
}
