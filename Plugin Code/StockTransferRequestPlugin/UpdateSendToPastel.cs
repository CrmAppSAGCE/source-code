﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlClient;
using StockTransferRequestPlugin.PastelService;

namespace Scoin.Crm.Plugins.stocktransferrequest
{
    public class UpdateSendToPastel : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];
            target = context.PreEntityImages["PreImage"];
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            //Need to send the Line commands to Pastel
            trace.Trace("Getting the Customer Code");
            var str = new CreateDocumentRq
            {
                DocumentType = EnumDocumentType.InventoryJournal,
                Header = new DocumentHeader()
                {
                    Date = DateTime.Today
                    ,
                    ClosingDate = DateTime.Today
                    ,
                    CustomerCode = "DUMP"
                        //CustomerCode = order.GetAttributeValue<string>("isp_customercode")
                    ,
                    
                    IncExcl = false
                    ,
                    DeliveryAddress = new ArrayOfString()
                    ,
                    Address = new ArrayOfString()
                    ,
                    InvoiceMessage = new ArrayOfString()
                    ,
                    OnHold = false
                    ,
                    OrderNumber = "OrderNr"
                    ,
                    Contact = target.Attributes.Contains("isp_contactid") ? target.GetAttributeValue<EntityReference>("isp_contactid").Name : ""
                    ,
                    //SalesAnalysisCode = (string)order.GetAttributeValue<AliasedValue>("su.isp_salescode").Value
                    SalesAnalysisCode = "DAN"
                    ,
                    ShipDeliver = "Collect", 
                },
                Line = new DocumentLine[2]

            };
            str.Line[0] = new DocumentLine(){
                Code = "k001-1oz",
                Description = "Test From",
                MultiStore = "010",
                Quantity = -1,
                LineType =  EnumLineType.Inventory
              
            };
            str.Line[1] = new DocumentLine(){
                Code = "k001-1oz",
                Description = "Test To",
                MultiStore = "011",
                Quantity = 1,
                LineType = EnumLineType.Inventory
            };
            var binding = new System.ServiceModel.BasicHttpBinding(System.ServiceModel.BasicHttpSecurityMode.None) { CloseTimeout = new TimeSpan(0, 0, 25), OpenTimeout = new TimeSpan(0, 0, 25), SendTimeout = new TimeSpan(0, 0, 25), ReceiveTimeout = new TimeSpan(0, 0, 25) };
            PastelServiceSoapClient client = new PastelServiceSoapClient(binding, new System.ServiceModel.EndpointAddress("http://sagcesql01/PastelService.asmx"));
            string PastelCompany = "CRMTEST";
            short PastelUser = 31;
            var rs = client.CreateDocument(str, PastelCompany, PastelUser);
            if (rs.Success)
            {

            }
            else
            {
                throw new Exception("Error from Pastel: " + rs.ErrorMessage);
            }
        }
    }
    }
