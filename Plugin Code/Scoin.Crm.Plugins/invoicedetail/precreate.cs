﻿using System;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;
using System.Collections.Generic;

namespace Scoin.Crm.Plugins.invoicedetail
{
    public class precreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            EntityReference pastelWarehouse = null;
            var target = (Entity)context.InputParameters["Target"];
            EntityHelper helper = new EntityHelper(context, service);

            Invoice invoice = (from i in orgContext.CreateQuery<Invoice>()
                               where i.InvoiceId.Value == target.GetAttributeValue<EntityReference>("invoiceid").Id
                               select new Invoice
                               {
                                   InvoiceId = i.InvoiceId,
                                   isp_atp = i.isp_atp,
                                   isp_AccountId = i.isp_AccountId
                               }).FirstOrDefault();

            SalesOrderDetail salesOrderDetail = (from sod in orgContext.CreateQuery<SalesOrderDetail>()
                                                 where sod.SalesOrderId.Id == invoice.isp_atp.Id
                                                 && sod.ProductId.Id == target.GetAttributeValue<EntityReference>("productid").Id
                                                 select new SalesOrderDetail
                                                 {
                                                     SalesOrderDetailId = sod.SalesOrderDetailId,
                                                     SalesOrderId = sod.SalesOrderId,
                                                     ProductId = sod.ProductId,
                                                     isp_VatTotalAmount = sod.isp_VatTotalAmount,
                                                     isp_VatAmount = sod.isp_VatAmount,
                                                     isp_VatExcludedAmount = sod.isp_VatExcludedAmount,
                                                     isp_VatExcludedTotalAmount = sod.isp_VatExcludedTotalAmount,
                                                     PricePerUnit = sod.PricePerUnit,
                                                     Quantity = sod.Quantity,
                                                     isp_Stock = sod.isp_Stock,
                                                 }).FirstOrDefault();

            Product product = (from p in orgContext.CreateQuery<Product>()
                               where p.ProductId.Value == salesOrderDetail.ProductId.Id
                               select new Product
                               {
                                   ProductId = p.ProductId,
                                   isp_vatcode = p.isp_vatcode,
                                   ProductNumber = p.ProductNumber
                               }).FirstOrDefault();
            helper.updateTarget(target, "isp_accountid", new EntityReference(Account.EntityLogicalName, invoice.isp_AccountId.Id));

            if (target.GetAttributeValue<EntityReference>("productid").Id == salesOrderDetail.ProductId.Id)
            {
                trace.Trace("6");
                var taxCode = product.isp_vatcode.ToString();
                var TaxPercentage = string.Empty;
                if (taxCode == "1")
                    TaxPercentage = "14.00%";
                else if (taxCode == "2")
                    TaxPercentage = "0.00%";
                else
                    TaxPercentage = "100.00%";

                trace.Trace("7");
                helper.updateTarget(target, "isp_code", product.ProductNumber);
                trace.Trace("8");
                helper.updateTarget(target, "isp_vattotal", new Money(salesOrderDetail.isp_VatTotalAmount.Value));
                trace.Trace("9");
                helper.updateTarget(target, "isp_vatexcl", new Money(salesOrderDetail.isp_VatExcludedAmount.Value));
                helper.updateTarget(target, "isp_vatexcltotal", new Money(salesOrderDetail.isp_VatExcludedTotalAmount.Value));
                trace.Trace("10");
                helper.updateTarget(target, "isp_priceperunit", new Money(salesOrderDetail.PricePerUnit.Value));
                trace.Trace("11");
                helper.updateTarget(target, "isp_quantity", salesOrderDetail.Quantity);
                helper.updateTarget(target, "isp_unitprice", new Money(salesOrderDetail.PricePerUnit.Value));
                trace.Trace("12");
                helper.updateTarget(target, "isp_tax", new Money((salesOrderDetail.PricePerUnit.Value - salesOrderDetail.isp_VatExcludedAmount.Value) * salesOrderDetail.Quantity.Value));
                trace.Trace("13");
                helper.updateTarget(target, "isp_vatpercentage", TaxPercentage);
                trace.Trace("14");
                helper.updateTarget(target, "isp_nettprice", new Money(salesOrderDetail.PricePerUnit.Value));
            }
            if (salesOrderDetail.isp_Stock != null)
            {
                isp_stockcheck stock = (from s in orgContext.CreateQuery<isp_stockcheck>()
                                        where s.isp_stockcheckId.Value == salesOrderDetail.isp_Stock.Id
                                        select new isp_stockcheck
                                        {
                                            isp_stockcheckId = s.isp_stockcheckId,
                                            isp_PastelWarehouse = s.isp_PastelWarehouse
                                        }).FirstOrDefault();
                pastelWarehouse = stock.isp_PastelWarehouse;
            }
            if (pastelWarehouse != null)
            {
                helper.updateTarget(target, "isp_stockoriginalpastelwarehouse", pastelWarehouse);
            }
        }
    }
}
