﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.invoicedetail
{
    public class postcreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                Entity target = (Entity)context.InputParameters["Target"];

                Invoice invoice = (from i in orgContext.CreateQuery<Invoice>()
                                   where i.InvoiceId.Value == target.GetAttributeValue<EntityReference>("invoiceid").Id
                                   select new Invoice
                                   {
                                       InvoiceId = i.InvoiceId,
                                       isp_atp = i.isp_atp
                                   }).FirstOrDefault();

                var invoiceStockProduct = (from invd in orgContext.CreateQuery<InvoiceDetail>()
                                           join p in orgContext.CreateQuery<Product>()
                                           on invd.ProductId.Id equals p.ProductId.Value
                                           join sodl in orgContext.CreateQuery<SalesOrderDetail>()
                                           on p.ProductId.Value equals sodl.ProductId.Id
                                           join sc in orgContext.CreateQuery<isp_stockcheck>()
                                           on sodl.isp_Stock.Id equals sc.isp_stockcheckId.Value
                                           where sodl.SalesOrderId.Id == invoice.isp_atp.Id
                                           where invd.InvoiceDetailId.Value == context.PrimaryEntityId
                                           select new
                                           {
                                               salesorderdetailid = sodl.SalesOrderDetailId,
                                               quantity = sodl.Quantity,
                                               lineitemnumber = sodl.LineItemNumber,
                                               productId = p.ProductId,
                                               productnumber = p.ProductNumber,
                                               currentcost = p.CurrentCost,
                                               isp_vatcode = p.isp_vatcode,
                                               isp_producttype = p.isp_ProductType,
                                               InvoiceDetailId = invd.InvoiceDetailId,
                                               isp_stockcheckId = sc.isp_stockcheckId,
                                               isp_stocklevel = sc.isp_StockLevel
                                           }).ToList();
                InvoiceDetail invoiceDetail = (from id in orgContext.CreateQuery<InvoiceDetail>()
                                               where id.InvoiceDetailId.Value == context.PrimaryEntityId
                                               select new InvoiceDetail
                                               {
                                                   InvoiceDetailId = id.InvoiceDetailId,
                                                   LineItemNumber = id.LineItemNumber,
                                                   InvoiceId = id.InvoiceId,
                                                   isp_stockcheckid = id.isp_stockcheckid
                                               }).FirstOrDefault();
                if (invoiceStockProduct.Count > 0)
                {
                    invoiceDetail.LineItemNumber = 1;
                    invoiceDetail.isp_stockcheckid = new EntityReference(isp_stockcheck.EntityLogicalName, invoiceStockProduct[0].isp_stockcheckId.Value);
                }
                else
                {
                    invoiceDetail.LineItemNumber = 100;
                }

                orgContext.ClearChanges();
                orgContext.Attach(invoiceDetail);
                orgContext.UpdateObject(invoiceDetail);
                orgContext.SaveChanges();
            }
        }
    }
}
