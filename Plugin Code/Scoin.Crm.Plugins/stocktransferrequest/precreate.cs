﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlClient;

namespace Scoin.Crm.Plugins.stocktransferrequest
{
    public class precreate : IPlugin
    {

      public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            if (target.Attributes.ContainsKey("isp_atpnumber_ledit"))
            {
                if (target.Attributes["isp_atpnumber_ledit"] != null && !string.IsNullOrWhiteSpace(target.Attributes["isp_atpnumber_ledit"].ToString()))
                {
                    return;
                }
            }


            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);


            var oSettingsQuery = new QueryExpression("isp_systemsettings") 
            {
                Criteria = new FilterExpression(), 
                ColumnSet = new ColumnSet(new[] { "isp_name", "isp_value", "isp_systemsettingsid" })
                //,PageInfo = new PagingInfo { Count = 1, PageNumber = 1 } 
            };
            oSettingsQuery.Criteria.AddCondition("isp_name", ConditionOperator.Equal, "isp_stocktransferrequestname");
            int maxRefNum = 0;
            Guid gSystemSettingsId = Guid.Empty;
            var RetrieveResult = service.RetrieveMultiple(oSettingsQuery);
            if (RetrieveResult.Entities.Count > 0 )
            {                
                maxRefNum = Convert.ToInt32(RetrieveResult.Entities[0].GetAttributeValue<string>("isp_value")) + 1;
                gSystemSettingsId = RetrieveResult.Entities[0].GetAttributeValue<Guid>("isp_systemsettingsid");
            }
            //maxRefNum++;
            string newRefNumber = "STR-" + maxRefNum.ToString().PadLeft(5, '0');//+ "-" + SB.ToString();


            //used for the precreate
          if (!target.Attributes.ContainsKey("isp_stocktransferrequestname"))
              target.Attributes.Add("isp_stocktransferrequestname", newRefNumber);
            else
              target.Attributes["isp_stocktransferrequestname"] = newRefNumber;

          #region update the counter setting
          Entity oSetting = new Entity("isp_systemsettings");
          oSetting.Attributes.Add("isp_systemsettingsid", gSystemSettingsId);
          oSetting.Attributes.Add("isp_value", maxRefNum.ToString());
          service.Update(oSetting);
          #endregion

            //string AllowedChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
          //StringBuilder SB = new StringBuilder("");
          //Random rand = new Random();
          //for(int i = 0; i < 6; i++)
          //{
          //    SB.Append(AllowedChars.ElementAt(rand.Next(0,36)));
          //}
          //string newRefNumber = "STR-" + maxRefNum.ToString().PadLeft(5, '0') + "-" + SB.ToString();
        
        }
    }

}
