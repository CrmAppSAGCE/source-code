﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Scoin.Crm.Plugins
{
    internal class ProviderSettings
    {
        public ProviderSettings(string settings)
        {
            ParseSettings(settings);   
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public string Account { get; set; }


        private void ParseSettings(string settings)
        {
            if (string.IsNullOrWhiteSpace(settings))
                throw new InvalidOperationException("Settings string cannot be blank");

            var configElement = XDocument.Parse(settings).Element("accountdetails");
            if (configElement == null) return;

            Username = configElement.Element("username").Value;
            Password = configElement.Element("password").Value;
            Account = configElement.Element("account").Value;

        }
    }
}
