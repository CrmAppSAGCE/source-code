﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;
using Microsoft.Xrm.Sdk.Client;


namespace Scoin.Crm.Plugins.salesorderdetail
{
    public class Update : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];
            var UpdateTarget = (Entity)context.InputParameters["Target"];
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            EntityHelper helper = new EntityHelper(context, service);
            var updateTarget = (Entity)context.InputParameters["Target"];

            var PreImage = (context.PreEntityImages.Contains("salesorderdetail") ? (Entity)context.PreEntityImages["salesorderdetail"] : null);

            //  Entity postImageEntity = (context.PostEntityImages != null && context.PostEntityImages.Contains("salesorderdetail")) ? context.PostEntityImages["salesorderdetail"] : null;

            target = PreImage;

            SalesOrderDetail salesOrderDetail = (from so in orgContext.CreateQuery<SalesOrderDetail>()
                                                 where so.SalesOrderDetailId.Value == context.PrimaryEntityId
                                                 select new SalesOrderDetail
                                                 {
                                                     SalesOrderDetailId = so.SalesOrderDetailId,
                                                     isp_VatAmount = so.isp_VatAmount,
                                                     isp_VatTotalAmount = so.isp_VatTotalAmount,
                                                     isp_VatExcludedAmount = so.isp_VatExcludedAmount,
                                                     isp_VatExcludedTotalAmount = so.isp_VatExcludedTotalAmount,
                                                     ProductId = so.ProductId,
                                                 }).FirstOrDefault();

            if (target.Contains("quantity"))
            {
                helper.updateTarget(updateTarget, "isp_vattotalamount", new Money(target.GetAttributeValue<decimal>("quantity") * salesOrderDetail.isp_VatAmount.Value));
                helper.updateTarget(updateTarget, "isp_vatexcludedtotalamount", new Money(target.GetAttributeValue<decimal>("quantity") * salesOrderDetail.isp_VatExcludedAmount.Value));
            }

            if (target.GetAttributeValue<EntityReference>("isp_productswithstock") != null)
            {
                var query = new QueryExpression("salesorderdetail") { ColumnSet = new ColumnSet(new[] { "quantity" }) };
                query.Criteria.AddCondition("isp_productswithstock", ConditionOperator.Equal, target.GetAttributeValue<EntityReference>("isp_productswithstock").Id);
                var SalesOrderLink = query.AddLink("salesorder", "salesorderid", "salesorderid");
                SalesOrderLink.EntityAlias = "s";
                SalesOrderLink.LinkCriteria.AddCondition("statuscode", ConditionOperator.Equal, 863300000);

                var results = service.RetrieveMultiple(query);
                Decimal RunningTotal = 0;
                for (int i = 0; i < results.Entities.Count; i++)
                {
                    RunningTotal += results[i].GetAttributeValue<Decimal>("quantity");
                }
                target = (Entity)context.InputParameters["Target"];
                target.Attributes["isp_stockordered"] = RunningTotal;
            }
            else if (target.GetAttributeValue<EntityReference>("isp_productswithstock") != null)
            {
                var query = new QueryExpression("salesorderdetail") { ColumnSet = new ColumnSet(new[] { "quantity" }) };
                query.Criteria.AddCondition("isp_productswithstock", ConditionOperator.Equal, target.GetAttributeValue<EntityReference>("isp_productswithstock").Id);
                var SalesOrderLink = query.AddLink("salesorder", "salesorderid", "salesorderid");
                SalesOrderLink.EntityAlias = "s";
                SalesOrderLink.LinkCriteria.AddCondition("statuscode", ConditionOperator.Equal, 863300000);

                var results = service.RetrieveMultiple(query);
                Decimal RunningTotal = 0;
                for (int i = 0; i < results.Entities.Count; i++)
                {
                    RunningTotal += results[i].GetAttributeValue<Decimal>("quantity");
                }
                target = (Entity)context.InputParameters["Target"];
                target.Attributes["isp_stockordered"] = RunningTotal;

                //throw new Exception("RunningTotal = " + RunningTotal);                         
            }
        }
    }
}
