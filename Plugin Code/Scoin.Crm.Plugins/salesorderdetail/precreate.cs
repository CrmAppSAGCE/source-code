﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;
using Microsoft.Xrm.Sdk.Client;

namespace Scoin.Crm.Plugins.salesorderdetail
{
    public class precreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            EntityHelper helper = new EntityHelper(context, service);

            ////check to see if Parent ATP already has the same product already

            Guid ATPGuid = target.GetAttributeValue<EntityReference>("salesorderid").Id;

            SalesOrder salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                                     where so.SalesOrderId.Value == target.GetAttributeValue<EntityReference>("salesorderid").Id
                                     && so.StatusCode.Value == 1
                                     select new SalesOrder
                                     {
                                         SalesOrderId = so.SalesOrderId
                                     }).FirstOrDefault();

            List<SalesOrderDetail> salesOrderDetails = (from sod in orgContext.CreateQuery<SalesOrderDetail>()
                                                        where sod.SalesOrderId.Id == target.GetAttributeValue<EntityReference>("salesorderid").Id
                                                        select new SalesOrderDetail
                                                        {
                                                            SalesOrderDetailId = sod.SalesOrderDetailId,
                                                            ProductId = sod.ProductId,
                                                            isp_Stock = sod.isp_Stock,
                                                            isp_serviceitem = sod.isp_serviceitem
                                                        }).ToList();

            if (target.Contains("isp_stock") && target.GetAttributeValue<EntityReference>("isp_stock") != null)
            {
                helper.updateTarget(target,"lineitemnumber",  Convert.ToInt16(1));
            }

            if (target.Contains("isp_serviceitem") && target.GetAttributeValue<EntityReference>("isp_serviceitem") != null)
            {
                helper.updateTarget(target, "lineitemnumber", Convert.ToInt16(100));
            }
            //Checking whether the same stock/service item exists and if so throws an error
            foreach (SalesOrderDetail sod in salesOrderDetails)
            {
                if (target.Contains("isp_stock"))
                {
                    if (sod.isp_Stock != null)
                    {
                        if (target.GetAttributeValue<EntityReference>("isp_stock").Id == sod.isp_Stock.Id)
                        {
                            throw new InvalidPluginExecutionException("You cannot add two of the same product. Rather increase the Quantity of the current product");
                        }
                    }
                }
                if (target.Contains("isp_serviceitem"))
                {
                    if (sod.isp_serviceitem != null)
                    {
                        if (target.GetAttributeValue<EntityReference>("isp_serviceitem").Id == sod.isp_serviceitem.Id)
                        {
                            throw new InvalidPluginExecutionException("You cannot add two of the same product. ");
                        }
                    }
                }
            }
        }
    }
}

