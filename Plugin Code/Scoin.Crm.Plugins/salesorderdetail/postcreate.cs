﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace Scoin.Crm.Plugins.salesorderdetail
{
    public class postcreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                var target = (Entity)context.InputParameters["Target"];
                var salesorderitems = (from soi in orgContext.CreateQuery("salesorderdetail")
                                       join s in orgContext.CreateQuery("isp_stockcheck")
                                       on soi["isp_stock"] equals s["isp_stockcheckid"]
                                       where soi["salesorderid"] == target.GetAttributeValue<EntityReference>("salesorderid")
                                       && soi["isp_stock"] != null
                                       select new
                                       {
                                           salesorderdetailid = soi["salesorderdetailid"],
                                           warehouse = s["isp_pastelwarehouse"]
                                       }).ToList();
                if (salesorderitems.Count > 0)
                {
                    EntityReference validWarehouse = (EntityReference)salesorderitems[0].warehouse;
                    foreach (var so in salesorderitems)
                    {
                        if (((EntityReference)so.warehouse).Id != validWarehouse.Id)
                        {
                            throw new InvalidPluginExecutionException("You cannot add stock items from different warehouses. \nPlease check that the stock items come from the same warehouse and try again.\nIf one of the products is from Prebooked, please invoice seperately");
                        }
                    }
                }             
            }
        }
    }
}
