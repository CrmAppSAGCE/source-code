﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.salesorderdetail
{
    public class postupdate : IPlugin
    {
        // Create the invoice in Pastel
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                Entity target = (Entity)context.InputParameters["Target"];
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                EntityHelper helper = new EntityHelper(context, service);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                Entity preMessageImage = null;
                Entity postMessageImage = null;

                if (context.PreEntityImages.Contains("PreImage") && context.PreEntityImages["PreImage"] is Entity)
                {
                    preMessageImage = (Entity)context.PreEntityImages["PreImage"];
                }
                if (context.PostEntityImages.Contains("PostImage") && context.PostEntityImages["PostImage"] is Entity)
                {
                    postMessageImage = (Entity)context.PostEntityImages["PostImage"];
                }
                if (helper.HasValueChanged(preMessageImage, postMessageImage, "priceperunit"))
                {
                    SalesOrderDetail salesOrderDetail = (from so in orgContext.CreateQuery<SalesOrderDetail>()
                                                         where so.SalesOrderDetailId.Value == context.PrimaryEntityId
                                                         select new SalesOrderDetail
                                                          {
                                                              SalesOrderDetailId = so.SalesOrderDetailId,
                                                              isp_VatAmount = so.isp_VatAmount,
                                                              isp_VatTotalAmount = so.isp_VatTotalAmount,
                                                              isp_VatExcludedAmount = so.isp_VatExcludedAmount,
                                                              isp_VatExcludedTotalAmount = so.isp_VatExcludedTotalAmount,
                                                              ProductId = so.ProductId,
                                                          }).FirstOrDefault();

                    Product product = (from p in orgContext.CreateQuery<Product>()
                                       where p.ProductId.Value == salesOrderDetail.ProductId.Id
                                       select new Product
                                       {
                                           ProductId = p.ProductId,
                                           isp_vatcode = p.isp_vatcode
                                       }).FirstOrDefault();
                    decimal quantity = (Decimal)postMessageImage.Attributes["quantity"];
                    decimal pricePerUnit = ((Money)postMessageImage.Attributes["priceperunit"]).Value;
                    decimal extendedAmount = quantity * pricePerUnit;

                    trace.Trace("Extended Amount: " + extendedAmount.ToString());
                    trace.Trace("Price Per Unit: " + ((Money)postMessageImage.Attributes["priceperunit"]).Value.ToString());
                    trace.Trace("Quantity: " + ((Decimal)postMessageImage.Attributes["quantity"]).ToString());
                    if (!(bool)postMessageImage.Attributes["ispriceoverridden"])
                    {
                        trace.Trace("1");
                        if (product.isp_vatcode.Value == 1)
                        {
                            trace.Trace("1");

                            trace.Trace("Quantity: " + ((Decimal)postMessageImage.Attributes["quantity"]).ToString());

                            decimal vatExcl = pricePerUnit * 100 / 114;
                            trace.Trace("vatExcl: " + vatExcl.ToString());
                            trace.Trace("vattotal: " + (vatExcl * quantity).ToString());
                            decimal vatCurr = pricePerUnit - vatExcl;
                            trace.Trace("vatCurr: " + vatCurr.ToString());
                            trace.Trace("vatexltotal: " + (vatCurr * quantity).ToString());

                            salesOrderDetail.isp_VatAmount = new Money(vatCurr);
                            salesOrderDetail.isp_VatTotalAmount = new Money(vatCurr * quantity);
                            salesOrderDetail.isp_VatExcludedAmount = new Money(vatExcl);
                            salesOrderDetail.isp_VatExcludedTotalAmount = new Money(vatExcl * quantity);
                        }
                        if (product.isp_vatcode.Value == 2)
                        {
                            trace.Trace("2");
                            salesOrderDetail.isp_VatAmount = new Money(0);
                            salesOrderDetail.isp_VatTotalAmount = new Money(0);
                            salesOrderDetail.isp_VatExcludedAmount = new Money(pricePerUnit);
                            salesOrderDetail.isp_VatExcludedTotalAmount = new Money(pricePerUnit * quantity);
                        }
                        if (product.isp_vatcode.Value == 6)
                        {
                            trace.Trace("3");
                            salesOrderDetail.isp_VatAmount = new Money(pricePerUnit);
                            salesOrderDetail.isp_VatTotalAmount = new Money(pricePerUnit * quantity);
                            salesOrderDetail.isp_VatExcludedAmount = new Money(0);
                            salesOrderDetail.isp_VatExcludedTotalAmount = new Money(0);
                        }
                        trace.Trace("4");
                        orgContext.UpdateObject(salesOrderDetail);
                        orgContext.SaveChanges();
                    }
                }
                else if (helper.HasValueChanged(preMessageImage, postMessageImage, "quantity"))
                {
                    SalesOrderDetail salesOrderDetail = (from so in orgContext.CreateQuery<SalesOrderDetail>()
                                                         where so.SalesOrderDetailId.Value == context.PrimaryEntityId
                                                         select new SalesOrderDetail
                                                         {
                                                             SalesOrderDetailId = so.SalesOrderDetailId,
                                                             isp_VatAmount = so.isp_VatAmount,
                                                             isp_VatTotalAmount = so.isp_VatTotalAmount,
                                                             isp_VatExcludedAmount = so.isp_VatExcludedAmount,
                                                             isp_VatExcludedTotalAmount = so.isp_VatExcludedTotalAmount,
                                                             ProductId = so.ProductId,
                                                         }).FirstOrDefault();

                    Product product = (from p in orgContext.CreateQuery<Product>()
                                       where p.ProductId.Value == salesOrderDetail.ProductId.Id
                                       select new Product
                                       {
                                           ProductId = p.ProductId,
                                           isp_vatcode = p.isp_vatcode
                                       }).FirstOrDefault();

                    decimal quantity = (Decimal)postMessageImage.Attributes["quantity"];
                    decimal pricePerUnit = ((Money)postMessageImage.Attributes["priceperunit"]).Value;
                    decimal vatCurr = ((Money)postMessageImage.Attributes["isp_vatamount"]).Value;
                    decimal vatExcl = ((Money)postMessageImage.Attributes["isp_vatexcludedamount"]).Value;
                    Entity salesOrderDetailUpdate = new Entity("salesorderdetail");
                    salesOrderDetailUpdate["salesorderdetailid"] = salesOrderDetail.SalesOrderDetailId.Value;

                    if (product.isp_vatcode.Value == 1)
                    {
                        salesOrderDetailUpdate["isp_vattotalamount"] = new Money(vatCurr * quantity);
                        salesOrderDetailUpdate["isp_vatexcludedtotalamount"] = new Money(vatExcl * quantity);
                    }
                    if (product.isp_vatcode.Value == 2)
                    {
                        salesOrderDetailUpdate["isp_vatexcludedtotalamount"] = new Money(pricePerUnit * quantity);
                    }
                    if (product.isp_vatcode.Value == 6)
                    {
                        salesOrderDetailUpdate["isp_vattotalamount"] = new Money(pricePerUnit * quantity);
                    }
                    trace.Trace("4");
                    service.Update(salesOrderDetailUpdate);
                }
            }
        }
    }
}
