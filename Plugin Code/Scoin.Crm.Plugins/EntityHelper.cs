﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

namespace Scoin.Crm.Plugins
{
    internal class EntityHelper
    {
        private Entity preimage;
        private readonly IPluginExecutionContext context;
        private readonly IOrganizationService service;

        public EntityHelper(IPluginExecutionContext context, IOrganizationService service)
        {
            this.context = context;
            this.service = service;
            Target = (Entity)context.InputParameters["Target"];

        }

        public EntityHelper(IOrganizationService service)
        {
            this.service = service;
            Target = (Entity)context.InputParameters["Target"];
        }

        public EntityHelper(IPluginExecutionContext context, Entity preImage)
        {
            preimage = preImage;
            this.context = context;
            Target = (Entity)context.InputParameters["Target"];
        }

        public EntityHelper()
        {

        }

        private Entity PreImage
        {
            get
            {
                if (preimage == null)
                {
                    if (context == null || service == null || context.MessageName.ToLower() == "create")
                        preimage = new Entity(Target.LogicalName) { Id = Target.Id };
                    else
                        preimage = service.Retrieve(context.PrimaryEntityName, context.PrimaryEntityId, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));
                }

                return preimage;
            }
        }

        public Entity Target { private get; set; }

        public T GetAttributeValue<T>(string attributeLogicalName) where T : new()
        {
            if (Target.Contains(attributeLogicalName))
                return Target.GetAttributeValue<T>(attributeLogicalName);

            if (PreImage.Contains(attributeLogicalName))
            {
                System.Runtime.Serialization.DataContractSerializer serializer = new System.Runtime.Serialization.DataContractSerializer(typeof(T));
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                serializer.WriteObject(ms, PreImage.GetAttributeValue<T>(attributeLogicalName));
                ms.Seek(0, System.IO.SeekOrigin.Begin);
                Target.Attributes.Add(attributeLogicalName, (T)serializer.ReadObject(ms));
            }
            else
                Target.Attributes.Add(attributeLogicalName, new T());

            return Target.GetAttributeValue<T>(attributeLogicalName);
        }

        public bool Contains(string attributeLogicalName)
        {
            if (Target.Contains(attributeLogicalName))
                return true;

            if (PreImage.Contains(attributeLogicalName))
                return true;

            return false;
        }

        public Guid Id
        {
            get
            {
                return Target.Id;
            }
            set
            {
                Target.Id = value;
            }
        }

        public override int GetHashCode()
        {
            return Target.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Entity)
                return ((Entity)obj).Id == Target.Id;
            return base.Equals(obj);
        }

        public object this[string attributeName]
        {
            get
            {
                if (Target.Contains(attributeName))
                    return Target[attributeName];

                if (PreImage.Contains(attributeName))
                    return PreImage[attributeName];

                return null;
            }
            set
            {
                if (Target.Contains(attributeName))
                    Target[attributeName] = value;
                else
                    Target.Attributes.Add(attributeName, value);
            }
        }

        public override string ToString()
        {
            return Target.ToString();
        }

        public static implicit operator EntityHelper(Entity target)
        {
            return new EntityHelper { Target = target };
        }

        public static implicit operator Entity(EntityHelper source)
        {
            return source.Target;
        }
        /// <summary>
        /// Returns a value indicating whether the value of an attribute has changed between two entity instances
        /// </summary>
        /// <param name="preEntity">The pre entity instance</param>
        /// <param name="postEntity">The post entity instance</param>
        /// <param name="attribute">The attribute to check</param>
        /// <returns>A value indicating whether the value of an attribute has changed between two entity instances</returns>
        public bool HasValueChanged(Entity preEntity, Entity postEntity, string attribute)
        {
            if (postEntity.Contains(attribute))
            {
                if (HasValue(postEntity, attribute))
                {
                    if (HasValue(preEntity, attribute))
                    {
                        if (postEntity[attribute].GetType() == typeof(EntityReference))
                        {
                            return ((EntityReference)preEntity[attribute]).Id != ((EntityReference)postEntity[attribute]).Id;
                        }
                        else if (postEntity[attribute].GetType() == typeof(Money))
                        {
                            return ((Money)preEntity[attribute]).Value != ((Money)postEntity[attribute]).Value;
                        }
                        else if (postEntity[attribute].GetType() == typeof(OptionSetValue))
                        {
                            return ((OptionSetValue)preEntity[attribute]).Value != ((OptionSetValue)postEntity[attribute]).Value;
                        }
                        else if (postEntity[attribute].GetType() == typeof(bool))
                        {
                            return ((bool)preEntity[attribute]) != ((bool)postEntity[attribute]);
                        }
                        else if (postEntity[attribute].GetType() == typeof(DateTime))
                        {
                            return !((DateTime)preEntity[attribute]).Equals(postEntity[attribute]);
                        }
                        else if (postEntity[attribute].GetType() == typeof(string))
                        {
                            return ((string)preEntity[attribute]) != ((string)postEntity[attribute]);
                        }
                        else if (postEntity[attribute].GetType() == typeof(int))
                        {
                            return ((int)preEntity[attribute]) != ((int)postEntity[attribute]);
                        }
                        else if (postEntity[attribute].GetType() == typeof(decimal))
                        {
                            return ((decimal)preEntity[attribute]) != ((decimal)postEntity[attribute]);
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return !HasValue(preEntity, attribute);
                }
            }

            return false;
        }

        /// <summary>
        /// Checks whether a property of an entity has a value
        /// </summary>
        /// <param name="entity">The entity</param>
        /// <param name="attribute">The attribute</param>
        /// <returns>Whether a property of an entity has a value</returns>
        public bool HasValue(Entity entity, string attribute)
        {
            if (entity.Attributes.Contains(attribute))
            {
                if (entity.Attributes[attribute] == null)
                {
                    return false;
                }
                else
                {
                    return !string.IsNullOrEmpty(entity.Attributes[attribute].ToString());
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the options set label from an optionset value
        /// </summary>
        /// <param name="service">The service being used</param>
        /// <param name="entity">The entity being used</param>
        /// <param name="attribute">The attribute</param>
        /// <param name="option">The int value of the optionset</param>
        /// <returns></returns>
        public string GetOptionSetValueLabel(IOrganizationService service, string entity, string attribute, OptionSetValue option)
        {
            string optionLabel = String.Empty;

            RetrieveAttributeRequest attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entity,
                LogicalName = attribute,
                RetrieveAsIfPublished = true
            };

            RetrieveAttributeResponse attributeResponse = (RetrieveAttributeResponse)service.Execute(attributeRequest);
            AttributeMetadata attrMetadata = (AttributeMetadata)attributeResponse.AttributeMetadata;
            PicklistAttributeMetadata picklistMetadata = (PicklistAttributeMetadata)attrMetadata;

            // For every status code value within all of our status codes values
            // (all of the values in the drop down list)
            foreach (OptionMetadata optionMeta in
            picklistMetadata.OptionSet.Options)
            {
                // Check to see if our current value matches
                if (optionMeta.Value == option.Value)
                {
                    // If our numeric value matches, set the string to our status code
                    // label
                    optionLabel = optionMeta.Label.UserLocalizedLabel.Label;
                }
            }

            return optionLabel;
        }

        public bool HasChanged(string attributeName)
        {
            if (context == null && service == null && preimage == null)
                throw new Exception("No Pre-Image available");

            if (context != null && context.MessageName.ToLower() == "create")
                return Target.Contains(attributeName);

            if (Target.Contains(attributeName))
            {
                if (!PreImage.Contains(attributeName) || (Target[attributeName] == null) != (PreImage[attributeName] == null))
                    return true;
                if (Target[attributeName] == null && PreImage[attributeName] == null)
                    return false;
                return !Target[attributeName].Equals(PreImage[attributeName]);
            }

            return false;
        }

        //update the target with values
        public void updateTarget(Entity target, string fieldName, object value)
        {
            if (target.Contains(fieldName))
            {
                target.Attributes[fieldName] = value;
            }
            else
            {
                target.Attributes.Add(fieldName, value);
            }
        }

    }
}
