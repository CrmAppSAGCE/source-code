﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
namespace Scoin.Crm.Plugins.opportunity
{
    public class update : IPlugin
    {
        
            public void Execute(IServiceProvider serviceProvider)
            {

                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
                var target = (Entity)context.InputParameters["Target"];

                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);


                EntityHelper helper = new EntityHelper(context, service);
                if (helper.HasChanged("statuscode"))
                {
                    if (target.GetAttributeValue<OptionSetValue>("statuscode").Value == 3)
                    {

                        var OppQuery = new QueryExpression("systemuser") { ColumnSet = new ColumnSet(true) };
                        OppQuery.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, context.UserId);
                        var OppDetails = service.RetrieveMultiple(OppQuery).Entities;

                        Entity NewATP = new Entity("salesorder");
                        var Customer = helper.GetAttributeValue<EntityReference>("customerid");
                        
                        
                        EntityReference pricelist = new EntityReference("pricelevel", new Guid("148DBD52-7908-E111-8B71-0800272BC48D"));
                        NewATP.Attributes.Add("isp_pastelwarehousecode", OppDetails[0].GetAttributeValue<EntityReference>("isp_pastelwarehouse"));
                        NewATP.Attributes.Add("pricelevelid", pricelist);
                        NewATP.Attributes.Add("isp_contactid", Customer);
                        NewATP.Attributes.Add("customerid", Customer);
                        var GUID = service.Create(NewATP);
                        target.Attributes["isp_atpcreated"] = GUID.ToString();

                        
            
                         
                        }
                    }


                }
            }
        
    }

