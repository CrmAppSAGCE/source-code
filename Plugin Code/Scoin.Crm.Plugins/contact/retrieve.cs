﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.contact
{
    public class retrieve : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
           // IOrganizationService service = (IOrganizationService)serviceProvider.GetService(typeof(IOrganizationService));
            //service.ServiceConfiguration.CurrentServiceEndpoint.Behaviors.Add(new ProxyTypesBehavior());

            if (!target.Attributes.ContainsKey("ownerid"))
                return;

            if (target.GetAttributeValue<EntityReference>("ownerid").Id != context.UserId)
            {
                ActivityParty toAccessManagerParty = new ActivityParty
                {
                    PartyId = new EntityReference(SystemUser.EntityLogicalName, GetManager(context.UserId, service))
                };

                ActivityParty toOwningManagerParty = new ActivityParty
                {
                    PartyId = new EntityReference(SystemUser.EntityLogicalName, GetManager(target.GetAttributeValue<EntityReference>("ownerid").Id, service))
                };

                ActivityParty fromParty = new ActivityParty
                {
                    PartyId = new EntityReference(SystemUser.EntityLogicalName, new Guid("98F841D3-6A07-E111-838C-0800272BC48D"))
                };

                Email email = new Email
                {
                    //To = new ActivityParty[] { toAccessManagerParty, toOwningManagerParty },
                    From = new ActivityParty[] { fromParty },
                    DirectionCode = true
                };

                if (toAccessManagerParty.PartyId.Id == Guid.Empty && toOwningManagerParty.PartyId.Id == Guid.Empty)
                    return;

                var to = new List<ActivityParty>();

                if (toAccessManagerParty.PartyId.Id != Guid.Empty)
                    to.Add(toAccessManagerParty);

                if (toOwningManagerParty.PartyId.Id != Guid.Empty)
                    to.Add(toOwningManagerParty);

                email.To = to;

                SendEmailFromTemplateRequest emailUsingTemplateReq = new SendEmailFromTemplateRequest
                {
                    Target = email,

                    // Use a built-in Email Template of type "contact".
                    TemplateId = new Guid("bebc36b8-5317-e111-954a-78acc08839c9"),

                    // The regarding Id is required, and must be of the same type as the Email Template.
                    RegardingId = target.Id,
                    RegardingType = Contact.EntityLogicalName
                };

                service.Execute(emailUsingTemplateReq);

               // SendEmailFromTemplateResponse emailUsingTemplateResp = (SendEmailFromTemplateResponse)service.Execute(emailUsingTemplateReq);
                /*
                if (emailUsingTemplateResp.Id == null)
                    throw new Exception("Error");   */
            }
        }

        private Guid GetManager(Guid userID, IOrganizationService service)
        {
            SystemUser retrieveResult = (SystemUser)service.Retrieve("systemuser", userID, new ColumnSet(new[] { "parentsystemuserid" }));
            if (retrieveResult.ParentSystemUserId == null)
                return Guid.Empty;
            return retrieveResult.ParentSystemUserId.Id;
        }

    }
}
