﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace Scoin.Crm.Plugins.contact
{
    public class precreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            //comment
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            EntityHelper helper = new EntityHelper(context, service);

            helper.updateTarget(target, "isp_ownerguid", target.GetAttributeValue<EntityReference>("ownerid").Id.ToString().Trim());
            helper.updateTarget(target, "isp_customerguid", context.PrimaryEntityId.ToString().Trim());

            //If the target's attributes change then update the string according to the totitle case
            if (target.Contains("firstname"))
            {
                if (!string.IsNullOrEmpty((string)target["firstname"]))
                {
                    string firstName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["firstname"]).ToLower());
                    target.Attributes["firstname"] = firstName;
                }
            }
            if (target.Contains("middlename"))
            {
                string middleName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["middlename"]).ToLower());
                target.Attributes["middlename"] = middleName;
            }
            if (target.Contains("lastname"))
            {
                if (!string.IsNullOrEmpty((string)target["lastname"]))
                {
                    string lastName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["lastname"]).ToLower());
                    target.Attributes["lastname"] = lastName;
                }
            }
            if (target.Contains("fullname"))
            {
                if (!string.IsNullOrEmpty((string)target["fullname"]))
                {
                    string fullName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["fullname"]).ToLower());
                    target.Attributes["fullname"] = fullName;
                }
            }
            if (target.Contains("isp_preferredname"))
            {
                if (!string.IsNullOrEmpty((string)target["isp_preferredname"]))
                {
                    string preferredName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["isp_preferredname"]).ToLower());
                    target.Attributes["isp_preferredname"] = preferredName;
                }
            }
            if (target.Contains("isp_companyname"))
            {
                if (!string.IsNullOrEmpty((string)target["isp_companyname"]))
                {
                    string companyName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["isp_companyname"]).ToLower());
                    target.Attributes["isp_companyname"] = companyName;
                }
            }
            if (target.Contains("address1_line1"))
            {
                if (!string.IsNullOrEmpty((string)target["address1_line1"]))
                {
                    string address1 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address1_line1"]).ToLower());
                    target.Attributes["address1_line1"] = address1;
                }
            }
            if (target.Contains("address1_line2"))
            {
                if (!string.IsNullOrEmpty((string)target["address1_line2"]))
                {
                    string address2 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address1_line2"]).ToLower());
                    target.Attributes["address1_line2"] = address2;
                }
            }
            if (target.Contains("address1_line3"))
            {
                if (!string.IsNullOrEmpty((string)target["address1_line3"]))
                {
                    string address3 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address1_line3"]).ToLower());
                    target.Attributes["address1_line3"] = address3;
                }
            }
            if (target.Contains("address1_city"))
            {
                if (!string.IsNullOrEmpty((string)target["address1_city"]))
                {
                    string city1 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address1_city"]).ToLower());
                    target.Attributes["address1_city"] = city1;
                }
            }
            if (target.Contains("address2_line1"))
            {
                if (!string.IsNullOrEmpty((string)target["address2_line1"]))
                {
                    string address4 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address2_line1"]).ToLower());
                    target.Attributes["address2_line1"] = address4;
                }
            }
            if (target.Contains("address2_line2"))
            {
                if (!string.IsNullOrEmpty((string)target["address2_line2"]))
                {
                    string address5 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address2_line2"]).ToLower());
                    target.Attributes["address2_line2"] = address5;
                }
            }
            if (target.Contains("address2_line3"))
            {
                if (!string.IsNullOrEmpty((string)target["address2_line3"]))
                {
                    string address6 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["fullname"]).ToLower());
                    target.Attributes["address2_line3"] = address6;
                }
            }
            if (target.Contains("address2_city"))
            {
                if (!string.IsNullOrEmpty((string)target["address2_city"]))
                {
                    string city2 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address2_city"]).ToLower());
                    target.Attributes["address2_city"] = city2;
                }
            }
            if (target.Contains("address2_country"))
            {
                if (!string.IsNullOrEmpty((string)target["address2_country"]))
                {
                    string country = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address2_country"]).ToLower());
                    target.Attributes["address2_country"] = country;
                }
            }
            if (target.Contains("isp_employer"))
            {
                if (!string.IsNullOrEmpty((string)target["emailaddress1"]))
                {
                    string employer = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["isp_employer"]).ToLower());
                    target.Attributes["isp_employer"] = employer;
                }
            }
            if (target.Contains("managername"))
            {
                if (!string.IsNullOrEmpty((string)target["managername"]))
                {
                    string managerName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["managername"]).ToLower());
                    target.Attributes["managername"] = managerName;
                }
            }
            if (target.Contains("jobtitle"))
            {
                if (!string.IsNullOrEmpty((string)target["jobtitle"]))
                {
                    string jobTitle = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["jobtitle"]).ToLower());
                    target.Attributes["jobtitle"] = jobTitle;
                }
            }
            if (target.Contains("department"))
            {
                if (!string.IsNullOrEmpty((string)target["department"]))
                {
                    string department = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["department"]).ToLower());
                    target.Attributes["department"] = department;
                }
            }
            if (target.Contains("assistantname"))
            {
                if (!string.IsNullOrEmpty((string)target["assistantname"]))
                {
                    string assistantName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["assistantname"]).ToLower());
                    target.Attributes["assistantname"] = assistantName;
                }
            }
             //if (!ValidTelephoneNumber(target.GetAttributeValue<string>("telephone1")) || !ValidTelephoneNumber(target.GetAttributeValue<string>("telephone2")) || !ValidTelephoneNumber(target.GetAttributeValue<string>("mobilephone")))
            //{
            // throw new Exception("Not a valid telephone number");
            //}
            //if(!TestEmailValidity(target.GetAttributeValue<string>("emailaddress1")))
            //{
            //  throw new Exception("Not a valid email address");
            //}
            //   if(target.Contains("isp_country") && target.GetAttributeValue<string>("isp_country").ToLower() != "other")
            //     target.Attributes["address1_country"] = target.GetAttributeValue<string>("isp_country");
        }
        bool TestEmailValidity(string EmailToTest)
        {
            //if (EmailToTest == "" || EmailToTest == null)
            //    return true;
            //Regex regexObj = new Regex(@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$");
            //if (regexObj.IsMatch(EmailToTest))
            //    return true;
            //else
            //    return false;

            return true;
        }
        bool ValidTelephoneNumber(string PhoneNumber)
        {
            if (PhoneNumber == "" || PhoneNumber == null)
                return true;
            Regex regexObj = new Regex(@"^.+\d$");
            if (regexObj.IsMatch(PhoneNumber))
                return true;
            else
                return false;
        }
    }
}
