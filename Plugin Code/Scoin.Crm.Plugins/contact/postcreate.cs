﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.contact
{
    public class postcreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            EntityHelper helper = new EntityHelper(context, service);
            var target = (Entity)context.InputParameters["Target"];
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            Entity updatedTarget = new Entity(target.LogicalName)
            {
                Id = target.Id
            };

            List<Account> accounts = null;
            if (target.Contains("isp_idnumber"))
            {
                accounts = (from a in orgContext.CreateQuery<Account>()
                            where a.isp_idnumber == target.GetAttributeValue<string>("isp_idnumber")
                            select new Account
                            {
                                AccountId = a.AccountId
                            }).ToList();
            }
            else if (target.Contains("emailaddress1") && !string.IsNullOrEmpty(target.GetAttributeValue<string>("emailaddress1")))
            {
                accounts = (from a in orgContext.CreateQuery<Account>()
                            where a.EMailAddress1 == target.GetAttributeValue<string>("emailaddress1")
                            select new Account
                            {
                                AccountId = a.AccountId
                            }).ToList();
            }

            else if (target.Contains("mobilephone") && !string.IsNullOrEmpty(target.GetAttributeValue<string>("mobilephone")))
            {
                accounts = (from a in orgContext.CreateQuery<Account>()
                            where a.isp_cellphonenumber == target.GetAttributeValue<string>("mobilephone")
                            select new Account
                            {
                                AccountId = a.AccountId
                            }).ToList();
            }
            else if (target.Contains("telephone1") && !string.IsNullOrEmpty(target.GetAttributeValue<string>("telephone1")))
            {
                accounts = (from a in orgContext.CreateQuery<Account>()
                            where a.isp_businessphonenumber == target.GetAttributeValue<string>("telephone1")
                            select new Account
                            {
                                AccountId = a.AccountId
                            }).ToList();
            }
            else if (target.Contains("telephone2") && !string.IsNullOrEmpty(target.GetAttributeValue<string>("telephone2")))
            {
                accounts = (from a in orgContext.CreateQuery<Account>()
                            where a.isp_businessphonenumber == target.GetAttributeValue<string>("telephone2")
                            select new Account
                            {
                                AccountId = a.AccountId
                            }).ToList();
            }
            //Shouldnt ever match more than one Account
            if (accounts.Count > 1)
            {
                trace.Trace("3");
                throw new InvalidPluginExecutionException("There cannot be more than one account linked. Please contact CRM for support");
            }
            //If it finds an account add the customer and update the Account's details
            else if (accounts.Count == 1)
            {
                helper.updateTarget(updatedTarget, "parentcustomerid", new EntityReference(Account.EntityLogicalName, accounts[0].AccountId.Value));
                Entity updateAccount = new Entity("account");
                updateAccount["accountid"] = accounts[0].AccountId.Value;
                if (target.Contains("isp_idnumber"))
                {
                    updateAccount["isp_idnumber"] = target.GetAttributeValue<string>("isp_idnumber");
                }
                if (target.Contains("emailaddress1"))
                {
                    updateAccount["emailaddress1"] = target.GetAttributeValue<string>("emailaddress1");
                }
                if (target.Contains("mobilephone"))
                {
                    updateAccount["isp_cellphonenumber"] = target.GetAttributeValue<string>("mobilephone");
                }
                if (target.Contains("telephone1"))
                {
                    updateAccount["isp_businessphonenumber"] = target.GetAttributeValue<string>("telephone1");
                }              
                if (target.Contains("fax"))
                {
                    updateAccount["fax"] = target.GetAttributeValue<string>("fax");
                }
                if (target.Contains("address1_line1"))
                {
                    updateAccount["address1_line1"] = target.GetAttributeValue<string>("address1_line1");
                }
                if (target.Contains("address1_line2"))
                {
                    updateAccount["address1_line2"] = target.GetAttributeValue<string>("address1_line2");
                }
                if (target.Contains("address1_line3"))
                {
                    updateAccount["address1_line3"] = target.GetAttributeValue<string>("address1_line3");
                }
                 if (target.Contains("address1_city"))
                {
                    updateAccount["address1_city"] = target.GetAttributeValue<string>("address1_city");
                }
                if (target.Contains("gendercode"))
                {
                    if (target.GetAttributeValue<OptionSetValue>("gendercode").Value == 1 || target.GetAttributeValue<OptionSetValue>("gendercode").Value == 2)
                    {
                        updateAccount["isp_gender"] = target.GetAttributeValue<OptionSetValue>("gendercode").Value == 1 ? true : false;
                    }
                }
                if (target.Contains("address1_postalcode"))
                {
                    updateAccount["address1_postalcode"] = target.GetAttributeValue<string>("address1_postalcode");
                }
                if (target.Contains("address1_country"))
                {
                    updateAccount["address1_country"] = target.GetAttributeValue<string>("address1_country");
                }
                if (target.Contains("isp_salutation"))
                {
                    updateAccount["isp_salutation"] = target.GetAttributeValue<OptionSetValue>("isp_salutation");
                }
                if (target.Contains("address1_county"))
                {
                    updateAccount["address1_stateorprovince"] = target.GetAttributeValue<string>("address1_county");
                }
                service.Update(updateAccount);
            }
            //If no accounts are found create a new Account
            else if (accounts.Count == 0)
            {
                Account newAccount = new Account();
                string name = string.Empty;
                if (target.Contains("firstname"))
                {
                    name += target.GetAttributeValue<string>("firstname");
                }
                if (target.Contains("lastname"))
                {
                    name += " " + target.GetAttributeValue<string>("lastname");
                }
                if (target.Contains("isp_companyname") && !string.IsNullOrEmpty(target.GetAttributeValue<string>("isp_companyname")))
                {
                    name = target.GetAttributeValue<string>("isp_companyname");
                }
                newAccount.Name = name;
                if (target.Contains("isp_idnumber"))
                {
                    newAccount.isp_idnumber = target.GetAttributeValue<string>("isp_idnumber");
                }
                if (target.Contains("emailaddress1"))
                {
                    newAccount.EMailAddress1 = target.GetAttributeValue<string>("emailaddress1");
                }
                if (target.Contains("mobilephone"))
                {
                    newAccount.isp_cellphonenumber = target.GetAttributeValue<string>("mobilephone");
                }
                if (target.Contains("telephone1"))
                {
                    newAccount.isp_businessphonenumber = target.GetAttributeValue<string>("telephone1");
                }
                 if (target.Contains("firstname"))
                {
                    newAccount.isp_firstname = target.GetAttributeValue<string>("firstname");
                }
                if (target.Contains("lastname"))
                {
                    newAccount.isp_lastname = target.GetAttributeValue<string>("lastname");
                }
                if (target.Contains("fax"))
                {
                    newAccount.Fax = target.GetAttributeValue<string>("fax");
                }
                if (target.Contains("address1_line1"))
                {
                    newAccount.Address1_Line1 = target.GetAttributeValue<string>("address1_line1");
                }
                if (target.Contains("address1_line2"))
                {
                    newAccount.Address1_Line2 = target.GetAttributeValue<string>("address1_line2");
                }
                if (target.Contains("address1_line3"))
                {
                    newAccount.Address1_Line3 = target.GetAttributeValue<string>("address1_line3");
                }
                if (target.Contains("address1_city"))
                {
                    newAccount.Address1_City = target.GetAttributeValue<string>("address1_city");
                }
                if (target.Contains("gendercode"))
                {
                    if (target.GetAttributeValue<OptionSetValue>("gendercode").Value == 1 || target.GetAttributeValue<OptionSetValue>("gendercode").Value == 2)
                    {
                        newAccount.isp_gender = target.GetAttributeValue<OptionSetValue>("gendercode").Value == 1 ? true : false;
                    }
                }
                if (target.Contains("address1_postalcode"))
                {
                    newAccount.Address1_PostalCode = target.GetAttributeValue<string>("address1_postalcode");
                }
                if (target.Contains("address1_country"))
                {
                    newAccount.Address1_Country = target.GetAttributeValue<string>("address1_country");
                }
                if (target.Contains("isp_salutation"))
                {
                    newAccount.isp_salutation = target.GetAttributeValue<OptionSetValue>("isp_salutation");
                }
                if (target.Contains("address1_county"))
                {
                    newAccount.Address1_StateOrProvince = target.GetAttributeValue<string>("address1_county");
                }
                Guid accountId = service.Create(newAccount);
                helper.updateTarget(updatedTarget, "parentcustomerid", new EntityReference(Account.EntityLogicalName, accountId));
            }
            service.Update(updatedTarget);
        }
    }
}
