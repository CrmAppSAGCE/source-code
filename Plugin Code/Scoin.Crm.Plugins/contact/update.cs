﻿using System;
using System.Threading;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
namespace Scoin.Crm.Plugins.contact
{
    public class update : IPlugin
    {

        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            if (context.Depth > 1)
            {
                return;
            }

            var target = (Entity)context.InputParameters["Target"];
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            bool update = false;
            EntityHelper helper = new EntityHelper(context, service);

            if (target.Contains("firstname"))
            {
                if (!string.IsNullOrEmpty((string)target["firstname"]))
                {
                    string firstName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["firstname"]).ToLower());
                    target.Attributes["firstname"] = firstName;
                    update = true;
                    trace.Trace("1");
                }
            }
            if (target.Contains("middlename"))
            {
                if (!string.IsNullOrEmpty((string)target["middlename"]))
                {
                    string middleName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["middlename"]).ToLower());
                    target.Attributes["middlename"] = middleName;
                    update = true;
                    trace.Trace("2");
                }
            }
            if (target.Contains("lastname"))
            {
                if (!string.IsNullOrEmpty((string)target["lastname"]))
                {
                    string lastName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["lastname"]).ToLower());
                    target.Attributes["lastname"] = lastName;
                    update = true;
                    trace.Trace("3");
                }
            }
            if (target.Contains("fullname"))
            {
                if (!string.IsNullOrEmpty((string)target["fullname"]))
                {
                    string fullName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["fullname"]).ToLower());
                    target.Attributes["fullname"] = fullName;
                    update = true;
                    trace.Trace("4");
                }
            }
            if (target.Contains("isp_preferredname"))
            {
                if (!string.IsNullOrEmpty((string)target["isp_preferredname"]))
                {
                    string preferredName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["fullname"]).ToLower());
                    target.Attributes["isp_preferredname"] = preferredName;
                    update = true;
                    trace.Trace("5");
                }
            }
            if (target.Contains("isp_companyname"))
            {
                if (!string.IsNullOrEmpty((string)target["isp_companyname"]))
                {
                    string companyName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["isp_companyname"]).ToLower());
                    target.Attributes["isp_companyname"] = companyName;
                    update = true;
                    trace.Trace("6");
                }
            }
            if (target.Contains("address1_line1"))
            {
                if (!string.IsNullOrEmpty((string)target["address1_line1"]))
                {
                    string address1 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address1_line1"]).ToLower());
                    target.Attributes["address1_line1"] = address1;
                    update = true;
                    trace.Trace("7");
                }
            }
            if (target.Contains("address1_line2"))
            {
                if (!string.IsNullOrEmpty((string)target["address1_line2"]))
                {
                    string address2 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address1_line2"]).ToLower());
                    target.Attributes["address1_line2"] = address2;
                    update = true;
                    trace.Trace("8");
                }
            }
            if (target.Contains("address1_line3"))
            {
                if (!string.IsNullOrEmpty((string)target["address1_line3"]))
                {
                    string address3 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address1_line3"]).ToLower());
                    target.Attributes["address1_line3"] = address3;
                    update = true;
                    trace.Trace("9");
                }
            }
            if (target.Contains("address1_city"))
            {
                if (!string.IsNullOrEmpty((string)target["address1_city"]))
                {
                    string city1 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address1_city"]).ToLower());
                    target.Attributes["address1_city"] = city1;
                    update = true;
                    trace.Trace("10");
                }
            }
            if (target.Contains("address2_line1"))
            {
                if (!string.IsNullOrEmpty((string)target["address2_line1"]))
                {
                    string address4 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address2_line1"]).ToLower());
                    target.Attributes["address2_line1"] = address4;
                    update = true;
                    trace.Trace("11");
                }
            }
            if (target.Contains("address2_line2"))
            {
                if (!string.IsNullOrEmpty((string)target["address2_line2"]))
                {
                    string address5 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address2_line2"]).ToLower());
                    target.Attributes["address2_line2"] = address5;
                    update = true;
                    trace.Trace("12");
                }
            }
            if (target.Contains("address2_line3"))
            {
                if (!string.IsNullOrEmpty((string)target["address2_line3"]))
                {
                    string address6 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address2_line3"]).ToLower());
                    target.Attributes["address2_line3"] = address6;
                    update = true;
                    trace.Trace("13");
                }
            }
            if (target.Contains("address2_city"))
            {
                if (!string.IsNullOrEmpty((string)target["address2_city"]))
                {
                    string city2 = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address2_city"]).ToLower());
                    target.Attributes["address2_city"] = city2;
                    update = true;
                    trace.Trace("14");
                }
            }
            if (target.Contains("address2_country"))
            {
                if (!string.IsNullOrEmpty((string)target["address2_country"]))
                {
                    string country = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["address2_country"]).ToLower());
                    target.Attributes["address2_country"] = country;
                    update = true;
                    trace.Trace("15");
                }
            }
            if (target.Contains("isp_employer"))
            {
                if (!string.IsNullOrEmpty((string)target["isp_employer"]))
                {
                    string employer = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["isp_employer"]).ToLower());
                    target.Attributes["isp_employer"] = employer;
                    update = true;
                    trace.Trace("17");
                }
            }
            if (target.Contains("managername"))
            {
                if (!string.IsNullOrEmpty((string)target["managername"]))
                {
                    string managerName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["managername"]).ToLower());
                    target.Attributes["managername"] = managerName;
                    update = true;
                    trace.Trace("18");
                }
            }
            if (target.Contains("jobtitle"))
            {
                if (!string.IsNullOrEmpty((string)target["jobtitle"]))
                {
                    string jobTitle = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["jobtitle"]).ToLower());
                    target.Attributes["jobtitle"] = jobTitle;
                    update = true;
                    trace.Trace("19");
                }
            }
            if (target.Contains("department"))
            {
                if (!string.IsNullOrEmpty((string)target["department"]))
                {
                    string department = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["department"]).ToLower());
                    target.Attributes["department"] = department;
                    update = true;
                    trace.Trace("20");
                }
            }
            if (target.Contains("assistantname"))
            {
                if (!string.IsNullOrEmpty((string)target["assistantname"]))
                {
                    string assistantName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(((string)target["assistantname"]).ToLower());
                    target.Attributes["assistantname"] = assistantName;
                    update = true;
                    trace.Trace("21");
                }
            }
            if (update)
            {
                trace.Trace("22");
                service.Update(target);
            }
        }
        //bool TestEmailValidity(string EmailToTest)
        //{
        //    //if (EmailToTest == "" || EmailToTest == null)
        //    //    return true;
        //    //Regex regexObj = new Regex(@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$");
        //    //if (regexObj.IsMatch(EmailToTest.ToLower()))
        //    //    return true;
        //    //else
        //    //    return false;
        //    return true;
        //}
        //bool ValidTelephoneNumber(string PhoneNumber)
        //{
        //    if (PhoneNumber == "" || PhoneNumber == null)
        //        return true;
        //    Regex regexObj = new Regex(@"^.+\d$");
        //    if (regexObj.IsMatch(PhoneNumber))
        //        return true;
        //    else
        //        return false;
        //}
    }
}
