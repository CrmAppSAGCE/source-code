﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;


namespace Scoin.Crm.Plugins.brokercommission
{
    public class postupdate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                Entity target = (Entity)context.InputParameters["Target"];
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                Entity preMessageImage = null;
                Entity postMessageImage = null;
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

                EntityHelper helper = new EntityHelper(context, service);

                if (context.PreEntityImages.Contains("PreImage") && context.PreEntityImages["PreImage"] is Entity)
                {
                    preMessageImage = (Entity)context.PreEntityImages["PreImage"];
                }
                if (context.PostEntityImages.Contains("PostImage") && context.PostEntityImages["PostImage"] is Entity)
                {
                    postMessageImage = (Entity)context.PostEntityImages["PostImage"];
                }

                isp_brokercommission brokerCommission = (from bc in orgContext.CreateQuery<isp_brokercommission>()
                                                         where bc.isp_brokercommissionId.Value == context.PrimaryEntityId
                                                         select new isp_brokercommission
                                                         {
                                                             isp_brokercommissionId = bc.isp_brokercommissionId,
                                                             isp_BullionValue = bc.isp_BullionValue,
                                                             isp_BullionCount = bc.isp_BullionCount,
                                                             isp_RareCommission = bc.isp_RareCommission,
                                                             isp_Tier = bc.isp_Tier,
                                                             isp_BullionCommission = bc.isp_BullionCommission,
                                                             isp_PurchaseNoteCommission = bc.isp_PurchaseNoteCommission,
                                                             isp_PurchaseNoteItemQuantity = bc.isp_PurchaseNoteItemQuantity,
                                                             isp_PurchaseNoteItemCommissionValue = bc.isp_PurchaseNoteItemCommissionValue,
                                                             isp_PurchaseNoteBullionCommissionValue = bc.isp_PurchaseNoteBullionCommissionValue,
                                                             isp_PurchaseNoteBullionQuantity = bc.isp_PurchaseNoteBullionQuantity,
                                                             isp_CommissionableAmount = bc.isp_CommissionableAmount,
                                                             OwnerId = bc.OwnerId
                                                         }).FirstOrDefault();

                decimal rareCommission = brokerCommission.isp_RareCommission.Value;
                trace.Trace("rare : " + brokerCommission.isp_RareCommission.Value.ToString());
                decimal bullionCommission = brokerCommission.isp_BullionCommission.Value;
                decimal purchaseNoteCommission = brokerCommission.isp_PurchaseNoteCommission.Value;
                decimal commissionEarned;
                bool update = false;
                //Rare Rule
                if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_commissionableamount"))
                {
                    decimal tier = brokerCommission.isp_Tier.Value;
                    List<isp_commissionrule> rareRule = (from br in orgContext.CreateQuery<isp_commissionrule>()
                                                         where br.isp_ProductType.Value == 1
                                                          && br.statecode == isp_commissionruleState.Active
                                                         select new isp_commissionrule
                                                         {
                                                             isp_commissionruleId = br.isp_commissionruleId,
                                                             isp_MinimumValue = br.isp_MinimumValue,
                                                             isp_MaximumValue = br.isp_MaximumValue,
                                                             isp_Tier = br.isp_Tier
                                                         }).ToList<isp_commissionrule>();

                    foreach (isp_commissionrule cr in rareRule)
                    {
                        if (cr.isp_MinimumValue != null && cr.isp_MaximumValue != null)
                        {
                            if (brokerCommission.isp_CommissionableAmount.Value >= cr.isp_MinimumValue.Value && brokerCommission.isp_CommissionableAmount.Value <= cr.isp_MaximumValue.Value)
                            {
                                tier = cr.isp_Tier.Value;
                            }
                        }
                        if (cr.isp_MinimumValue != null && cr.isp_MaximumValue == null)
                        {
                            if (brokerCommission.isp_CommissionableAmount.Value >= cr.isp_MinimumValue.Value)
                            {
                                tier = cr.isp_Tier.Value;
                            }
                        }
                    }
                    rareCommission = brokerCommission.isp_CommissionableAmount.Value * tier / 100;
                    update = true;
                    brokerCommission.isp_RareCommission = new Money(rareCommission);
                    brokerCommission.isp_Tier = tier;
                }
                //Bullion Rule
                if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_bullioncount"))
                {
                    isp_commissionrule bullionRule = (from br in orgContext.CreateQuery<isp_commissionrule>()
                                                      where br.isp_ProductType.Value == 2
                                                       && br.statecode == isp_commissionruleState.Active
                                                      select new isp_commissionrule
                                                      {
                                                          isp_commissionruleId = br.isp_commissionruleId,
                                                          isp_BullionCommissionValue = br.isp_BullionCommissionValue
                                                      }).FirstOrDefault();
                    bullionCommission = Convert.ToDecimal(brokerCommission.isp_BullionCount.Value) * bullionRule.isp_BullionCommissionValue.Value;
                    brokerCommission.isp_BullionCommission = new Money(bullionCommission);
                    update = true;
                }
                //Purchase Note Rule
                if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_purchasenoteitemquantity") || helper.HasValueChanged(preMessageImage, postMessageImage, "isp_purchasenotebullionquantity"))
                {
                    trace.Trace("1");
                    decimal otherCommissionPN = brokerCommission.isp_PurchaseNoteItemCommissionValue.Value;
                    trace.Trace("other : " + brokerCommission.isp_PurchaseNoteItemCommissionValue.Value.ToString());
                    decimal bullionCommissionPN = brokerCommission.isp_PurchaseNoteBullionCommissionValue.Value;
                    trace.Trace("bullion : " + brokerCommission.isp_PurchaseNoteBullionCommissionValue.Value.ToString());
                    isp_commissionrule pnRule = (from pnr in orgContext.CreateQuery<isp_commissionrule>()
                                                 where pnr.isp_ProductType.Value == 3
                                                 && pnr.statecode == isp_commissionruleState.Active
                                                 select new isp_commissionrule
                                                 {
                                                     isp_commissionruleId = pnr.isp_commissionruleId,
                                                     isp_BuyBackCommissionValue = pnr.isp_BuyBackCommissionValue,
                                                     isp_BuyBackBullionCommissionValue = pnr.isp_BuyBackBullionCommissionValue
                                                 }).FirstOrDefault();

                    trace.Trace("2");
                    if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_purchasenoteitemquantity"))
                    {
                        trace.Trace("3");
                        update = true;
                        trace.Trace("pnrule : " + pnRule.isp_BuyBackCommissionValue.Value.ToString());
                        trace.Trace("item quantity: " + brokerCommission.isp_PurchaseNoteItemQuantity.Value.ToString());
                        
                        otherCommissionPN = Convert.ToDecimal(brokerCommission.isp_PurchaseNoteItemQuantity.Value) * pnRule.isp_BuyBackCommissionValue.Value;
                        brokerCommission.isp_PurchaseNoteItemCommissionValue = new Money(otherCommissionPN);
                        trace.Trace("other quantity: " + otherCommissionPN.ToString());

                    }
                    if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_purchasenotebullionquantity"))
                    {
                        trace.Trace("4");
                        trace.Trace("pnrule : " + pnRule.isp_BuyBackBullionCommissionValue.Value.ToString());
                        trace.Trace("item quantity: " + brokerCommission.isp_PurchaseNoteBullionQuantity.Value.ToString());
                        update = true;
                        bullionCommissionPN = Convert.ToDecimal(brokerCommission.isp_PurchaseNoteBullionQuantity.Value) * pnRule.isp_BuyBackBullionCommissionValue.Value;
                        brokerCommission.isp_PurchaseNoteBullionCommissionValue = new Money(bullionCommissionPN);
                        trace.Trace("bullioncommissionPN = " + bullionCommissionPN.ToString());
                    }
                    trace.Trace("4.1");
                    purchaseNoteCommission = otherCommissionPN + bullionCommissionPN;
                    trace.Trace("other Commission :  " + otherCommissionPN.ToString());
                    trace.Trace("bullion Commission :  " + bullionCommissionPN.ToString());
                    trace.Trace("PN Commission :  " + purchaseNoteCommission.ToString());
                    trace.Trace("4.2");
                    brokerCommission.isp_PurchaseNoteCommission = new Money(purchaseNoteCommission);
                    trace.Trace("5");
                    trace.Trace("update : " + update.ToString());

                }
                if (update)
                {
                    trace.Trace("6");
                    commissionEarned = bullionCommission + rareCommission + purchaseNoteCommission;
                    brokerCommission.isp_CommissionEarned = new Money(commissionEarned);
                    trace.Trace("7");
                    orgContext.UpdateObject(brokerCommission);
                    trace.Trace("8");
                    orgContext.SaveChanges();
                  //  throw new InvalidPluginExecutionException("Stop");
                    trace.Trace("9");
                }
            }
        }
    }
}