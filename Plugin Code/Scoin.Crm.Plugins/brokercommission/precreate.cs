﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.brokercommission
{
    public class precreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            var target = (Entity)context.InputParameters["Target"];
            EntityHelper entityHelper = new EntityHelper();
            decimal tier = 0;
            decimal bullionCommisson = 0;
            decimal rareCommission = 0;
            decimal commissionEarned = 0;
            decimal bullionCommissionPN = 0;
            decimal otherCommissionPN = 0;
            decimal purchaseNoteCommission = 0;
            int bullionCount = 0;
            int otherCount = 0;

            //Rare Rule
            if (target.GetAttributeValue<Money>("isp_commissionableamount").Value != 0)
            {
                List<isp_commissionrule> rareRule = (from br in orgContext.CreateQuery<isp_commissionrule>()
                                                     where br.isp_ProductType.Value == 1
                                                     && br.statecode == isp_commissionruleState.Active
                                                     select new isp_commissionrule
                                                     {
                                                         isp_commissionruleId = br.isp_commissionruleId,
                                                         isp_MinimumValue = br.isp_MinimumValue,
                                                         isp_MaximumValue = br.isp_MaximumValue,
                                                         isp_Tier = br.isp_Tier
                                                     }).ToList<isp_commissionrule>();

                foreach (isp_commissionrule cr in rareRule)
                {
                    if (cr.isp_MinimumValue != null && cr.isp_MaximumValue != null)
                    {
                        if (target.GetAttributeValue<Money>("isp_commissionableamount").Value >= cr.isp_MinimumValue.Value && target.GetAttributeValue<Money>("isp_commissionableamount").Value <= cr.isp_MaximumValue.Value)
                        {
                            tier = cr.isp_Tier.Value;
                        }
                    }
                    if (cr.isp_MinimumValue != null && cr.isp_MaximumValue == null)
                    {
                        if (target.GetAttributeValue<Money>("isp_commissionableamount").Value >= cr.isp_MinimumValue.Value)
                        {
                            tier = cr.isp_Tier.Value;
                        }
                    }
                }
                rareCommission = target.GetAttributeValue<Money>("isp_commissionableamount").Value * tier / 100;
            }
            //Bullion Rule
            if (target.GetAttributeValue<int>("isp_bullioncount") != 0)
            {
                isp_commissionrule bullionRule = (from br in orgContext.CreateQuery<isp_commissionrule>()
                                                  where br.isp_ProductType.Value == 2
                                                  && br.statecode == isp_commissionruleState.Active
                                                  select new isp_commissionrule
                                                  {
                                                      isp_commissionruleId = br.isp_commissionruleId,
                                                      isp_BullionCommissionValue = br.isp_BullionCommissionValue
                                                  }).FirstOrDefault();
                bullionCommisson = Convert.ToDecimal(target.GetAttributeValue<int>("isp_bullioncount")) * bullionRule.isp_BullionCommissionValue.Value;

            }
            //Purchase Note Rule
            if (target.GetAttributeValue<int>("isp_purchasenoteitemquantity") != 0 || target.GetAttributeValue<int>("isp_purchasenotebullionquantity") != 0)
            {
                isp_commissionrule pnRule = (from pnr in orgContext.CreateQuery<isp_commissionrule>()
                                             where pnr.isp_ProductType.Value == 3
                                             && pnr.statecode == isp_commissionruleState.Active
                                             select new isp_commissionrule
                                             {
                                                 isp_commissionruleId = pnr.isp_commissionruleId,
                                                 isp_BuyBackCommissionValue = pnr.isp_BuyBackCommissionValue,
                                                 isp_BuyBackBullionCommissionValue = pnr.isp_BuyBackBullionCommissionValue
                                             }).FirstOrDefault();
                if (target.GetAttributeValue<int>("isp_purchasenoteitemquantity") != 0)
                {
                    otherCount = target.GetAttributeValue<int>("isp_purchasenoteitemquantity");
                }
                if (target.GetAttributeValue<int>("isp_purchasenotebullionquantity") != 0)
                {
                    bullionCount = target.GetAttributeValue<int>("isp_purchasenotebullionquantity");
                }
                otherCommissionPN = Convert.ToDecimal(otherCount) * pnRule.isp_BuyBackBullionCommissionValue.Value;
                bullionCommissionPN = Convert.ToDecimal(bullionCount) * pnRule.isp_BuyBackBullionCommissionValue.Value;
                purchaseNoteCommission = otherCommissionPN + bullionCommissionPN;
            }
            commissionEarned = bullionCommisson + rareCommission + purchaseNoteCommission;
            entityHelper.updateTarget(target, "isp_purchasenoteitemcommissionvalue", new Money(otherCommissionPN));
            entityHelper.updateTarget(target, "isp_purchasenotebullioncommissionvalue", new Money(bullionCommissionPN));
            entityHelper.updateTarget(target, "isp_bullioncommission", new Money(bullionCommisson));
            entityHelper.updateTarget(target, "isp_rarecommission", new Money(rareCommission));
            entityHelper.updateTarget(target, "isp_tier", tier);
            entityHelper.updateTarget(target, "isp_commissionearned", new Money(commissionEarned));
            entityHelper.updateTarget(target, "isp_purchasenotecommission", new Money(purchaseNoteCommission));
        }
    }
}
