﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.brokercommission
{
    public class preupdate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            var target = (Entity)context.InputParameters["Target"];
            EntityHelper entityHelper = new EntityHelper();

            if (context.Depth > 1)
            {
                return;
            }

            isp_brokercommission brokerCommission = (from bc in orgContext.CreateQuery<isp_brokercommission>()
                                                     where bc.isp_brokercommissionId.Value == context.PrimaryEntityId
                                                     select new isp_brokercommission
                                                     {
                                                         isp_brokercommissionId = bc.isp_brokercommissionId,
                                                         isp_BullionValue = bc.isp_BullionValue,
                                                         isp_BullionCount = bc.isp_BullionCount,
                                                         isp_RareCommission = bc.isp_RareCommission,
                                                         isp_Tier = bc.isp_Tier,
                                                         isp_BullionCommission = bc.isp_BullionCommission,
                                                         isp_PurchaseNoteCommission = bc.isp_PurchaseNoteCommission,
                                                         isp_PurchaseNoteItemCommissionValue = bc.isp_PurchaseNoteItemCommissionValue,
                                                         isp_PurchaseNoteBullionCommissionValue = bc.isp_PurchaseNoteBullionCommissionValue,
                                                         OwnerId = bc.OwnerId
                                                     }).FirstOrDefault();

            decimal tier = brokerCommission.isp_Tier.Value;
            decimal bullionCommisson = brokerCommission.isp_BullionCommission.Value;
            decimal rareCommission = brokerCommission.isp_RareCommission.Value;
            decimal commissionEarned;
            decimal otherCommissionPN = brokerCommission.isp_PurchaseNoteItemCommissionValue.Value;
            decimal bullionCommissionPN = brokerCommission.isp_PurchaseNoteBullionCommissionValue.Value;
            decimal purchaseNoteCommission = brokerCommission.isp_PurchaseNoteCommission.Value;

            //Rare Rule
            if (target.Contains("isp_commissionableamount"))
            {
                List<isp_commissionrule> rareRule = (from br in orgContext.CreateQuery<isp_commissionrule>()
                                                     where br.isp_ProductType.Value == 1
                                                      && br.statecode == isp_commissionruleState.Active
                                                     select new isp_commissionrule
                                                     {
                                                         isp_commissionruleId = br.isp_commissionruleId,
                                                         isp_MinimumValue = br.isp_MinimumValue,
                                                         isp_MaximumValue = br.isp_MaximumValue,
                                                         isp_Tier = br.isp_Tier
                                                     }).ToList<isp_commissionrule>();

                foreach (isp_commissionrule cr in rareRule)
                {
                    if (cr.isp_MinimumValue != null && cr.isp_MaximumValue != null)
                    {
                        if (target.GetAttributeValue<Money>("isp_commissionableamount").Value >= cr.isp_MinimumValue.Value && target.GetAttributeValue<Money>("isp_commissionableamount").Value <= cr.isp_MaximumValue.Value)
                        {
                            tier = cr.isp_Tier.Value;
                        }
                    }
                    if (cr.isp_MinimumValue != null && cr.isp_MaximumValue == null)
                    {
                        if (target.GetAttributeValue<Money>("isp_commissionableamount").Value >= cr.isp_MinimumValue.Value)
                        {
                            tier = cr.isp_Tier.Value;
                        }
                    }
                }
                rareCommission = target.GetAttributeValue<Money>("isp_commissionableamount").Value * tier / 100;
                entityHelper.updateTarget(target, "isp_rarecommission", new Money(rareCommission));
                entityHelper.updateTarget(target, "isp_tier", tier);
            }
            //Bullion Rule
            if (target.Contains("isp_bullioncount"))
            {
                isp_commissionrule bullionRule = (from br in orgContext.CreateQuery<isp_commissionrule>()
                                                  where br.isp_ProductType.Value == 2
                                                   && br.statecode == isp_commissionruleState.Active
                                                  select new isp_commissionrule
                                                  {
                                                      isp_commissionruleId = br.isp_commissionruleId,
                                                      isp_BullionCommissionValue = br.isp_BullionCommissionValue
                                                  }).FirstOrDefault();
                bullionCommisson = Convert.ToDecimal(target.GetAttributeValue<int>("isp_bullioncount")) * bullionRule.isp_BullionCommissionValue.Value;
                entityHelper.updateTarget(target, "isp_bullioncommission", new Money(bullionCommisson));

            }
            //Purchase Note Rule
            if (target.Contains("isp_purchasenoteitemquantity") || target.Contains("isp_purchasenotebullionquantity"))
            {
                isp_commissionrule pnRule = (from pnr in orgContext.CreateQuery<isp_commissionrule>()
                                             where pnr.isp_ProductType.Value == 3
                                             && pnr.statecode == isp_commissionruleState.Active
                                             select new isp_commissionrule
                                             {
                                                 isp_commissionruleId = pnr.isp_commissionruleId,
                                                 isp_BuyBackCommissionValue = pnr.isp_BuyBackCommissionValue,
                                                 isp_BuyBackBullionCommissionValue = pnr.isp_BuyBackBullionCommissionValue
                                             }).FirstOrDefault();
                if (target.Contains("isp_purchasenoteitemquantity") && target.Contains("isp_purchasenotebullionquantity"))
                {
                    otherCommissionPN = Convert.ToDecimal(target.GetAttributeValue<int>("isp_purchasenoteitemquantity")) * pnRule.isp_BuyBackCommissionValue.Value;
                    bullionCommissionPN = Convert.ToDecimal(target.GetAttributeValue<int>("isp_purchasenotebullionquantity")) * pnRule.isp_BuyBackBullionCommissionValue.Value;
                    entityHelper.updateTarget(target, "isp_purchasenoteitemcommissionvalue", new Money(otherCommissionPN));
                    entityHelper.updateTarget(target, "isp_purchasenotebullioncommissionvalue", new Money(bullionCommissionPN));
                }
                if (target.Contains("isp_purchasenoteitemquantity") && !target.Contains("isp_purchasenotebullionquantity"))
                {
                    otherCommissionPN = Convert.ToDecimal(target.GetAttributeValue<int>("isp_purchasenoteitemquantity")) * pnRule.isp_BuyBackCommissionValue.Value;
                    entityHelper.updateTarget(target, "isp_purchasenoteitemcommissionvalue", new Money(otherCommissionPN));
                }
                if (!target.Contains("isp_purchasenoteitemquantity") && target.Contains("isp_purchasenotebullionquantity"))
                {
                    bullionCommissionPN = Convert.ToDecimal(target.GetAttributeValue<int>("isp_purchasenotebullionquantity")) * pnRule.isp_BuyBackBullionCommissionValue.Value;
                    entityHelper.updateTarget(target, "isp_purchasenotebullioncommissionvalue", new Money(bullionCommissionPN));
                }
                purchaseNoteCommission = otherCommissionPN + bullionCommissionPN;
                entityHelper.updateTarget(target, "isp_purchasenotecommission", new Money(purchaseNoteCommission));
            }
            commissionEarned = bullionCommisson + rareCommission + purchaseNoteCommission;
            entityHelper.updateTarget(target, "isp_commissionearned", new Money(commissionEarned));
        }
    }
}
