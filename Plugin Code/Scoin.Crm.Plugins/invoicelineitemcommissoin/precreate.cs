﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Scoin.Crm.Plugins.invoicelineitemcommissoin
{
    struct CommissionPeriod
    {
        public DateTime StartDate;
        public DateTime EndDate;
    }

    public class precreate : IPlugin
    {
        private static Dictionary<int, string> map;

        public void Execute(IServiceProvider serviceProvider)
        {
            //if (map == null)
            //{
            //    map = new Dictionary<int, string>();
            //    map.Add(3, "isp_shopmonthlycommissionid");
            //    map.Add(2, "isp_regionalmonthlycommissionid");
            //    map.Add(1, "isp_channelmonthlycommissionid");
            //}

            //IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            //isp_InvoiceLineItemCommission target = (isp_InvoiceLineItemCommission)context.InputParameters["Target"];

            //IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            //IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            //var user = service.Retrieve("systemuser", context.UserId, new ColumnSet(true)).ToEntity<SystemUser>();

            //BusinessUnit bu = GetBusinessUnit(user, service);
            //isp_MonthlyCommission MonthlyComm = GetMonthlyCommEntry(user, null, service);
            ////Invoice inv = 

            //target.isp_brokermonthlycommissionid = GetMonthlyCommEntry(user, null, service).ToEntityReference();

            //var productInfo = GetProductInfo(Guid.Parse(target.isp_invoiceproductid), service);

            //if(productInfo == null)
            //    return;

            //Decimal split = 1;

            //if (target.isp_splitpercentage.HasValue)
            //    split = target.isp_splitpercentage.Value;

            //target.isp_commissionamount = GetCommission(bu, productInfo.GetAttributeValue<int>("isp_ProductType"), true, productInfo.GetAttributeValue<Money>("Price").Value, split, service);
            //MonthlyComm.isp_commissionontotalsales.Value += target.isp_commissionamount.Value;
            //MonthlyComm.isp_totalsalesamount.Value += productInfo.GetAttributeValue<Money>("Price").Value;

            //service.Update(MonthlyComm);

            //while (bu != null)
            //{
            //    if (bu.Contains("isp_businessunitlevel"))
            //    {
            //        var buLevel = bu.GetAttributeValue<int>("isp_businessunitlevel");
            //        if (map.ContainsKey(buLevel))
            //        {
            //            var field = map[buLevel];
            //            MonthlyComm = GetMonthlyCommEntry(null, bu, service);

            //            if (target.Contains(field))
            //                target.Attributes[field] = MonthlyComm.ToEntityReference();
            //            else
            //                target.Attributes.Add(field, MonthlyComm.ToEntityReference());

            //            MonthlyComm.isp_totalsalesamount.Value += productInfo.GetAttributeValue<Money>("Price").Value;
            //            service.Update(MonthlyComm);
            //        }

            //        if (buLevel <= 1)
            //            bu = null;
            //    }
                
            //    if (bu != null && bu.ParentBusinessUnitId != null)
            //        bu = (BusinessUnit)service.Retrieve("businessunit", bu.ParentBusinessUnitId.Id, new ColumnSet(true));
            //    else
            //        bu = null;
            //}
        }

        private isp_MonthlyCommission GetMonthlyCommEntry(SystemUser User, BusinessUnit BU, IOrganizationService Service)
        {
            var monthlyCommQuery = new QueryExpression("isp_monthlycommission") { Criteria = new FilterExpression(), ColumnSet = new ColumnSet(true) };
            if (User != null)
                monthlyCommQuery.Criteria.AddCondition("isp_userid", ConditionOperator.Equal, User.Id);
            else
            {
                monthlyCommQuery.Criteria.AddCondition("isp_businessunitlevel", ConditionOperator.Equal, BU.isp_businessunitlevel);
                monthlyCommQuery.Criteria.AddCondition("isp_businessunitid", ConditionOperator.Equal, BU.Id);
            }

            monthlyCommQuery.Criteria.AddCondition("isp_cyclestartdate", ConditionOperator.LessEqual, DateTime.Now);
            monthlyCommQuery.Criteria.AddCondition("isp_cycleenddate", ConditionOperator.GreaterEqual, DateTime.Now);

            var monthlyCommResult = Service.RetrieveMultiple(monthlyCommQuery);

            if (monthlyCommResult.Entities.Count == 0)
            {
                isp_MonthlyCommission comm = new isp_MonthlyCommission();

                if (User != null)
                    comm.isp_userid = User.ToEntityReference();
                else
                {
                    comm.isp_businessunitid = BU.ToEntityReference();
                    comm.isp_businessunitlevel = BU.GetAttributeValue<int>("isp_businessunitlevel");
                }

                CommissionPeriod newCommPeriod = CalculatePeriod(comm.isp_userid.Id.ToString(), Service);
                comm.isp_cyclestartdate = newCommPeriod.StartDate.Day;
                comm.isp_cycleenddate = newCommPeriod.EndDate.Day;

                Guid id = Service.Create(comm);

                comm.Id = id;

                return comm;
            }

            return monthlyCommResult.Entities[0].ToEntity<isp_MonthlyCommission>();
        }

        private BusinessUnit GetBusinessUnit(SystemUser User, IOrganizationService Service)
        {
            return (BusinessUnit)Service.Retrieve("businessunit", User.GetAttributeValue<Guid>("businessunitid"), new ColumnSet(true));
        }

        private CommissionPeriod CalculatePeriod(string UserID, IOrganizationService Service)
        {
            var result = Service.Retrieve("systemuser", Guid.Parse(UserID), new ColumnSet(new[] { "isp_cycleclosedate" }));
            int cycleCloseDay = Convert.ToInt32(result.Attributes["isp_cycleclosedate"]);

            CommissionPeriod CycleDates = new CommissionPeriod();

            if (cycleCloseDay >= DateTime.Now.Day)
            {
                CycleDates.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, cycleCloseDay);
                CycleDates.StartDate = CycleDates.EndDate.AddMonths(-1);
                CycleDates.StartDate = CycleDates.StartDate.AddDays(1);
            }
            else
            {
                CycleDates.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, cycleCloseDay + 1);
                CycleDates.EndDate = CycleDates.StartDate.AddMonths(1);
                CycleDates.EndDate = CycleDates.EndDate.AddDays(-1);
            }

            return CycleDates;
        }

        private Money GetTotalProductAmount(int ProductType, Invoice Invoice, IOrganizationService Service)
        {
            var totalAMountQuery = new QueryExpression("Product") { Criteria = new FilterExpression(), ColumnSet = new ColumnSet(true) };
            totalAMountQuery.AddLink("InvoiceDetail", "productid", "productid", JoinOperator.Inner);
            totalAMountQuery.Criteria.AddCondition("invoiceid", ConditionOperator.Equal, Invoice.Id);
            totalAMountQuery.Criteria.AddCondition("producttype", ConditionOperator.Equal, ProductType);

            var Products = Service.RetrieveMultiple(totalAMountQuery);

            Money totalAmount = new Money(0);

            for (int i = 0; i < Products.Entities.Count; i++)
            {
                if (Products.Entities[i].Attributes.ContainsKey("Price"))
                    totalAmount.Value += Products.Entities[i].GetAttributeValue<Money>("Price").Value;
            }

            return totalAmount;
        }

        //private Entity GetProductInfo(Guid InvoiceProductID, IOrganizationService Service)
        //{
        //    var productTypeQuery = new QueryExpression("product") { Criteria = new FilterExpression(), ColumnSet = new ColumnSet("isp_ProductType", "Price") };
        //    productTypeQuery.AddLink("invoicedetail", "productid", "ProductId", JoinOperator.Inner).LinkCriteria.AddCondition("invoicedetailid", ConditionOperator.Equal, InvoiceProductID);

        //    var result = Service.RetrieveMultiple(productTypeQuery);

        //    if (result.Entities.Count > 0 && result.Entities[0] != null)
        //        if (result.Entities[0].Contains("isp_ProductType") && result.Entities[0].Contains("Price"))
        //            return result.Entities[0];

        //    return null;
        //}

        private Money GetCommission(BusinessUnit BU, int ProductType, bool IsBuyBack, Decimal SaleAmount, Decimal CommModifier, IOrganizationService Service)
        {
            var commissionRuleQuery = new QueryExpression("isp_CommissionRule"){Criteria = new FilterExpression(), ColumnSet = new ColumnSet(true)};
            commissionRuleQuery.Criteria.AddCondition("isp_businessunitid", ConditionOperator.Equal, BU.ToEntityReference());
            commissionRuleQuery.Criteria.AddCondition("isp_producttype", ConditionOperator.Equal, ProductType);
            commissionRuleQuery.Criteria.AddCondition("isp_isbuyback", ConditionOperator.Equal, IsBuyBack);
            commissionRuleQuery.Criteria.AddCondition("isp_isaggregaterule", ConditionOperator.NotEqual, true);

            var result = Service.RetrieveMultiple(commissionRuleQuery);
            if (result.Entities.Count > 0)
            {
                isp_CommissionRule rule = (isp_CommissionRule)result.Entities[0];
                if (rule.isp_defaultcommissionpercentage.HasValue)
                    return new Money(rule.isp_defaultcommissionpercentage.Value * SaleAmount * CommModifier);

                return new Money (rule.isp_defaultcommissionamount.Value * CommModifier);
            }

            commissionRuleQuery.Criteria.Conditions.Clear();
            commissionRuleQuery.Criteria.AddCondition("isp_businessunitid", ConditionOperator.Equal, BU.ToEntityReference());
            commissionRuleQuery.Criteria.AddCondition("isp_producttype", ConditionOperator.Null);
            commissionRuleQuery.Criteria.AddCondition("isp_isbuyback", ConditionOperator.Equal, IsBuyBack);
            commissionRuleQuery.Criteria.AddCondition("isp_isaggregaterule", ConditionOperator.NotEqual, true);

            result = Service.RetrieveMultiple(commissionRuleQuery);
            if (result.Entities.Count > 0)
            {
                isp_CommissionRule rule = (isp_CommissionRule)result.Entities[0];
                if (rule.isp_defaultcommissionpercentage.HasValue)
                    return new Money(rule.isp_defaultcommissionpercentage.Value * SaleAmount * CommModifier);

                return new Money(rule.isp_defaultcommissionamount.Value * CommModifier);
            }

            commissionRuleQuery.Criteria.Conditions.Clear();
            commissionRuleQuery.Criteria.AddCondition("isp_businessunitid", ConditionOperator.Null);
            commissionRuleQuery.Criteria.AddCondition("isp_producttype", ConditionOperator.Equal, ProductType);
            commissionRuleQuery.Criteria.AddCondition("isp_isbuyback", ConditionOperator.Equal, IsBuyBack);
            commissionRuleQuery.Criteria.AddCondition("isp_isaggregaterule", ConditionOperator.NotEqual, true);

            result = Service.RetrieveMultiple(commissionRuleQuery);
            if (result.Entities.Count > 0)
            {
                isp_CommissionRule rule = (isp_CommissionRule)result.Entities[0];
                if (rule.isp_defaultcommissionpercentage.HasValue)
                    return new Money(rule.isp_defaultcommissionpercentage.Value * SaleAmount * CommModifier);

                return new Money(rule.isp_defaultcommissionamount.Value * CommModifier);
            }

            return new Money(0);
        }
    }
}
