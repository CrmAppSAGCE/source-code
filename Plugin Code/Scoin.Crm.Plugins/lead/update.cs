﻿using System;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.lead
{
    public class update : IPlugin
    {

        public void Execute(IServiceProvider serviceProvider)
        {

            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];
            var UpdateTarget = (Entity)context.InputParameters["Target"];
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            var PreImage = (context.PreEntityImages.Contains("lead") ? (Entity)context.PreEntityImages["lead"] : null);
            target = PreImage;
            EntityHelper helper = new EntityHelper(context, service);

            #region Creating the Customer
            EntityReference Customer = new EntityReference();
            if (helper.HasChanged("isp_createcustomer") || helper.GetAttributeValue<Boolean>("isp_createatp") || helper.GetAttributeValue<Boolean>("isp_createbuyback"))
            {
                Contact c = new Contact();
                c.FirstName = target.GetAttributeValue<string>("firstname");
                c.LastName = target.GetAttributeValue<string>("lastname");
                c.isp_CompanyName = target.GetAttributeValue<string>("isp_companyname");
                c.isp_salutation = target.GetAttributeValue<OptionSetValue>("isp_title");
                c.Address1_Line1 = target.GetAttributeValue<string>("address1_line1");
                c.Address1_Line2 = target.GetAttributeValue<string>("address1_line2");
                c.Address1_Line3 = target.GetAttributeValue<string>("address1_line3");
                c.Address1_PostalCode = target.GetAttributeValue<string>("address1_postalcode");
                c.Address1_City = target.GetAttributeValue<string>("address1_city");
                c.EMailAddress1 = target.GetAttributeValue<string>("emailaddress1");
                c.MobilePhone = target.GetAttributeValue<string>("mobilephone");
                c.Telephone1 = target.GetAttributeValue<string>("telephone1");
                c.Telephone2 = target.GetAttributeValue<string>("telephone2");
                c.isp_FromLead = new EntityReference(Lead.EntityLogicalName, target.Id);
                c.isp_scoinshopbroker = target.GetAttributeValue<EntityReference>("isp_scoinshopbroker");
                c.Address1_Country = target.GetAttributeValue<string>("address1_country");
                c.Address1_County = target.GetAttributeValue<string>("address1_stateorprovince");
                if (target.Contains("donotbulkemail"))
                {
                    c.DoNotBulkEMail = target.GetAttributeValue<bool>("donotbulkemail");
                }
                if (target.Contains("donotemail"))
                {
                    c.DoNotEMail = target.GetAttributeValue<bool>("donotemail");
                }
                if (target.Contains("isp_langpref"))
                {
                    switch (target.GetAttributeValue<OptionSetValue>("isp_langpref").Value)
                    {
                            //Afrik
                        case 863300000:
                            c.isp_homelanguage = new OptionSetValue(100000000);
                            break;
                            //Eng
                        case 863300001:
                            c.isp_homelanguage = new OptionSetValue(100000001);
                            break;
                            //isiNdebele
                        case 863300002:
                            c.isp_homelanguage = new OptionSetValue(100000002);
                            break;
                            //isiXhosa
                        case 863300003:
                            c.isp_homelanguage = new OptionSetValue(100000003);
                            break;
                            //isiZulu
                        case 863300004:
                            c.isp_homelanguage = new OptionSetValue(100000004);
                            break;
                            //Sesotho
                        case 863300005:
                            c.isp_homelanguage = new OptionSetValue(100000005);
                            break;
                           //Sesotho seLeboa
                        case 863300006:
                            c.isp_homelanguage = new OptionSetValue(100000006);
                            break;
                            //Sestwana
                        case 863300007:
                            c.isp_homelanguage = new OptionSetValue(100000007);
                            break;
                            //Seswati
                        case 863300008:
                            c.isp_homelanguage = new OptionSetValue(100000008);
                            break;
                            //ThiVenda
                        case 863300009:
                            c.isp_homelanguage = new OptionSetValue(100000009);
                            break;
                            //Xitsonga
                        case 863300010:
                            c.isp_homelanguage = new OptionSetValue(100000010);
                            break;
                    }
                }
                var Cust = service.Create(c);

                Customer = new EntityReference(Contact.EntityLogicalName, Cust);
                UpdateTarget.Attributes["isp_customerguid"] = Cust.ToString();

                var notesQuery = new QueryExpression("annotation") { ColumnSet = new ColumnSet(true) };
                notesQuery.Criteria.AddCondition("objectid", ConditionOperator.Equal, target.Id);

                var notes = service.RetrieveMultiple(notesQuery);
                for (int i = 0; i < notes.Entities.Count; i++)
                {
                    Annotation annotation = new Annotation
                    {
                        Subject = notes.Entities[i].GetAttributeValue<string>("subject")
                   ,
                        FileName = notes.Entities[i].GetAttributeValue<string>("filename")
                    ,
                        MimeType = notes.Entities[i].GetAttributeValue<string>("mimetype")
                    ,
                        IsDocument = notes.Entities[i].GetAttributeValue<bool>("isdocument")
                    ,
                        DocumentBody = notes.Entities[i].GetAttributeValue<string>("documentbody")
                    ,
                        NoteText = notes.Entities[i].GetAttributeValue<string>("notetext")
                    ,
                        ObjectId = new EntityReference("contact", Cust)
                    ,
                        ObjectTypeCode = "contact"
                    };

                    service.Create(annotation);
                }
                var activityQuery = new QueryExpression("activitypointer") { ColumnSet = new ColumnSet(true) };
                activityQuery.Criteria.AddCondition("regardingobjectid", ConditionOperator.Equal, target.Id);
                var activity = service.RetrieveMultiple(activityQuery);

                for (int i = 0; i < activity.Entities.Count; i++)
                {
                    ActivityPointer activitypointer = new ActivityPointer
                    {
                        OwnerId = activity.Entities[i].GetAttributeValue<EntityReference>("ownerid")
                        ,
                        ActualStart = activity.Entities[i].GetAttributeValue<DateTime>("actualstart")
                        ,
                        ActualEnd = activity.Entities[i].GetAttributeValue<DateTime>("actualend")
                        ,
                        Description = activity.Entities[i].GetAttributeValue<string>("description")
                        ,
                        RegardingObjectId = activity.Entities[i].GetAttributeValue<EntityReference>("regardingobjectid")
                        ,
                        Subject = activity.Entities[i].GetAttributeValue<string>("subject")
                    };
                }
            }
            #endregion

            #region Creating the ATP
            if (helper.GetAttributeValue<Boolean>("isp_createatp"))
            {
                var OppQuery = new QueryExpression("systemuser") { ColumnSet = new ColumnSet(true) };
                OppQuery.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, context.UserId);
                var OppDetails = service.RetrieveMultiple(OppQuery).Entities;

                Entity NewATP = new Entity("salesorder");
                EntityReference pricelist = new EntityReference(PriceLevel.EntityLogicalName, new Guid("148DBD52-7908-E111-8B71-0800272BC48D"));
                NewATP.Attributes.Add("isp_pastelwarehousecode", OppDetails[0].GetAttributeValue<EntityReference>("isp_pastelwarehouse"));
                NewATP.Attributes.Add("pricelevelid", pricelist);
                NewATP.Attributes.Add("isp_contactid", Customer);
                NewATP.Attributes.Add("customerid", Customer);
                NewATP.Attributes.Add("isp_fromlead", new EntityReference("lead", target.Id));
                var GUID = service.Create(NewATP);
                UpdateTarget.Attributes["isp_atpguid"] = GUID.ToString();
                //Set state to qulified and converted to Atp
                SetStateRequest setStatRequest = new SetStateRequest()
                {
                    EntityMoniker = new EntityReference
                    {
                        Id = target.Id,
                        LogicalName = Lead.EntityLogicalName,
                    },
                    State = new OptionSetValue(1),
                    Status = new OptionSetValue(863300004)
                };
                service.Execute(setStatRequest);
            }
            #endregion

            #region Create Buy Back
            if (helper.GetAttributeValue<Boolean>("isp_createbuyback"))
            {
                var OppQuery = new QueryExpression("systemuser") { ColumnSet = new ColumnSet(true) };
                OppQuery.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, context.UserId);
                var OppDetails = service.RetrieveMultiple(OppQuery).Entities;
                Entity NewBuyBack = new Entity("isp_buyback");

                // Set the customer
                NewBuyBack.Attributes.Add("isp_customer", Customer);
                NewBuyBack.Attributes.Add("transactioncurrencyid", new EntityReference("transactioncurrency", new Guid("FC83003D-6C07-E111-838C-0800272BC48D")));
                var GUID = service.Create(NewBuyBack);
                UpdateTarget.Attributes["isp_buybackguid"] = GUID.ToString();
                //Set state to qulified and converted to Buyback
                SetStateRequest setStatRequest = new SetStateRequest()
                {
                    EntityMoniker = new EntityReference
                    {
                        Id = target.Id,
                        LogicalName = Lead.EntityLogicalName,
                    },
                    State = new OptionSetValue(1),
                    Status = new OptionSetValue(863300007)
                };
                service.Execute(setStatRequest);
            }
            #endregion

            if (helper.HasChanged("isp_createcustomer") && !helper.GetAttributeValue<Boolean>("isp_createatp") && !helper.GetAttributeValue<Boolean>("isp_createbuyback"))
            {
                //Set state to qualified and converted to Customer
                SetStateRequest setStatRequest = new SetStateRequest()
                {
                    EntityMoniker = new EntityReference
                    {
                        Id = target.Id,
                        LogicalName = Lead.EntityLogicalName,
                    },
                    State = new OptionSetValue(1),
                    Status = new OptionSetValue(863300003)
                };
                service.Execute(setStatRequest);
            }
        }
    }
}

