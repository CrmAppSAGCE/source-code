﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.invoice
{
    public class PostSSRSEmail : IPlugin
    {

        public void Execute(IServiceProvider serviceProvider)
        {

            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            var target = (Entity)context.InputParameters["Target"];
             EntityHelper helper = new EntityHelper(context, service);
            if (context.ParentContext != null)
            {

                //Lock the pricing down for the invoices
             
                trace.Trace("Creating the query for the Order");
                var orderQuery = new QueryExpression("salesorder") { ColumnSet = new ColumnSet(true) };

                orderQuery.Criteria.AddCondition("salesorderid", ConditionOperator.Equal, helper.GetAttributeValue<EntityReference>("isp_atp").Id);
                var pastelWarehouseLink = orderQuery.AddLink("isp_pastelwarehouse", "isp_pastelwarehousecode", "isp_pastelwarehouseid");
                pastelWarehouseLink.EntityAlias = "pw";
                var pastelCompanyLink = pastelWarehouseLink.AddLink("isp_pastelcompany", "isp_pastelcompanyid", "isp_pastelcompanyid");
                pastelCompanyLink.EntityAlias = "pc";
                pastelCompanyLink.Columns.AddColumn("isp_serviceurl");
                var userLink = orderQuery.AddLink("systemuser", "ownerid", "systemuserid");
                userLink.EntityAlias = "o";
                userLink.Columns.AddColumn("isp_usertype");

                var order = service.RetrieveMultiple(orderQuery).Entities[0];
                trace.Trace("Retrieved the query");
                var isATP = (order.GetAttributeValue<OptionSetValue>("isp_ordertype").Value == 100000000);

                trace.Trace("retrieved the Order : ");

                if (order.Contains("pc.isp_serviceurl"))
                {
                    trace.Trace("query ran succesfully....");
                    //    var path = string.Format((string)order.GetAttributeValue<AliasedValue>("pc.isp_serviceurl").Value, "Generate.aspx");
                    var documentnumber = target.GetAttributeValue<string>("isp_pastelinvoicenumber");
                    var path = string.Format((string)order.GetAttributeValue<AliasedValue>("pc.isp_serviceurl").Value, "Generate.aspx");
                    var WebReq = HttpWebRequest.Create(string.Format("{0}?usertype={1}&DocumentNumber={2}", path, ((OptionSetValue)order.GetAttributeValue<AliasedValue>("o.isp_usertype").Value).Value, documentnumber));
                    WebReq.Method = "GET";
                    HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                    trace.Trace("retrieved the web response");
                    BinaryReader sr = new BinaryReader(WebResp.GetResponseStream());
                    MemoryStream ms = new MemoryStream();
                    var buffer = new byte[4096];

                    var length = sr.Read(buffer, 0, buffer.Length);
                    while (length > 0)
                    {
                        ms.Write(buffer, 0, length);
                        length = sr.Read(buffer, 0, buffer.Length);
                    }
                    Annotation annotation = new Annotation
                    {
                        Subject = (isATP) ? "Pastel Document" : "Purchase note"
                    ,
                        FileName = documentnumber + ".pdf"
                    ,
                        MimeType = "application/pdf"
                    ,
                        DocumentBody = Convert.ToBase64String(ms.GetBuffer())
                    ,
                        ObjectId = new EntityReference("invoice", target.Id)
                    ,
                        ObjectTypeCode = "invoice"
                    };

                    service.Create(annotation);
                    trace.Trace("Created the Annotation");
                    var customer = order.GetAttributeValue<EntityReference>("isp_contactid");
                    //bool DoNotEmail = order.GetAttributeValue<bool>("donotemail");
                    //if (DoNotEmail)
                    {
                        var templateReq = new Microsoft.Crm.Sdk.Messages.InstantiateTemplateRequest
                        {
                            TemplateId = (isATP) ? new Guid("BCCC0BD1-9A45-E111-957F-78ACC08839C9") : new Guid("112A703D-6C0C-E111-9675-78ACC08839C9")
                                                                                                    ,
                            ObjectId = target.Id
                                                                                                    ,
                            ObjectType = target.LogicalName
                        };
                        var templateResponse = (Microsoft.Crm.Sdk.Messages.InstantiateTemplateResponse)service.Execute(templateReq);

                        QueryExpression DefaultsQE = new QueryExpression("isp_default");
                        DefaultsQE.ColumnSet = new ColumnSet(true);
                        var Defaults = service.RetrieveMultiple(DefaultsQE).Entities[0];

                        #region "Activity Party"
                        Int16 ShopCount = 0, BrokerCount = 0;
                        String[] ShopEmails = new String[4];
                        if (Defaults.GetAttributeValue<EntityReference>("isp_shopemail1") != null)
                        {
                            ShopCount++;
                            ShopEmails[1] = Defaults.GetAttributeValue<EntityReference>("isp_shopemail1").Id.ToString();
                            //Shop1 = Defaults.GetAttributeValue<EntityReference>("isp_ShopEmail1").Id.ToString();
                        }
                        if (Defaults.GetAttributeValue<EntityReference>("isp_shopemail2") != null)
                        {
                            ShopCount++;
                            ShopEmails[2] = Defaults.GetAttributeValue<EntityReference>("isp_shopemail2").Id.ToString();
                            // Shop2 = Defaults.GetAttributeValue<EntityReference>("isp_ShopEmail2").Id.ToString();
                        }
                        if (Defaults.GetAttributeValue<EntityReference>("isp_shopemail3") != null)
                        {
                            ShopCount++;
                            ShopEmails[3] = Defaults.GetAttributeValue<EntityReference>("isp_shopemail3").Id.ToString();
                            // Shop3 = Defaults.GetAttributeValue<EntityReference>("isp_ShopEmail3").Id.ToString();
                        }
                        if (Defaults.GetAttributeValue<EntityReference>("isp_shopemail4") != null)
                        {
                            ShopCount++;
                            ShopEmails[4] = Defaults.GetAttributeValue<EntityReference>("isp_shopemail4").Id.ToString();
                            // Shop4 = Defaults.GetAttributeValue<EntityReference>("isp_ShopEmail4").Id.ToString();
                        }

                        trace.Trace("got the Shop Email Addresses");
                        String[] BrokerEmails = new String[4];

                        if (Defaults.GetAttributeValue<EntityReference>("isp_brokeremail1") != null)
                        {
                            BrokerCount++;
                            BrokerEmails[1] = Defaults.GetAttributeValue<EntityReference>("isp_brokeremail1").Id.ToString();
                            //Broker1 = Defaults.GetAttributeValue<EntityReference>("isp_brokeremail1").Id.ToString();
                        }
                        if (Defaults.GetAttributeValue<EntityReference>("isp_brokeremail2") != null)
                        {

                            BrokerCount++;
                            BrokerEmails[2] = Defaults.GetAttributeValue<EntityReference>("isp_brokeremail2").Id.ToString();
                            // Broker2 = Defaults.GetAttributeValue<EntityReference>("isp_brokeremail2").Id.ToString();
                        }
                        if (Defaults.GetAttributeValue<EntityReference>("isp_BrokerEmail3") != null)
                        {
                            BrokerCount++;
                            BrokerEmails[3] = Defaults.GetAttributeValue<EntityReference>("isp_brokeremail3").Id.ToString();
                            // Broker3 = Defaults.GetAttributeValue<EntityReference>("isp_BrokerEmail3").Id.ToString();
                        }
                        if (Defaults.GetAttributeValue<EntityReference>("isp_BrokerEmail4") != null)
                        {
                            BrokerCount++;
                            BrokerEmails[4] = Defaults.GetAttributeValue<EntityReference>("isp_brokeremail4").Id.ToString();
                            //Broker4 = Defaults.GetAttributeValue<EntityReference>("isp_BrokerEmail4").Id.ToString();
                        }
                        //  throw new Exception("CRM IS DOWN! PANIC!!!");

                        trace.Trace("Got the Brokers Email Addresses");

                        ActivityParty[] ToParty = new ActivityParty[4];
                        //If it is a Shop
                        if (((OptionSetValue)order.GetAttributeValue<AliasedValue>("o.isp_usertype").Value).Value == 2)
                        {
                            ToParty = new ActivityParty[ShopCount + 1];

                            ToParty[0] = new ActivityParty();
                            ToParty[0].PartyId = new EntityReference("systemuser", helper.GetAttributeValue<EntityReference>("ownerid").Id);

                            for (int i = 1; i < ShopCount + 1; i++)
                            {
                                ToParty[i] = new ActivityParty();
                                ToParty[i].PartyId = new EntityReference("systemuser", new Guid(ShopEmails[i]));
                            }

                            // ToParty[1] = new ActivityParty();//Tracey West
                            // ToParty[1].PartyId = new EntityReference("systemuser", new Guid("5FEE305D-E209-E111-A61C-78ACC08839C9"));
                        }//If it is a Broker
                        else if (((OptionSetValue)order.GetAttributeValue<AliasedValue>("o.isp_usertype").Value).Value == 1)
                        {
                            ToParty = new ActivityParty[BrokerCount + 1];
                            ToParty[0] = new ActivityParty();
                            ToParty[0].PartyId = new EntityReference("systemuser", helper.GetAttributeValue<EntityReference>("ownerid").Id);


                            for (int i = 1; i < BrokerCount + 1; i++)
                            {
                                ToParty[i] = new ActivityParty();
                                ToParty[i].PartyId = new EntityReference("systemuser", new Guid(BrokerEmails[i]));
                            }
                            /*
                            ToParty[1] = new ActivityParty();//Tracey West
                            ToParty[1].PartyId = new EntityReference("systemuser", new Guid("5FEE305D-E209-E111-A61C-78ACC08839C9"));

                            ToParty[2] = new ActivityParty();//Hayley Weber
                            ToParty[2].PartyId = new EntityReference("systemuser", new Guid("8718CEF8-E109-E111-A61C-78ACC08839C9"));


                            */
                        }
                        else
                        {
                            ToParty = new ActivityParty[2];

                            ToParty[0] = new ActivityParty();
                            ToParty[0].PartyId = new EntityReference("systemuser", helper.GetAttributeValue<EntityReference>("ownerid").Id);

                            ToParty[1] = new ActivityParty();//Tracey West
                            ToParty[1].PartyId = new EntityReference("systemuser", new Guid("5FEE305D-E209-E111-A61C-78ACC08839C9"));
                        }
                        #endregion

                        trace.Trace("Creating the email");

                        Email email = (Email)templateResponse.EntityCollection[0];
                        //Not to overwrite the template for sending the email through...
                        email.To = ToParty;
                        //email.Cc = new[] { new ActivityParty { PartyId = order. } };
                        email.From = new[] { new ActivityParty { PartyId = order.GetAttributeValue<EntityReference>("ownerid") } };
                        email.DirectionCode = true;
                        email.Subject = (isATP) ? "Invoice " + documentnumber + " for customer " + customer.Name : "Goods Received";
                        email.RegardingObjectId = target.ToEntityReference();
                        email.Id = service.Create(email);

                        trace.Trace("Starting with 2nd Report");
                        //Call Updated Report for the EMAIL...


                        var path2 = string.Format((string)order.GetAttributeValue<AliasedValue>("pc.isp_serviceurl").Value, "Generate.aspx");
                        //trace.Trace("Report2 path:" + path2);
                        //var WebReq2 = HttpWebRequest.Create(string.Format("{0}?usertype={1}&DocumentNumber={2}", path2, ((OptionSetValue)order.GetAttributeValue<AliasedValue>("o.isp_usertype").Value).Value, documentnumber));
                        //var WebReq2 = HttpWebRequest.Create(path2 + "?DocumentNumber=" + documentnumber.ToUpper());
                        trace.Trace("Specified WebReq2");
                        //WebClient client = new WebClient();

                        //byte[] myDataBuffer = client.DownloadData((new Uri(path2 + "?DocumentNumber=" + documentnumber.ToUpper())));

                        //MemoryStream memstream = new MemoryStream();
                        //memstream.SetLength(myDataBuffer.Length);
                        //memstream.Write(myDataBuffer, 0, (int)memstream.Length);
                        //memstream.Flush();

                        Annotation annotation2 = new Annotation
                        {
                            Subject = (isATP) ? "Pastel Document" : "Purchase note"
                        ,
                            FileName = "NewFormat-" + documentnumber + ".pdf"
                        ,
                            MimeType = "application/pdf"
                        ,
                            DocumentBody = Convert.ToBase64String(ms.GetBuffer())
                        ,
                            ObjectId = new EntityReference("invoice", target.Id)
                        ,
                            ObjectTypeCode = "invoice"
                        };
                        service.Create(annotation2);
                        ActivityMimeAttachment attachment = new ActivityMimeAttachment
                        {
                            ActivityId = email.ToEntityReference(),
                            FileName = string.Format("{0}.pdf", (isATP) ? "Invoice" : "PurchaseNote"),
                            Subject = email.Subject,
                            AttachmentNumber = 1,

                            Body = Convert.ToBase64String(ms.GetBuffer()),
                            //Body = Convert.ToBase64String(ms2.GetBuffer()),

                            MimeType = "application/pdf"
                        };



                        //var path2 = string.Format((string)order.GetAttributeValue<AliasedValue>("pc.isp_serviceurl").Value, "Generate.aspx");
                        //trace.Trace("Report2 path:" + path2);
                        //var WebReq2 = HttpWebRequest.Create(string.Format("{0}?usertype={1}&DocumentNumber={2}", path2, ((OptionSetValue)order.GetAttributeValue<AliasedValue>("o.isp_usertype").Value).Value, documentnumber));
                        //var WebReq2 = HttpWebRequest.Create(path2 + "?DocumentNumber=" + documentnumber.ToUpper());
                        trace.Trace("Specified WebReq2");


                        attachment.Id = service.Create(attachment);

                        annotation2.ObjectId = email.ToEntityReference();
                        annotation2.ObjectTypeCode = email.LogicalName;
                        service.Create(annotation2);



                        var sendMail = new Microsoft.Crm.Sdk.Messages.SendEmailRequest
                        {
                            IssueSend = true
                        ,
                            EmailId = email.Id
                        ,
                            TrackingToken = ((Microsoft.Crm.Sdk.Messages.GetTrackingTokenEmailResponse)service.Execute(new Microsoft.Crm.Sdk.Messages.GetTrackingTokenEmailRequest { Subject = email.Subject })).TrackingToken
                        };
                        service.Execute(sendMail);
                        trace.Trace("sent the mail");
                    }

                    //}
                }








            }
        }



    }

}
