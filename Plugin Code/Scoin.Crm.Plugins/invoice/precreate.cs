﻿using System;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;
using Scoin.Crm.Plugins.PastelService;

namespace Scoin.Crm.Plugins.invoice
{
    public class precreate : IPlugin
    {
        // Create the invoice in Pastel
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null && context.ParentContext.MessageName == "ConvertSalesOrderToInvoice")
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                var target = (Entity)context.InputParameters["Target"];
                decimal totalAmountPaid = 0;
                int? stocklevel = 0;

                var orderQuery = new QueryExpression("salesorder") { ColumnSet = new ColumnSet(true) };

                var invoiceProducts = (from sod in orgContext.CreateQuery<SalesOrderDetail>()
                                       join p in orgContext.CreateQuery<Product>()
                                       on sod.ProductId.Id equals p.ProductId.Value
                                       join sc in orgContext.CreateQuery<isp_stockcheck>()
                                       on sod.isp_Stock.Id equals sc.isp_stockcheckId.Value
                                       where sod.SalesOrderId.Id == target.GetAttributeValue<EntityReference>("isp_atp").Id
                                       select new
                                       {
                                           salesorderdetailid = sod.SalesOrderDetailId,
                                           quantity = sod.Quantity,
                                           lineitemnumber = sod.LineItemNumber,
                                           productId = p.ProductId,
                                           productnumber = p.ProductNumber,
                                           currentcost = p.CurrentCost,
                                           isp_vatcode = p.isp_vatcode,
                                           isp_producttype = p.isp_ProductType,
                                           isp_stockcheckId = sc.isp_stockcheckId,
                                           isp_stocklevel = sc.isp_StockLevel,
                                           isp_pastelwarehouse = sc.isp_PastelWarehouse
                                       }).ToList();
                

                //Retrieve the system user and pastel warehouse linked
                Decimal AmountPaid = 0;
                orderQuery.Criteria.AddCondition("salesorderid", ConditionOperator.Equal, target.GetAttributeValue<EntityReference>("salesorderid").Id);
                var pastelWarehouseLink = orderQuery.AddLink("isp_pastelwarehouse", "isp_pastelwarehousecode", "isp_pastelwarehouseid");
                pastelWarehouseLink.EntityAlias = "pw";
                pastelWarehouseLink.Columns.AddColumn("isp_pastelwarehousecode");
                var pastelCompanyLink = pastelWarehouseLink.AddLink("isp_pastelcompany", "isp_pastelcompanyid", "isp_pastelcompanyid");
                pastelCompanyLink.EntityAlias = "pc";
                pastelCompanyLink.Columns.AddColumn("isp_serviceurl");
                pastelCompanyLink.Columns.AddColumn("isp_name");
                pastelCompanyLink.Columns.AddColumn("isp_pastelcompanyid");
                var systemuserLink = orderQuery.AddLink("systemuser", "ownerid", "systemuserid");
                systemuserLink.EntityAlias = "su";
                systemuserLink.Columns.AddColumn("isp_salescode");
                systemuserLink.Columns.AddColumn("isp_pasteluser");
                systemuserLink.Columns.AddColumn("isp_customercode");

                var order = service.RetrieveMultiple(orderQuery).Entities[0];
                trace.Trace("Retrieve the Relevant details from CRM via Query");

                // Get the invoice line items
                QueryExpression qe = new QueryExpression("salesorderdetail");
                qe.ColumnSet = new ColumnSet(true);
                qe.Criteria.AddCondition("salesorderid", ConditionOperator.Equal, target.GetAttributeValue<EntityReference>("salesorderid").Id);
                var productLink = qe.AddLink("product", "productid", "productid");
                productLink.EntityAlias = "p";
                productLink.Columns.AddColumn("productnumber");
                productLink.Columns.AddColumn("currentcost");
                productLink.Columns.AddColumn("isp_vatcode");
                productLink.Columns.AddColumn("isp_producttype");
                trace.Trace("Error in Product Link");

                var pslLink = qe.AddLink("isp_stockcheck", "isp_stock", "isp_stockcheckid", JoinOperator.LeftOuter);
                pslLink.EntityAlias = "psl";
                pslLink.Columns.AddColumn("isp_stocklevel");
                pslLink.Columns.AddColumn("isp_stockcheckid");
                pslLink.Columns.AddColumn("isp_pastelwarehouse");
                pslLink.Columns.AddColumn("isp_vat");


                trace.Trace("Error with Stock Link");
                qe.AddOrder("lineitemnumber", OrderType.Ascending);

                var lineItems = service.RetrieveMultiple(qe).Entities;

                //Added this line now for the removing of ORDERED PRODUCTS line items
                var StoreValue = (string)order.GetAttributeValue<AliasedValue>("pw.isp_pastelwarehousecode").Value;
                trace.Trace(StoreValue.ToString());
                trace.Trace(lineItems.Count.ToString());

                Guid prevWarehouse = lineItems[0].GetAttributeValue<EntityReference>("isp_pastelwarehousecode").Id;
                trace.Trace("Before if");
                trace.Trace(lineItems[0].GetAttributeValue<EntityReference>("isp_pastelwarehousecode").Id.ToString());

                if (lineItems[0].GetAttributeValue<EntityReference>("isp_stock") == null && lineItems[0].GetAttributeValue<EntityReference>("isp_serviceitem") == null)
                {
                    throw new InvalidPluginExecutionException("The Order Product being used does not contain stock associated with it.\nPlease check your Order Product");
                }
                if ((lineItems[0].GetAttributeValue<EntityReference>("isp_stock") != null))
                {
                    trace.Trace("Entered if");
                    prevWarehouse = ((EntityReference)lineItems[0].GetAttributeValue<AliasedValue>("psl.isp_pastelwarehouse").Value).Id;
                    trace.Trace(prevWarehouse.ToString());
                }

                trace.Trace("preWarehouse");
                for (int i = 1; i < lineItems.Count; i++)
                {
                    trace.Trace("preWarehouse1");
                    if (lineItems[i].GetAttributeValue<EntityReference>("isp_stock") != null)
                    {
                        trace.Trace("preWarehouse2");
                        //Check to see if there are any Store 012 items
                        if (lineItems[i].GetAttributeValue<AliasedValue>("psl.isp_pastelwarehouse") != null)
                        {
                            trace.Trace("preWarehouse3");
                            if (((EntityReference)lineItems[i].GetAttributeValue<AliasedValue>("psl.isp_pastelwarehouse").Value).Id != prevWarehouse)//There is a discrepency between the products
                            {
                                throw new Exception("You cannot add stock items from different warehouses. \nPlease check that the stock items come from the same warehouse and try again.\nIf one of the products is from Prebooked, please invoice seperately");
                            }
                            trace.Trace("preWarehouse4");
                            prevWarehouse = ((EntityReference)lineItems[i].GetAttributeValue<AliasedValue>("psl.isp_pastelwarehouse").Value).Id;
                        }
                    }
                }

                trace.Trace("Getting the Delivery Method");
                string Delivery = string.Empty;
                if (order.GetAttributeValue<OptionSetValue>("shippingmethodcode").Value == 100000001)
                {
                    Delivery = "Collect";
                    if (target.Contains("shippingmethodcode"))
                    {
                        target["shippingmethodcode"] = new OptionSetValue(100000001);
                    }
                    else
                    {
                        target.Attributes.Add("shippingmethodcode", new OptionSetValue(100000001));
                    }

                }
                if (order.GetAttributeValue<OptionSetValue>("shippingmethodcode").Value == 100000000)
                {
                    Delivery = "Courier";
                    if (target.Contains("shippingmethodcode"))
                    {
                        target["shippingmethodcode"] = new OptionSetValue(100000000);
                    }
                    else
                    {
                        target.Attributes.Add("shippingmethodcode", new OptionSetValue(100000000));
                    }
                }
                if (order.GetAttributeValue<OptionSetValue>("shippingmethodcode").Value == 100000002)
                {
                    Delivery = "Safe Custody";
                    if (target.Contains("shippingmethodcode"))
                    {
                        target["shippingmethodcode"] = new OptionSetValue(100000002);
                    }
                    else
                    {
                        target.Attributes.Add("shippingmethodcode", new OptionSetValue(100000002));
                    }
                }
                trace.Trace("Delivery Method = " + Delivery);
                string SalesCode = (string)order.GetAttributeValue<AliasedValue>("su.isp_salescode").Value;

                //CheckToSeeIfIndependentBroker is selected
                if (order.GetAttributeValue<EntityReference>("isp_independentbroker") != null)
                {
                    var IU = service.Retrieve("isp_independentuser", order.GetAttributeValue<EntityReference>("isp_independentbroker").Id, new ColumnSet(true));
                    SalesCode = IU.GetAttributeValue<string>("isp_salescode");
                    trace.Trace("New Salescode : " + SalesCode);
                }
                string CustomerCode = "CASH";
                if (order.GetAttributeValue<AliasedValue>("su.isp_customercode") != null)
                    CustomerCode = (string)order.GetAttributeValue<AliasedValue>("su.isp_customercode").Value;
                trace.Trace("Getting the Customer Code");

                target.Attributes["isp_accountid"] = new EntityReference(Account.EntityLogicalName, order.GetAttributeValue<EntityReference>("isp_accountid").Id);

                QueryExpression DefaultsQE = new QueryExpression("isp_default");
                DefaultsQE.ColumnSet = new ColumnSet(true);
                var defaults = service.RetrieveMultiple(DefaultsQE).Entities[0];
                var prevWarehouseCode = service.Retrieve("isp_pastelwarehouse", prevWarehouse, new ColumnSet(true));
                trace.Trace("Check To see PW Warehouse");
                trace.Trace("Defaults Warehouse: ");
                trace.Trace(defaults.GetAttributeValue<string>("isp_pbpastelwarehouse"));
                trace.Trace("Other Warehouse");
                trace.Trace(prevWarehouseCode.GetAttributeValue<string>("isp_pastelwarehousecode"));

                bool preBooked = false;
                if (defaults.GetAttributeValue<string>("isp_pbpastelwarehouse") == prevWarehouseCode.GetAttributeValue<string>("isp_pastelwarehousecode"))
                {
                    trace.Trace("Pre Booked");
                    preBooked = true;
                }
                var invoice = new CreateDocumentRq
                {
                    DocumentType = EnumDocumentType.CustomerInvoice
                 ,
                    Header = new DocumentHeader
                    {
                        Date = DateTime.Today
                    ,
                        ClosingDate = DateTime.Today
                    ,
                        CustomerCode = preBooked ? defaults.GetAttributeValue<String>("isp_prebookedcustomercode").ToString() : CustomerCode
                    ,
                        IncExcl = false
                    ,
                        DeliveryAddress = new ArrayOfString()
                    ,
                        Address = new ArrayOfString()
                    ,
                        InvoiceMessage = new ArrayOfString()
                    ,
                        OnHold = false
                    ,
                        OrderNumber = order.GetAttributeValue<string>("isp_customerreference")
                    ,
                        Contact = target.Attributes.Contains("isp_contactid") ? target.GetAttributeValue<EntityReference>("isp_contactid").Name : ""
                    ,
                        SalesAnalysisCode = SalesCode
                    ,
                        ShipDeliver = Delivery
                    }
                                                    ,
                    Line = new DocumentLine[lineItems.Count]
                };
                trace.Trace("Setting Up the Invoice Object");

                //use the Company field here instead of the shipto_contactname
                String ContactName = "";
                String VatIdNumber = "";
                var customer = service.Retrieve("contact", order.GetAttributeValue<EntityReference>("isp_contactid").Id, new ColumnSet(true));
                if (order.Attributes.Contains("shipto_contactname") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_contactname")))
                    ContactName = order.GetAttributeValue<string>("shipto_contactname");
                if (order.Attributes.Contains("isp_companyname") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("isp_companyname")))
                    ContactName = order.GetAttributeValue<string>("isp_companyname");
                invoice.Header.DeliveryAddress.Add(ContactName);

                if (customer.Attributes.Contains("isp_idnumber") && !string.IsNullOrWhiteSpace(customer.GetAttributeValue<string>("isp_idnumber")))
                    VatIdNumber = customer.GetAttributeValue<string>("isp_idnumber");
                if (order.Attributes.Contains("isp_vatnumber") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("isp_vatnumber")))
                {
                    if (order.GetAttributeValue<string>("isp_vatnumber") != "")
                        VatIdNumber = order.GetAttributeValue<string>("isp_vatnumber");

                }
                invoice.Header.DeliveryAddress.Add(VatIdNumber);
                if (order.Attributes.Contains("shipto_line1") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_line1")))
                    invoice.Header.DeliveryAddress.Add(order.GetAttributeValue<string>("shipto_line1"));
                if (order.Attributes.Contains("shipto_line2") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_line2")))
                    invoice.Header.DeliveryAddress.Add(order.GetAttributeValue<string>("shipto_line2"));

                String CityCode = "";
                if (order.Attributes.Contains("shipto_city") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_city")))
                    CityCode += order.GetAttributeValue<string>("shipto_city");
                if (order.Attributes.Contains("shipto_postalcode") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_postalcode")))
                    CityCode += "-" + order.GetAttributeValue<string>("shipto_postalcode");

                invoice.Header.DeliveryAddress.Add(CityCode);

                if (order.Attributes.Contains("shipto_telephone") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_telephone")))
                    invoice.Header.Telephone = order.GetAttributeValue<string>("shipto_telephone");
                if (order.Attributes.Contains("shipto_fax") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_fax")))
                    invoice.Header.Fax = order.GetAttributeValue<string>("shipto_fax");

                if (order.Attributes.Contains("discountpercentage"))
                    invoice.Header.Discount = order.GetAttributeValue<decimal>("discountpercentage");
                Decimal RunningTotal = 0;

                trace.Trace("checking for Duplicate Line items added");

                //Doing a check to see if there are numerous lineitems with the same productid AND ALSO checking to ensure that there is only one item for Store 12
                for (int i = 0; i < lineItems.Count; i++)
                {
                    for (int j = 1; j < lineItems.Count; j++)
                    {
                        if (lineItems[i].GetAttributeValue<Guid>("salesorderdetailid") != lineItems[j].GetAttributeValue<Guid>("salesorderdetailid"))
                        {
                            if (lineItems[i].GetAttributeValue<EntityReference>("productid").Id == lineItems[j].GetAttributeValue<EntityReference>("productid").Id)
                            {
                                throw new Exception("You cannot have numerous line items on an ATP with the same Product. Remove one and Process the invoice again.");
                            }
                        }
                    }
                }
                Money TotalTotal = new Money();
                Money TaxTotal = new Money();

                short productType = 0;
                for (var i = 0; i < lineItems.Count; i++)
                {
                    productType = short.Parse((lineItems[i].GetAttributeValue<AliasedValue>("p.isp_vatcode").Value.ToString()));
                    if (productType == 2)
                    {
                        TaxTotal = new Money(TaxTotal.Value + new Money(new Decimal(0)).Value);
                    }
                    if (productType == 6 || productType == 0)
                    {
                        TaxTotal = new Money(TaxTotal.Value + lineItems[i].GetAttributeValue<Money>("priceperunit").Value * lineItems[i].GetAttributeValue<decimal>("quantity"));
                    }
                    if (productType == 1)
                    {
                        // this one forgot to multiply the priceperunit by the quantity
                        TaxTotal = new Money((TaxTotal.Value + ((lineItems[i].GetAttributeValue<Money>("priceperunit").Value * lineItems[i].GetAttributeValue<decimal>("quantity")) - ((lineItems[i].GetAttributeValue<Money>("priceperunit").Value / new Decimal(1.14))) * lineItems[i].GetAttributeValue<decimal>("quantity"))));
                    }
                    TotalTotal = new Money(TotalTotal.Value + (Decimal.Parse(lineItems[i].GetAttributeValue<Money>("priceperunit").Value.ToString()) * lineItems[i].GetAttributeValue<decimal>("quantity")));

                    invoice.Line[i] = new DocumentLine
                    {
                        Code = (string)lineItems[i].GetAttributeValue<AliasedValue>("p.productnumber").Value
                    ,
                        MultiStore = preBooked ? defaults.GetAttributeValue<string>("isp_pbpastelwarehouse") : (string)order.GetAttributeValue<AliasedValue>("pw.isp_pastelwarehousecode").Value
                    ,
                        CostPrice = lineItems[i].Attributes.Contains("p.currentcost") ? (decimal)lineItems[i].GetAttributeValue<AliasedValue>("p.currentcost").Value : 0
                    ,
                        Description = lineItems[i].GetAttributeValue<EntityReference>("productid").Name
                    ,
                        InclusivePrice = lineItems[i].Attributes.Contains("quantity") && lineItems[i].GetAttributeValue<decimal>("quantity") != 0 ? lineItems[i].GetAttributeValue<Money>("extendedamount").Value / lineItems[i].GetAttributeValue<decimal>("quantity") : 0
                    ,
                        Unit = ""
                    ,
                        Quantity = lineItems[i].GetAttributeValue<decimal>("quantity")
                    ,
                        UnitSellingPrice = lineItems[i].Attributes.Contains("priceperunit") ? lineItems[i].GetAttributeValue<Money>("priceperunit").Value : 0
                    ,
                        TaxType = productType
                    ,
                        LineType = EnumLineType.Inventory
                    };
                    RunningTotal += lineItems[i].Attributes.Contains("priceperunit") ? (lineItems[i].GetAttributeValue<decimal>("quantity") * Decimal.Parse(lineItems[i].GetAttributeValue<Money>("priceperunit").Value.ToString())) : 0;
                    trace.Trace("Appending Line Item");
                }
                if (order.Contains("isp_totalamountpaid"))
                {
                    totalAmountPaid = order.GetAttributeValue<Money>("isp_totalamountpaid").Value;
                }
                if (order.Attributes.Contains("isp_amountpaid") && order.Attributes["isp_amountpaid"] != null)
                {
                    AmountPaid = order.GetAttributeValue<Money>("isp_amountpaid").Value;
                    if (RunningTotal > AmountPaid + totalAmountPaid)
                    {
                        target.Attributes["isp_fullypaid"] = false;
                    }
                    else
                    {
                        target.Attributes["isp_fullypaid"] = true;
                    }
                }

                //Payment Details
                Entity Payment = new Entity("isp_payment");
                Payment.Attributes.Add("isp_amount", new Money(AmountPaid));
                Payment.Attributes.Add("isp_atp", target.GetAttributeValue<EntityReference>("salesorderid"));
                Payment.Attributes.Add("transactioncurrencyid", new EntityReference("transactioncurrency", new Guid("FC83003D-6C07-E111-838C-0800272BC48D")));
                Payment.Attributes.Add("isp_paymentdetails", order.GetAttributeValue<string>("ordernumber") + "-P1");
                Payment.Attributes.Add("isp_paymentreferencenumber", order.GetAttributeValue<string>("isp_paymentauthorisationnumber"));
                Payment.Attributes.Add("isp_paymentauthorisedby", new EntityReference("systemuser", context.UserId));
                Payment.Attributes.Add("isp_chequepayment", order.GetAttributeValue<bool>("isp_chequeuepayment"));
                Payment.Attributes.Add("isp_datepaymentreceived", order.GetAttributeValue<DateTime>("isp_datepaymentconfirmed"));
                if (order.Attributes.Contains("isp_paymentmethods"))
                    Payment.Attributes.Add("isp_paymentmethod", order.GetAttributeValue<OptionSetValue>("isp_paymentmethods"));

                service.Create(Payment);
                trace.Trace("Created the Payment Record");

                trace.Trace("Locking the ATP Down");
                if (!order.GetAttributeValue<Boolean>("ispricelocked"))
                {
                    LockSalesOrderPricingRequest req = new LockSalesOrderPricingRequest();
                    req.SalesOrderId = order.GetAttributeValue<Guid>("salesorderid");
                    LockSalesOrderPricingResponse resp = (LockSalesOrderPricingResponse)service.Execute(req);
                }
                if (order.Contains("isp_pastelswitch") && order.GetAttributeValue<Boolean>("isp_pastelswitch") != false)
                {
                    if (order.Contains("pc.isp_serviceurl"))
                    {
                        var binding = new System.ServiceModel.BasicHttpBinding(System.ServiceModel.BasicHttpSecurityMode.None) { CloseTimeout = new TimeSpan(0, 0, 25), OpenTimeout = new TimeSpan(0, 0, 25), SendTimeout = new TimeSpan(0, 0, 25), ReceiveTimeout = new TimeSpan(0, 0, 25) };
                        PastelServiceSoapClient client = new PastelServiceSoapClient(binding, new System.ServiceModel.EndpointAddress(string.Format((string)order.GetAttributeValue<AliasedValue>("pc.isp_serviceurl").Value, "PastelService.asmx")));
                        string PastelCompany = order.GetAttributeValue<AliasedValue>("pc.isp_name").Value.ToString();
                        trace.Trace("Middle of Invoice Created");
                        short PastelUser = 0;
                        if (order.GetAttributeValue<AliasedValue>("su.isp_pasteluser").Value != null)
                            PastelUser = Int16.Parse(order.GetAttributeValue<AliasedValue>("su.isp_pasteluser").Value.ToString());
                        if (preBooked)
                            PastelUser = Int16.Parse(defaults.GetAttributeValue<Int32>("isp_prebookeduserid").ToString());
                        trace.Trace("sending the invoice to pastel...");

                        // Invoices specific to EFT and Request approval status keep breaking before the below trace runs, this part has changed, although very minor, just doing this as a test.
                        trace.Trace("Extra Invoice Items");
                        if (target.Contains("isp_contactname"))
                            target.Attributes["isp_contactname"] = ContactName;
                        else
                            target.Attributes.Add("isp_contactname", ContactName);

                        if (target.Contains("isp_idnumber") && customer.Attributes.Contains("isp_idnumber") && !string.IsNullOrWhiteSpace(customer.GetAttributeValue<string>("isp_idnumber")))
                            target.Attributes["isp_idnumber"] = customer.GetAttributeValue<string>("isp_idnumber");
                        else
                            target.Attributes.Add("isp_idnumber", customer.GetAttributeValue<string>("isp_idnumber"));

                        trace.Trace("Contact and IDNumber");
                        if (target.Contains("isp_account"))
                            target.Attributes["isp_account"] = CustomerCode;
                        else
                            target.Attributes.Add("isp_account", CustomerCode);
                        if (target.Contains("isp_yourref"))
                            target.Attributes["isp_yourref"] = order.GetAttributeValue<string>("isp_customerreference");
                        else
                            target.Attributes.Add("isp_yourref", order.GetAttributeValue<string>("isp_customerreference"));

                        if (target.Contains("isp_taxref") && order.GetAttributeValue<string>("isp_vatnumber") != "")
                            target.Attributes["isp_taxref"] = order.GetAttributeValue<string>("isp_vatnumber");
                        else
                            target.Attributes.Add("isp_taxref", order.GetAttributeValue<string>("isp_vatnumber"));
                        trace.Trace("Account and YourRef and TexRef Created");
                        if (target.Contains("isp_salescode"))
                            target.Attributes["isp_salescode"] = SalesCode;
                        else
                            target.Attributes.Add("isp_salescode", SalesCode);
                        trace.Trace("SalesCode");
                        if (target.Contains("isp_deliverymethod"))
                            target.Attributes["isp_deliverymethod"] = Delivery;
                        else
                            target.Attributes.Add("isp_deliverymethod", Delivery);
                        trace.Trace("Deliverymethod");
                        if (target.Contains("isp_deliverytype") && order.Contains("isp_customerreference"))
                            target.Attributes["isp_deliverytype"] = order.GetAttributeValue<string>("isp_customerreference");
                        else
                            target.Attributes.Add("isp_deliverytype", order.GetAttributeValue<string>("isp_customerreference"));
                        trace.Trace("DeliveryType");
                        if (target.Contains("isp_deliverypoint") && order.Contains("isp_shoptocollectfrom") && order.GetAttributeValue<EntityReference>("isp_shoptocollectfrom") != null)              // commented out due to errors
                            target.Attributes["isp_deliverypoint"] = order.GetAttributeValue<EntityReference>("isp_shoptocollectfrom").Name;
                        else if (order.GetAttributeValue<EntityReference>("isp_shoptocollectfrom") != null)
                            target.Attributes.Add("isp_deliverypoint", order.GetAttributeValue<EntityReference>("isp_shoptocollectfrom").Name);
                        trace.Trace("Delivery Point");
                        if (target.Contains("isp_deliverydate") && order.Attributes.Contains("isp_expecteddeliverycollectiondate") && order.GetAttributeValue<DateTime>("isp_expecteddeliverycollectiondate") != null)
                            target.Attributes["isp_deliverydate"] = order.GetAttributeValue<DateTime>("isp_expecteddeliverycollectiondate").ToShortDateString();
                        else if (order.GetAttributeValue<DateTime>("isp_expecteddeliverycollectiondate") != null)
                            target.Attributes.Add("isp_deliverydate", order.GetAttributeValue<DateTime>("isp_expecteddeliverycollectiondate").ToShortDateString());
                        trace.Trace("Delivery Date");
                        if (target.Contains("isp_subtotal"))
                            target.Attributes["isp_subtotal"] = new Money(TotalTotal.Value - TaxTotal.Value);
                        else
                            target.Attributes.Add("isp_subtotal", new Money(TotalTotal.Value - TaxTotal.Value));
                        if (target.Contains("isp_amountexcltax"))
                            target.Attributes["isp_amountexcltax"] = new Money(TotalTotal.Value - TaxTotal.Value);
                        else
                            target.Attributes.Add("isp_amountexcltax", new Money(TotalTotal.Value - TaxTotal.Value));
                        if (target.Contains("isp_tax"))
                            target.Attributes["isp_tax"] = TaxTotal;
                        else
                            target.Attributes.Add("isp_tax", TaxTotal);
                        if (target.Contains("isp_total"))
                            target.Attributes["isp_total"] = TotalTotal;
                        else
                            target.Attributes.Add("isp_total", TotalTotal);
                        trace.Trace("Calculated Amounts");

                        trace.Trace("Pastel Query");
                        trace.Trace("Trying to get doc");
                        string invoiceOrderNumber = invoice.Header.OrderNumber;
                        if (invoice.Header.OrderNumber.Length > 15)
                        {
                            invoiceOrderNumber = invoice.Header.OrderNumber.Substring(0, 15);
                        }
                        var DocExists = client.GetRecord("acchisth", 11, invoiceOrderNumber, PastelCompany, PastelUser, 3);
                        trace.Trace("Got doc exists");
                        if (DocExists != null)
                        {
                            trace.Trace(DocExists.ToString());
                            if (DocExists.Substring(0, 1) == "0")
                            {
                                var ExistingDoc = DocExists.Substring(2);

                                for (int i = 0; i < invoiceProducts.Count; i++)
                                {
                                    //   Get all the stock check products linked to the invoice via the order product
                                    trace.Trace("looping through the invoice products");
                                    isp_stockcheck updateStock = (from s in orgContext.CreateQuery<isp_stockcheck>()
                                                                  join pw in orgContext.CreateQuery<isp_pastelwarehouse>()
                                                                  on s.isp_PastelWarehouse.Id equals pw.isp_pastelwarehouseId.Value
                                                                  where s.isp_stockcheckId.Value == invoiceProducts[i].isp_stockcheckId.Value
                                                                  select new isp_stockcheck
                                                                  {
                                                                      isp_stockcheckId = s.isp_stockcheckId,
                                                                      isp_StockLevel = s.isp_StockLevel
                                                                  }).FirstOrDefault();
                                    trace.Trace("Got the stock");
                                    if (updateStock.isp_StockLevel > 0)
                                    {
                                        //  Get all the stock items and for each calculate the new stock level by taking the old stock level and minus the quantity of the invoiced stock item
                                        trace.Trace("stock level greater than 0");
                                        updateStock.isp_StockLevel = updateStock.isp_StockLevel - Convert.ToInt32(invoiceProducts[i].quantity.Value);
                                        trace.Trace("Calculate new stock level");
                                        orgContext.UpdateObject(updateStock);
                                        orgContext.SaveChanges();
                                    }
                                }
                                // PASTEL SWITCH PORTION

                                if (target.Contains("isp_pastelinvoicenumber"))
                                    target.Attributes["isp_pastelinvoicenumber"] = ExistingDoc;
                                else
                                    target.Attributes.Add("isp_pastelinvoicenumber", ExistingDoc);

                                if (target.Contains("name"))
                                    target.Attributes["name"] = ExistingDoc;
                                else
                                    target.Attributes.Add("name", ExistingDoc);
                                return;
                            }
                        }
                        trace.Trace("Queried.");
                        for (int i = 0; i < invoiceProducts.Count; i++)
                        {
                            //Get all the stock check products linked to the invoice via the order product
                            trace.Trace("looping through the invoice products");
                            isp_stockcheck calculationStock = (from s in orgContext.CreateQuery<isp_stockcheck>()
                                                               join pw in orgContext.CreateQuery<isp_pastelwarehouse>()
                                                               on s.isp_PastelWarehouse.Id equals pw.isp_pastelwarehouseId.Value
                                                               where s.isp_stockcheckId.Value == invoiceProducts[i].isp_stockcheckId.Value
                                                               select new isp_stockcheck
                                                               {
                                                                   isp_stockcheckId = s.isp_stockcheckId,
                                                                   isp_StockLevel = s.isp_StockLevel
                                                               }).FirstOrDefault();

                            trace.Trace("Got the stock");
                            if (calculationStock.isp_StockLevel > 0)
                            {
                                //Get all the stock items and for each calculate the new stock level by taking the old stock level and minus the quantity of the invoiced stock item
                                trace.Trace("stock level greater than 0");
                                stocklevel = calculationStock.isp_StockLevel - Convert.ToInt32(invoiceProducts[i].quantity.Value);
                                trace.Trace("Calculate new stock level");
                                if (stocklevel < 0)
                                {
                                    throw new InvalidPluginExecutionException("You are trying to sell more stock than you have. \nPlease check your stock levels, update the amount you want to sell and then try again");
                                }
                            }
                        }
                        var rs = client.CreateDocument(invoice, PastelCompany, PastelUser);
                        trace.Trace("...sent the command off to Pastel sucessfully");
                        if (rs.Success)
                        {
                            for (int i = 0; i < invoiceProducts.Count; i++)
                            {
                                //  Get all the stock check products linked to the invoice via the order product
                                trace.Trace("looping through the invoice products");
                                isp_stockcheck updateStock = (from s in orgContext.CreateQuery<isp_stockcheck>()
                                                              join pw in orgContext.CreateQuery<isp_pastelwarehouse>()
                                                              on s.isp_PastelWarehouse.Id equals pw.isp_pastelwarehouseId.Value
                                                              where s.isp_stockcheckId.Value == invoiceProducts[i].isp_stockcheckId.Value
                                                              select new isp_stockcheck
                                                              {
                                                                  isp_stockcheckId = s.isp_stockcheckId,
                                                                  isp_StockLevel = s.isp_StockLevel
                                                              }).FirstOrDefault();
                                trace.Trace("Got the stock");
                                if (updateStock.isp_StockLevel > 0)
                                {
                                    //  Get all the stock items and for each calculate the new stock level by taking the old stock level and minus the quantity of the invoiced stock item
                                    trace.Trace("stock level greater than 0");
                                    updateStock.isp_StockLevel = updateStock.isp_StockLevel - Convert.ToInt32(invoiceProducts[i].quantity.Value);
                                    trace.Trace("Calculate new stock level");
                                    orgContext.UpdateObject(updateStock);
                                    orgContext.SaveChanges();
                                }
                            }
                            trace.Trace("Pastel Returned succesfully");
                            string DocumentNumber = rs.Output.Trim();
                            trace.Trace("Invoice Succesfully sent to Pastel");
                            if (target.Contains("isp_pastelinvoicenumber"))
                                target.Attributes["isp_pastelinvoicenumber"] = DocumentNumber;
                            else
                                target.Attributes.Add("isp_pastelinvoicenumber", DocumentNumber);

                            if (target.Contains("name"))
                                target.Attributes["name"] = DocumentNumber;
                            else
                                target.Attributes.Add("name", DocumentNumber);
                        }
                        else
                            throw new Exception("Error from Pastel: \r\n" + rs.ErrorMessage);
                    }
                }
            }
        }
    }
}
