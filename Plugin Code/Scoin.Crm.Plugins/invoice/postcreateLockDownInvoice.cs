﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;

namespace Scoin.Crm.Plugins.invoice
{
    public class postcreateLockDownInvoice : IPlugin
    {
         
        public void Execute(IServiceProvider serviceProvider)
        {

            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            var PreImage = (context.PreEntityImages.Contains("preimage") ? (Entity)context.PreEntityImages["preimage"] : null);
            var target = (Entity)context.InputParameters["Target"];

            if (context.ParentContext != null)
            {
                
                 //Lock the pricing down for the invoices

                LockInvoicePricingRequest lockInvoiceRequest =
                new LockInvoicePricingRequest()
                {
                    InvoiceId = target.Id
                };
                service.Execute(lockInvoiceRequest);



                //var SalesOrderGuid = PreImage.GetAttributeValue<EntityReference>("isp_atp");
                //var ATP = service.Retrieve("salesorder", SalesOrderGuid.Id, new ColumnSet(new[] { "ispricelocked" }));
                //if (!ATP.GetAttributeValue<bool>("ispricelocked"))
                //{
                //    Entity ATPtoUpdate = new Entity("salesorder");
                //    ATPtoUpdate.Attributes["ispricelocked"] = true;
                //}

            }
        }
      





    }

}
