﻿using System;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.invoice
{
    public class postcreate : IPlugin
    {
        // Get the invoice pdf from reporting services (pastel)
        public void Execute(IServiceProvider serviceProvider)
        {

            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null && context.ParentContext.MessageName == "ConvertSalesOrderToInvoice")
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                var target = (Entity)context.InputParameters["Target"];

                trace.Trace("Got invoice");
                SalesOrder salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                                         where so.SalesOrderId.Value == target.GetAttributeValue<EntityReference>("isp_atp").Id
                                         select new SalesOrder
                                         {
                                             SalesOrderId = so.SalesOrderId,
                                             isp_collectfrom = so.isp_collectfrom,
                                             isp_shoptocollectfrom = so.isp_shoptocollectfrom,
                                             isp_expecteddeliverycollectiondate = so.isp_expecteddeliverycollectiondate
                                         }).FirstOrDefault();
                if (salesOrder.isp_collectfrom != null)
                {
                        target.Attributes.Add("isp_collectfrom", new OptionSetValue(salesOrder.isp_collectfrom.Value));
                }
                if (salesOrder.isp_shoptocollectfrom != null)
                {
                        target.Attributes.Add("isp_shoptocollectfrom", new EntityReference(SystemUser.EntityLogicalName, salesOrder.isp_shoptocollectfrom.Id));
                }
                if (salesOrder.isp_expecteddeliverycollectiondate != null)
                {
                    target.Attributes.Add("isp_expecteddeliverycollection", salesOrder.isp_expecteddeliverycollectiondate.Value);
                }
                target.Attributes.Add("isp_invoiceguid", context.PrimaryEntityId.ToString().Trim().ToUpper());
                service.Update(target);
                trace.Trace("Got invoice products");

                var orderQuery = new QueryExpression("salesorder") { ColumnSet = new ColumnSet(true) };

                orderQuery.Criteria.AddCondition("salesorderid", ConditionOperator.Equal, target.GetAttributeValue<EntityReference>("salesorderid").Id);
                var pastelWarehouseLink = orderQuery.AddLink("isp_pastelwarehouse", "isp_pastelwarehousecode", "isp_pastelwarehouseid");
                pastelWarehouseLink.EntityAlias = "pw";
                var pastelCompanyLink = pastelWarehouseLink.AddLink("isp_pastelcompany", "isp_pastelcompanyid", "isp_pastelcompanyid");
                pastelCompanyLink.EntityAlias = "pc";
                pastelCompanyLink.Columns.AddColumn("isp_serviceurl");
                var userLink = orderQuery.AddLink("systemuser", "ownerid", "systemuserid");
                userLink.EntityAlias = "o";
                userLink.Columns.AddColumn("isp_usertype");

                var order = service.RetrieveMultiple(orderQuery).Entities[0];
                var isATP = (order.GetAttributeValue<OptionSetValue>("isp_ordertype").Value == 100000000);

                var updateOrder = new Entity("salesorder");
                updateOrder.Attributes.Add("salesorderid", order.Id);

                updateOrder.Attributes.Add("isp_invoice", new EntityReference("invoice", target.Id));
                if (order.GetAttributeValue<OptionSetValue>("statuscode").Value != 863300013)
                {
                    updateOrder.Attributes.Add("isp_totalamountpaid", order.GetAttributeValue<Money>("isp_amountpaid"));
                }
                updateOrder.Attributes.Add("isp_amountpaid", new Money(0));
                updateOrder.Attributes.Add("isp_paymentauthorisationnumber", "");
                updateOrder.Attributes.Add("isp_chequeuepayment", false);
                updateOrder.Attributes.Add("isp_paymentreceiptconfirmedby", null);
                updateOrder.Attributes.Add("isp_datepaymentconfirmed", null);

                if (order.GetAttributeValue<Boolean>("ispricelocked") == false)
                {
                    updateOrder.Attributes.Add("ispricelocked", true);

                }
                service.Update(updateOrder);

                var setInvoiced = new Microsoft.Crm.Sdk.Messages.SetStateRequest
                {
                    EntityMoniker = order.ToEntityReference()
                ,
                    State = new OptionSetValue { Value = 4 }
                ,
                    Status = new OptionSetValue { Value = 100003 }
                };
                service.Execute(setInvoiced);

                if (context.UserId != order.GetAttributeValue<EntityReference>("ownerid").Id)
                    service.Execute(new Microsoft.Crm.Sdk.Messages.AssignRequest { Target = new EntityReference("invoice", target.Id), Assignee = order.GetAttributeValue<EntityReference>("ownerid") });

                int shippingmethod = order.Contains("shippingmethodcode") ? order.GetAttributeValue<OptionSetValue>("shippingmethodcode").Value : 0;

                Microsoft.Crm.Sdk.Messages.SetStateRequest setInvoicePaid = new Microsoft.Crm.Sdk.Messages.SetStateRequest();
                if (shippingmethod == 100000000)//Courier
                    setInvoicePaid = new Microsoft.Crm.Sdk.Messages.SetStateRequest
                    {
                        EntityMoniker = new EntityReference("invoice", target.Id),
                        State = new OptionSetValue { Value = 0 },
                        Status = new OptionSetValue { Value = 863300009 }
                    };
                if (shippingmethod == 100000001)//To Collect
                    setInvoicePaid = new Microsoft.Crm.Sdk.Messages.SetStateRequest
                    {
                        EntityMoniker = new EntityReference("invoice", target.Id),
                        State = new OptionSetValue { Value = 0 },
                        Status = new OptionSetValue { Value = 863300006 }
                    };
                if (shippingmethod == 100000002)//Safe Custody
                    setInvoicePaid = new Microsoft.Crm.Sdk.Messages.SetStateRequest
                    {
                        EntityMoniker = new EntityReference("invoice", target.Id),
                        State = new OptionSetValue { Value = 0 },
                        Status = new OptionSetValue { Value = 863300007 }
                    };
                service.Execute(setInvoicePaid);

                if (shippingmethod == 100000002)        // Safe Custody
                {
                    var sc = new Entity("isp_safecustody");
                    sc.Attributes.Add("isp_atpnumber", new EntityReference("salesorder", order.Id));
                    sc.Attributes.Add("isp_relatedinvoice", new EntityReference("invoice", target.Id));
                    sc.Attributes.Add("isp_customer", order.GetAttributeValue<EntityReference>("isp_contactid"));
                    IOrganizationService scService = factory.CreateOrganizationService(order.GetAttributeValue<EntityReference>("ownerid").Id);
                    scService.Create(sc);
                }
            }
        }
    }
}
