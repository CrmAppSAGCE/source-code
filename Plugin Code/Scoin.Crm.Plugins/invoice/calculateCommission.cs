﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.invoice
{
    public class calculateCommission : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {

            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);

            decimal vat = 0;
            decimal commissonTotal = 0;
            decimal percentage = 0;

            SalesOrder salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                                     where so.SalesOrderId.Value == target.GetAttributeValue<EntityReference>("isp_atp").Id
                                     select so).FirstOrDefault();

            List<SalesOrderDetail> salesOrderDetails = (from sod in orgContext.CreateQuery<SalesOrderDetail>()
                                                        where sod.SalesOrderId.Id == target.GetAttributeValue<EntityReference>("isp_atp").Id
                                                        select new SalesOrderDetail
                                                        {
                                                            SalesOrderDetailId = sod.SalesOrderDetailId,
                                                            isp_vatcur = sod.isp_vatcur,
                                                            isp_vatexcl = sod.isp_vatexcl,
                                                            isp_vattotal = sod.isp_vattotal,
                                                          isp_vatexcltotal = sod.isp_vatexcltotal,                     
                                                        }).ToList();

            SystemUser systemUser = (from su in orgContext.CreateQuery<SystemUser>()
                                     where su.SystemUserId.Value == salesOrder.OwnerId.Id
                                     select new SystemUser
                                     {
                                         SystemUserId = su.SystemUserId,
                                         isp_usertype = su.isp_usertype
                                     }).FirstOrDefault();

            foreach (SalesOrderDetail sod in salesOrderDetails)
            {
                if (sod.isp_vattotal.Value > 0)
                {
                    vat += sod.isp_vattotal.Value;
                }
            }

            commissonTotal = salesOrder.TotalAmount.Value - vat;
            percentage = salesOrder.isp_TotalAmountPaid.Value / vat;

            if (salesOrder.StatusCode.Value == 863300009)
            {

            }
            else
            {

            }

           // salesOrder.isp_TotalCommissionReceivable = new Money(commissonTotal);
        }
    }
}
