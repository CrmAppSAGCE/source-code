﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using invoice;

namespace Scoin.Crm.Plugins.invoice
{
    public class postCreateReport : IPlugin
    {
        // Get the invoice pdf from reporting services (pastel)
        public void Execute(IServiceProvider serviceProvider)
        {

            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null && context.ParentContext.MessageName == "ConvertSalesOrderToInvoice")
            {

                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                var target = (Entity)context.InputParameters["Target"];



                var path = @"http://172.19.4.14/ReportServer/Pages/ReportViewer.aspx?%2fCoinnect_MSCRM%2fCustomReports%2fInvoice+Original+Report&rs:Command=Render&rs:Format=PDF&invoicenumber=IN128133";
                //var path = "http://172.19.4.14/ReportServer/Pages/ReportViewer.aspx?%2fCoinnect_MSCRM%2fCustomReports%2f" + ReportName.Get(executionContext) + "&rs:Command=Render&rs:Format=PDF&" + Parameter1.Get(executionContext) + "=" + Parameter1Value.Get(executionContext);
                var WebReq = HttpWebRequest.Create(path);
                //var WebReq = HttpWebRequest.Create(string.Format("{0}?invoicenumber={1}", path, documentnumber));

                WebReq.Credentials = CredentialCache.DefaultCredentials;
                WebReq.ImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                trace.Trace("retrieved the web response");
                BinaryReader sr = new BinaryReader(WebResp.GetResponseStream());
                MemoryStream ms = new MemoryStream();
                FileStream fs = new FileStream(@"C:\test\", FileMode.OpenOrCreate);
                var buffer = new byte[4096];

                var length = sr.Read(buffer, 0, buffer.Length);
                while (length > 0)
                {
                    fs.Write(buffer, 0, length);
                    length = sr.Read(buffer, 0, buffer.Length);
                }
                var result = ms.GetBuffer();

                fs.Close();

                //var WebReq = HttpWebRequest.Create(@"http:/172.19.4.33/testcoinnect/crmreports/viewer/viewer.aspx?action=filter&helpID=Document1.rdl&id=%7b459BE454-32CC-E111-B3D6-78ACC08839C9%7d&p:invoicenumber=IN128133");
                //WebReq.Method = "GET";
                //HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

                //BinaryReader sr = new BinaryReader(WebResp.GetResponseStream());
                //MemoryStream ms = new MemoryStream();
                //FileStream fs = new FileStream(@"C:\test\", FileMode.OpenOrCreate);
                //var buffer = new byte[4096];
                //byte[] data = new byte[fs.Length];
                //var length = sr.Read(buffer, 0, buffer.Length);
                //while (length > 0)
                //{
                //    fs.Write(buffer, 0, length);
                //    length = sr.Read(buffer, 0, buffer.Length);
                //}
                //fs.Close();
              
            
            }

        }
    }
}
