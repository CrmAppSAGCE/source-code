﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Web.Services.Protocols;
//using PrintReport.reportserver;
using System.Runtime.InteropServices;
using System.Net; // For Marshal.Copy
namespace invoice
{
    /// <summary>
    /// A simple console application that demonstrates one way to
    /// print Reporting Services reports to a printer.
    /// </summary>
    class AutoPrint
    {

       // ReportingService rs;
        private byte[][] m_renderedReport;
        private Graphics.EnumerateMetafileProc m_delegate = null;
        private MemoryStream m_currentPageStream;
        private Metafile m_metafile = null;
        int m_numberOfPages;
        private int m_currentPrintingPage;
        private int m_lastPrintingPage;

  
        public string RenderReport(string reportPath)
        {
            // Private variables for rendering
            //var path = string.Format((string)order.GetAttributeValue<AliasedValue>("pc.isp_serviceurl").Value, "Generate.aspx");
            //var documentnumber = target.GetAttributeValue<string>("isp_pastelinvoicenumber");
            var documentnumber = "XIN11140";
            var WebReq = HttpWebRequest.Create(string.Format(reportPath + "?invoicenumber=" + documentnumber));
            WebReq.Method = "GET";
            HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

            BinaryReader sr = new BinaryReader(WebResp.GetResponseStream());
            MemoryStream ms = new MemoryStream();
            var buffer = new byte[4096];

            var length = sr.Read(buffer, 0, buffer.Length);
            while (length > 0)
            {
                ms.Write(buffer, 0, length);
                length = sr.Read(buffer, 0, buffer.Length);
            }
            string deviceInfo = null;
            string format = "IMAGE";
            Byte[] firstPage = null;
            string encoding;
            string mimeType;
           // Warning[] warnings = null;
           // ParameterValue[] reportHistoryParameters = null;
            string[] streamIDs = null;
            Byte[][] pages = null;

            // Build device info based on the start page
            deviceInfo =
               String.Format(@"<DeviceInfo><OutputFormat>{0}</OutputFormat></DeviceInfo>", "emf");

            //Exectute the report and get page count.
            //try
            //{
            //    // Renders the first page of the report and returns streamIDs for 
            //    // subsequent pages
            //    firstPage = ms. rs.Render(
            //       reportPath,
            //       format,
            //       null,
            //       deviceInfo,
            //       null,
            //       null,
            //       null,
            //       out encoding,
            //       out mimeType,
            //     //  out reportHistoryParameters,
            //     //  out warnings,
            //       out streamIDs);
            //    // The total number of pages of the report is 1 + the streamIDs         
            //    m_numberOfPages = streamIDs.Length + 1;
            //    pages = new Byte[m_numberOfPages][];

            //    // The first page was already rendered
            //    pages[0] = firstPage;

            //    for (int pageIndex = 1; pageIndex < m_numberOfPages; pageIndex++)
            //    {
            //        // Build device info based on start page
            //        deviceInfo =
            //           String.Format(@"<DeviceInfo><OutputFormat>{0}</OutputFormat><StartPage>{1}</StartPage></DeviceInfo>",
            //             "emf", pageIndex + 1);
            //        pages[pageIndex] = rs.Render(
            //           reportPath,
            //           format,
            //           null,
            //           deviceInfo,
            //           null,
            //           null,
            //           null,
            //           out encoding,
            //           out mimeType,
            //         //  out reportHistoryParameters,
            //         //  out warnings,
            //           out streamIDs);
            //    }
            //}
            //catch (SoapException ex)
            //{
            //    Console.WriteLine(ex.Detail.InnerXml);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //finally
            //{
            //    Console.WriteLine("Number of pages: {0}", pages.Length);
            //}
            return Convert.ToBase64String(ms.GetBuffer());
        }

        public bool PrintReport(string printerName)
        {
            //this.RenderedReport = this.RenderReport("/SampleReports/Company Sales");
            //RenderReport
            try
            {
                // Wait for the report to completely render.
                if (m_numberOfPages < 1)
                    return false;
                PrinterSettings printerSettings = new PrinterSettings();
                printerSettings.MaximumPage = m_numberOfPages;
                printerSettings.MinimumPage = 1;
                printerSettings.PrintRange = PrintRange.SomePages;
                printerSettings.FromPage = 1;
                printerSettings.ToPage = m_numberOfPages;
                printerSettings.PrinterName = printerName;
                PrintDocument pd = new PrintDocument();
                m_currentPrintingPage = 1;
                m_lastPrintingPage = m_numberOfPages;
                pd.PrinterSettings = printerSettings;
                // Print report
               // Console.WriteLine("Printing report...");
                pd.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);
                pd.Print();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // Clean up goes here.
            }
            return true;
        }
        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            ev.HasMorePages = false;
            if (m_currentPrintingPage <= m_lastPrintingPage && MoveToPage(m_currentPrintingPage))
            {
                // Draw the page
                ReportDrawPage(ev.Graphics);
                // If the next page is less than or equal to the last page, 
                // print another page.
                if (++m_currentPrintingPage <= m_lastPrintingPage)
                    ev.HasMorePages = true;
            }
        }

        // Method to draw the current emf memory stream 
        private void ReportDrawPage(Graphics g)
        {
            if (null == m_currentPageStream || 0 == m_currentPageStream.Length || null == m_metafile)
                return;
            lock (this)
            {
                // Set the metafile delegate.
                int width = m_metafile.Width;
                int height = m_metafile.Height;
                m_delegate = new Graphics.EnumerateMetafileProc(MetafileCallback);
                // Draw in the rectangle
                Point destPoint = new Point(0, 0);
                g.EnumerateMetafile(m_metafile, destPoint, m_delegate);
                // Clean up
                m_delegate = null;
            }
        }
        private bool MoveToPage(Int32 page)
        {
            // Check to make sure that the current page exists in
            // the array list
            if (null == this.RenderedReport[m_currentPrintingPage - 1])
                return false;
            // Set current page stream equal to the rendered page
            m_currentPageStream = new MemoryStream(this.RenderedReport[m_currentPrintingPage - 1]);
            // Set its postion to start.
            m_currentPageStream.Position = 0;
            // Initialize the metafile
            if (null != m_metafile)
            {
                m_metafile.Dispose();
                m_metafile = null;
            }
            // Load the metafile image for this page
            m_metafile = new Metafile((Stream)m_currentPageStream);
            return true;
        }
        private bool MetafileCallback(
           EmfPlusRecordType recordType,
           int flags,
           int dataSize,
           IntPtr data,
           PlayRecordCallback callbackData)
        {
            byte[] dataArray = null;
            // Dance around unmanaged code.
            if (data != IntPtr.Zero)
            {
                // Copy the unmanaged record to a managed byte buffer 
                // that can be used by PlayRecord.
                dataArray = new byte[dataSize];
                Marshal.Copy(data, dataArray, 0, dataSize);
            }
            // play the record.      
            m_metafile.PlayRecord(recordType, flags, dataSize, dataArray);

            return true;
        }
        public byte[][] RenderedReport
        {
            get
            {
                return m_renderedReport;
            }
            set
            {
                m_renderedReport = value;
            }
        }
    }
}