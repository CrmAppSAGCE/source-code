﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;
using Scoin.Crm.Plugins.PastelService;

namespace Scoin.Crm.Plugins.invoice
{
    public class CreditNote : IPlugin
    {
        // Create the invoice in Pastel
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);

                var target = (Entity)context.InputParameters["Target"];

                Invoice invoice = (from i in orgContext.CreateQuery<Invoice>()
                                   where i.InvoiceId.Value == context.PrimaryEntityId
                                   select new Invoice
                                   {
                                       InvoiceId = i.InvoiceId,
                                       SalesOrderId = i.SalesOrderId
                                   }).FirstOrDefault();

                SalesOrder salesOrder = (from s in orgContext.CreateQuery<SalesOrder>()
                                         where s.SalesOrderId.Value == invoice.SalesOrderId.Id
                                         select new SalesOrder
                                         {
                                             SalesOrderId = s.SalesOrderId,
                                             isp_IndependentBroker = s.isp_IndependentBroker
                                         }).FirstOrDefault();

                var CreditNoteEntity = new Entity("isp_creditnote");
                CreditNoteEntity.Attributes.Add("isp_invoice", new EntityReference("invoice", target.Id));
                CreditNoteEntity.Attributes.Add("isp_creditnotenumber", "Pending");
                if (salesOrder.isp_IndependentBroker != null)
                {
                    CreditNoteEntity.Attributes.Add("isp_independentbroker", salesOrder.isp_IndependentBroker);
                }

                IOrganizationService scService = factory.CreateOrganizationService(context.UserId);

                Guid CredGUID = scService.Create(CreditNoteEntity);
                target.Attributes["isp_creditnote"] = new EntityReference("isp_creditnote", CredGUID);
                target.Attributes["isp_creditnotenumber"] = "PendingCreditNoteCreation";
            }
        }
    }
}
