﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.invoice
{
    public class postupdate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                Entity target = (Entity)context.InputParameters["Target"];
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                EntityHelper helper = new EntityHelper(context, service);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                Entity preMessageImage = null;
                Entity postMessageImage = null;

                if (context.PreEntityImages.Contains("PreImage") && context.PreEntityImages["PreImage"] is Entity)
                {
                    preMessageImage = (Entity)context.PreEntityImages["PreImage"];
                }
                if (context.PostEntityImages.Contains("PostImage") && context.PostEntityImages["PostImage"] is Entity)
                {
                    postMessageImage = (Entity)context.PostEntityImages["PostImage"];
                }
                if (postMessageImage != null && preMessageImage != null)
                {
                    if (helper.HasValueChanged(preMessageImage, postMessageImage, "shippingmethodcode"))
                    {
                        Invoice invoice = (from i in orgContext.CreateQuery<Invoice>()
                                           where i.InvoiceId.Value == context.PrimaryEntityId
                                           select new Invoice
                                           {
                                               InvoiceId = i.InvoiceId,
                                               SalesOrderId = i.SalesOrderId,
                                               StatusCode = i.StatusCode,
                                               ShippingMethodCode = i.ShippingMethodCode
                                           }).FirstOrDefault();
                        int status = 0;
                        //Courier
                        if (invoice.ShippingMethodCode.Value == 100000000)
                        {
                            status = 863300005;
                        }
                        //Collect
                        if (invoice.ShippingMethodCode.Value == 100000001)
                        {
                            status = 863300006;
                        }
                        //Safe Custody
                        if (invoice.ShippingMethodCode.Value == 100000002)
                        {
                            status = 863300007;
                        }
                        SetStateRequest setStateReq = new SetStateRequest();
                        setStateReq.EntityMoniker = new EntityReference(Invoice.EntityLogicalName, context.PrimaryEntityId);
                        setStateReq.State = new OptionSetValue(0);
                        setStateReq.Status = new OptionSetValue(status);
                        SetStateResponse response = (SetStateResponse)service.Execute(setStateReq);
                    }
                }
            }
        }
    }
}
