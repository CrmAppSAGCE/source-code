﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;
namespace Scoin.Crm.Plugins.buybackproduct
{
    public class postcreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                var target = (Entity)context.InputParameters["Target"];
                decimal runningTotal = 0;

                isp_buyback buyBack = (from bb in orgContext.CreateQuery<isp_buyback>()
                                       where bb.isp_buybackId.Value == (target.GetAttributeValue<EntityReference>("isp_buybackproductsidrelationship")).Id
                                       select new isp_buyback
                                       {
                                           isp_buybackId = bb.isp_buybackId,
                                           statuscode = bb.statuscode,
                                           isp_total = bb.isp_total,
                                       }).FirstOrDefault();

                List<isp_buybackproducts> buyBackProducts = (from bbp in orgContext.CreateQuery<isp_buybackproducts>()
                                                             where bbp.isp_BuyBackProductsIdRelationship.Id == buyBack.isp_buybackId.Value
                                                             && bbp.statuscode.Value != 863300002
                                                             select new isp_buybackproducts
                                                             {
                                                                 isp_buybackproductsId = bbp.isp_buybackproductsId,
                                                                 isp_ProductsBuyBackPrice = bbp.isp_ProductsBuyBackPrice,
                                                                 isp_Quantity = bbp.isp_Quantity
                                                             }).ToList<isp_buybackproducts>();

                foreach (isp_buybackproducts bbp in buyBackProducts)
                {
                    if (bbp.isp_ProductsBuyBackPrice != null && bbp.isp_Quantity != null)
                    {
                        runningTotal += (bbp.isp_ProductsBuyBackPrice.Value * bbp.isp_Quantity.Value);
                    }
                }
                buyBack.isp_total = new Money(runningTotal);
                orgContext.UpdateObject(buyBack);
                orgContext.SaveChanges();
            }
        }
    }
}
