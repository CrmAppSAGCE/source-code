﻿using System;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.buybackproduct
{
    public class precreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);

                var target = (Entity)context.InputParameters["Target"];

                isp_buyback buyBack = (from bb in orgContext.CreateQuery<isp_buyback>()
                                       where bb.isp_buybackId.Value == (target.GetAttributeValue<EntityReference>("isp_buybackproductsidrelationship")).Id
                                       select new isp_buyback
                                       {
                                           isp_buybackId = bb.isp_buybackId,
                                           statuscode = bb.statuscode
                                       }).FirstOrDefault();

                if (buyBack.statuscode.Value == 863300001 || buyBack.statuscode.Value == 863300002 || buyBack.statuscode.Value == 863300004 || buyBack.statuscode.Value == 863300004 || buyBack.statuscode.Value == 863300005 || buyBack.statuscode.Value == 863300006)
                {
                    throw new InvalidPluginExecutionException("The Buy Back is in a state in which no more products may be added.");
                }
            }
        }
    }
}
