﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.buybackproduct
{
    public class postupdate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                Entity target = (Entity)context.InputParameters["Target"];
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                Entity preMessageImage = null;
                Entity postMessageImage = null;
                decimal runningTotal = 0;

                EntityHelper helper = new EntityHelper(context, service);
                if (context.Depth > 1)
                {
                    return;
                }
                if (context.PreEntityImages.Contains("PreImage") && context.PreEntityImages["PreImage"] is Entity)
                {
                    preMessageImage = (Entity)context.PreEntityImages["PreImage"];
                }
                if (context.PostEntityImages.Contains("PostImage") && context.PostEntityImages["PostImage"] is Entity)
                {
                    postMessageImage = (Entity)context.PostEntityImages["PostImage"];
                }
                if (target.Contains("isp_productsbuybackprice") || target.Contains("isp_quantity"))
                {
                    isp_buyback buyBack = (from bb in orgContext.CreateQuery<isp_buyback>()
                                           where bb.isp_buybackId.Value == ((EntityReference)postMessageImage.Attributes["isp_buybackproductsidrelationship"]).Id
                                           select new isp_buyback
                                           {
                                               isp_buybackId = bb.isp_buybackId,
                                               statuscode = bb.statuscode,
                                               isp_total = bb.isp_total,
                                           }).FirstOrDefault();

                    List<isp_buybackproducts> buyBackProducts = (from bbp in orgContext.CreateQuery<isp_buybackproducts>()
                                                                 where bbp.isp_BuyBackProductsIdRelationship.Id == buyBack.isp_buybackId.Value
                                                                 && bbp.statuscode.Value != 2
                                                                 select new isp_buybackproducts
                                                                 {
                                                                     isp_buybackproductsId = bbp.isp_buybackproductsId,
                                                                     isp_ProductsBuyBackPrice = bbp.isp_ProductsBuyBackPrice,
                                                                     isp_Quantity = bbp.isp_Quantity
                                                                 }).ToList<isp_buybackproducts>();

                    foreach (isp_buybackproducts bbp in buyBackProducts)
                    {
                        if (bbp.isp_ProductsBuyBackPrice != null && bbp.isp_Quantity != null)
                        {
                            runningTotal += (bbp.isp_ProductsBuyBackPrice.Value * bbp.isp_Quantity.Value);
                        }
                    }
                    buyBack.isp_total = new Money(runningTotal);
                    orgContext.UpdateObject(buyBack);
                    orgContext.SaveChanges();
                }
                if (postMessageImage != null && preMessageImage != null)
                {
                    if (postMessageImage.Contains("isp_approved") && postMessageImage.Contains("isp_verified"))
                    {
                        if (((bool)postMessageImage.Attributes["isp_approved"]) && (bool)postMessageImage.Attributes["isp_verified"])
                        {
                            SetStateRequest setStateRequest = new SetStateRequest()
                            {
                                EntityMoniker = new EntityReference
                                {
                                    Id = context.PrimaryEntityId,
                                    LogicalName = isp_buybackproducts.EntityLogicalName
                                },
                                State = new OptionSetValue(0),
                                Status = new OptionSetValue(863300005)
                            };
                            service.Execute(setStateRequest);

                        }
                    }
                    if (((OptionSetValue)postMessageImage.Attributes["statecode"]).Value == 1)
                    {
                        isp_buyback buyBack = (from bb in orgContext.CreateQuery<isp_buyback>()
                                               where bb.isp_buybackId.Value == ((EntityReference)postMessageImage.Attributes["isp_buybackproductsidrelationship"]).Id
                                               select new isp_buyback
                                               {
                                                   isp_buybackId = bb.isp_buybackId,
                                                   statuscode = bb.statuscode,
                                                   isp_total = bb.isp_total,
                                               }).FirstOrDefault();

                        List<isp_buybackproducts> buyBackProducts = (from bbp in orgContext.CreateQuery<isp_buybackproducts>()
                                                                     where bbp.isp_BuyBackProductsIdRelationship.Id == buyBack.isp_buybackId.Value
                                                                     && bbp.statuscode.Value != 2
                                                                     select new isp_buybackproducts
                                                                     {
                                                                         isp_buybackproductsId = bbp.isp_buybackproductsId,
                                                                         isp_ProductsBuyBackPrice = bbp.isp_ProductsBuyBackPrice,
                                                                         isp_Quantity = bbp.isp_Quantity
                                                                     }).ToList<isp_buybackproducts>();

                        foreach (isp_buybackproducts bbp in buyBackProducts)
                        {
                            if (bbp.isp_ProductsBuyBackPrice != null && bbp.isp_Quantity != null)
                            {
                                runningTotal += (bbp.isp_ProductsBuyBackPrice.Value * bbp.isp_Quantity.Value);
                            }
                        }
                        buyBack.isp_total = new Money(runningTotal);
                        orgContext.UpdateObject(buyBack);
                        orgContext.SaveChanges();
                    }
                }
            }
        }
    }
}
