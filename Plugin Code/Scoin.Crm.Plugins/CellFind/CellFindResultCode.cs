﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Scoin.Plugins.CellFind
{
    public enum CellFindResultCode
    {
        [Description("Call Limit Reached")] CallLimitReached = -121,
        [Description("Duplicate")]Duplicate = -120,
        [Description("Failed on Blank message")]NoText = -119,
        [Description("Number not Active")]NotActive = -115,
        [Description("Mobile number is Invalid")]InvalidMobileNumber = -112,
        [Description("Not a Valid Token")]NotAValidToken = -99,
        [Description("Failed to Insert the data")]FailedToInsertData = -88,
        [Description("Falied to return valid data")]SelectError = -55,  
        [Description("The User does not exist")]UserDotNotExist = -33,
        [Description("Failed to update the detail")]UpdateFailed = -22,
        [Description("Not a CellFind Administrator")]NotCellFAdmin = -11,
        [Description("Not and Account Administrator")]NotAccountAdmin = -7,
        [Description("The added exclusion exist")]ExclusionExist = -4,
        [Description("Operation Successful")]Success = -1,
    }
}
