﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.salesorder
{
    public class postupdate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            Entity preMessageImage = null;
            Entity postMessageImage = null;

            EntityHelper helper = new EntityHelper(context, service);

            if (context.PreEntityImages.Contains("PreImage") && context.PreEntityImages["PreImage"] is Entity)
            {
                preMessageImage = (Entity)context.PreEntityImages["PreImage"];
            }
            if (context.PostEntityImages.Contains("PostImage") && context.PostEntityImages["PostImage"] is Entity)
            {
                postMessageImage = (Entity)context.PostEntityImages["PostImage"];
            }
            if (preMessageImage != null && postMessageImage != null)
            {
                if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_invoice"))
                {
                    List<GeneratedCode.isp_payment> payments = (from p in orgContext.CreateQuery<Scoin.Crm.Plugins.GeneratedCode.isp_payment>()
                                                                where p.isp_atp.Id == context.PrimaryEntityId
                                                                && p.statecode.Value != GeneratedCode.isp_paymentState.Inactive
                                                                && p.statuscode.Value != 863300001
                                                                select new Scoin.Crm.Plugins.GeneratedCode.isp_payment
                                                                {
                                                                    isp_paymentId = p.isp_paymentId,
                                                                    isp_Invoice = p.isp_Invoice,
                                                                    isp_Amount = p.isp_Amount
                                                                }).ToList();
                    foreach (GeneratedCode.isp_payment payment in payments)
                    {
                            payment.isp_Invoice = target.GetAttributeValue<EntityReference>("isp_invoice");
                            orgContext.ClearChanges();
                            orgContext.Attach(payment);
                            orgContext.UpdateObject(payment);
                            orgContext.SaveChanges();
                    }
                }
            }
        }
    }
}
