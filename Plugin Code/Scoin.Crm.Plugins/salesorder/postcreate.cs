﻿using System;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.salesorder
{
   public class postcreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                Entity target = (Entity)context.InputParameters["Target"];

                EntityHelper helper = new EntityHelper(context, service);
                if (target.Contains("isp_contactid"))
                {
                    Contact contact = (from c in orgContext.CreateQuery<Contact>()
                                       where c.ContactId.Value == ((EntityReference)target["isp_contactid"]).Id
                                       select new Contact
                                       {
                                           ContactId = c.ContactId,
                                           isp_CompanyName = c.isp_CompanyName,
                                           Address1_Line1 = c.Address1_Line1,
                                           Address1_Line2 = c.Address1_Line2,
                                           Address1_Line3 = c.Address1_Line3,
                                           Address1_PostalCode = c.Address1_PostalCode,
                                           Address1_Country = c.Address1_Country,
                                           isp_province = c.isp_province,
                                           isp_country = c.isp_country,
                                           isp_VATNumber = c.isp_VATNumber,
                                           Fax = c.Fax,
                                           MobilePhone = c.MobilePhone,
                                           EMailAddress1 = c.EMailAddress1,
                                           Address1_City = c.Address1_City
                                       }).FirstOrDefault();
                    if (target.Contains("isp_companyname"))
                    {
                        if (contact.isp_CompanyName != (string)target["isp_companyname"])
                        {
                            contact.isp_CompanyName = (string)target["isp_companyname"];
                        }
                    }
                    if (target.Contains("shipto_line1"))
                    {
                        if (contact.Address1_Line1 != (string)target["shipto_line1"])
                        {
                            contact.Address1_Line1 = (string)target["shipto_line1"];
                        }
                    }
                    if (target.Contains("shipto_line2"))
                    {
                        if (contact.Address1_Line2 != (string)target["shipto_line2"])
                        {
                            contact.Address1_Line2 = (string)target["shipto_line2"];
                        }
                    }
                    if (target.Contains("shipto_line3"))
                    {
                        if (contact.Address1_Line3 != (string)target["shipto_line3"])
                        {
                            contact.Address1_Line3 = (string)target["shipto_line3"];
                        }
                    }
                    if (target.Contains("shipto_postalcode"))
                    {
                        if (contact.Address1_PostalCode != (string)target["shipto_postalcode"])
                        {
                            contact.Address1_PostalCode = (string)target["shipto_postalcode"];
                        }
                    }
                    if (target.Contains("isp_vatnumber"))
                    {
                        if (contact.isp_VATNumber != (string)target["isp_vatnumber"])
                        {
                            contact.isp_VATNumber = (string)target["isp_vatnumber"];
                        }
                    }
                    if (target.Contains("shipto_fax"))
                    {
                        if (contact.Fax != (string)target["shipto_fax"])
                        {
                            contact.Fax = (string)target["shipto_fax"];
                        }
                    }
                    if (target.Contains("shipto_city"))
                    {
                        if (contact.Address1_City != (string)target["shipto_city"])
                        {
                            contact.Address1_City = (string)target["shipto_city"];
                        }
                    }
                    if (target.Contains("shipto_telephone"))
                    {
                        if (contact.MobilePhone != (string)target["shipto_telephone"])
                        {
                            contact.MobilePhone = (string)target["shipto_telephone"];
                        }
                    }
                    if (target.Contains("isp_emailaddress"))
                    {
                        if (contact.EMailAddress1 != (string)target["isp_emailaddress"])
                        {
                            contact.EMailAddress1 = (string)target["isp_emailaddress"];
                        }
                    }
                    if (target.Contains("shipto_stateorprovince"))
                    {
                        if (helper.GetOptionSetValueLabel(service, "contact", "isp_province", contact.isp_province).Replace(" ", string.Empty).ToLower().Trim() != ((string)target["shipto_stateorprovince"]).Replace(" ", string.Empty).ToLower().Trim())
                        {
                            switch (((string)target["shipto_stateorprovince"]).Replace(" ", string.Empty).ToLower().Trim())
                            {
                                case "gauteng":
                                    contact.isp_province = new OptionSetValue(1);
                                    break;
                                case "westerncape":
                                    contact.isp_province = new OptionSetValue(2);
                                    break;
                                case "mpumalanga":
                                    contact.isp_province = new OptionSetValue(3);
                                    break;
                                case "kwazulunatal":
                                    contact.isp_province = new OptionSetValue(4);
                                    break;
                                case "easterncape":
                                    contact.isp_province = new OptionSetValue(5);
                                    break;
                                case "freestate":
                                    contact.isp_province = new OptionSetValue(6);
                                    break;
                                case "limpopo":
                                    contact.isp_province = new OptionSetValue(7);
                                    break;
                                case "northerncape":
                                    contact.isp_province = new OptionSetValue(8);
                                    break;
                                case "northwest":
                                    contact.isp_province = new OptionSetValue(9);
                                    break;
                                default:
                                    throw new Exception("Please type in one of the provinces in the state/province field as listed below: \r\n\r\nGauteng\r\nWestern Cape\r\nMpumalanga\r\nKwazulu Natal\r\nEastern Cape\r\nFree State\r\nLimpopo\r\nNorthern Cape\r\nNorth West");
                            }
                        }
                    }
                    if (target.Contains("shipto_country"))
                    {
                        if (helper.GetOptionSetValueLabel(service, "contact", "isp_country", contact.isp_country).Replace(" ", string.Empty).ToLower().Trim() != ((string)target["shipto_country"]).Replace(" ", string.Empty).ToLower().Trim())
                        {
                            switch (((string)target["shipto_country"]).Replace(" ", string.Empty).ToLower().Trim())
                            {
                                case "southafrica":
                                    contact.isp_country = new OptionSetValue(0);
                                    break;
                                case "unitedkingdom":
                                    contact.isp_country = new OptionSetValue(1);
                                    break;
                                case "canada":
                                    contact.isp_country = new OptionSetValue(2);
                                    break;
                                case "usa":
                                    contact.isp_country = new OptionSetValue(3);
                                    break;
                                case "france":
                                    contact.isp_country = new OptionSetValue(4);
                                    break;
                                case "australia":
                                    contact.isp_country = new OptionSetValue(5);
                                    break;
                                case "belgium":
                                    contact.isp_country = new OptionSetValue(6);
                                    break;
                                case "namibia":
                                    contact.isp_country = new OptionSetValue(863300000);
                                    break;
                                default:
                                    contact.isp_country = new OptionSetValue(7);
                                    contact.Address1_Country = (string)target["shipto_country"];
                                    break;
                            }
                        }
                    }
                    orgContext.UpdateObject(contact);
                    orgContext.SaveChanges();
                }
            }
        }
    }
}