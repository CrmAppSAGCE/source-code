﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;

namespace Scoin.Crm.Plugins.salesorder
{
    public class RequestClearance : IPlugin   
    {
        public void Execute(IServiceProvider serviceProvider)
        {

            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);


            EntityHelper helper = new EntityHelper(context, service);
            if (helper.GetAttributeValue<Boolean>("ispricelocked") == false)
            {
                LockSalesOrderPricingRequest req = new LockSalesOrderPricingRequest();
                req.SalesOrderId = target.Id;
                LockSalesOrderPricingResponse resp = (LockSalesOrderPricingResponse)service.Execute(req);
            }
            //Microsoft.Crm.Sdk.Messages.SetStateRequest LockSalesOrder = new Microsoft.Crm.Sdk.Messages.SetStateRequest
            //{
            //    EntityMoniker = new EntityReference("salesorder", target.Id),
            //    State = new OptionSetValue { Value = 1 },
            //    Status = new OptionSetValue { Value = 863300011 }
            //};
            //service.Execute(LockSalesOrder);


        }  
    }
}
