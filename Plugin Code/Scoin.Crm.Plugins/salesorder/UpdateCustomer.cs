﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;

namespace Scoin.Crm.Plugins.salesorder
{
    public class UpdateCustomer : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {

            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            EntityHelper helper = new EntityHelper();

            var PreImage = (context.PreEntityImages.Contains("pre") ? (Entity)context.PreEntityImages["pre"] : null);

            #region "Contact Change"

            trace.Trace("Started the Plugin");
            Entity EntityToWorkWith = new Entity();

            QueryExpression contactDetailsQuery = new QueryExpression("contact");
            if (context.MessageName.ToLower() == "update")
            {
                if (target.Contains("isp_contactid"))
                {
                    helper.updateTarget(target, "customerid", target.GetAttributeValue<EntityReference>("isp_contactid"));
                }
                trace.Trace("Update Portion");
                contactDetailsQuery.Criteria.AddCondition("contactid", ConditionOperator.Equal, PreImage.GetAttributeValue<EntityReference>("isp_contactid").Id);
                trace.Trace("PreImage " + PreImage.GetAttributeValue<EntityReference>("isp_contactid").Id);
                EntityToWorkWith = PreImage;
            }
            if (context.MessageName.ToLower() == "create")
            {
                contactDetailsQuery.Criteria.AddCondition("contactid", ConditionOperator.Equal, target.GetAttributeValue<EntityReference>("isp_contactid").Id);
                EntityToWorkWith = target;
            }
            contactDetailsQuery.ColumnSet = new ColumnSet(true);
            var contactDetails = service.RetrieveMultiple(contactDetailsQuery);

            StringBuilder DuplicateDetails = new StringBuilder("");
            //Compare the ATP details to other
            //Customer: Email, ID, Contact numbers

            trace.Trace("Setting up the List");
            List<Guid> Managers = new List<Guid>();
            trace.Trace("Checking if there is an ID Number");
            if (contactDetails.Entities[0].GetAttributeValue<string>("isp_idnumber") != null)
            {
                trace.Trace("ID Number found");
                QueryExpression queryID = new QueryExpression("contact");
                queryID.ColumnSet = new ColumnSet(true);
                queryID.Criteria.AddCondition("statecode", ConditionOperator.NotEqual, 1);
                queryID.Criteria.AddCondition("statuscode", ConditionOperator.Equal, 1);
                queryID.Criteria.AddCondition("ownerid", ConditionOperator.NotEqual, EntityToWorkWith.GetAttributeValue<EntityReference>("ownerid").Id);

                trace.Trace("Found ownerid: ");
                queryID.Criteria.AddCondition("isp_idnumber", ConditionOperator.Equal, contactDetails.Entities[0].GetAttributeValue<string>("isp_idnumber"));
                var systemuserLinkId = queryID.AddLink("systemuser", "ownerid", "systemuserid");
                systemuserLinkId.EntityAlias = "su";
                systemuserLinkId.Columns.AddColumn("parentsystemuserid");

                trace.Trace("Going to Fetch The Data");
                var resultsId = service.RetrieveMultiple(queryID);
                trace.Trace("Received the Results ID");

                for (int i = 0; i < resultsId.Entities.Count; i++)
                {
                    if (resultsId.Entities[i].GetAttributeValue<AliasedValue>("su.parentsystemuserid") != null && !Managers.Contains(((EntityReference)resultsId.Entities[i].GetAttributeValue<AliasedValue>("su.parentsystemuserid").Value).Id))
                        Managers.Add(((EntityReference)resultsId.Entities[i].GetAttributeValue<AliasedValue>("su.parentsystemuserid").Value).Id);
                    DuplicateDetails.Append("Created On: " + resultsId.Entities[i].GetAttributeValue<DateTime>("createdon").ToShortDateString() + " | Owner: " + resultsId.Entities[i].GetAttributeValue<EntityReference>("ownerid").Name + " | Contact: " + resultsId.Entities[i].GetAttributeValue<string>("fullname") + " | ID Number: " + contactDetails.Entities[0].GetAttributeValue<string>("isp_idnumber") + "\r\n");
                }
                trace.Trace("Finished with the ID Dupes");
            }

            trace.Trace("Starting with the Emails");
            if (contactDetails.Entities[0].GetAttributeValue<string>("emailaddress1") != null)
            {
                QueryExpression queryEmail = new QueryExpression("contact");
                queryEmail.ColumnSet = new ColumnSet(true);
                queryEmail.Criteria.AddCondition("statecode", ConditionOperator.NotEqual, 1);
                queryEmail.Criteria.AddCondition("statuscode", ConditionOperator.Equal, 1);
                queryEmail.Criteria.AddCondition("ownerid", ConditionOperator.NotEqual, EntityToWorkWith.GetAttributeValue<EntityReference>("ownerid").Id);
                queryEmail.Criteria.AddCondition("emailaddress1", ConditionOperator.Equal, contactDetails.Entities[0].GetAttributeValue<string>("emailaddress1"));
                var systemuserLinkEmail = queryEmail.AddLink("systemuser", "ownerid", "systemuserid");
                systemuserLinkEmail.EntityAlias = "su";
                systemuserLinkEmail.Columns.AddColumn("parentsystemuserid");

                var resultsEmail = service.RetrieveMultiple(queryEmail);

                trace.Trace("Received the Results Email");
                for (int i = 0; i < resultsEmail.Entities.Count; i++)
                {

                    if (resultsEmail.Entities[i].GetAttributeValue<AliasedValue>("su.parentsystemuserid") != null && !Managers.Contains(((EntityReference)resultsEmail.Entities[i].GetAttributeValue<AliasedValue>("su.parentsystemuserid").Value).Id))
                        Managers.Add(((EntityReference)resultsEmail.Entities[i].GetAttributeValue<AliasedValue>("su.parentsystemuserid").Value).Id);
                    DuplicateDetails.Append("Created On: " + resultsEmail.Entities[i].GetAttributeValue<DateTime>("createdon").ToShortDateString() + " | Owner: " + resultsEmail.Entities[i].GetAttributeValue<EntityReference>("ownerid").Name + " | Contact: " + resultsEmail.Entities[i].GetAttributeValue<string>("fullname") + " | e-mail : " + contactDetails.Entities[0].GetAttributeValue<string>("emailaddress1") + "\r\n");
                }
            }

            if (contactDetails.Entities[0].GetAttributeValue<string>("mobilephone") != null)
            {
                if (contactDetails.Entities[0].GetAttributeValue<string>("mobilephone") == "")
                    return;

                QueryExpression queryMobile = new QueryExpression("contact");
                queryMobile.ColumnSet = new ColumnSet(true);
                queryMobile.Criteria.AddCondition("statecode", ConditionOperator.NotEqual, 1);
                queryMobile.Criteria.AddCondition("statuscode", ConditionOperator.Equal, 1);
                queryMobile.Criteria.AddCondition("ownerid", ConditionOperator.NotEqual, EntityToWorkWith.GetAttributeValue<EntityReference>("ownerid").Id);
                queryMobile.Criteria.AddCondition("mobilephone", ConditionOperator.Equal, contactDetails.Entities[0].GetAttributeValue<string>("mobilephone"));
                var systemuserLinkMobile = queryMobile.AddLink("systemuser", "ownerid", "systemuserid");
                systemuserLinkMobile.EntityAlias = "su";
                systemuserLinkMobile.Columns.AddColumn("parentsystemuserid");

                var resultsMobile = service.RetrieveMultiple(queryMobile);

                trace.Trace("Received the Results Mobile");
                for (int i = 0; i < resultsMobile.Entities.Count; i++)
                {
                    if (resultsMobile.Entities[i].GetAttributeValue<AliasedValue>("su.parentsystemuserid") != null && !Managers.Contains(((EntityReference)resultsMobile.Entities[i].GetAttributeValue<AliasedValue>("su.parentsystemuserid").Value).Id))
                        Managers.Add(((EntityReference)resultsMobile.Entities[i].GetAttributeValue<AliasedValue>("su.parentsystemuserid").Value).Id);
                    DuplicateDetails.Append("Created On: " + resultsMobile.Entities[i].GetAttributeValue<DateTime>("createdon").ToShortDateString() + " | Owner: " + resultsMobile.Entities[i].GetAttributeValue<EntityReference>("ownerid").Name + " | Contact: " + resultsMobile.Entities[i].GetAttributeValue<string>("fullname") + " | Mobile : " + contactDetails.Entities[0].GetAttributeValue<string>("mobilephone") + "\r\n");
                }

            }
            target.Attributes["isp_duplicatedetails"] = DuplicateDetails.ToString();
            trace.Trace("Setting Up the Managers");
            for (int i = 0; i < Managers.Count; i++)
            {
                if (i == 0)
                    target.Attributes["isp_dupemanager1"] = new EntityReference("systemuser", Managers[0]);
                if (i == 1)
                    target.Attributes["isp_dupemanager2"] = new EntityReference("systemuser", Managers[1]);
                if (i == 2)
                    target.Attributes["isp_dupemanager3"] = new EntityReference("systemuser", Managers[2]);
                if (i == 3)
                    target.Attributes["isp_dupemanager4"] = new EntityReference("systemuser", Managers[3]);
            }


            #endregion
        }
    }
}
