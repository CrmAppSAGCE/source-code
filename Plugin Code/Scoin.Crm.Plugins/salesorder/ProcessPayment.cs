﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.PastelService;

namespace Scoin.Crm.Plugins.salesorder
{
    public class ProcessPayment : IPlugin
    {
        // Create the invoice in Pastel
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                var target = (Entity)context.InputParameters["Target"];
                trace.Trace("Starting the query");

                if (context.Depth > 1)
                {
                    return;
                }

                var PreImage = (context.PreEntityImages.Contains("pre") ? (Entity)context.PreEntityImages["pre"] : null);
                var orderQuery = new QueryExpression("salesorder") { ColumnSet = new ColumnSet(true) };

                Decimal AmountPaid = 0;
                orderQuery.Criteria.AddCondition("salesorderid", ConditionOperator.Equal, target.Id);
                var pastelWarehouseLink = orderQuery.AddLink("isp_pastelwarehouse", "isp_pastelwarehousecode", "isp_pastelwarehouseid");
                pastelWarehouseLink.EntityAlias = "pw";
                pastelWarehouseLink.Columns.AddColumn("isp_pastelwarehousecode");

                var pastelCompanyLink = pastelWarehouseLink.AddLink("isp_pastelcompany", "isp_pastelcompanyid", "isp_pastelcompanyid");
                pastelCompanyLink.EntityAlias = "pc";
                pastelCompanyLink.Columns.AddColumn("isp_serviceurl");
                pastelCompanyLink.Columns.AddColumn("isp_name");

                var systemuserLink = orderQuery.AddLink("systemuser", "ownerid", "systemuserid");
                systemuserLink.EntityAlias = "su";
                systemuserLink.Columns.AddColumn("isp_salescode");
                systemuserLink.Columns.AddColumn("isp_pasteluser");

                var order = service.RetrieveMultiple(orderQuery).Entities[0];
                trace.Trace("Returned the order");

                // Get the invoice line items
                QueryExpression qe = new QueryExpression("salesorderdetail");
                qe.ColumnSet = new ColumnSet(true);
                qe.Criteria.AddCondition("salesorderid", ConditionOperator.Equal, target.Id);
                var productLink = qe.AddLink("product", "productid", "productid");
                productLink.EntityAlias = "p";
                productLink.Columns.AddColumn("productnumber");
                productLink.Columns.AddColumn("currentcost");
                productLink.Columns.AddColumn("isp_vatcode");
                productLink.Columns.AddColumn("isp_producttype");

                qe.AddOrder("lineitemnumber", OrderType.Ascending);

                var isbuyback = order.GetAttributeValue<OptionSetValue>("isp_ordertype").Value != 100000000;
                var lineItems = service.RetrieveMultiple(qe).Entities;
                trace.Trace("Returned the line items");

                string Delivery = "";
                if (order.GetAttributeValue<OptionSetValue>("shippingmethodcode").Value == 100000001)
                    Delivery = "Collect";
                if (order.GetAttributeValue<OptionSetValue>("shippingmethodcode").Value == 100000000)
                    Delivery = "Courier";
                if (order.GetAttributeValue<OptionSetValue>("shippingmethodcode").Value == 100000002)
                    Delivery = "Safe Custody";
                trace.Trace("Delivery Method sorted");

                string SalesCode = (string)order.GetAttributeValue<AliasedValue>("su.isp_salescode").Value;
                //CheckToSeeIfIndependentBroker is selected
                if (order.GetAttributeValue<EntityReference>("isp_independentbroker") != null)
                {
                    trace.Trace("Independent broker NOT null");
                    var IU = service.Retrieve("isp_independentuser", order.GetAttributeValue<EntityReference>("isp_independentbroker").Id, new ColumnSet(true));
                    SalesCode = IU.GetAttributeValue<string>("isp_salescode");
                }

                trace.Trace("starting with the Invoice object");
                var invoice = new CreateDocumentRq
                {
                    DocumentType = isbuyback ? EnumDocumentType.SupplierInvoice : EnumDocumentType.CustomerInvoice
                 ,
                    Header = new DocumentHeader
                    {
                        Date = DateTime.Today
                    ,
                        ClosingDate = DateTime.Today
                    ,
                        CustomerCode = isbuyback ? "DUMP" : "CASH"
                    ,
                        IncExcl = false
                    ,
                        DeliveryAddress = new ArrayOfString()
                    ,
                        Address = new ArrayOfString()
                    ,
                        InvoiceMessage = new ArrayOfString()
                    ,
                        OnHold = false
                    ,
                        OrderNumber = order.GetAttributeValue<string>("isp_customerreference")
                    ,
                        Contact = target.Attributes.Contains("isp_contactid") ? target.GetAttributeValue<EntityReference>("isp_contactid").Name : ""
                    ,
                        SalesAnalysisCode = SalesCode
                    ,
                        ShipDeliver = Delivery
                    }
                                                    ,
                    Line = new DocumentLine[lineItems.Count]
                };

                //use the Company field here instead of the shipto_contactname
                String ContactName = "";
                String VatIdNumber = "";
                trace.Trace("fetching the DATA");
                var customer = service.Retrieve("contact", order.GetAttributeValue<EntityReference>("isp_contactid").Id, new ColumnSet(true));
                trace.Trace("Received the Contact data");

                if (order.Attributes.Contains("shipto_contactname") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_contactname")))
                    ContactName = order.GetAttributeValue<string>("shipto_contactname");
                if (order.Attributes.Contains("isp_companyname") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("isp_companyname")))
                    ContactName = order.GetAttributeValue<string>("isp_companyname");
                trace.Trace("got the customer name");

                invoice.Header.DeliveryAddress.Add(ContactName);
                if (customer.Attributes.Contains("isp_idnumber") && !string.IsNullOrWhiteSpace(customer.GetAttributeValue<string>("isp_idnumber")))
                    VatIdNumber = customer.GetAttributeValue<string>("isp_idnumber");
                if (order.Attributes.Contains("isp_vatnumber") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("isp_idnumber")))
                    VatIdNumber = order.GetAttributeValue<string>("isp_vatnumber");
                invoice.Header.DeliveryAddress.Add(VatIdNumber);
                trace.Trace("the ID Number");

                if (order.Attributes.Contains("shipto_line1") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_line1")))
                    invoice.Header.DeliveryAddress.Add(order.GetAttributeValue<string>("shipto_line1"));
                if (order.Attributes.Contains("shipto_line2") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_line2")))
                    invoice.Header.DeliveryAddress.Add(order.GetAttributeValue<string>("shipto_line2"));
                trace.Trace("Address Lines");

                String CityCode = "";
                if (order.Attributes.Contains("shipto_city") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_city")))
                    CityCode += order.GetAttributeValue<string>("shipto_city");
                trace.Trace("City");
                if (order.Attributes.Contains("shipto_postalcode") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_postalcode")))
                    CityCode += "-" + order.GetAttributeValue<string>("shipto_postalcode");
                trace.Trace("building the invoice");

                invoice.Header.DeliveryAddress.Add(CityCode);

                if (order.Attributes.Contains("shipto_telephone") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_telephone")))
                    invoice.Header.Telephone = order.GetAttributeValue<string>("shipto_telephone");
                if (order.Attributes.Contains("shipto_fax") && !string.IsNullOrWhiteSpace(order.GetAttributeValue<string>("shipto_fax")))
                    invoice.Header.Fax = order.GetAttributeValue<string>("shipto_fax");

                if (order.Attributes.Contains("discountpercentage"))
                    invoice.Header.Discount = order.GetAttributeValue<decimal>("discountpercentage");
                Decimal RunningTotal = 0;
                for (var i = 0; i < lineItems.Count; i++)
                {
                    trace.Trace("Line item " + i);
                    invoice.Line[i] = new DocumentLine
                    {
                        Code = (string)lineItems[i].GetAttributeValue<AliasedValue>("p.productnumber").Value
                    ,
                        MultiStore = (string)order.GetAttributeValue<AliasedValue>("pw.isp_pastelwarehousecode").Value
                    ,
                        CostPrice = lineItems[i].Attributes.Contains("p.currentcost") ? (decimal)lineItems[i].GetAttributeValue<AliasedValue>("p.currentcost").Value : 0
                    ,
                        Description = lineItems[i].GetAttributeValue<EntityReference>("productid").Name
                    ,
                        InclusivePrice = lineItems[i].Attributes.Contains("quantity") && lineItems[i].GetAttributeValue<decimal>("quantity") != 0 ? lineItems[i].GetAttributeValue<Money>("extendedamount").Value / lineItems[i].GetAttributeValue<decimal>("quantity") : 0
                    ,
                        Unit = ""
                    ,
                        Quantity = lineItems[i].GetAttributeValue<decimal>("quantity")
                    ,
                        UnitSellingPrice = lineItems[i].Attributes.Contains("priceperunit") ? lineItems[i].GetAttributeValue<Money>("priceperunit").Value : 0
                    ,
                        TaxType = short.Parse((lineItems[i].GetAttributeValue<AliasedValue>("p.isp_vatcode").Value.ToString()))
                    ,
                        LineType = EnumLineType.Inventory
                    };
                    RunningTotal += lineItems[i].Attributes.Contains("priceperunit") ? (Decimal.Parse(lineItems[i].GetAttributeValue<Money>("priceperunit").Value.ToString()) * lineItems[i].GetAttributeValue<decimal>("quantity")) : 0;
                }
                trace.Trace("getting the amount paid");
                AmountPaid = Decimal.Parse(target.GetAttributeValue<Money>("isp_amountpaid").Value.ToString());
                trace.Trace("isp_amountpaid to string");

                var TotalPaid = order.GetAttributeValue<Money>("isp_totalamountpaid").Value;
                trace.Trace("TotalPaid == isp_totalamountpaid");

                var PaymentQuery = new QueryExpression("isp_payment") { ColumnSet = new ColumnSet(true) };
                trace.Trace("PaymentQuery");

                PaymentQuery.Criteria.AddCondition("isp_atp", ConditionOperator.Equal, target.Id);
                trace.Trace("isp_atp target id");

                var Payments = service.RetrieveMultiple(PaymentQuery);
                trace.Trace("payment retrieve multiple");

                var PaymentNumber = Payments.Entities.Count + 1;
                trace.Trace("Payment Entity Building");
                DateTime datePaymentConfirmed = DateTime.Now;
                if (target.Contains("isp_datepaymentconfirmed"))
                {
                    datePaymentConfirmed = target.GetAttributeValue<DateTime>("isp_datepaymentconfirmed");
                }

                Entity Payment = new Entity("isp_payment");
                Payment.Attributes.Add("isp_amount", new Money(AmountPaid));
                Payment.Attributes.Add("isp_atp", new EntityReference("salesorder", target.Id));
                Payment.Attributes.Add("transactioncurrencyid", new EntityReference("transactioncurrency", new Guid("FC83003D-6C07-E111-838C-0800272BC48D")));
                Payment.Attributes.Add("isp_paymentdetails", order.GetAttributeValue<string>("ordernumber") + "-P" + PaymentNumber);
                Payment.Attributes.Add("isp_paymentreferencenumber", target.GetAttributeValue<string>("isp_paymentauthorisationnumber"));
                Payment.Attributes.Add("isp_paymentauthorisedby", new EntityReference("systemuser", context.UserId));
                Payment.Attributes.Add("isp_chequepayment", target.GetAttributeValue<bool>("isp_chequeuepayment"));
                Payment.Attributes.Add("isp_paymentmethod", target.GetAttributeValue<OptionSetValue>("isp_paymentmethods"));
                Payment.Attributes.Add("isp_datepaymentreceived", datePaymentConfirmed);
                service.Create(Payment);

                target.Attributes["isp_amountpaid"] = new Money(0);
                target.Attributes["isp_paymentauthorisationnumber"] = "";
                target.Attributes["isp_chequeuepayment"] = false;
                target.Attributes["isp_paymentreceiptconfirmedby"] = null;
                target.Attributes["isp_datepaymentconfirmed"] = null;

                if (AmountPaid + TotalPaid >= RunningTotal)
                {
                    trace.Trace("final");
                    Entity InvoiceToUpdate = new Entity("invoice");
                    InvoiceToUpdate.Attributes.Add("invoiceid", order.GetAttributeValue<EntityReference>("isp_invoice").Id);
                    InvoiceToUpdate.Attributes.Add("isp_fullypaid", true);
                    service.Update(InvoiceToUpdate);
                    Microsoft.Crm.Sdk.Messages.SetStateRequest LockSalesOrder = new Microsoft.Crm.Sdk.Messages.SetStateRequest
                    {
                        EntityMoniker = new EntityReference("salesorder", target.Id),
                        State = new OptionSetValue { Value = 4 },
                        Status = new OptionSetValue { Value = 100003 }
                    };
                    trace.Trace("locking");
                    service.Execute(LockSalesOrder);
                }
            }
        }
    }
}
