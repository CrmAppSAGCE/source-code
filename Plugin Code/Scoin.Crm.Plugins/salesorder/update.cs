﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.salesorder
{
    public class update : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);

            EntityHelper helper = new EntityHelper(context, service);

            #region StatusCode Change
            trace.Trace("1");
            if (helper.HasChanged("statuscode"))
            {
                if (helper.GetAttributeValue<OptionSetValue>("statuscode").Value == 863300010)
                {
                    trace.Trace("1");
                    CancelSalesOrderRequest req = new CancelSalesOrderRequest();
                    OrderClose orderClose = new OrderClose();
                    orderClose.SalesOrderId = new EntityReference("salesorder", target.Id);
                    orderClose.Subject = "Manager Declined";
                    req.OrderClose = orderClose;
                    OptionSetValue o = new OptionSetValue();
                    o.Value = 863300008;
                    req.Status = o;
                    CancelSalesOrderResponse resp = (CancelSalesOrderResponse)service.Execute(req);
                }
                if (target.GetAttributeValue<OptionSetValue>("statuscode").Value == 1)
                {
                    trace.Trace("2");
                    SalesOrder salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                                             where so.SalesOrderId.Value == context.PrimaryEntityId
                                             select so).FirstOrDefault();
                    if (salesOrder.IsPriceLocked != null && salesOrder.IsPriceLocked.Value)
                    {
                        UnlockSalesOrderPricingRequest unlock = new UnlockSalesOrderPricingRequest();
                        unlock.SalesOrderId = context.PrimaryEntityId;
                        UnlockSalesOrderPricingResponse unlockResponse = (UnlockSalesOrderPricingResponse)service.Execute(unlock);
                    }
                }
                else if (target.GetAttributeValue<OptionSetValue>("statuscode").Value == 863300000)//for each of the the salesorderproducts connected to the ATP
                {
                    var orderProducts = (from sod in orgContext.CreateQuery<SalesOrderDetail>()
                                         join p in orgContext.CreateQuery<Product>()
                                         on sod.ProductId.Id equals p.ProductId.Value
                                         join sc in orgContext.CreateQuery<isp_stockcheck>()
                                         on sod.isp_Stock.Id equals sc.isp_stockcheckId.Value
                                         join pw in orgContext.CreateQuery<isp_pastelwarehouse>()
                                         on sc.isp_PastelWarehouse.Id equals pw.isp_pastelwarehouseId.Value
                                         join s in orgContext.CreateQuery<SalesOrder>()
                                         on sod.SalesOrderId.Id equals s.SalesOrderId.Value
                                         where sod.SalesOrderId.Id == context.PrimaryEntityId
                                         where s.StatusCode.Value == 863300000
                                         select new
                                         {
                                             salesorderdetailid = sod.SalesOrderDetailId,
                                             quantity = sod.Quantity,
                                             lineitemnumber = sod.LineItemNumber,
                                             isp_product = sod.ProductId,
                                             productId = p.ProductId,
                                             productnumber = p.ProductNumber,
                                             currentcost = p.CurrentCost,
                                             isp_vatcode = p.isp_vatcode,
                                             isp_producttype = p.isp_ProductType,
                                             isp_stockcheckId = sc.isp_stockcheckId,
                                             isp_stocklevel = sc.isp_StockLevel,
                                             isp_productcode = sc.isp_ProductCode,
                                             isp_pastelwarehouse = sc.isp_PastelWarehouse,
                                             salesorderid = s.SalesOrderId
                                         }).ToList();
                    trace.Trace("3");
                    for (int i = 0; i < orderProducts.Count; i++)
                    {
                        trace.Trace("4");
                        trace.Trace(orderProducts.Count.ToString());
                        trace.Trace(orderProducts[i].isp_stocklevel.Value.ToString());
                        trace.Trace(orderProducts[i].quantity.Value.ToString());
                        if (orderProducts[i].quantity.Value > orderProducts[i].isp_stocklevel.Value)
                        {
                            throw new Exception("Not enough stock for product : \r\n\r\nProduct Code: " + orderProducts[i].isp_productcode + "\r\n\r\nProduct Name: " + orderProducts[i].isp_product.Name + "\r\n\r\nQuantity Requested on ATP :  " + orderProducts[i].quantity.Value + "\r\n\r\nLocal stock in Warehouse :  " + orderProducts[i].isp_stocklevel.Value);  //+ "\r\n\r\nReserved Stock By Management :  " + Decimal.ToInt32(RunningTotal) + "\r\nSellable Stock : " + (Decimal.ToInt32(Decimal.Parse(OrderedProducts[i].GetAttributeValue<AliasedValue>("psl.isp_stocklevel").Value.ToString())) - Decimal.ToInt32(RunningTotal))
                        }
                    }
                    if (helper.GetAttributeValue<Boolean>("ispricelocked") != true)
                    {
                        target.Attributes["ispricelocked"] = true;

                        target.Attributes["isp_approvedby"] = new EntityReference("systemuser", context.UserId);
                        target.Attributes["isp_dateapproved"] = DateTime.Now;
                        trace.Trace("5");
                    }
                }
                else if (target.GetAttributeValue<OptionSetValue>("statuscode").Value == 2)//Sent for approval
                {
                    target.Attributes["submitdate"] = DateTime.Now;
                    trace.Trace("6");
                }
                else if (target.GetAttributeValue<OptionSetValue>("statuscode").Value == 863300009)//Partial Payment
                {

                }
                else if (target.GetAttributeValue<OptionSetValue>("statuscode").Value == 863300011)//Request Clearance
                {
                    if (target.GetAttributeValue<Boolean>("ispricelocked") == false)
                    {
                        target.Attributes["ispricelocked"] = true;
                    }
                }
            }
            #endregion
            if (helper.HasChanged("isp_partialpaymentdate"))
            {
                if (helper.GetAttributeValue<Boolean>("ispricelocked") == false)
                {
                    target.Attributes["ispricelocked"] = true;
                }
            }
        }
    }
}
