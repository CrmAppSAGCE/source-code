﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Scoin.Crm.Plugins.salesorder
{
    public class precreate : IPlugin
    {

      public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];



            if (target.Contains("ordernumber") && !target.Contains("name"))
                target.Attributes.Add("name", target.GetAttributeValue<string>("ordernumber"));

            if (target.Attributes.ContainsKey("isp_customerreference"))
            {
                if (target.Attributes["isp_customerreference"] != null && !string.IsNullOrWhiteSpace(target.Attributes["isp_customerreference"].ToString()))
                {
                    return;
                }
            }
            if (target.Attributes.ContainsKey("isp_datetoescalate"))
            {
                if (target.Attributes["isp_datetoescalate"] != null && !string.IsNullOrWhiteSpace(target.Attributes["isp_datetoescalate"].ToString()))
                {
                    return;
                }
            }
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            string NewId = "";
          //how to get the entity form

         string  surname = "";
         // if(target.GetAttributeValue<string>("firstname").Length > 0)
         var contactEntity = service.Retrieve("contact", ((EntityReference)target.Attributes["isp_contactid"]).Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));
         if (contactEntity.Attributes.ContainsKey("lastname"))
         {
            if (contactEntity.Attributes["lastname"] != null && !string.IsNullOrWhiteSpace(contactEntity.Attributes["lastname"].ToString()))
                surname = contactEntity.Attributes["lastname"].ToString();
         }

          //Surname(maxlength(12)-ATPNumber

         if (surname.Length > 12)
         {
             surname = surname.Substring(0,11);
         }
         string[] Ref = target.Attributes["ordernumber"].ToString().Split('-');
         if (Ref.Length >= 1)
         {
             NewId =  Ref[1] + "-" + surname;
         }
         else
         {
             NewId = surname;
         }
          //NewId = target.Attributes["ordernumber"].ToString() + " - " + firstName;
            //used for the precreate
            if (!target.Attributes.ContainsKey("isp_customercode"))
                target.Attributes.Add("isp_customerreference", NewId);
            else
                target.Attributes["isp_customerreference"] = NewId;

          
         

          //Updating the closedDate, if the time is after 3PM two days are added instead of one
            DateTime NewDate = DateTime.Now.Date;
            if (DateTime.Now.Hour > 15)
            {
                NewDate = NewDate.AddDays(2);
            }
            else
            {
                NewDate = NewDate.AddDays(1);
            }
          

            if (!target.Attributes.ContainsKey("isp_datetoescalate"))
                target.Attributes.Add("isp_datetoescalate", NewDate);
            else
                target.Attributes["isp_datetoescalate"] = NewDate;
        }
    }
}
