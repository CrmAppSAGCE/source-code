﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.creditnote
{
    public class setstatedynamicentity : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            decimal rareCommission = 0;
            int bullionCoins = 0;
            decimal bullionTotal = 0;
            Guid shopMonthCommisionId = Guid.Empty;
            Guid brokerMonthCommisionId = Guid.Empty;
            decimal paymentAmount = 0;
            decimal doubleComm = 0;

            trace.Trace("1");
            if (context.InputParameters.Contains("Status"))
            {
                trace.Trace("Contains status");
                if (((OptionSetValue)context.InputParameters["Status"]).Value == 863300003)
                {
                    trace.Trace("status = 863300003");
                    isp_creditnote creditNote = (from c in orgContext.CreateQuery<isp_creditnote>()
                                                 where c.isp_creditnoteId.Value == context.PrimaryEntityId
                                                 select new isp_creditnote
                                                 {
                                                     isp_creditnoteId = c.isp_creditnoteId,
                                                     isp_Invoice = c.isp_Invoice,
                                                     isp_ProcessedOn = c.isp_ProcessedOn,
                                                     isp_creditnotenumber = c.isp_creditnotenumber,
                                                     isp_NewInvoiceRequired = c.isp_NewInvoiceRequired,
                                                     OwnerId = c.OwnerId
                                                 }).FirstOrDefault();
                    trace.Trace("Got Credit Note");

                    SalesOrder salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                                             where so.isp_Invoice.Id == creditNote.isp_Invoice.Id
                                             select new SalesOrder
                                       {
                                           SalesOrderId = so.SalesOrderId,
                                           isp_PaymentMethods = so.isp_PaymentMethods,
                                           isp_ContactId = so.isp_ContactId,
                                           OwnerId = so.OwnerId,
                                           isp_PartialPaymentDate = so.isp_PartialPaymentDate
                                       }).FirstOrDefault();
                    trace.Trace("Got Salesorder");

                    List<Scoin.Crm.Plugins.GeneratedCode.isp_payment> payments = (from p in orgContext.CreateQuery<Scoin.Crm.Plugins.GeneratedCode.isp_payment>()
                                                                                  where p.isp_Invoice.Id == creditNote.isp_Invoice.Id
                                                                                  && p.statecode.Value == isp_paymentState.Active
                                                                                  && p.isp_CreditNoteId == null
                                                                                  select new Scoin.Crm.Plugins.GeneratedCode.isp_payment
                                                                                  {
                                                                                      isp_paymentId = p.isp_paymentId,
                                                                                      isp_Amount = p.isp_Amount,
                                                                                      isp_RareCoinsTurnover = p.isp_RareCoinsTurnover,
                                                                                      isp_BullionCoins = p.isp_BullionCoins,
                                                                                      isp_BullionTurnover = p.isp_BullionTurnover,
                                                                                      isp_DoubleCommission = p.isp_DoubleCommission,
                                                                                      isp_atp = p.isp_atp,
                                                                                      isp_Invoice = p.isp_Invoice,
                                                                                      isp_BrokerCommissionId = p.isp_BrokerCommissionId,
                                                                                      isp_ShopCommissionId = p.isp_ShopCommissionId,
                                                                                      isp_Customer = p.isp_Customer
                                                                                  }).ToList();
                    trace.Trace("Got Payments");

                    SystemUser user = (from u in orgContext.CreateQuery<SystemUser>()
                                       where u.SystemUserId.Value == salesOrder.OwnerId.Id
                                       select new SystemUser
                                       {
                                           SystemUserId = u.SystemUserId,
                                           isp_usertype = u.isp_usertype
                                       }).FirstOrDefault();
                    trace.Trace("Got all data");
                    if (creditNote.isp_NewInvoiceRequired.Value)
                    {
                        trace.Trace("New invoice required");
                        //Change the Status of the ATP to Active - Credit Noted
                        SetStateRequest setSalesOrderStateRequest = new SetStateRequest()
                        {
                            EntityMoniker = new EntityReference
                            {
                                Id = salesOrder.SalesOrderId.Value,
                                LogicalName = SalesOrder.EntityLogicalName,
                            },
                            State = new OptionSetValue((int)SalesOrderState.Active),
                            Status = new OptionSetValue(863300013)
                        };
                        service.Execute(setSalesOrderStateRequest);

                        //Unlock the prices
                        UnlockSalesOrderPricingRequest req = new UnlockSalesOrderPricingRequest();
                        req.SalesOrderId = salesOrder.SalesOrderId.Value;
                        UnlockSalesOrderPricingResponse resp = (UnlockSalesOrderPricingResponse)service.Execute(req);

                        salesOrder.isp_PaymentMethods = new OptionSetValue(863300005);
                        salesOrder.isp_PartialPaymentDate = null;
                        orgContext.UpdateObject(salesOrder);
                        orgContext.SaveChanges();
                    }
                    if (!creditNote.isp_NewInvoiceRequired.Value)
                    {
                        trace.Trace("Credit note not required");
                        SetStateRequest setSalesOrderStateRequest = new SetStateRequest()
                        {
                            EntityMoniker = new EntityReference
                            {
                                Id = salesOrder.SalesOrderId.Value,
                                LogicalName = SalesOrder.EntityLogicalName,
                            },
                            State = new OptionSetValue((int)SalesOrderState.Active),
                            Status = new OptionSetValue(863300014)
                        };
                        trace.Trace("Executing SalesorderStateRequest");
                        service.Execute(setSalesOrderStateRequest);
                    }
                    trace.Trace("Checking Payments");
                    if (payments.Count > 0)
                    {
                        trace.Trace("Payment count is bigger than 0");
                        GeneratedCode.isp_payment reversePayment = new GeneratedCode.isp_payment();
                        Guid commissionId = Guid.Empty;

                        foreach (Scoin.Crm.Plugins.GeneratedCode.isp_payment payment in payments)
                        {
                            if (payment.isp_BullionCoins.Value > 0)
                            {
                                bullionCoins -= payment.isp_BullionCoins.Value;
                            }
                            if (payment.isp_RareCoinsTurnover.Value > 0)
                            {
                                rareCommission -= payment.isp_RareCoinsTurnover.Value;
                            }
                            if (payment.isp_BullionTurnover.Value > 0)
                            {
                                bullionTotal -= payment.isp_BullionTurnover.Value;
                            }
                            if (payment.isp_Amount.Value > 0)
                            {
                                paymentAmount -= payment.isp_Amount.Value;
                            }
                            if (payment.isp_DoubleCommission != null && payment.isp_DoubleCommission.Value > 0)
                            {
                                doubleComm -= payment.isp_DoubleCommission.Value;
                            }
                            trace.Trace("Got all coints");
                            payment.isp_CreditNoteId = new EntityReference(isp_creditnote.EntityLogicalName, creditNote.isp_creditnoteId.Value);
                            payment.statuscode = new OptionSetValue(863300001);
                            orgContext.ClearChanges();
                            orgContext.Attach(payment);
                            orgContext.UpdateObject(payment);
                            orgContext.SaveChanges();
                            trace.Trace("Updated Payment");
                        }
                        trace.Trace("Creating Reverse Payment");
                        if (salesOrder.isp_ContactId != null)
                        {
                            reversePayment.isp_Customer = salesOrder.isp_ContactId;
                        }
                        reversePayment.isp_BullionCoins = bullionCoins;
                        reversePayment.isp_BullionTurnover = new Money(bullionTotal);
                        reversePayment.isp_RareCoinsTurnover = new Money(rareCommission);
                        reversePayment.isp_DoubleCommission = new Money(doubleComm);
                        reversePayment.isp_Amount = new Money(paymentAmount);
                        reversePayment.isp_atp = payments[0].isp_atp;
                        reversePayment.isp_Invoice = payments[0].isp_Invoice;
                        reversePayment.OwnerId = creditNote.OwnerId;
                        reversePayment.statuscode = new OptionSetValue(863300001);
                        reversePayment.isp_CreditNoteId = new EntityReference(isp_creditnote.EntityLogicalName, creditNote.isp_creditnoteId.Value);
                        reversePayment.isp_PaymentreferenceNumber = "Reversal: Credit Note " + creditNote.isp_creditnotenumber;
                        orgContext.AddObject(reversePayment);
                        orgContext.SaveChanges();
                        trace.Trace("Added Reverse Payment");
                    }
                }
            }
        }
    }
}
