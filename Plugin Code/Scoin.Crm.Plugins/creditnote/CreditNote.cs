﻿using System;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.PastelService;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;
using Microsoft.Crm.Sdk.Messages;

namespace Scoin.Crm.Plugins.creditnote
{
    public class CreditNote : IPlugin
    {
        // Create the invoice in Pastel
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                if (context.Depth > 1)
                {
                    return;
                }

                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                Entity target = (Entity)context.InputParameters["Target"];
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

                //Retrieve the Credit note being used
                isp_creditnote creditNote = (from c in orgContext.CreateQuery<isp_creditnote>()
                                             where c.isp_creditnoteId.Value == context.PrimaryEntityId
                                             select new isp_creditnote
                                             {
                                                 isp_creditnoteId = c.isp_creditnoteId,
                                                 isp_creditnotenumber = c.isp_creditnotenumber,
                                                 isp_Invoice = c.isp_Invoice,
                                                 OwnerId = c.OwnerId
                                             }).FirstOrDefault();
                trace.Trace("Got credit note");

                if (creditNote.isp_creditnotenumber != "Pending")
                {
                    throw new Exception("The Creditnote has already been processed.");
                }
                //Retrieve the invoice linked to the credit note
                Invoice invoice = (from i in orgContext.CreateQuery<Invoice>()
                                   where i.InvoiceId.Value == creditNote.isp_Invoice.Id
                                   select new Invoice
                                   {
                                       InvoiceId = i.InvoiceId,
                                       Name = i.Name,
                                       isp_atp = i.isp_atp,
                                       OwnerId = i.OwnerId,
                                       ShippingMethodCode = i.ShippingMethodCode,
                                       isp_pastelwarehousecode = i.isp_pastelwarehousecode,
                                       isp_contactname = i.isp_contactname,
                                       isp_IndependantBroker = i.isp_IndependantBroker,
                                       isp_CustomerReferenceNumber = i.isp_CustomerReferenceNumber,
                                       isp_yourref = i.isp_yourref,
                                       isp_idnumber = i.isp_idnumber,
                                       isp_taxref = i.isp_taxref,
                                       ShipTo_Line1 = i.ShipTo_Line1,
                                       ShipTo_Line2 = i.ShipTo_Line2,
                                       ShipTo_City = i.ShipTo_City,
                                       ShipTo_PostalCode = i.ShipTo_PostalCode,
                                       ShipTo_Telephone = i.ShipTo_Telephone,
                                       ShipTo_Fax = i.ShipTo_Fax,
                                       DiscountPercentage = i.DiscountPercentage
                                   }).FirstOrDefault();
                trace.Trace("Got invoice");

                //Retrieve the user linked to the pastelwarehouse and pastelcompany entities
                var user = (from s in orgContext.CreateQuery<SystemUser>()
                            join pw in orgContext.CreateQuery<isp_pastelwarehouse>()
                            on s.isp_pastelwarehouse.Id equals pw.isp_pastelwarehouseId.Value
                            join pc in orgContext.CreateQuery<isp_PastelCompany>()
                            on pw.isp_pastelcompanyid.Id equals pc.isp_PastelCompanyId.Value
                            where s.SystemUserId.Value == creditNote.OwnerId.Id
                            select new
                            {
                                SystemUserId = s.SystemUserId,
                                isp_salescode = s.isp_salescode,
                                isp_pasteluser = s.isp_PastelUser,
                                isp_customercode = s.isp_CustomerCode,
                                isp_pastelwarehouseid = pw.isp_pastelwarehouseId,
                                isp_pastelwarehousecode = pw.isp_pastelwarehousecode,
                                isp_pastelcompanyid = pc.isp_PastelCompanyId,
                                isp_serviceurl = pc.isp_ServiceUrl,
                                isp_name = pc.isp_name
                            }).FirstOrDefault();
                trace.Trace("Got user");


                //Retrieve the invoiceproducts linked to product and stock check and order them by line item number to get a stock product as the top line product to get their warehouse if service items exist aswell
                //The warehouse used must always be from a stock product unless a service item has no other stock items

                var invoiceProducts = (from ip in orgContext.CreateQuery<InvoiceDetail>()
                                       join p in orgContext.CreateQuery<Product>()
                                       on ip.ProductId.Id equals p.ProductId.Value
                                       where ip.InvoiceId.Id == invoice.InvoiceId.Value
                                       select new
                                       {
                                           invoicedetailid = ip.InvoiceDetailId,
                                           productentityId = ip.ProductId,
                                           quantity = ip.Quantity,
                                           extendedamount = ip.ExtendedAmount,
                                           priceperunit = ip.PricePerUnit,
                                           lineitemnumber = ip.LineItemNumber,
                                           productId = p.ProductId,
                                           productnumber = p.ProductNumber,
                                           currentcost = p.CurrentCost,
                                           isp_vatcode = p.isp_vatcode,
                                           isp_producttype = p.isp_ProductType,
                                           isp_stockoriginalpastelwarehouse = ip.isp_StockOriginalPastelWarehouse
                                       }).ToList();
                trace.Trace("Got invoice products");
                invoiceProducts = invoiceProducts.OrderBy(ip => ip.lineitemnumber).ToList();

                var invoiceStockProducts = (from ip in orgContext.CreateQuery<InvoiceDetail>()
                                            join p in orgContext.CreateQuery<Product>()
                                            on ip.ProductId.Id equals p.ProductId.Value
                                            join sc in orgContext.CreateQuery<isp_stockcheck>()
                                            on ip.isp_stockcheckid.Id equals sc.isp_stockcheckId.Value
                                            where ip.InvoiceId.Id == invoice.InvoiceId.Value
                                            select new
                                            {
                                                invoicedetailid = ip.InvoiceDetailId,
                                                productentityId = ip.ProductId,
                                                quantity = ip.Quantity,
                                                extendedamount = ip.ExtendedAmount,
                                                priceperunit = ip.PricePerUnit,
                                                lineitemnumber = ip.LineItemNumber,
                                                productId = p.ProductId,
                                                productnumber = p.ProductNumber,
                                                currentcost = p.CurrentCost,
                                                isp_vatcode = p.isp_vatcode,
                                                isp_producttype = p.isp_ProductType,
                                                isp_stockcheckId = sc.isp_stockcheckId,
                                                isp_stocklevel = sc.isp_StockLevel,
                                                isp_pastelwarehouse = sc.isp_PastelWarehouse,
                                                isp_vat = sc.isp_vat
                                            }).ToList();
                invoiceStockProducts = invoiceStockProducts.OrderBy(ip => ip.lineitemnumber).ToList();
                trace.Trace("Got invoice stock products");

                Guid prevWarehouse = invoice.isp_pastelwarehousecode.Id;
                //Prebooked not used anymore
                //if (invoiceProducts[0].isp_stockoriginalpastelwarehouse != null)
                //{
                //    prevWarehouse = invoiceProducts[0].isp_stockoriginalpastelwarehouse.Id;
                //    trace.Trace("Got stock warehouse products");
                //}

                var StoreValue = user.isp_pastelwarehousecode;
                trace.Trace("Got store value");

                #region Delivery Method
                //Check what the delivery method is
                string Delivery = string.Empty;
                if (invoice.ShippingMethodCode == null)
                {
                    throw new InvalidPluginExecutionException("Please Fill in the Shipping method on the invoice and try again");
                }
                if (invoice.ShippingMethodCode.Value == 100000000)
                {
                    Delivery = "Courier";
                }
                if (invoice.ShippingMethodCode.Value == 100000001)
                {
                    Delivery = "Collect";
                }
                if (invoice.ShippingMethodCode.Value == 100000002)
                {
                    Delivery = "Safe Custody";
                }
                trace.Trace("Got shipping method");
                #endregion
                string SalesCode = user.isp_salescode;
                trace.Trace("Got sales code");
                if (invoice.isp_IndependantBroker != null)
                {
                    //Retreive the independant broker
                    isp_independentuser independantuser = (from i in orgContext.CreateQuery<isp_independentuser>()
                                                           where i.isp_independentuserId.Value == invoice.isp_IndependantBroker.Id
                                                           select new isp_independentuser
                                                           {
                                                               isp_independentuserId = i.isp_independentuserId,
                                                               isp_SalesCode = i.isp_SalesCode
                                                           }).FirstOrDefault();
                    trace.Trace("Got independant broker");
                    SalesCode = independantuser.isp_SalesCode;
                }

                #region Customer code

                string CustomerCode = "CASH";
                //Customer code is cash unless the customer code contains a value
                if (user.isp_customercode != null)
                {
                    CustomerCode = user.isp_customercode;
                    trace.Trace("Got customer code");
                }
                //Retrieve defualts
                isp_default defaults = (from d in orgContext.CreateQuery<isp_default>()
                                        select d).FirstOrDefault();
                trace.Trace("Got defaults");
                //Retrieve the pastelwarehouse
                isp_pastelwarehouse pastelwarehouse = (from pw in orgContext.CreateQuery<isp_pastelwarehouse>()
                                                       where pw.isp_pastelwarehouseId == prevWarehouse
                                                       select new isp_pastelwarehouse
                                                       {
                                                           isp_pastelwarehouseId = pw.isp_pastelwarehouseId,
                                                           isp_pastelwarehousecode = pw.isp_pastelwarehousecode
                                                       }).FirstOrDefault();
                trace.Trace("Got ipastelwarehouse");

                //check if the warehouse is prebooked
                bool preBooked = false;
                if (defaults.isp_PBPastelWarehouse == pastelwarehouse.isp_pastelwarehousecode)
                {
                    preBooked = true;
                    trace.Trace("Prebooked is true");
                }
                //invoice number + peron's surname
                #endregion
                string[] Ref = invoice.isp_atp.Name.Split('-');
                string orderNumber = Ref[1] + "-" + invoice.Name;
                //Create the pastel header
                var creditNoteDocument = new CreateDocumentRq
                {
                    DocumentType = EnumDocumentType.CustomerCreditNote,
                    Header = new DocumentHeader
                    {
                        Date = DateTime.Today,
                        ClosingDate = DateTime.Today,
                        CustomerCode = preBooked ? defaults.isp_PreBookedCustomerCode : CustomerCode,
                        IncExcl = false,
                        DeliveryAddress = new ArrayOfString(),
                        Address = new ArrayOfString(),
                        InvoiceMessage = new ArrayOfString(),
                        OnHold = false,
                        OrderNumber = orderNumber,
                        Contact = string.Empty,
                        SalesAnalysisCode = SalesCode,
                        ShipDeliver = Delivery
                    },
                    Line = new DocumentLine[invoiceProducts.Count],
                };
                trace.Trace("created header");

                String ContactName = string.Empty;
                String VatIdNumber = string.Empty;
                String CityCode = string.Empty;

                //Populate the delivery address sent to pastel
                if (!string.IsNullOrEmpty(invoice.isp_contactname))
                {
                    ContactName = invoice.isp_contactname;
                }
                creditNoteDocument.Header.DeliveryAddress.Add(ContactName);
                trace.Trace("added contact name");

                if (!string.IsNullOrEmpty(invoice.isp_idnumber))
                {
                    VatIdNumber = invoice.isp_idnumber;
                }
                if (!string.IsNullOrEmpty(invoice.isp_taxref))
                {
                    VatIdNumber = invoice.isp_taxref;
                }
                creditNoteDocument.Header.DeliveryAddress.Add(VatIdNumber);
                trace.Trace("created delivery address");

                if (!string.IsNullOrEmpty(invoice.ShipTo_Line1))
                {
                    creditNoteDocument.Header.DeliveryAddress.Add(invoice.ShipTo_Line1);
                }
                if (!string.IsNullOrEmpty(invoice.ShipTo_Line2))
                {
                    creditNoteDocument.Header.DeliveryAddress.Add(invoice.ShipTo_Line2);
                }
                if (!string.IsNullOrEmpty(invoice.ShipTo_City))
                {
                    CityCode += invoice.ShipTo_City;
                }
                if (!string.IsNullOrEmpty(invoice.ShipTo_PostalCode))
                {
                    CityCode += "-" + invoice.ShipTo_PostalCode;
                }
                creditNoteDocument.Header.DeliveryAddress.Add(CityCode);
                trace.Trace("added city code");

                //Update the telephone header
                if (!string.IsNullOrEmpty(invoice.ShipTo_Telephone))
                {
                    creditNoteDocument.Header.Telephone = invoice.ShipTo_Telephone;
                }
                trace.Trace("added telephone");
                //Update the fax header
                if (!string.IsNullOrEmpty(invoice.ShipTo_Fax))
                {
                    creditNoteDocument.Header.Fax = invoice.ShipTo_Fax;
                }
                trace.Trace("added ship to fax");
                //Update the discount header
                if (invoice.DiscountPercentage != null)
                {
                    creditNoteDocument.Header.Discount = invoice.DiscountPercentage.Value;
                }
                trace.Trace("added discount percentage");
                //Build up the line of the invoice products to be sent through to pastel
                for (var i = 0; i < invoiceProducts.Count; i++)
                {
                    creditNoteDocument.Line[i] = new DocumentLine
                    {
                        Code = (string)invoiceProducts[i].productnumber
                    ,
                        MultiStore = preBooked ? defaults.isp_PBPastelWarehouse : user.isp_pastelwarehousecode
                    ,
                        CostPrice = invoiceProducts[i].currentcost != null ? invoiceProducts[i].currentcost.Value : 0
                    ,
                        Description = invoiceProducts[i].productentityId.Name
                    ,
                        InclusivePrice = invoiceProducts[i].quantity != null && invoiceProducts[i].quantity != 0 ? invoiceProducts[i].priceperunit.Value : 0//* invoiceProducts[i].quantity.Value : 0
                    ,
                        Unit = string.Empty
                    ,
                        Quantity = invoiceProducts[i].quantity.Value
                    ,
                        UnitSellingPrice = invoiceProducts[i].priceperunit != null ? invoiceProducts[i].priceperunit.Value : 0
                    ,
                        TaxType = short.Parse((invoiceProducts[i].isp_vatcode.Value.ToString()))
                    ,
                        LineType = EnumLineType.Inventory
                    };
                }
                trace.Trace("done discount lines");
                //Send through the details to pastel
                if (user.isp_serviceurl != null)
                {
                    trace.Trace("Got service url");
                    var binding = new System.ServiceModel.BasicHttpBinding(System.ServiceModel.BasicHttpSecurityMode.None) { CloseTimeout = new TimeSpan(0, 0, 25), OpenTimeout = new TimeSpan(0, 0, 25), SendTimeout = new TimeSpan(0, 0, 25), ReceiveTimeout = new TimeSpan(0, 0, 25) };
                    trace.Trace("Got binding");
                    PastelServiceSoapClient client = new PastelServiceSoapClient(binding, new System.ServiceModel.EndpointAddress(string.Format(user.isp_serviceurl, "PastelService.asmx")));
                    trace.Trace("Got pastel client");
                    string PastelCompany = user.isp_name;
                    trace.Trace("Got pastel company");

                    short PastelUser = preBooked ? Int16.Parse(defaults.isp_PreBookedUserId.Value.ToString()) : Int16.Parse(user.isp_pasteluser);
                    trace.Trace("Got pastel user");
                    var InvoiceToUpdate = new Entity("invoice");
                    InvoiceToUpdate.Attributes.Add("invoiceid", creditNote.isp_Invoice.Id);
                    InvoiceToUpdate.Attributes.Add("isp_creditnote", new EntityReference("isp_creditnote", target.Id));
                    InvoiceToUpdate.Attributes.Add("isp_creditnotenumber", "");
                    service.Update(InvoiceToUpdate);
                    trace.Trace("Updated invoice");
                    service.Execute(new Microsoft.Crm.Sdk.Messages.SetStateRequest { EntityMoniker = creditNote.isp_Invoice, Status = new OptionSetValue(100003), State = new OptionSetValue(3) });
                    trace.Trace("Setting state");
                    string CreditNoteOrderNumber = creditNoteDocument.Header.OrderNumber;
                    if (creditNoteDocument.Header.OrderNumber.Length > 15)
                    {
                        CreditNoteOrderNumber = creditNoteDocument.Header.OrderNumber.Substring(0, 15);
                    }
                    var DocExists = client.GetRecord("acchisth", 11, CreditNoteOrderNumber, PastelCompany, PastelUser, 4);
                    trace.Trace("Got doc exists");
                    trace.Trace(creditNoteDocument.Header.OrderNumber);
                    if (DocExists != null)
                    {
                        trace.Trace("doc exists is not null");
                        trace.Trace(DocExists.ToString());
                        if (DocExists.Substring(0, 1) == "0")
                        {
                            var ExistingDoc = DocExists.Substring(2);
                            if (target.Contains("isp_creditnotenumber"))
                                target.Attributes["isp_creditnotenumber"] = ExistingDoc;
                            else
                                target.Attributes.Add("isp_creditnotenumber", ExistingDoc);
                            return;
                        }
                    }
                    trace.Trace("Queried.");
                    var rs = client.CreateDocument(creditNoteDocument, PastelCompany, PastelUser);
                    trace.Trace("Create document in pastel");
                    if (rs.Success)
                    {
                        trace.Trace("Call successful");
                        rs.Output = rs.Output.Trim();
                        trace.Trace("Trimming output");
                        var CreditNoteNumber = rs.Output;
                        if (CreditNoteNumber.Contains("IC") || CreditNoteNumber.Contains("CN") || CreditNoteNumber.Contains("CT") || CreditNoteNumber.Contains("IT"))
                        {
                            for (int i = 0; i < invoiceStockProducts.Count; i++)
                            {
                                //Get all the stock check products linked to the invoice via the invoice stock product
                                trace.Trace("looping through the invoice products");
                                isp_stockcheck stock = (from s in orgContext.CreateQuery<isp_stockcheck>()
                                                        join pw in orgContext.CreateQuery<isp_pastelwarehouse>()
                                                        on s.isp_PastelWarehouse.Id equals pw.isp_pastelwarehouseId.Value
                                                        where s.isp_stockcheckId.Value == invoiceStockProducts[i].isp_stockcheckId.Value
                                                        select new isp_stockcheck
                                                        {
                                                            isp_stockcheckId = s.isp_stockcheckId,
                                                            isp_StockLevel = s.isp_StockLevel
                                                        }).FirstOrDefault();
                                trace.Trace("Got the stock");
                                if (stock.isp_StockLevel != null)
                                {
                                    //Get all the stock items and for each calculate the new stock level by taking the old stock level and adding the quantity of the credit noted stock item
                                    stock.isp_StockLevel = stock.isp_StockLevel + Convert.ToInt32(invoiceStockProducts[i].quantity.Value);
                                    trace.Trace("Calculate new stock level");
                                    orgContext.UpdateObject(stock);
                                    orgContext.SaveChanges();
                                    trace.Trace("Updated stock level");
                                }
                            }
                        }
                        if (target.Contains("isp_creditnotenumber"))
                        {
                            target.Attributes["isp_creditnotenumber"] = CreditNoteNumber;
                            trace.Trace("Updated credit note number1");
                        }
                        else
                        {
                            target.Attributes.Add("isp_creditnotenumber", CreditNoteNumber);
                            trace.Trace("Updated credit note number2");
                        }
                        service.Update(target);
                        trace.Trace("Updating target");
                    }
                    else
                        throw new Exception("Error from Pastel: \r\n" + rs.ErrorMessage);
                }
            }
        }
    }
}
