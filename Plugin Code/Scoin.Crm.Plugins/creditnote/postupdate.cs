﻿using System;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.creditnote
{
    public class postupdate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                if (context.Depth > 1)
                {
                    return;
                }
                var target = (Entity)context.InputParameters["Target"];
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);

                EntityHelper helper = new EntityHelper(context, service);
                Entity preMessageImage = null;
                Entity postMessageImage = null;

                if (context.PreEntityImages.Contains("PreImage") && context.PreEntityImages["PreImage"] is Entity)
                {
                    preMessageImage = (Entity)context.PreEntityImages["PreImage"];
                }
                if (context.PostEntityImages.Contains("PostImage") && context.PostEntityImages["PostImage"] is Entity)
                {
                    postMessageImage = (Entity)context.PostEntityImages["PostImage"];
                }
                if (postMessageImage != null && preMessageImage != null)
                {
                    if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_processedon"))
                    {
                        SetStateRequest setStatRequest = new SetStateRequest()
                        {
                            EntityMoniker = new EntityReference
                            {
                                Id = context.PrimaryEntityId,
                                LogicalName = isp_creditnote.EntityLogicalName,
                            },
                            State = new OptionSetValue(1),
                            Status = new OptionSetValue(863300003)
                        };
                        service.Execute(setStatRequest);
                    }
                }
            }
        }
    }
}

