﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.creditnote
{
    public class postupdatecredithistorycalculations : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                Entity target = (Entity)context.InputParameters["Target"];
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                EntityHelper helper = new EntityHelper(context, service);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

                Entity preMessageImage = null;
                Entity postMessageImage = null;
                decimal rareCommission = 0;
                int bullionCoins = 0;
                decimal bullionTotal = 0;
                Guid shopMonthCommisionId = Guid.Empty;
                Guid brokerMonthCommisionId = Guid.Empty;
                isp_shopcommission shopCommissionPeriod = null;
                isp_brokercommission brokerCommissionPeriod = null;
                decimal paymentAmount = 0;

                if (context.PreEntityImages.Contains("PreImage") && context.PreEntityImages["PreImage"] is Entity)
                {
                    preMessageImage = (Entity)context.PreEntityImages["PreImage"];
                }
                if (context.PostEntityImages.Contains("PostImage") && context.PostEntityImages["PostImage"] is Entity)
                {
                    postMessageImage = (Entity)context.PostEntityImages["PostImage"];
                }
                if (postMessageImage != null && preMessageImage != null)
                {
                    if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_runpaymentdatacalculator"))
                    {
                        if (((OptionSetValue)postMessageImage.Attributes["statuscode"]).Value == 863300003)
                        {
                            isp_creditnote creditNote = (from c in orgContext.CreateQuery<isp_creditnote>()
                                                         where c.isp_creditnoteId.Value == context.PrimaryEntityId
                                                         select new isp_creditnote
                                                         {
                                                             isp_creditnoteId = c.isp_creditnoteId,
                                                             isp_Invoice = c.isp_Invoice,
                                                             isp_ProcessedOn = c.isp_ProcessedOn,
                                                             isp_creditnotenumber = c.isp_creditnotenumber
                                                         }).FirstOrDefault();
                            Invoice invoice = (from i in orgContext.CreateQuery<Invoice>()
                                               where i.InvoiceId.Value == creditNote.isp_Invoice.Id
                                               select new Invoice
                                               {
                                                   InvoiceId = i.InvoiceId,
                                                   isp_atp = i.isp_atp
                                               }).FirstOrDefault();

                            SalesOrder salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                                                     where so.SalesOrderId.Value == invoice.isp_atp.Id
                                                     select new SalesOrder
                                                     {
                                                         SalesOrderId = so.SalesOrderId,
                                                         OwnerId = so.OwnerId,
                                                         isp_Invoice = so.isp_Invoice,
                                                     }).FirstOrDefault();

                            List<SalesOrderDetail> salesOrderDetails = (from sod in orgContext.CreateQuery<SalesOrderDetail>()
                                                                        where sod.SalesOrderId.Id == salesOrder.SalesOrderId.Value
                                                                        select sod).ToList();

                            List<GeneratedCode.isp_payment> payments = (from p in orgContext.CreateQuery<GeneratedCode.isp_payment>()
                                                                        where p.isp_Invoice.Id == salesOrder.isp_Invoice.Id
                                                                        && p.statecode.Value == isp_paymentState.Active
                                                                        && p.statuscode.Value == 863300001
                                                                        && p.isp_CreditNoteId == null
                                                                        select new GeneratedCode.isp_payment
                                                                        {
                                                                            isp_paymentId = p.isp_paymentId,
                                                                            isp_Amount = p.isp_Amount,
                                                                            isp_RareCoinsTurnover = p.isp_RareCoinsTurnover,
                                                                            isp_BullionCoins = p.isp_BullionCoins,
                                                                            isp_BullionTurnover = p.isp_BullionTurnover,
                                                                            isp_atp = p.isp_atp,
                                                                            isp_Invoice = p.isp_Invoice
                                                                        }).ToList();

                            SystemUser user = (from u in orgContext.CreateQuery<SystemUser>()
                                               where u.SystemUserId.Value == salesOrder.OwnerId.Id
                                               select new SystemUser
                                               {
                                                   SystemUserId = u.SystemUserId,
                                                   isp_usertype = u.isp_usertype
                                               }).FirstOrDefault();
                            if (payments.Count > 0)
                            {
                                GeneratedCode.isp_payment reversePayment = new GeneratedCode.isp_payment();
                                Guid commissionId = Guid.Empty;

                                foreach (GeneratedCode.isp_payment payment in payments)
                                {
                                    if (payment.isp_Amount.Value > 0)
                                    {
                                        paymentAmount -= payment.isp_Amount.Value;
                                    }
                                    if (payment.isp_BullionCoins.Value > 0)
                                    {
                                        bullionCoins -= payment.isp_BullionCoins.Value;
                                    }
                                    if (payment.isp_RareCoinsTurnover.Value > 0)
                                    {
                                        rareCommission -= payment.isp_RareCoinsTurnover.Value;
                                    }
                                    if (payment.isp_BullionTurnover.Value > 0)
                                    {
                                        bullionTotal -= payment.isp_BullionTurnover.Value;
                                    }
                                    //payment.isp_CreditNoteId = new EntityReference(isp_creditnote.EntityLogicalName, creditNote.isp_creditnoteId.Value);
                                    //orgContext.UpdateObject(payment);
                                    //orgContext.SaveChanges();
                                }
                                if (bullionCoins != 0 || bullionTotal != 0 || rareCommission != 0)
                                {
                                    //broker
                                    if (user.isp_usertype.Value == 1)
                                    {
                                        isp_brokercommission brokerMonthCommission = new isp_brokercommission();
                                        List<isp_brokercommission> brokerCommission = null;
                                        isp_commissionperiod commission = null;

                                        List<isp_commissionperiod> commissionPeriod = (from cp in orgContext.CreateQuery<isp_commissionperiod>()
                                                                                       where cp.isp_CommissionFor.Value == 1
                                                                                       && cp.statecode.Value == isp_commissionperiodState.Active
                                                                                       select new isp_commissionperiod
                                                                                       {
                                                                                           isp_commissionperiodId = cp.isp_commissionperiodId,
                                                                                           isp_BeginDate = cp.isp_BeginDate,
                                                                                           isp_EndDate = cp.isp_EndDate,
                                                                                           isp_CommissionFor = cp.isp_CommissionFor
                                                                                       }).ToList();
                                        foreach (isp_commissionperiod cp in commissionPeriod)
                                        {
                                            if (cp.isp_BeginDate.Value.ToLocalTime().Date <= creditNote.isp_ProcessedOn.Value.ToLocalTime().Date && cp.isp_EndDate.Value.ToLocalTime().Date >= creditNote.isp_ProcessedOn.Value.ToLocalTime().Date)
                                            {
                                                commission = cp;
                                            }
                                        }

                                        brokerCommission = (from bc in orgContext.CreateQuery<isp_brokercommission>()
                                                            where bc.OwnerId.Id == salesOrder.OwnerId.Id
                                                            select new isp_brokercommission
                                                            {
                                                                isp_brokercommissionId = bc.isp_brokercommissionId,
                                                                isp_CommissionableAmount = bc.isp_CommissionableAmount,
                                                                isp_BullionValue = bc.isp_BullionValue,
                                                                isp_BullionCount = bc.isp_BullionCount,
                                                                isp_CommissionPeriodStartDate = bc.isp_CommissionPeriodStartDate,
                                                                isp_CommissionPeriodEndDate = bc.isp_CommissionPeriodEndDate
                                                            }).ToList();

                                        foreach (isp_brokercommission bc in brokerCommission)
                                        {
                                            if (bc.isp_CommissionPeriodStartDate.Value.ToLocalTime().Date <= creditNote.isp_ProcessedOn.Value.ToLocalTime().Date && bc.isp_CommissionPeriodEndDate.Value.ToLocalTime().Date >= creditNote.isp_ProcessedOn.Value.ToLocalTime().Date)
                                            {
                                                brokerCommissionPeriod = bc;
                                            }
                                        }

                                        if (brokerCommissionPeriod != null)
                                        {
                                            brokerCommissionPeriod.isp_BullionValue = new Money(brokerCommissionPeriod.isp_BullionValue.Value + bullionTotal);
                                            brokerCommissionPeriod.isp_BullionCount = brokerCommissionPeriod.isp_BullionCount.Value + bullionCoins;
                                            brokerCommissionPeriod.isp_CommissionableAmount = new Money(rareCommission + brokerCommissionPeriod.isp_CommissionableAmount.Value);
                                            orgContext.UpdateObject(brokerCommissionPeriod);
                                            orgContext.SaveChanges();
                                            commissionId = brokerCommissionPeriod.isp_brokercommissionId.Value;
                                        }
                                        else
                                        {
                                            brokerMonthCommisionId = Guid.NewGuid();
                                            brokerMonthCommission.isp_brokercommissionId = brokerMonthCommisionId;
                                            brokerMonthCommission.isp_name = "Commission For: " + salesOrder.OwnerId.Name + " " + commission.isp_BeginDate.Value.ToLocalTime().ToString("dd MMMM yyyy") + "-" + commission.isp_EndDate.Value.ToLocalTime().ToString("dd MMMM yyyy");
                                            brokerMonthCommission.OwnerId = salesOrder.OwnerId;
                                            brokerMonthCommission.isp_BullionCount = bullionCoins;
                                            brokerMonthCommission.isp_BullionValue = new Money(bullionTotal);
                                            brokerMonthCommission.isp_CommissionableAmount = new Money(rareCommission);
                                            brokerMonthCommission.isp_CommissionPeriodStartDate = commission.isp_BeginDate;
                                            brokerMonthCommission.isp_CommissionPeriodEndDate = commission.isp_EndDate;
                                            orgContext.AddObject(brokerMonthCommission);
                                            orgContext.SaveChanges();
                                            commissionId = brokerMonthCommisionId;
                                        }
                                    }
                                    //Shop
                                    if (user.isp_usertype.Value == 2)
                                    {
                                        isp_shopcommission shopMonthCommission = new isp_shopcommission();
                                        List<isp_shopcommission> shopCommission = null;
                                        isp_commissionperiod commission = null;

                                        List<isp_commissionperiod> commissionPeriod = (from cp in orgContext.CreateQuery<isp_commissionperiod>()
                                                                                       where cp.isp_CommissionFor.Value == 2
                                                                                       && cp.statecode.Value == isp_commissionperiodState.Active
                                                                                       select new isp_commissionperiod
                                                                                       {
                                                                                           isp_commissionperiodId = cp.isp_commissionperiodId,
                                                                                           isp_BeginDate = cp.isp_BeginDate,
                                                                                           isp_EndDate = cp.isp_EndDate,
                                                                                           isp_CommissionFor = cp.isp_CommissionFor
                                                                                       }).ToList();
                                        foreach (isp_commissionperiod cp in commissionPeriod)
                                        {
                                            if (cp.isp_BeginDate.Value.ToLocalTime().Date <= creditNote.isp_ProcessedOn.Value.ToLocalTime().Date && cp.isp_EndDate.Value.ToLocalTime().Date >= creditNote.isp_ProcessedOn.Value.ToLocalTime().Date)
                                            {
                                                commission = cp;
                                            }
                                        }
                                        shopCommission = (from c in orgContext.CreateQuery<isp_shopcommission>()
                                                          where c.isp_ShopId.Id == salesOrder.OwnerId.Id
                                                          select new isp_shopcommission
                                                          {
                                                              isp_shopcommissionId = c.isp_shopcommissionId,
                                                              isp_ShopId = c.isp_ShopId,
                                                              isp_CommissionableAmount = c.isp_CommissionableAmount,
                                                              isp_BullionValue = c.isp_BullionValue,
                                                              isp_BullionCount = c.isp_BullionCount,
                                                              isp_CommissionPeriodEndDate = c.isp_CommissionPeriodEndDate,
                                                              isp_CommissionPeriodStartDate = c.isp_CommissionPeriodStartDate
                                                          }).ToList();
                                        foreach (isp_shopcommission sc in shopCommission)
                                        {
                                            if (sc.isp_CommissionPeriodStartDate.Value.ToLocalTime().Date <= creditNote.isp_ProcessedOn.Value.ToLocalTime().Date && sc.isp_CommissionPeriodEndDate.Value.ToLocalTime().Date >= creditNote.isp_ProcessedOn.Value.ToLocalTime().Date)
                                            {
                                                shopCommissionPeriod = sc;
                                            }
                                        }
                                        if (shopCommissionPeriod != null)
                                        {
                                            shopCommissionPeriod.isp_BullionValue = new Money(shopCommissionPeriod.isp_BullionValue.Value + bullionTotal);
                                            shopCommissionPeriod.isp_BullionCount = shopCommissionPeriod.isp_BullionCount.Value + bullionCoins;
                                            shopCommissionPeriod.isp_CommissionableAmount = new Money(rareCommission + shopCommissionPeriod.isp_CommissionableAmount.Value);
                                            orgContext.UpdateObject(shopCommissionPeriod);
                                            orgContext.SaveChanges();
                                            commissionId = shopCommissionPeriod.isp_shopcommissionId.Value;
                                        }
                                        else
                                        {
                                            shopMonthCommisionId = Guid.NewGuid();
                                            shopMonthCommission.isp_shopcommissionId = shopMonthCommisionId;
                                            shopMonthCommission.isp_name = "Commission For: " + salesOrder.OwnerId.Name + " " + commission.isp_BeginDate.Value.ToLocalTime().ToString("dd MMMM yyyy") + "-" + commission.isp_EndDate.Value.ToLocalTime().ToString("dd MMMM yyyy");
                                            shopMonthCommission.isp_ShopId = salesOrder.OwnerId;
                                            shopMonthCommission.isp_BullionCount = bullionCoins;
                                            shopMonthCommission.isp_BullionValue = new Money(bullionTotal);
                                            shopMonthCommission.isp_CommissionableAmount = new Money(rareCommission);
                                            shopMonthCommission.isp_CommissionPeriodStartDate = commission.isp_BeginDate.Value.ToLocalTime();
                                            shopMonthCommission.isp_CommissionPeriodEndDate = commission.isp_EndDate.Value.ToLocalTime();
                                            orgContext.AddObject(shopMonthCommission);
                                            orgContext.SaveChanges();
                                            commissionId = shopMonthCommisionId;
                                        }
                                    }
                                }
                                reversePayment.isp_BullionCoins = bullionCoins;
                                reversePayment.isp_BullionTurnover = new Money(bullionTotal);
                                reversePayment.isp_RareCoinsTurnover = new Money(rareCommission);
                                reversePayment.isp_Amount = new Money(paymentAmount);
                                reversePayment.isp_atp = payments[0].isp_atp;
                                reversePayment.isp_Invoice = payments[0].isp_Invoice;
                                if (user.isp_usertype.Value == 1)
                                {
                                    reversePayment.isp_BrokerCommissionId = new EntityReference(isp_brokercommission.EntityLogicalName, commissionId);
                                }
                                if (user.isp_usertype.Value == 2)
                                {
                                    reversePayment.isp_ShopCommissionId = new EntityReference(isp_shopcommission.EntityLogicalName, commissionId);
                                }
                                reversePayment.statuscode = new OptionSetValue(863300001);
                                reversePayment.isp_CreditNoteId = new EntityReference(isp_creditnote.EntityLogicalName, creditNote.isp_creditnoteId.Value);
                                reversePayment.isp_PaymentreferenceNumber = "Reversal: Credit Note " + creditNote.isp_creditnotenumber;
                                orgContext.AddObject(reversePayment);
                                orgContext.SaveChanges();
                            }
                        }
                    }
                }
            }
        }
    }
}
