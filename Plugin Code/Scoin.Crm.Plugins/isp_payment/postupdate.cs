﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.isp_payment
{
    public class postupdate : IPlugin
    {
        // Create the invoice in Pastel
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                var target = (Entity)context.InputParameters["Target"];
                EntityHelper helper = new EntityHelper(context, service);

                Entity preMessageImage = null;
                Entity postMessageImage = null;
                decimal total = 0;
                if (context.PreEntityImages.Contains("PreImage") && context.PreEntityImages["PreImage"] is Entity)
                {
                    preMessageImage = (Entity)context.PreEntityImages["PreImage"];
                }
                if (context.PostEntityImages.Contains("PostImage") && context.PostEntityImages["PostImage"] is Entity)
                {
                    postMessageImage = (Entity)context.PostEntityImages["PostImage"];
                }
                if (postMessageImage != null && preMessageImage != null)
                {
                    if (helper.HasValueChanged(preMessageImage, postMessageImage, "statuscode"))
                    {
                        if (((OptionSetValue)postMessageImage.Attributes["statecode"]).Value == 1 || ((OptionSetValue)target.Attributes["statuscode"]).Value == 863300000 || ((OptionSetValue)target.Attributes["statuscode"]).Value == 863300001 || ((OptionSetValue)target.Attributes["statuscode"]).Value == 2)
                        {
                            SalesOrder salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                                                     where so.SalesOrderId.Value == ((EntityReference)postMessageImage.Attributes["isp_atp"]).Id
                                                     && so.StateCode.Value == SalesOrderState.Active
                                                     select new SalesOrder
                                                     {
                                                         SalesOrderId = so.SalesOrderId,
                                                         isp_TotalAmountPaid = so.isp_TotalAmountPaid
                                                     }).FirstOrDefault();
                            if (salesOrder != null)
                            {
                                var payments = from p in orgContext.CreateQuery("isp_payment")
                                               where ((EntityReference)p["isp_atp"]).Id == salesOrder.SalesOrderId.Value
                                               && (int)p["statecode"] != 1
                                               select new
                                               {
                                                   paymentId = p["isp_paymentid"],
                                                   payment = p["isp_amount"]
                                               };
                                foreach (var p in payments)
                                {
                                    total += ((Money)p.payment).Value;
                                }
                                salesOrder.isp_TotalAmountPaid.Value = total;

                                orgContext.UpdateObject(salesOrder);
                                orgContext.SaveChanges();
                            }
                        }
                    }
                    if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_brokercommissionid"))
                    {
                        GeneratedCode.isp_payment payment = (from p in orgContext.CreateQuery<GeneratedCode.isp_payment>()
                                                             where p.isp_paymentId.Value == context.PrimaryEntityId
                                                             select new GeneratedCode.isp_payment
                                                             {
                                                                 isp_paymentId = p.isp_paymentId,
                                                                 isp_BullionCoins = p.isp_BullionCoins,
                                                                 isp_BullionTurnover = p.isp_BullionTurnover,
                                                                 isp_RareCoinsTurnover = p.isp_RareCoinsTurnover,
                                                                 isp_PurchaseNoteRareQuantity = p.isp_PurchaseNoteRareQuantity,
                                                                 isp_PurchaseNoteBullionQuantity = p.isp_PurchaseNoteBullionQuantity,
                                                                 isp_BrokerCommissionId = p.isp_BrokerCommissionId
                                                             }).FirstOrDefault();
                        if (preMessageImage.Contains("isp_brokercommissionid") && postMessageImage.Contains("isp_brokercommissionid"))
                        {
                            isp_brokercommission deductCommission = (from dc in orgContext.CreateQuery<isp_brokercommission>()
                                                                     where dc.isp_brokercommissionId.Value == ((EntityReference)preMessageImage.Attributes["isp_brokercommissionid"]).Id
                                                                     select new isp_brokercommission
                                                                     {
                                                                         isp_brokercommissionId = dc.isp_brokercommissionId,
                                                                         isp_CommissionableAmount = dc.isp_CommissionableAmount,
                                                                         isp_BullionCount = dc.isp_BullionCount,
                                                                         isp_BullionValue = dc.isp_BullionValue,
                                                                         isp_PurchaseNoteItemQuantity = dc.isp_PurchaseNoteItemQuantity,
                                                                         isp_PurchaseNoteBullionQuantity = dc.isp_PurchaseNoteBullionQuantity,
                                                                     }).FirstOrDefault();

                            isp_brokercommission addCommission = (from ac in orgContext.CreateQuery<isp_brokercommission>()
                                                                  where ac.isp_brokercommissionId.Value == ((EntityReference)postMessageImage.Attributes["isp_brokercommissionid"]).Id
                                                                  select new isp_brokercommission
                                                                  {
                                                                      isp_brokercommissionId = ac.isp_brokercommissionId,
                                                                      isp_CommissionableAmount = ac.isp_CommissionableAmount,
                                                                      isp_BullionCount = ac.isp_BullionCount,
                                                                      isp_BullionValue = ac.isp_BullionValue,
                                                                      isp_PurchaseNoteItemQuantity = ac.isp_PurchaseNoteItemQuantity,
                                                                      isp_PurchaseNoteBullionQuantity = ac.isp_PurchaseNoteBullionQuantity,
                                                                  }).FirstOrDefault();
                            int purchaseNoteRareQuantity = 0;
                            int PurchaseNoteBullionQuantity = 0;

                            if (payment.isp_BullionCoins != null)
                            {
                                addCommission.isp_BullionCount = addCommission.isp_BullionCount.Value + payment.isp_BullionCoins.Value;
                                deductCommission.isp_BullionCount = deductCommission.isp_BullionCount.Value - payment.isp_BullionCoins.Value;
                            }
                            if (payment.isp_BullionTurnover != null)
                            {
                                addCommission.isp_BullionValue = new Money(addCommission.isp_BullionValue.Value + payment.isp_BullionTurnover.Value);
                                deductCommission.isp_BullionValue = new Money(deductCommission.isp_BullionValue.Value - payment.isp_BullionTurnover.Value);
                            }
                            if (payment.isp_RareCoinsTurnover != null)
                            {
                                addCommission.isp_CommissionableAmount = new Money(addCommission.isp_CommissionableAmount.Value + payment.isp_RareCoinsTurnover.Value);
                                deductCommission.isp_CommissionableAmount = new Money(deductCommission.isp_CommissionableAmount.Value - payment.isp_RareCoinsTurnover.Value);
                            }
                            if (payment.isp_PurchaseNoteRareQuantity != null)
                            {
                                purchaseNoteRareQuantity = payment.isp_PurchaseNoteRareQuantity.Value;
                            }
                            if (payment.isp_PurchaseNoteBullionQuantity != null)
                            {
                                PurchaseNoteBullionQuantity = payment.isp_PurchaseNoteBullionQuantity.Value;
                            }
                            if (purchaseNoteRareQuantity != 0 || PurchaseNoteBullionQuantity != 0)
                            {
                                addCommission.isp_PurchaseNoteItemQuantity = addCommission.isp_PurchaseNoteItemQuantity.Value + purchaseNoteRareQuantity;
                                deductCommission.isp_PurchaseNoteItemQuantity = deductCommission.isp_PurchaseNoteItemQuantity.Value - purchaseNoteRareQuantity;
                                addCommission.isp_PurchaseNoteBullionQuantity = addCommission.isp_PurchaseNoteBullionQuantity.Value + PurchaseNoteBullionQuantity;
                                deductCommission.isp_PurchaseNoteBullionQuantity = deductCommission.isp_PurchaseNoteBullionQuantity.Value - PurchaseNoteBullionQuantity;
                            }
                            orgContext.UpdateObject(addCommission);
                            orgContext.UpdateObject(deductCommission);
                            orgContext.SaveChanges();
                        }
                    }
                    if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_shopcommissionid"))
                    {
                        if (preMessageImage.Contains("isp_shopcommissionid") && postMessageImage.Contains("isp_shopcommissionid"))
                        {
                            GeneratedCode.isp_payment payment = (from p in orgContext.CreateQuery<GeneratedCode.isp_payment>()
                                                                 where p.isp_paymentId.Value == context.PrimaryEntityId
                                                                 select new GeneratedCode.isp_payment
                                                                 {
                                                                     isp_paymentId = p.isp_paymentId,
                                                                     isp_BullionCoins = p.isp_BullionCoins,
                                                                     isp_BullionTurnover = p.isp_BullionTurnover,
                                                                     isp_RareCoinsTurnover = p.isp_RareCoinsTurnover,
                                                                     isp_PurchaseNoteRareQuantity = p.isp_PurchaseNoteRareQuantity,
                                                                     isp_PurchaseNoteBullionQuantity = p.isp_PurchaseNoteBullionQuantity,
                                                                     isp_BrokerCommissionId = p.isp_BrokerCommissionId,
                                                                     isp_DoubleCommission = p.isp_DoubleCommission
                                                                 }).FirstOrDefault();

                            isp_shopcommission deductCommission = (from dc in orgContext.CreateQuery<isp_shopcommission>()
                                                                   where dc.isp_shopcommissionId.Value == ((EntityReference)preMessageImage.Attributes["isp_shopcommissionid"]).Id
                                                                   select new isp_shopcommission
                                                                     {
                                                                         isp_shopcommissionId = dc.isp_shopcommissionId,
                                                                         isp_CommissionableAmount = dc.isp_CommissionableAmount,
                                                                         isp_BullionCount = dc.isp_BullionCount,
                                                                         isp_BullionValue = dc.isp_BullionValue,
                                                                         isp_RareCommission = dc.isp_RareCommission,
                                                                         isp_DoubleCommission = dc.isp_DoubleCommission,
                                                                     }).FirstOrDefault();

                            isp_shopcommission addCommission = (from ac in orgContext.CreateQuery<isp_shopcommission>()
                                                                where ac.isp_shopcommissionId.Value == ((EntityReference)postMessageImage.Attributes["isp_shopcommissionid"]).Id
                                                                select new isp_shopcommission
                                                                  {
                                                                      isp_shopcommissionId = ac.isp_shopcommissionId,
                                                                      isp_CommissionableAmount = ac.isp_CommissionableAmount,
                                                                      isp_BullionCount = ac.isp_BullionCount,
                                                                      isp_BullionValue = ac.isp_BullionValue,
                                                                      isp_RareCommission = ac.isp_RareCommission,
                                                                      isp_DoubleCommission = ac.isp_DoubleCommission
                                                                  }).FirstOrDefault();
                            decimal rareAmount = 0;
                            decimal doubleCommission = 0;
                            if (payment.isp_BullionCoins != null)
                            {
                                addCommission.isp_BullionCount = addCommission.isp_BullionCount.Value + payment.isp_BullionCoins.Value;
                                deductCommission.isp_BullionCount = deductCommission.isp_BullionCount.Value - payment.isp_BullionCoins.Value;
                            }
                            if (payment.isp_BullionTurnover != null)
                            {
                                addCommission.isp_BullionValue = new Money(addCommission.isp_BullionValue.Value + payment.isp_BullionTurnover.Value);
                                deductCommission.isp_BullionValue = new Money(deductCommission.isp_BullionValue.Value - payment.isp_BullionTurnover.Value);
                            }
                            if (payment.isp_RareCoinsTurnover != null)
                            {
                                addCommission.isp_RareCommission = new Money(addCommission.isp_RareCommission.Value + payment.isp_RareCoinsTurnover.Value);
                                deductCommission.isp_RareCommission = new Money(deductCommission.isp_RareCommission.Value - payment.isp_RareCoinsTurnover.Value);
                                rareAmount = payment.isp_RareCoinsTurnover.Value;
                            }
                            if (payment.isp_DoubleCommission != null)
                            {
                                addCommission.isp_DoubleCommission = new Money(addCommission.isp_DoubleCommission.Value + payment.isp_DoubleCommission.Value);
                                deductCommission.isp_DoubleCommission = new Money(deductCommission.isp_DoubleCommission.Value - payment.isp_DoubleCommission.Value);
                                doubleCommission = payment.isp_DoubleCommission.Value;
                            }
                            if (payment.isp_RareCoinsTurnover != null || payment.isp_DoubleCommission != null)
                            {
                                addCommission.isp_CommissionableAmount = new Money(addCommission.isp_CommissionableAmount.Value + rareAmount + doubleCommission);
                                deductCommission.isp_CommissionableAmount = new Money(deductCommission.isp_CommissionableAmount.Value - (rareAmount + doubleCommission));
                            }
                            orgContext.UpdateObject(addCommission);
                            orgContext.UpdateObject(deductCommission);
                            orgContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }
}
