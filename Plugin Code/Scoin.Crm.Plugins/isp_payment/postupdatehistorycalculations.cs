﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.isp_payment
{
    public class postupdatehistorycalculations : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                Entity target = (Entity)context.InputParameters["Target"];
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                EntityHelper helper = new EntityHelper(context, service);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                Entity preMessageImage = null;
                Entity postMessageImage = null;
                CommissionHelper commissionHelper = new CommissionHelper();

                if (context.PreEntityImages.Contains("PreImage") && context.PreEntityImages["PreImage"] is Entity)
                {
                    preMessageImage = (Entity)context.PreEntityImages["PreImage"];
                }
                if (context.PostEntityImages.Contains("PostImage") && context.PostEntityImages["PostImage"] is Entity)
                {
                    postMessageImage = (Entity)context.PostEntityImages["PostImage"];
                }

                if (postMessageImage != null && preMessageImage != null)
                {
                    Scoin.Crm.Plugins.GeneratedCode.isp_payment payment = (from p in orgContext.CreateQuery<Scoin.Crm.Plugins.GeneratedCode.isp_payment>()
                                                                           where p.isp_paymentId.Value == context.PrimaryEntityId
                                                                           select p).FirstOrDefault();
                    if (payment.statecode.Value == isp_paymentState.Active)
                    {
                        if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_runpaymentdatacalculator"))
                        {
                            decimal vat = 0;
                            decimal bullionTotal = 0;
                            int bullionCount = 0;
                            decimal otherItemsTotal = 0;
                            decimal VatDeductedTotal = 0;
                            decimal percentage = 0;
                            decimal CommissionDeducedTotal = 0;
                            decimal doubleCommPercent = 0;
                            decimal doubleComm = 0;

                            SalesOrder salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                                                     where so.SalesOrderId.Value == ((EntityReference)postMessageImage.Attributes["isp_atp"]).Id
                                                     select so).FirstOrDefault();

                            List<SalesOrderDetail> salesOrderDetails = (from sod in orgContext.CreateQuery<SalesOrderDetail>()
                                                                        where sod.SalesOrderId.Id == salesOrder.SalesOrderId.Value
                                                                        select sod).ToList();

                            SystemUser systemUser = (from su in orgContext.CreateQuery<SystemUser>()
                                                     where su.SystemUserId.Value == salesOrder.OwnerId.Id
                                                     select new SystemUser
                                                     {
                                                         SystemUserId = su.SystemUserId,
                                                         isp_usertype = su.isp_usertype
                                                     }).FirstOrDefault();

                            List<Scoin.Crm.Plugins.GeneratedCode.isp_payment> paymentCount = (from pay in orgContext.CreateQuery<Scoin.Crm.Plugins.GeneratedCode.isp_payment>()
                                                                                              where pay.isp_atp.Id == salesOrder.SalesOrderId.Value
                                                                                              && pay.statecode.Value == isp_paymentState.Active
                                                                                              && pay.statuscode.Value != 863300001
                                                                                              select pay).ToList();

                            paymentCount = paymentCount.OrderBy(x => x.CreatedOn.Value).ToList();

                            foreach (SalesOrderDetail sod in salesOrderDetails)
                            {
                                Product product = (from p in orgContext.CreateQuery<Product>()
                                                   where p.ProductId.Value == sod.ProductId.Id
                                                   select new Product
                                                   {
                                                       ProductId = p.ProductId,
                                                       isp_ProductType = p.isp_ProductType,
                                                       isp_NoCommission = p.isp_NoCommission,
                                                       isp_NonCommissionableAmount = p.isp_NonCommissionableAmount,
                                                       isp_doublecommission = p.isp_doublecommission
                                                   }).FirstOrDefault();
                                bool noCommission = product.isp_NoCommission != null ? product.isp_NoCommission.Value : false;
                                bool bundledSet = product.isp_NonCommissionableAmount != null && product.isp_NonCommissionableAmount.Value > 0 ? true : false;
                                trace.Trace("Stock isnt null");
                                if (product.isp_ProductType.Value == 0 || product.isp_ProductType.Value == 863300000 || noCommission)
                                {
                                    otherItemsTotal += sod.ExtendedAmount.Value;
                                }
                                if (bundledSet)
                                {
                                    otherItemsTotal += product.isp_NonCommissionableAmount.Value * sod.Quantity.Value;
                                }
                                if (sod.isp_VatTotalAmount.Value > 0 && product.isp_ProductType.Value == 2)
                                {
                                    vat += sod.isp_VatTotalAmount.Value;
                                    if (product.isp_doublecommission != null && product.isp_doublecommission.Value && systemUser.isp_usertype.Value == 2)
                                    {
                                        doubleComm += sod.isp_VatExcludedTotalAmount.Value;
                                    }
                                }
                                if (sod.isp_VatTotalAmount.Value == 0 && product.isp_ProductType.Value == 2 && product.isp_doublecommission != null && product.isp_doublecommission.Value && systemUser.isp_usertype.Value == 2)
                                {
                                    doubleComm += sod.ExtendedAmount.Value;
                                }
                                if (product.isp_ProductType.Value == 1)
                                {
                                    bullionTotal += sod.ExtendedAmount.Value;
                                    bullionCount += Convert.ToInt16(sod.Quantity.Value);
                                }
                            }
                            //Get the total that commission can be deduced from
                            VatDeductedTotal = salesOrder.TotalAmount.Value - (vat + bullionTotal + otherItemsTotal);

                            if (doubleComm != 0)
                            {
                                doubleCommPercent = doubleComm / (salesOrder.TotalAmount.Value - (bullionTotal + otherItemsTotal));
                            }
                            if (vat > 0)
                            {
                                //Work out the percentage of what was paid
                                percentage = VatDeductedTotal / (salesOrder.TotalAmount.Value - (bullionTotal + otherItemsTotal));

                                //If more than one payment  then there cannot be any bullions
                                if (paymentCount.Count > 1)
                                {
                                    bullionTotal = 0;
                                    bullionCount = 0;
                                    otherItemsTotal = 0;
                                }
                                //The total that the commission can be deduced from
                                CommissionDeducedTotal = (((Money)postMessageImage.Attributes["isp_amount"]).Value - (bullionTotal + otherItemsTotal)) * percentage;
                            }
                            if (vat == 0)
                            {
                                //If more than one payment  then there cannot be any bullions
                                if (paymentCount.Count > 1)
                                {
                                    bullionTotal = 0;
                                    bullionCount = 0;
                                    otherItemsTotal = 0;
                                }
                                CommissionDeducedTotal = ((Money)target.Attributes["isp_amount"]).Value - (bullionTotal + otherItemsTotal);
                            }
                            if (doubleComm != 0)
                            {
                                doubleComm = (((Money)target.Attributes["isp_amount"]).Value - (bullionTotal + otherItemsTotal)) * doubleCommPercent;
                            }
                            target.Attributes.Add("isp_bullioncoins", bullionCount);
                            target.Attributes.Add("isp_bullionturnover", new Money(bullionTotal));
                            target.Attributes.Add("isp_rarecoinsturnover", new Money(CommissionDeducedTotal));
                            target.Attributes.Add("isp_invoice", salesOrder.isp_Invoice);
                            target.Attributes.Add("isp_doublecommission", new Money(doubleComm));
                            service.Update(target);
                        }
                        if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_runbrokerpaymentdatacalculator") && !postMessageImage.Contains("isp_purchasenoteid"))
                        {
                            decimal bullionTotal = 0;
                            int bullionCount = 0;
                            decimal CommissionDeducedTotal = 0;
                            bool isCommissionable = false;
                            Guid shopMonthCommisionId = Guid.Empty;
                            Guid brokerMonthCommisionId = Guid.Empty;

                            SalesOrder salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                                                     where so.SalesOrderId.Value == ((EntityReference)postMessageImage.Attributes["isp_atp"]).Id
                                                     select so).FirstOrDefault();

                            List<SalesOrderDetail> salesOrderDetails = (from sod in orgContext.CreateQuery<SalesOrderDetail>()
                                                                        where sod.SalesOrderId.Id == salesOrder.SalesOrderId.Value
                                                                        select sod).ToList();

                            SystemUser systemUser = (from su in orgContext.CreateQuery<SystemUser>()
                                                     where su.SystemUserId.Value == salesOrder.OwnerId.Id
                                                     select new SystemUser
                                                     {
                                                         SystemUserId = su.SystemUserId,
                                                         isp_usertype = su.isp_usertype
                                                     }).FirstOrDefault();

                            List<Scoin.Crm.Plugins.GeneratedCode.isp_payment> paymentCount = (from pay in orgContext.CreateQuery<Scoin.Crm.Plugins.GeneratedCode.isp_payment>()
                                                                                              where pay.isp_atp.Id == salesOrder.SalesOrderId.Value
                                                                                              && pay.statecode.Value == isp_paymentState.Active
                                                                                              && pay.statuscode.Value != 863300001
                                                                                              select pay).ToList();

                            bullionCount = payment.isp_BullionCoins.Value;
                            bullionTotal = payment.isp_BullionTurnover.Value;
                            CommissionDeducedTotal = payment.isp_RareCoinsTurnover.Value;
                            isCommissionable = true;
                            if (isCommissionable)
                            {
                                DateTime date = payment.CreatedOn.Value;
                                if (payment.isp_DatePaymentReceived != null)
                                {
                                    date = payment.isp_DatePaymentReceived.Value;
                                }
                                //  If broker, check that an existing commission record exists between a commission period dates, otherwise create a new commission record for the commission period
                                if (systemUser.isp_usertype.Value == 1)
                                {
                                    isp_commissionperiod commission = commissionHelper.getCommissionPeriod(orgContext, date, 1, trace);
                                    isp_brokercommission brokerCommission = commissionHelper.getBrokerCommission(orgContext, salesOrder.OwnerId.Id, date);


                                    if (brokerCommission != null)
                                    {
                                        brokerCommission.isp_BullionValue = new Money(brokerCommission.isp_BullionValue.Value + bullionTotal);
                                        brokerCommission.isp_BullionCount = brokerCommission.isp_BullionCount.Value + bullionCount;
                                        brokerCommission.isp_CommissionableAmount = new Money(brokerCommission.isp_CommissionableAmount.Value + CommissionDeducedTotal);
                                        orgContext.UpdateObject(brokerCommission);
                                        orgContext.SaveChanges();

                                        target.Attributes.Add("isp_brokercommissionid", new EntityReference(isp_brokercommission.EntityLogicalName, brokerCommission.isp_brokercommissionId.Value));
                                    }
                                    else
                                    {
                                        string commissionName = "Commission For: " + salesOrder.OwnerId.Name + " " + commission.isp_BeginDate.Value.ToLocalTime().ToString("dd MMMM yyyy") + "-" + commission.isp_EndDate.Value.ToLocalTime().ToString("dd MMMM yyyy");
                                        Guid brokerCommissionId = commissionHelper.createBrokerCommission(orgContext, commissionName, salesOrder.OwnerId, bullionCount, new Money(bullionTotal), new Money(CommissionDeducedTotal), commission.isp_BeginDate.Value, commission.isp_EndDate.Value, 0, 0, commission.isp_CommissionPeriodNumber.Value);
                                        target.Attributes.Add("isp_brokercommissionid", new EntityReference(isp_brokercommission.EntityLogicalName, brokerCommissionId));
                                    }
                                }
                            }
                            service.Update(target);
                        }
                        if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_runshopcommissiondatacalculator"))
                        {
                            if (postMessageImage.Contains("isp_atp"))
                            {
                                if (((EntityReference)postMessageImage.Attributes["isp_atp"]) != null)
                                {
                                    SalesOrder salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                                                             where so.SalesOrderId.Value == ((EntityReference)postMessageImage.Attributes["isp_atp"]).Id
                                                             select so).FirstOrDefault();

                                    SystemUser systemUser = (from su in orgContext.CreateQuery<SystemUser>()
                                                             where su.SystemUserId.Value == salesOrder.OwnerId.Id
                                                             select new SystemUser
                                                             {
                                                                 SystemUserId = su.SystemUserId,
                                                                 isp_usertype = su.isp_usertype
                                                             }).FirstOrDefault();

                                    DateTime date = payment.CreatedOn.Value;
                                    if (payment.isp_DatePaymentReceived != null)
                                    {
                                        date = payment.isp_DatePaymentReceived.Value;
                                    }

                                    if (systemUser.isp_usertype.Value == 2 && !preMessageImage.Contains("isp_shopcommissionid"))
                                    {
                                        isp_commissionperiod commission = commissionHelper.getCommissionPeriod(orgContext, date, 2, trace);
                                        isp_shopcommission shopCommission = commissionHelper.getShopCommission(orgContext, salesOrder.OwnerId.Id, date);
                                        EntityReference shopCommissionId;
                                        if (shopCommission != null)
                                        {
                                            shopCommission.isp_DoubleCommission = new Money(shopCommission.isp_DoubleCommission.Value + payment.isp_DoubleCommission.Value);
                                            shopCommission.isp_BullionValue = new Money(shopCommission.isp_BullionValue.Value + payment.isp_BullionTurnover.Value);
                                            shopCommission.isp_BullionCount = shopCommission.isp_BullionCount + payment.isp_BullionCoins;
                                            shopCommission.isp_RareCommission = new Money(shopCommission.isp_RareCommission.Value + payment.isp_RareCoinsTurnover.Value);
                                            shopCommission.isp_CommissionableAmount = new Money(shopCommission.isp_CommissionableAmount.Value + payment.isp_RareCoinsTurnover.Value + payment.isp_DoubleCommission.Value);
                                            orgContext.UpdateObject(shopCommission);
                                            orgContext.SaveChanges();

                                            shopCommissionId = new EntityReference(isp_shopcommission.EntityLogicalName, shopCommission.isp_shopcommissionId.Value);
                                        }
                                        else
                                        {
                                            string commissionName = "Commission For: " + salesOrder.OwnerId.Name + " " + commission.isp_BeginDate.Value.ToLocalTime().ToString("dd MMMM yyyy") + "-" + commission.isp_EndDate.Value.ToLocalTime().ToString("dd MMMM yyyy");
                                            Guid shopCommisionId = commissionHelper.createShopCommission(orgContext, commissionName, salesOrder.OwnerId, payment.isp_BullionCoins.Value, new Money(payment.isp_BullionTurnover.Value), new Money(payment.isp_RareCoinsTurnover.Value), payment.isp_DoubleCommission, new Money(payment.isp_RareCoinsTurnover.Value), commission.isp_BeginDate.Value, commission.isp_EndDate.Value, commission.isp_CommissionPeriodNumber.Value);
                                            shopCommissionId = new EntityReference(isp_shopcommission.EntityLogicalName, shopCommisionId);
                                        }
                                        target.Attributes.Add("isp_shopcommissionid", shopCommissionId);
                                        service.Update(target);
                                    }
                                }
                            }
                        }
                        if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_runbrokerpaymentdatacalculator") && payment.isp_PurchaseNoteId != null)
                        {
                            int bullionQuantity = 0;
                            int rareQuantity = 0;
                            decimal rareValue = 0;
                            decimal bullionValue = 0;

                            GeneratedCode.isp_purchasenote purchaseNote = (from pn in orgContext.CreateQuery<GeneratedCode.isp_purchasenote>()
                                                                           where pn.isp_purchasenoteId.Value == ((EntityReference)postMessageImage.Attributes["isp_purchasenoteid"]).Id
                                                                           select pn).FirstOrDefault();
                            trace.Trace("1");

                            isp_buyback buyBack = (from bb in orgContext.CreateQuery<isp_buyback>()
                                                   where bb.isp_buybackId.Value == purchaseNote.isp_buybacknumber.Id
                                                   select new isp_buyback
                                                   {
                                                       isp_buybackId = bb.isp_buybackId,
                                                       OwnerId = bb.OwnerId
                                                   }).FirstOrDefault();
                            trace.Trace("2");
                            List<isp_buybackproducts> buyBackProducts = (from bbp in orgContext.CreateQuery<isp_buybackproducts>()
                                                                         where bbp.isp_BuyBackProductsIdRelationship.Id == buyBack.isp_buybackId.Value
                                                                         && bbp.statecode.Value == isp_buybackproductsState.Active
                                                                         select new isp_buybackproducts
                                                                         {
                                                                             isp_buybackproductsId = bbp.isp_buybackproductsId,
                                                                             isp_BuyBackProductsIdRelationship = bbp.isp_BuyBackProductsIdRelationship,
                                                                             isp_Quantity = bbp.isp_Quantity,
                                                                             isp_ProductsBuyBackPrice = bbp.isp_ProductsBuyBackPrice,
                                                                             isp_ExistingProducts = bbp.isp_ExistingProducts
                                                                         }).ToList();
                            trace.Trace("3");

                            foreach (isp_buybackproducts bbp in buyBackProducts)
                            {
                                Product product = (from p in orgContext.CreateQuery<Product>()
                                                   where p.ProductId.Value == bbp.isp_ExistingProducts.Id
                                                   select new Product
                                                   {
                                                       ProductId = p.ProductId,
                                                       isp_ProductType = p.isp_ProductType
                                                   }).FirstOrDefault();

                                if (product.isp_ProductType.Value == 1)
                                {
                                    bullionValue += bbp.isp_ProductsBuyBackPrice.Value * Convert.ToDecimal(bbp.isp_Quantity);
                                    bullionQuantity += bbp.isp_Quantity.Value;
                                }
                                if (product.isp_ProductType.Value == 2)
                                {
                                    rareValue += bbp.isp_ProductsBuyBackPrice.Value * Convert.ToDecimal(bbp.isp_Quantity);
                                    rareQuantity += bbp.isp_Quantity.Value;
                                }
                            }
                            trace.Trace("Bullion Q : " + bullionQuantity.ToString());
                            trace.Trace("Bullion V : " + bullionValue.ToString());
                            trace.Trace("Rare Q : " + rareQuantity.ToString());
                            trace.Trace("Rare V : " + rareValue.ToString());
                            trace.Trace("4");
                            SystemUser systemUser = (from su in orgContext.CreateQuery<SystemUser>()
                                                     where su.SystemUserId.Value == payment.OwnerId.Id
                                                     select new SystemUser
                                                     {
                                                         SystemUserId = su.SystemUserId,
                                                         isp_usertype = su.isp_usertype
                                                     }).FirstOrDefault();
                            trace.Trace("5");
                            if (systemUser.isp_usertype.Value == 1)
                            {
                                trace.Trace("6");
                                isp_commissionperiod commission = commissionHelper.getCommissionPeriod(orgContext, purchaseNote.CreatedOn.Value, 1, trace);
                                isp_brokercommission brokerCommission = commissionHelper.getBrokerCommission(orgContext, purchaseNote.OwnerId.Id, purchaseNote.CreatedOn.Value);
                                trace.Trace("7");
                                if (brokerCommission != null)
                                {
                                    trace.Trace("8");
                                    brokerCommission.isp_PurchaseNoteBullionQuantity = brokerCommission.isp_PurchaseNoteBullionQuantity.Value + bullionQuantity;
                                    brokerCommission.isp_PurchaseNoteItemQuantity = brokerCommission.isp_PurchaseNoteItemQuantity.Value + rareQuantity;
                                    orgContext.UpdateObject(brokerCommission);
                                    orgContext.SaveChanges();

                                    target.Attributes.Add("isp_brokercommissionid", new EntityReference(isp_brokercommission.EntityLogicalName, brokerCommission.isp_brokercommissionId.Value));
                                }
                                else
                                {
                                    trace.Trace("9");
                                    string commissionName = "Commission For: " + purchaseNote.OwnerId.Name + " " + commission.isp_BeginDate.Value.ToLocalTime().ToString("dd MMMM yyyy") + "-" + commission.isp_EndDate.Value.ToLocalTime().ToString("dd MMMM yyyy");
                                    Guid brokerCommissionId = commissionHelper.createBrokerCommission(orgContext, commissionName, purchaseNote.OwnerId, 0, new Money(0), new Money(0), commission.isp_BeginDate.Value, commission.isp_EndDate.Value, rareQuantity, bullionQuantity, commission.isp_CommissionPeriodNumber.Value);
                                    target.Attributes.Add("isp_brokercommissionid", new EntityReference(isp_brokercommission.EntityLogicalName, brokerCommissionId));
                                }
                                service.Update(target);
                            }
                        }
                    }
                }
            }
        }
    }
}
