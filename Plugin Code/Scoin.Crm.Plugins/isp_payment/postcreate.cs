﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;
using System.Globalization;
namespace Scoin.Crm.Plugins.isp_payment
{
    public class postcreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                var target = (Entity)context.InputParameters["Target"];
                CommissionHelper commissionHelper = new CommissionHelper();

                int bullionCount = 0;
                decimal vat = 0;
                decimal bullionTotal = 0;
                decimal otherItemsTotal = 0;
                decimal VatDeductedTotal = 0;
                decimal percentage = 0;
                decimal CommissionDeducedTotal = 0;
                decimal doubleCommPercent = 0;
                decimal doubleComm = 0;
                bool isCommissionable = false;
                Guid shopCommisionId = Guid.Empty;
                Guid brokerCommissionId = Guid.Empty;

                Scoin.Crm.Plugins.GeneratedCode.isp_payment payment = (from p in orgContext.CreateQuery<Scoin.Crm.Plugins.GeneratedCode.isp_payment>()
                                                                       where p.isp_paymentId.Value == context.PrimaryEntityId
                                                                       select p).FirstOrDefault();
                trace.Trace("Starting");
                if (payment.statuscode.Value == 863300000 || payment.statuscode.Value == 1)
                {
                    trace.Trace("getting details");
                    SalesOrder salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                                             where so.SalesOrderId.Value == target.GetAttributeValue<EntityReference>("isp_atp").Id
                                             select so).FirstOrDefault();

                    List<SalesOrderDetail> salesOrderDetails = (from sod in orgContext.CreateQuery<SalesOrderDetail>()
                                                                where sod.SalesOrderId.Id == salesOrder.SalesOrderId.Value
                                                                select sod).ToList();

                    SystemUser systemUser = (from su in orgContext.CreateQuery<SystemUser>()
                                             where su.SystemUserId.Value == salesOrder.OwnerId.Id
                                             select new SystemUser
                                             {
                                                 SystemUserId = su.SystemUserId,
                                                 isp_usertype = su.isp_usertype
                                             }).FirstOrDefault();
                    List<Scoin.Crm.Plugins.GeneratedCode.isp_payment> paymentCount = (from p in orgContext.CreateQuery<Scoin.Crm.Plugins.GeneratedCode.isp_payment>()
                                                                                      where p.isp_atp.Id == salesOrder.SalesOrderId.Value
                                                                                      && p.statecode.Value == isp_paymentState.Active
                                                                                      && p.statuscode.Value != 863300001
                                                                                      select new Scoin.Crm.Plugins.GeneratedCode.isp_payment
                                                                                      {
                                                                                          isp_paymentId = p.isp_paymentId,
                                                                                          statuscode = p.statuscode
                                                                                      }).ToList();
                    trace.Trace("got details");
                    //Check the products to see if they have vat and also what type of product they are
                    foreach (SalesOrderDetail sod in salesOrderDetails)
                    {
                        trace.Trace("Looping through SOD");
                        Product product = (from p in orgContext.CreateQuery<Product>()
                                           where p.ProductId.Value == sod.ProductId.Id
                                           select new Product
                                           {
                                               ProductId = p.ProductId,
                                               isp_ProductType = p.isp_ProductType,
                                               isp_doublecommission = p.isp_doublecommission,
                                               isp_NoCommission = p.isp_NoCommission,
                                               isp_NonCommissionableAmount = p.isp_NonCommissionableAmount
                                           }).FirstOrDefault();
                        bool noCommission = product.isp_NoCommission != null ? product.isp_NoCommission.Value : false;
                        bool bundledSet = product.isp_NonCommissionableAmount != null && product.isp_NonCommissionableAmount.Value > 0 ? true : false;
                        trace.Trace("Stock isnt null");
                        if (product.isp_ProductType.Value == 0 || product.isp_ProductType.Value == 863300000 || noCommission)
                        {
                            otherItemsTotal += sod.ExtendedAmount.Value;
                        }
                        if (bundledSet)
                        {
                            otherItemsTotal += product.isp_NonCommissionableAmount.Value * sod.Quantity.Value;
                        }
                        if (sod.isp_VatTotalAmount.Value > 0 && product.isp_ProductType.Value == 2 && !noCommission)
                        {
                            vat += sod.isp_VatTotalAmount.Value;
                            trace.Trace("Got Vat");
                            if (product.isp_doublecommission != null && product.isp_doublecommission.Value && systemUser.isp_usertype.Value == 2 && salesOrder.CreatedOn.Value.ToLocalTime() > DateTime.ParseExact("07/20/2014", "MM/dd/yyyy", CultureInfo.InvariantCulture))
                            {
                                doubleComm += sod.isp_VatExcludedTotalAmount.Value;
                            }
                        }
                        if (sod.isp_VatTotalAmount.Value == 0 && product.isp_ProductType.Value == 2 && product.isp_doublecommission != null && product.isp_doublecommission.Value && systemUser.isp_usertype.Value == 2 && salesOrder.CreatedOn.Value.ToLocalTime() > DateTime.ParseExact("07/20/2014", "MM/dd/yyyy", CultureInfo.InvariantCulture))
                        {
                            doubleComm += sod.ExtendedAmount.Value;
                        }
                        if (product.isp_ProductType.Value == 1 || product.isp_ProductType.Value == 2)
                        {
                            isCommissionable = true;
                            trace.Trace("Commissionable");
                        }
                        if (product.isp_ProductType.Value == 1)
                        {
                            bullionTotal += sod.ExtendedAmount.Value;
                            bullionCount += Convert.ToInt16(sod.Quantity.Value);
                            trace.Trace("Bullion Product");
                        }                    
                    }
                    //Get the total that commission can be deduced from
                    VatDeductedTotal = salesOrder.TotalAmount.Value - (vat + bullionTotal + otherItemsTotal);
                    trace.Trace("Got Vat Deducted total");
                    if (doubleComm != 0)
                    {
                        doubleCommPercent = doubleComm / (salesOrder.TotalAmount.Value - (bullionTotal + otherItemsTotal));
                    }
                    if (vat > 0)
                    {
                        //Work out the percentage of what was paid
                        percentage = VatDeductedTotal / (salesOrder.TotalAmount.Value - (bullionTotal + otherItemsTotal));

                        trace.Trace("Work out percentage");
                        //If more than one payment  then there cannot be any bullions
                        if (paymentCount.Count > 1)
                        {
                            bullionTotal = 0;
                            bullionCount = 0;
                            otherItemsTotal = 0;
                        }
                        //The total that the commission can be deduced from
                        CommissionDeducedTotal = (((Money)target.Attributes["isp_amount"]).Value - (bullionTotal + otherItemsTotal)) * percentage;
                    }
                    if (vat == 0)
                    {
                        //If more than one payment  then there cannot be any bullions
                        if (paymentCount.Count > 1)
                        {
                            bullionTotal = 0;
                            bullionCount = 0;
                            otherItemsTotal = 0;
                        }
                        CommissionDeducedTotal = ((Money)target.Attributes["isp_amount"]).Value - (bullionTotal + otherItemsTotal);
                        trace.Trace("Commission Deduced total");
                    }
                    if (doubleComm != 0)
                    {
                        doubleComm = (((Money)target.Attributes["isp_amount"]).Value - (bullionTotal + otherItemsTotal)) * doubleCommPercent;
                    }

                    //Product being sold must be commissionable
                    if (isCommissionable)
                    {
                        trace.Trace("Commisionable");
                        //If broker, check that an existing commission record exists between a commission period dates, otherwise create a new commission record for the commission period
                        if (systemUser.isp_usertype.Value == 1)
                        {
                            isp_commissionperiod commission = commissionHelper.getCommissionPeriod(orgContext, payment.isp_DatePaymentReceived.Value, 1, trace);
                            isp_brokercommission brokerCommission = commissionHelper.getBrokerCommission(orgContext, salesOrder.OwnerId.Id, payment.isp_DatePaymentReceived.Value);

                            if (brokerCommission != null)
                            {
                                brokerCommission.isp_BullionValue = new Money(brokerCommission.isp_BullionValue.Value + bullionTotal);
                                brokerCommission.isp_BullionCount = brokerCommission.isp_BullionCount.Value + bullionCount;
                                brokerCommission.isp_CommissionableAmount = new Money(brokerCommission.isp_CommissionableAmount.Value + CommissionDeducedTotal);
                                orgContext.UpdateObject(brokerCommission);
                                orgContext.SaveChanges();

                                payment.isp_BrokerCommissionId = new EntityReference(isp_brokercommission.EntityLogicalName, brokerCommission.isp_brokercommissionId.Value);
                            }
                            else
                            {
                                string commissionName = "Commission For: " + salesOrder.OwnerId.Name + " " + commission.isp_BeginDate.Value.ToLocalTime().ToString("dd MMMM yyyy") + "-" + commission.isp_EndDate.Value.ToLocalTime().ToString("dd MMMM yyyy");
                                brokerCommissionId = commissionHelper.createBrokerCommission(orgContext, commissionName, salesOrder.OwnerId, bullionCount, new Money(bullionTotal), new Money(CommissionDeducedTotal), commission.isp_BeginDate.Value, commission.isp_EndDate.Value, 0, 0, commission.isp_CommissionPeriodNumber.Value);
                                payment.isp_BrokerCommissionId = new EntityReference(isp_brokercommission.EntityLogicalName, brokerCommissionId);
                            }
                        }
                        //If scoin shop, check that an existing commission record exists between a commission period dates, otherwise create a new commission record for the commission period
                        if (systemUser.isp_usertype.Value == 2)
                        {
                            trace.Trace("Getting Commission");
                            isp_commissionperiod commission = commissionHelper.getCommissionPeriod(orgContext, payment.isp_DatePaymentReceived.Value, 2, trace);
                            isp_shopcommission shopCommission = commissionHelper.getShopCommission(orgContext, salesOrder.OwnerId.Id, payment.isp_DatePaymentReceived.Value);

                            if (shopCommission != null)
                            {
                                decimal updateShopDoubleCommission = 0;
                                if (shopCommission.isp_CommissionableAmount != null)
                                {
                                    updateShopDoubleCommission = shopCommission.isp_DoubleCommission.Value;
                                }
                                shopCommission.isp_RareCommission = new Money(shopCommission.isp_RareCommission.Value + CommissionDeducedTotal);
                                shopCommission.isp_BullionValue = new Money(shopCommission.isp_BullionValue.Value + bullionTotal);
                                shopCommission.isp_BullionCount = shopCommission.isp_BullionCount + bullionCount;
                                shopCommission.isp_CommissionableAmount = new Money(shopCommission.isp_CommissionableAmount.Value + CommissionDeducedTotal + doubleComm);
                                shopCommission.isp_DoubleCommission = new Money(updateShopDoubleCommission + doubleComm);
                                orgContext.UpdateObject(shopCommission);
                                orgContext.SaveChanges();

                                payment.isp_ShopCommissionId = new EntityReference(isp_shopcommission.EntityLogicalName, shopCommission.isp_shopcommissionId.Value);
                            }
                            else
                            {
                                trace.Trace("None");
                                string commissionName = "Commission For: " + salesOrder.OwnerId.Name + " " + commission.isp_BeginDate.Value.ToLocalTime().ToString("dd MMMM yyyy") + "-" + commission.isp_EndDate.Value.ToLocalTime().ToString("dd MMMM yyyy");
                                trace.Trace("Go");
                                shopCommisionId = commissionHelper.createShopCommission(orgContext, commissionName, salesOrder.OwnerId, bullionCount, new Money(bullionTotal), new Money(CommissionDeducedTotal + doubleComm), new Money(doubleComm), new Money(CommissionDeducedTotal), commission.isp_BeginDate.Value, commission.isp_EndDate.Value, commission.isp_CommissionPeriodNumber.Value);
                                trace.Trace("Got ID");
                                payment.isp_ShopCommissionId = new EntityReference(isp_shopcommission.EntityLogicalName, shopCommisionId);
                                trace.Trace("Payment");
                            }

                        }
                        payment.isp_BullionCoins = bullionCount;
                        payment.isp_BullionTurnover = new Money(bullionTotal);
                        payment.isp_RareCoinsTurnover = new Money(CommissionDeducedTotal);
                        payment.isp_DoubleCommission = new Money(doubleComm);
                    }
                    payment.isp_Customer = salesOrder.isp_ContactId;
                }
                //If Credit Noted
                if (payment.statuscode.Value == 863300001)
                {
                    isp_creditnote creditNote = (from c in orgContext.CreateQuery<isp_creditnote>()
                                                 where c.isp_creditnoteId.Value == payment.isp_CreditNoteId.Id
                                                 select new isp_creditnote
                                                 {
                                                     isp_creditnoteId = c.isp_creditnoteId,
                                                     isp_ProcessedOn = c.isp_ProcessedOn,
                                                     isp_creditnotenumber = c.isp_creditnotenumber,
                                                     OwnerId = c.OwnerId
                                                 }).FirstOrDefault();

                    SalesOrder salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                                             where so.SalesOrderId.Value == payment.isp_atp.Id
                                             select new SalesOrder
                                             {
                                                 SalesOrderId = so.SalesOrderId,
                                                 OwnerId = so.OwnerId
                                             }).FirstOrDefault();

                    SystemUser systemUser = (from su in orgContext.CreateQuery<SystemUser>()
                                             where su.SystemUserId.Value == salesOrder.OwnerId.Id
                                             select new SystemUser
                                             {
                                                 SystemUserId = su.SystemUserId,
                                                 isp_usertype = su.isp_usertype
                                             }).FirstOrDefault();

                    //Broker
                    if (systemUser.isp_usertype.Value == 1)
                    {
                        isp_commissionperiod commission = commissionHelper.getCommissionPeriod(orgContext, creditNote.isp_ProcessedOn.Value, 1, trace);
                        isp_brokercommission brokerCommission = commissionHelper.getBrokerCommission(orgContext, salesOrder.OwnerId.Id, creditNote.isp_ProcessedOn.Value);

                        if (brokerCommission != null)
                        {
                            brokerCommission.isp_BullionValue = new Money(brokerCommission.isp_BullionValue.Value + payment.isp_BullionTurnover.Value);
                            brokerCommission.isp_BullionCount = brokerCommission.isp_BullionCount.Value + payment.isp_BullionCoins.Value;
                            brokerCommission.isp_CommissionableAmount = new Money(brokerCommission.isp_CommissionableAmount.Value + payment.isp_RareCoinsTurnover.Value);
                            orgContext.UpdateObject(brokerCommission);
                            orgContext.SaveChanges();

                            payment.isp_BrokerCommissionId = new EntityReference(isp_brokercommission.EntityLogicalName, brokerCommission.isp_brokercommissionId.Value);
                        }
                        else
                        {
                            string commissionName = "Commission For: " + creditNote.OwnerId.Name + " " + commission.isp_BeginDate.Value.ToLocalTime().ToString("dd MMMM yyyy") + "-" + commission.isp_EndDate.Value.ToLocalTime().ToString("dd MMMM yyyy");
                            brokerCommissionId = commissionHelper.createBrokerCommission(orgContext, commissionName, salesOrder.OwnerId, payment.isp_BullionCoins.Value, payment.isp_BullionTurnover, payment.isp_RareCoinsTurnover, commission.isp_BeginDate.Value, commission.isp_EndDate.Value, 0, 0, commission.isp_CommissionPeriodNumber.Value);
                            payment.isp_BrokerCommissionId = new EntityReference(isp_brokercommission.EntityLogicalName, brokerCommissionId);
                        }
                    }
                    //Shop
                    if (systemUser.isp_usertype.Value == 2)
                    {
                        isp_commissionperiod commission = commissionHelper.getCommissionPeriod(orgContext, creditNote.isp_ProcessedOn.Value, 2, trace);
                        isp_shopcommission shopCommission = commissionHelper.getShopCommission(orgContext, salesOrder.OwnerId.Id, creditNote.isp_ProcessedOn.Value);

                        if (shopCommission != null)
                        {
                            decimal doubleCommission = 0;
                            decimal shopDoubleCommission = 0;
                            if (payment.isp_DoubleCommission != null)
                            {
                                doubleCommission = payment.isp_DoubleCommission.Value;
                            }
                            if (shopCommission.isp_DoubleCommission != null)
                            {
                                shopDoubleCommission = shopCommission.isp_DoubleCommission.Value;
                            }

                            shopCommission.isp_BullionValue = new Money(shopCommission.isp_BullionValue.Value + payment.isp_BullionTurnover.Value);
                            shopCommission.isp_BullionCount = shopCommission.isp_BullionCount + payment.isp_BullionCoins.Value;
                            shopCommission.isp_RareCommission = new Money(shopCommission.isp_RareCommission.Value + payment.isp_RareCoinsTurnover.Value);
                            shopCommission.isp_CommissionableAmount = new Money(shopCommission.isp_CommissionableAmount.Value + payment.isp_RareCoinsTurnover.Value + doubleCommission);
                            shopCommission.isp_DoubleCommission = new Money(shopDoubleCommission + doubleCommission);
                            orgContext.UpdateObject(shopCommission);
                            orgContext.SaveChanges();

                            payment.isp_ShopCommissionId = new EntityReference(isp_shopcommission.EntityLogicalName, shopCommission.isp_shopcommissionId.Value);
                        }
                        else
                        {
                            string commissionName = "Commission For: " + salesOrder.OwnerId.Name + " " + commission.isp_BeginDate.Value.ToLocalTime().ToString("dd MMMM yyyy") + "-" + commission.isp_EndDate.Value.ToLocalTime().ToString("dd MMMM yyyy");
                            shopCommisionId = commissionHelper.createShopCommission(orgContext, commissionName, salesOrder.OwnerId, payment.isp_BullionCoins.Value, payment.isp_BullionTurnover, payment.isp_RareCoinsTurnover, payment.isp_DoubleCommission, payment.isp_RareCoinsTurnover, commission.isp_BeginDate.Value, commission.isp_EndDate.Value, commission.isp_CommissionPeriodNumber.Value);
                            payment.isp_ShopCommissionId = new EntityReference(isp_shopcommission.EntityLogicalName, shopCommisionId);
                        }
                    }
                }
                //If Purchase Noted
                if (payment.statuscode.Value == 863300003)
                {
                    trace.Trace("Purchase Noted");
                    GeneratedCode.isp_purchasenote purchaseNote = (from pn in orgContext.CreateQuery<GeneratedCode.isp_purchasenote>()
                                                                   where pn.isp_purchasenoteId.Value == payment.isp_PurchaseNoteId.Id
                                                                   select pn).FirstOrDefault();

                    SystemUser systemUser = (from su in orgContext.CreateQuery<SystemUser>()
                                             where su.SystemUserId.Value == purchaseNote.OwnerId.Id
                                             select new SystemUser
                                             {
                                                 SystemUserId = su.SystemUserId,
                                                 isp_usertype = su.isp_usertype
                                             }).FirstOrDefault();
                    trace.Trace("Got Details");
                    //Broker
                    if (systemUser.isp_usertype.Value == 1)
                    {
                        trace.Trace("Broker");
                        isp_commissionperiod commission = commissionHelper.getCommissionPeriod(orgContext, purchaseNote.CreatedOn.Value, 1, trace);
                        isp_brokercommission brokerCommission = commissionHelper.getBrokerCommission(orgContext, purchaseNote.OwnerId.Id, purchaseNote.CreatedOn.Value);

                        trace.Trace("Got Broker Commission Period");
                        if (brokerCommission != null)
                        {
                            trace.Trace("Updating commission");
                            brokerCommission.isp_PurchaseNoteBullionQuantity = brokerCommission.isp_PurchaseNoteBullionQuantity.Value + payment.isp_PurchaseNoteBullionQuantity.Value;
                            brokerCommission.isp_PurchaseNoteItemQuantity = brokerCommission.isp_PurchaseNoteItemQuantity.Value + payment.isp_PurchaseNoteRareQuantity.Value;
                            orgContext.UpdateObject(brokerCommission);
                            orgContext.SaveChanges();

                            payment.isp_BrokerCommissionId = new EntityReference(isp_brokercommission.EntityLogicalName, brokerCommission.isp_brokercommissionId.Value);
                            trace.Trace("Updated Commission");
                        }
                        else
                        {
                            Money PurchaseNoteValue = new Money(payment.isp_PurchaseNoteBullionValue.Value + payment.isp_PurchaseNoteRareValue.Value);
                            string commissionName = "Commission For: " + purchaseNote.OwnerId.Name + " " + commission.isp_BeginDate.Value.ToLocalTime().ToString("dd MMMM yyyy") + "-" + commission.isp_EndDate.Value.ToLocalTime().ToString("dd MMMM yyyy");
                            brokerCommissionId = commissionHelper.createBrokerCommission(orgContext, commissionName, purchaseNote.OwnerId, 0, new Money(0), new Money(0), commission.isp_BeginDate.Value, commission.isp_EndDate.Value, payment.isp_PurchaseNoteRareQuantity.Value, payment.isp_PurchaseNoteBullionQuantity.Value, commission.isp_CommissionPeriodNumber.Value);
                            payment.isp_BrokerCommissionId = new EntityReference(isp_brokercommission.EntityLogicalName, brokerCommissionId);
                            trace.Trace("Created new commission");
                        }
                    }
                }
                trace.Trace("Updating");
                orgContext.ClearChanges();
                orgContext.Attach(payment);
                orgContext.UpdateObject(payment);
                orgContext.SaveChanges();
                trace.Trace("Saved");
            }
        }
    }
}
