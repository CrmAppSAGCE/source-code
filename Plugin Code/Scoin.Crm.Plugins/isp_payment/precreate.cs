﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.isp_payment
{
    public class precreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            decimal bullionAmount = 0;
            decimal total = 0;
          //  int paymentCount = 0;
            SalesOrder salesOrder = null;
            if (target.Contains("isp_atp"))
            {
                salesOrder = (from so in orgContext.CreateQuery<SalesOrder>()
                              where so.SalesOrderId.Value == target.GetAttributeValue<EntityReference>("isp_atp").Id
                              select so).FirstOrDefault();
            }
            if (salesOrder != null)
            {
                if (target.Contains("ownerid"))
                {
                    target.Attributes["ownerid"] = salesOrder.OwnerId;
                }
                else
                {
                    target.Attributes.Add("ownerid", salesOrder.OwnerId);
                }
             }

            //If requested, received or credit noted update the total amount paid on the ATP
            if (((OptionSetValue)target.Attributes["statuscode"]).Value == 1 || ((OptionSetValue)target.Attributes["statuscode"]).Value == 863300000 || ((OptionSetValue)target.Attributes["statuscode"]).Value == 863300001)
            {
                trace.Trace("Start");

                trace.Trace("Got salesorder");
                List<SalesOrderDetail> salesOrderDetails = (from sod in orgContext.CreateQuery<SalesOrderDetail>()
                                                            where sod.SalesOrderId.Id == salesOrder.SalesOrderId.Value
                                                            select sod).ToList();
                trace.Trace("Got salesorderdetail");

                if (salesOrder.isp_Invoice != null && !target.Contains("isp_invoice"))
                {
                    trace.Trace("Adding invoice");
                    target.Attributes.Add("isp_invoice", salesOrder.isp_Invoice);
                    trace.Trace("Added invoice");
                }
                if (salesOrder.isp_TotalAmountPaid == null)
                {
                    trace.Trace("Got Adding total");
                    salesOrder.isp_TotalAmountPaid = ((Money)target.Attributes["isp_amount"]);
                    total = ((Money)target.Attributes["isp_amount"]).Value;
                    trace.Trace("Total: " + total.ToString());
                }
                else
                {
                    trace.Trace("Starting else");

                    List<GeneratedCode.isp_payment> payments = (from p in orgContext.CreateQuery<GeneratedCode.isp_payment>()
                                                                where p.isp_atp.Id == salesOrder.SalesOrderId.Value
                                                                && p.statecode.Value != isp_paymentState.Inactive                                                             
                                                                select new GeneratedCode.isp_payment
                                                                {
                                                                    isp_paymentId = p.isp_paymentId,
                                                                    isp_Amount = p.isp_Amount
                                                                }).ToList();
                    trace.Trace("Got Payments");

                    foreach (GeneratedCode.isp_payment p in payments)
                    {
                        total += p.isp_Amount.Value;
                    }
                    trace.Trace("Total: " + total.ToString());
                    total += ((Money)target.Attributes["isp_amount"]).Value;
                    trace.Trace("Added Total: " + total.ToString());
                    salesOrder.isp_TotalAmountPaid.Value = total;
                //    paymentCount = payments.Count + 1;
                    trace.Trace("Payment: " + payments.Count.ToString());
                }
                if (salesOrder.TotalAmount.Value < total)
                {
                    throw new InvalidPluginExecutionException("The amount paid is more than the total of the products. \nPlease check your amounts and then try again.");
                }
                trace.Trace("Updating");
                orgContext.UpdateObject(salesOrder);
                orgContext.SaveChanges();
                trace.Trace("Updated");
                List<GeneratedCode.isp_payment> paymentCount = (from p in orgContext.CreateQuery<GeneratedCode.isp_payment>()
                                                                where p.isp_atp.Id == salesOrder.SalesOrderId.Value
                                                                && p.statecode.Value != isp_paymentState.Inactive
                                                                && p.statuscode.Value != 863300001
                                                                select new GeneratedCode.isp_payment
                                                                {
                                                                    isp_paymentId = p.isp_paymentId,
                                                                    isp_Amount = p.isp_Amount
                                                                }).ToList();
                
                if (paymentCount.Count < 2)
                {
                    trace.Trace("Checking Bullion");
                    //If the amount paid is less than the total of the Bullion throw an error
                    if (((OptionSetValue)target.Attributes["statuscode"]).Value == 1 || ((OptionSetValue)target.Attributes["statuscode"]).Value == 863300000)
                    {
                        foreach (SalesOrderDetail sod in salesOrderDetails)
                        {
                            Product product = (from p in orgContext.CreateQuery<Product>()
                                               where p.ProductId.Value == sod.ProductId.Id
                                               select new Product
                                               {
                                                   ProductId = p.ProductId,
                                                   isp_ProductType = p.isp_ProductType
                                               }).FirstOrDefault();
                            trace.Trace("Got Products");
                            if (product.isp_ProductType.Value == 1)
                            {
                                bullionAmount += sod.ExtendedAmount.Value;
                            }
                            trace.Trace("Bullion amount: " + bullionAmount.ToString());
                        }
                        if (total < bullionAmount)
                        {
                            throw new InvalidPluginExecutionException("The amount paid is less than the total for Bullion coins.\nBullion coins must be paid in full.");
                        }
                    }
                }
            }
        }
    }
}
