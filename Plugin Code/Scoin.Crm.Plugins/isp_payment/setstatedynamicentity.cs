﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;


namespace Scoin.Crm.Plugins.isp_payment
{
    public class setstatedynamicentity : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            bool update = false;

            if (context.InputParameters.Contains("State"))
            {
                GeneratedCode.isp_payment payment = (from p in orgContext.CreateQuery<GeneratedCode.isp_payment>()
                                                     where p.isp_paymentId.Value == context.PrimaryEntityId
                                                     select new GeneratedCode.isp_payment
                                                     {
                                                         isp_paymentId = p.isp_paymentId,
                                                         isp_BullionCoins = p.isp_BullionCoins,
                                                         isp_BullionTurnover = p.isp_BullionTurnover,
                                                         isp_RareCoinsTurnover = p.isp_RareCoinsTurnover,
                                                         isp_PurchaseNoteRareQuantity = p.isp_PurchaseNoteRareQuantity,
                                                         isp_PurchaseNoteBullionQuantity = p.isp_PurchaseNoteBullionQuantity,
                                                         isp_BrokerCommissionId = p.isp_BrokerCommissionId,
                                                         isp_ShopCommissionId = p.isp_ShopCommissionId
                                                     }).FirstOrDefault();

                if (payment.isp_BrokerCommissionId != null)
                {
                    isp_brokercommission brokerCommission = (from bc in orgContext.CreateQuery<isp_brokercommission>()
                                                             where bc.isp_brokercommissionId.Value == payment.isp_BrokerCommissionId.Id
                                                             select new isp_brokercommission
                                                             {
                                                                 isp_brokercommissionId = bc.isp_brokercommissionId,
                                                                 isp_CommissionableAmount = bc.isp_CommissionableAmount,
                                                                 isp_BullionCount = bc.isp_BullionCount,
                                                                 isp_BullionValue = bc.isp_BullionValue,
                                                                 isp_PurchaseNoteItemQuantity = bc.isp_PurchaseNoteItemQuantity
                                                             }).FirstOrDefault();
                    trace.Trace("Contains status");
                    //Active
                    if (((OptionSetValue)context.InputParameters["State"]).Value == 0)
                    {
                        int purchaseNoteQuantity = 0;

                        if (payment.isp_BullionCoins != null)
                        {
                            brokerCommission.isp_BullionCount = brokerCommission.isp_BullionCount.Value + payment.isp_BullionCoins.Value;
                            update = true;
                        }
                        if (payment.isp_BullionTurnover != null)
                        {
                            brokerCommission.isp_BullionValue = new Money(brokerCommission.isp_BullionValue.Value + payment.isp_BullionTurnover.Value);
                            update = true;
                        }
                        if (payment.isp_RareCoinsTurnover != null)
                        {
                            brokerCommission.isp_CommissionableAmount = new Money(brokerCommission.isp_CommissionableAmount.Value + payment.isp_RareCoinsTurnover.Value);
                            update = true;
                        }
                        if (payment.isp_PurchaseNoteRareQuantity != null)
                        {
                            purchaseNoteQuantity = purchaseNoteQuantity + payment.isp_PurchaseNoteRareQuantity.Value;
                        }
                        if (payment.isp_PurchaseNoteBullionQuantity != null)
                        {
                            purchaseNoteQuantity = purchaseNoteQuantity + payment.isp_PurchaseNoteBullionQuantity.Value;
                        }
                        if (purchaseNoteQuantity != 0)
                        {
                            brokerCommission.isp_PurchaseNoteItemQuantity = brokerCommission.isp_PurchaseNoteItemQuantity.Value + purchaseNoteQuantity;
                            update = true;
                        }
                    }
                    //Inactive
                    if (((OptionSetValue)context.InputParameters["State"]).Value == 1)
                    {
                        int purchaseNoteQuantity = 0;

                        if (payment.isp_BullionCoins != null)
                        {
                            brokerCommission.isp_BullionCount = brokerCommission.isp_BullionCount.Value - payment.isp_BullionCoins.Value;
                            update = true;
                        }
                        if (payment.isp_BullionTurnover != null)
                        {
                            brokerCommission.isp_BullionValue = new Money(brokerCommission.isp_BullionValue.Value - payment.isp_BullionTurnover.Value);
                            update = true;
                        }
                        if (payment.isp_RareCoinsTurnover != null)
                        {
                            brokerCommission.isp_CommissionableAmount = new Money(brokerCommission.isp_CommissionableAmount.Value - payment.isp_RareCoinsTurnover.Value);
                            update = true;
                        }
                        if (payment.isp_PurchaseNoteRareQuantity != null)
                        {
                            purchaseNoteQuantity = purchaseNoteQuantity + payment.isp_PurchaseNoteRareQuantity.Value;
                        }
                        if (payment.isp_PurchaseNoteBullionQuantity != null)
                        {
                            purchaseNoteQuantity = purchaseNoteQuantity + payment.isp_PurchaseNoteBullionQuantity.Value;
                        }
                        if (purchaseNoteQuantity != 0)
                        {
                            brokerCommission.isp_PurchaseNoteItemQuantity = brokerCommission.isp_PurchaseNoteItemQuantity.Value - purchaseNoteQuantity;
                            update = true;
                        }
                    }
                    if (update)
                    {
                        orgContext.UpdateObject(brokerCommission);
                        orgContext.SaveChanges();
                    }
                }
                if (payment.isp_ShopCommissionId != null)
                {
                    isp_shopcommission shopCommission = (from bc in orgContext.CreateQuery<isp_shopcommission>()
                                                         where bc.isp_shopcommissionId.Value == payment.isp_ShopCommissionId.Id
                                                         select new isp_shopcommission
                                                             {
                                                                 isp_shopcommissionId = bc.isp_shopcommissionId,
                                                                 isp_CommissionableAmount = bc.isp_CommissionableAmount,
                                                                 isp_BullionCount = bc.isp_BullionCount,
                                                                 isp_BullionValue = bc.isp_BullionValue,
                                                             }).FirstOrDefault();
                    trace.Trace("Contains status");
                    //Active
                    if (((OptionSetValue)context.InputParameters["State"]).Value == 0)
                    {
                        if (payment.isp_BullionCoins != null)
                        {
                            shopCommission.isp_BullionCount = shopCommission.isp_BullionCount.Value + payment.isp_BullionCoins.Value;
                            update = true;
                        }
                        if (payment.isp_BullionTurnover != null)
                        {
                            shopCommission.isp_BullionValue = new Money(shopCommission.isp_BullionValue.Value + payment.isp_BullionTurnover.Value);
                            update = true;
                        }
                        if (payment.isp_RareCoinsTurnover != null)
                        {
                            shopCommission.isp_CommissionableAmount = new Money(shopCommission.isp_CommissionableAmount.Value + payment.isp_RareCoinsTurnover.Value);
                            update = true;
                        }
                    }
                    //Inactive
                    if (((OptionSetValue)context.InputParameters["State"]).Value == 1)
                    {
                        if (payment.isp_BullionCoins != null)
                        {
                            shopCommission.isp_BullionCount = shopCommission.isp_BullionCount.Value - payment.isp_BullionCoins.Value;
                            update = true;
                        }
                        if (payment.isp_BullionTurnover != null)
                        {
                            shopCommission.isp_BullionValue = new Money(shopCommission.isp_BullionValue.Value - payment.isp_BullionTurnover.Value);
                            update = true;
                        }
                        if (payment.isp_RareCoinsTurnover != null)
                        {
                            shopCommission.isp_CommissionableAmount = new Money(shopCommission.isp_CommissionableAmount.Value - payment.isp_RareCoinsTurnover.Value);
                            update = true;
                        }
                    }
                    if (update)
                    {
                        orgContext.UpdateObject(shopCommission);
                        orgContext.SaveChanges();
                    }
                }
            }
        }
    }
}
