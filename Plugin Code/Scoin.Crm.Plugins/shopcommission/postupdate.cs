﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.shopcommission
{
    public class postupdate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                Entity target = (Entity)context.InputParameters["Target"];
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                Entity preMessageImage = null;
                Entity postMessageImage = null;

                EntityHelper helper = new EntityHelper(context, service);
                if (context.PreEntityImages.Contains("PreImage") && context.PreEntityImages["PreImage"] is Entity)
                {
                    preMessageImage = (Entity)context.PreEntityImages["PreImage"];
                }
                if (context.PostEntityImages.Contains("PostImage") && context.PostEntityImages["PostImage"] is Entity)
                {
                    postMessageImage = (Entity)context.PostEntityImages["PostImage"];
                }
                bool update = false;

                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

                isp_shopcommission shopCommission = (from sc in orgContext.CreateQuery<isp_shopcommission>()
                                                     where sc.isp_shopcommissionId.Value == context.PrimaryEntityId
                                                     select new isp_shopcommission
                                                     {
                                                         isp_shopcommissionId = sc.isp_shopcommissionId,
                                                         isp_BullionCount = sc.isp_BullionCount,
                                                         isp_CommissionableAmount = sc.isp_CommissionableAmount,
                                                         isp_BullionCommissionAmount = sc.isp_BullionCommissionAmount,
                                                         isp_ShopId = sc.isp_ShopId,
                                                         isp_SalesConsultantTier = sc.isp_SalesConsultantTier,
                                                         isp_AdminManagerTier = sc.isp_AdminManagerTier,
                                                         isp_SalesManagerTier = sc.isp_SalesManagerTier,
                                                         isp_SalesConsultantCommissionEarned = sc.isp_SalesConsultantCommissionEarned,
                                                         isp_AdminManagerCommissionEarned = sc.isp_AdminManagerCommissionEarned,
                                                         isp_SalesManagerCommissionEarned = sc.isp_SalesManagerCommissionEarned
                                                     }).FirstOrDefault();
                trace.Trace("1");
                //If the bullion count changes, recalculate the bullion commission
                if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_bullioncount"))
                {
                    trace.Trace("2");
                    update = true;
                    isp_shopcommissionrule shopBullionRule = (from sbr in orgContext.CreateQuery<isp_shopcommissionrule>()
                                                              where sbr.isp_ProductType.Value == 2
                                                              && sbr.statecode == isp_shopcommissionruleState.Active
                                                              select new isp_shopcommissionrule
                                                              {
                                                                  isp_shopcommissionruleId = sbr.isp_shopcommissionruleId,
                                                                  isp_BullionValue = sbr.isp_BullionValue
                                                              }).FirstOrDefault();
                    if (shopBullionRule == null)
                    {
                        throw new InvalidPluginExecutionException("No bullion Rule exists for the shops");
                    }
                    decimal bullionCommission = shopBullionRule.isp_BullionValue.Value * Convert.ToDecimal(target.GetAttributeValue<int>("isp_bullioncount"));
                    shopCommission.isp_BullionCommissionAmount = new Money(bullionCommission);
                    trace.Trace("3");
                }
                //If the commissionable amount changes recalculate the rare commission based on the tier
                if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_commissionableamount"))
                {
                    trace.Trace("4");
                    update = true;
                    isp_shopcommissionrule shopRareRule = (from srr in orgContext.CreateQuery<isp_shopcommissionrule>()
                                                           where srr.isp_ShopId.Id == shopCommission.isp_ShopId.Id
                                                           && srr.statecode == isp_shopcommissionruleState.Active
                                                           select new isp_shopcommissionrule
                                                           {
                                                               isp_shopcommissionruleId = srr.isp_shopcommissionruleId,
                                                               isp_Target = srr.isp_Target,
                                                               isp_AdminManagerBelowTarget = srr.isp_AdminManagerBelowTarget,
                                                               isp_AdminManagerAboveTarget = srr.isp_AdminManagerAboveTarget,
                                                               isp_SalesConsultantBelowTarget = srr.isp_SalesConsultantBelowTarget,
                                                               isp_SalesConsultantAboveTarget = srr.isp_SalesConsultantAboveTarget,
                                                               isp_SalesManagerBelowTarget = srr.isp_SalesManagerBelowTarget,
                                                               isp_SalesManagerAboveTarget = srr.isp_SalesManagerAboveTarget
                                                           }).FirstOrDefault();
                    trace.Trace("5");
                    if (shopRareRule == null)
                    {
                        trace.Trace("6");
                        throw new InvalidPluginExecutionException("A rare rule does not exist for the shop");
                    }
                    trace.Trace("7");
                    decimal salesConsultantTier = 0;
                    decimal adminManagerTier = 0;
                    decimal salesManagerTier = 0;
                    //If below target, use below target value
                    if (shopCommission.isp_CommissionableAmount.Value < shopRareRule.isp_Target.Value)
                    {
                        trace.Trace("8");
                        salesConsultantTier = shopRareRule.isp_SalesConsultantBelowTarget.Value;
                        adminManagerTier = shopRareRule.isp_AdminManagerBelowTarget.Value;
                        salesManagerTier = shopRareRule.isp_SalesManagerBelowTarget.Value;
                        trace.Trace("8" + salesConsultantTier.ToString());
                        trace.Trace("8" + adminManagerTier.ToString());
                        trace.Trace("8" + salesManagerTier.ToString());
                        shopCommission.isp_SalesConsultantTier = salesConsultantTier;
                        shopCommission.isp_AdminManagerTier = adminManagerTier;
                        shopCommission.isp_SalesManagerTier = salesManagerTier;
                    }
                    //If above or equal to target use above target value
                    if (shopCommission.isp_CommissionableAmount.Value >= shopRareRule.isp_Target.Value)
                    {
                        trace.Trace("9");
                        salesConsultantTier = shopRareRule.isp_SalesConsultantAboveTarget.Value;
                        adminManagerTier = shopRareRule.isp_AdminManagerAboveTarget.Value;
                        salesManagerTier = shopRareRule.isp_SalesManagerAboveTarget.Value;
                        shopCommission.isp_SalesConsultantTier = salesConsultantTier;
                        shopCommission.isp_AdminManagerTier = adminManagerTier;
                        shopCommission.isp_SalesManagerTier = salesManagerTier;
                    }
                    trace.Trace("10");
                    //Calculate the commisson from the rare commissionable amount
                    decimal salesConsultantCommission = shopCommission.isp_CommissionableAmount.Value * salesConsultantTier / 100;
                    decimal adminManagerCommission = shopCommission.isp_CommissionableAmount.Value * adminManagerTier / 100;
                    decimal salesManagerCommission = shopCommission.isp_CommissionableAmount.Value * salesManagerTier / 100;

                    trace.Trace("8" + salesConsultantCommission.ToString());
                    trace.Trace("8" + adminManagerCommission.ToString());
                    trace.Trace("8" + salesManagerCommission.ToString());

                    shopCommission.isp_SalesConsultantCommissionEarned = new Money(salesConsultantCommission);
                    shopCommission.isp_AdminManagerCommissionEarned = new Money(adminManagerCommission);
                    shopCommission.isp_SalesManagerCommissionEarned = new Money(salesManagerCommission);
                    trace.Trace("11");
                }

                if (update)
                {
                    trace.Trace("12");
                    orgContext.UpdateObject(shopCommission);
                    trace.Trace("13");
                    orgContext.SaveChanges();
                    trace.Trace("14");
                }
            }
        }
    }
}