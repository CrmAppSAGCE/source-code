﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;


namespace Scoin.Crm.Plugins.shopcommission
{
    public class precreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            var target = (Entity)context.InputParameters["Target"];
            EntityHelper entityHelper = new EntityHelper();

            decimal bullionCommisson = 0;
            decimal salesConsultantTier = 0;
            decimal adminManagerTier = 0;
            decimal salesManagerTier = 0;
            decimal salesConsultantCommission = 0;
            decimal adminManagerCommission = 0;
            decimal salesManagerCommission = 0;

            //If there are bullions work out the bullion commission
            if (target.GetAttributeValue<int>("isp_bullioncount") != 0)
            {
                isp_shopcommissionrule shopBullionRule = (from scr in orgContext.CreateQuery<isp_shopcommissionrule>()
                                                          where scr.isp_ProductType.Value == 2
                                                          && scr.statecode == isp_shopcommissionruleState.Active
                                                          select new isp_shopcommissionrule
                                                      {
                                                          isp_shopcommissionruleId = scr.isp_shopcommissionruleId,
                                                          isp_BullionValue = scr.isp_BullionValue
                                                      }).FirstOrDefault();
                if (shopBullionRule == null)
                {
                    throw new InvalidPluginExecutionException("No bullion Rule exists for the shops");
                }
                bullionCommisson = Convert.ToDecimal(target.GetAttributeValue<int>("isp_bullioncount")) * shopBullionRule.isp_BullionValue.Value;
            }
            //If there is a commissionable amount work out the Commission based on the tier
            if (target.GetAttributeValue<Money>("isp_commissionableamount").Value != 0)
            {
                isp_shopcommissionrule shopRareRule = (from sr in orgContext.CreateQuery<isp_shopcommissionrule>()
                                                       where sr.isp_ShopId.Id == target.GetAttributeValue<EntityReference>("isp_shopid").Id
                                                             && sr.isp_ProductType.Value == 1
                                                             && sr.statecode == isp_shopcommissionruleState.Active
                                                       select new isp_shopcommissionrule
                                                       {
                                                           isp_shopcommissionruleId = sr.isp_shopcommissionruleId,
                                                           isp_Target = sr.isp_Target,
                                                           isp_AdminManagerBelowTarget = sr.isp_AdminManagerBelowTarget,
                                                           isp_AdminManagerAboveTarget = sr.isp_AdminManagerAboveTarget,
                                                           isp_SalesConsultantBelowTarget = sr.isp_SalesConsultantBelowTarget,
                                                           isp_SalesConsultantAboveTarget = sr.isp_SalesConsultantAboveTarget,
                                                           isp_SalesManagerBelowTarget = sr.isp_SalesManagerBelowTarget,
                                                           isp_SalesManagerAboveTarget = sr.isp_SalesManagerAboveTarget
                                                       }).FirstOrDefault();
                if (shopRareRule == null)
                {
                    throw new InvalidPluginExecutionException("A rare rule does not exist for the shop");
                }
                if (target.GetAttributeValue<Money>("isp_commissionableamount").Value < shopRareRule.isp_Target.Value)
                {
                    salesConsultantTier = shopRareRule.isp_SalesConsultantBelowTarget.Value;
                    adminManagerTier = shopRareRule.isp_AdminManagerBelowTarget.Value;
                    salesManagerTier = shopRareRule.isp_SalesManagerBelowTarget.Value;
                    entityHelper.updateTarget(target, "isp_salesconsultanttier", salesConsultantTier);
                    entityHelper.updateTarget(target, "isp_adminmanagertier", adminManagerTier);
                    entityHelper.updateTarget(target, "isp_salesmanagertier", salesManagerTier);
                }
                if (target.GetAttributeValue<Money>("isp_commissionableamount").Value >= shopRareRule.isp_Target.Value)
                {
                    salesConsultantTier = shopRareRule.isp_SalesConsultantAboveTarget.Value;
                    adminManagerTier = shopRareRule.isp_AdminManagerAboveTarget.Value;
                    salesManagerTier = shopRareRule.isp_SalesManagerAboveTarget.Value;
                    entityHelper.updateTarget(target, "isp_salesconsultanttier", salesConsultantTier);
                    entityHelper.updateTarget(target, "isp_adminmanagertier", adminManagerTier);
                    entityHelper.updateTarget(target, "isp_salesmanagertier", salesManagerTier);
                }
                salesConsultantCommission = target.GetAttributeValue<Money>("isp_commissionableamount").Value * salesConsultantTier / 100;
                adminManagerCommission = target.GetAttributeValue<Money>("isp_commissionableamount").Value * adminManagerTier / 100;
                salesManagerCommission = target.GetAttributeValue<Money>("isp_commissionableamount").Value * salesManagerTier / 100;
            }
            entityHelper.updateTarget(target, "isp_salesconsultantcommissionearned", new Money(salesConsultantCommission));
            entityHelper.updateTarget(target, "isp_adminmanagercommissionearned", new Money(adminManagerCommission));
            entityHelper.updateTarget(target, "isp_salesmanagercommissionearned", new Money(salesManagerCommission));
            entityHelper.updateTarget(target, "isp_bullioncommissionamount", new Money(bullionCommisson));
        }
    }
}
