﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.shopcommission
{
    public class preupdate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            var target = (Entity)context.InputParameters["Target"];
            EntityHelper entityHelper = new EntityHelper();

            if (context.Depth > 1)
            {
                return;
            }

            isp_shopcommission shopCommission = (from sc in orgContext.CreateQuery<isp_shopcommission>()
                                                 where sc.isp_shopcommissionId.Value == context.PrimaryEntityId
                                                 select new isp_shopcommission
                                                 {
                                                     isp_shopcommissionId = sc.isp_shopcommissionId,
                                                     isp_BullionCount = sc.isp_BullionCount,
                                                     isp_CommissionableAmount = sc.isp_CommissionableAmount,
                                                     isp_ShopId = sc.isp_ShopId
                                                 }).FirstOrDefault();
            //If the bullion count changes, recalculate the bullion commission
            if (target.Contains("isp_bullioncount"))
            {
                isp_shopcommissionrule shopBullionRule = (from sbr in orgContext.CreateQuery<isp_shopcommissionrule>()
                                                          where sbr.isp_ProductType.Value == 2
                                                          && sbr.statecode == isp_shopcommissionruleState.Active
                                                          select new isp_shopcommissionrule
                                                          {
                                                              isp_shopcommissionruleId = sbr.isp_shopcommissionruleId,
                                                              isp_BullionValue = sbr.isp_BullionValue
                                                          }).FirstOrDefault();
                if (shopBullionRule == null)
                {
                    throw new InvalidPluginExecutionException("No bullion Rule exists for the shops");
                }
                decimal bullionCommission = shopBullionRule.isp_BullionValue.Value * Convert.ToDecimal(target.GetAttributeValue<int>("isp_bullioncount"));
                entityHelper.updateTarget(target, "isp_bullioncommissionamount", new Money(bullionCommission));
            }
            //If the commissionable amount changes recalculate the rare commission based on the tier
            if (target.Contains("isp_commissionableamount"))
            {
                isp_shopcommissionrule shopRareRule = (from srr in orgContext.CreateQuery<isp_shopcommissionrule>()
                                                       where srr.isp_ShopId.Id == shopCommission.isp_ShopId.Id
                                                       && srr.statecode == isp_shopcommissionruleState.Active
                                                       select new isp_shopcommissionrule
                                                       {
                                                           isp_shopcommissionruleId = srr.isp_shopcommissionruleId,
                                                           isp_Target = srr.isp_Target,
                                                           isp_AdminManagerBelowTarget = srr.isp_AdminManagerBelowTarget,
                                                           isp_AdminManagerAboveTarget = srr.isp_AdminManagerAboveTarget,
                                                           isp_SalesConsultantBelowTarget = srr.isp_SalesConsultantBelowTarget,
                                                           isp_SalesConsultantAboveTarget = srr.isp_SalesConsultantAboveTarget,
                                                           isp_SalesManagerBelowTarget = srr.isp_SalesManagerBelowTarget,
                                                           isp_SalesManagerAboveTarget = srr.isp_SalesManagerAboveTarget
                                                       }).FirstOrDefault();
                if (shopRareRule == null)
                {
                    throw new InvalidPluginExecutionException("A rare rule does not exist for the shop");
                }
                decimal salesConsultantTier = 0;
                decimal adminManagerTier = 0;
                decimal salesManagerTier = 0;
                //If below target, use below target value
                if (target.GetAttributeValue<Money>("isp_commissionableamount").Value < shopRareRule.isp_Target.Value)
                {
                    salesConsultantTier = shopRareRule.isp_SalesConsultantBelowTarget.Value;
                    adminManagerTier = shopRareRule.isp_AdminManagerBelowTarget.Value;
                    salesManagerTier = shopRareRule.isp_SalesManagerBelowTarget.Value;
                    entityHelper.updateTarget(target, "isp_salesconsultanttier", salesConsultantTier);
                    entityHelper.updateTarget(target, "isp_adminmanagertier", adminManagerTier);
                    entityHelper.updateTarget(target, "isp_salesmanagertier", salesManagerTier);
                }
                //If above or equal to target use above target value
                if (target.GetAttributeValue<Money>("isp_commissionableamount").Value >= shopRareRule.isp_Target.Value)
                {
                    salesConsultantTier = shopRareRule.isp_SalesConsultantAboveTarget.Value;
                    adminManagerTier = shopRareRule.isp_AdminManagerAboveTarget.Value;
                    salesManagerTier = shopRareRule.isp_SalesManagerAboveTarget.Value;
                    entityHelper.updateTarget(target, "isp_salesconsultanttier", salesConsultantTier);
                    entityHelper.updateTarget(target, "isp_adminmanagertier", adminManagerTier);
                    entityHelper.updateTarget(target, "isp_salesmanagertier", salesManagerTier);
                }
                //Calculate the commisson from the rare commissionable amount
                decimal salesConsultantCommission = target.GetAttributeValue<Money>("isp_commissionableamount").Value * salesConsultantTier / 100;
                decimal adminManagerCommission = target.GetAttributeValue<Money>("isp_commissionableamount").Value * adminManagerTier / 100;
                decimal salesManagerCommission = target.GetAttributeValue<Money>("isp_commissionableamount").Value * salesManagerTier / 100;

                entityHelper.updateTarget(target, "isp_salesconsultantcommissionearned", new Money(salesConsultantCommission));
                entityHelper.updateTarget(target, "isp_adminmanagercommissionearned", new Money(adminManagerCommission));
                entityHelper.updateTarget(target, "isp_salesmanagercommissionearned", new Money(salesManagerCommission));
            }     
        }
    }
}
