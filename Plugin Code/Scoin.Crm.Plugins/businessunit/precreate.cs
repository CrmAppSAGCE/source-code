﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Scoin.Crm.Plugins.businessunit
{
    public class precreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            string ParentBusinessUnitID = target.GetAttributeValue<EntityReference>("parentbusinessunitid").Id.ToString();
            Int32? TreeDepth = null;

            if (!string.IsNullOrEmpty(ParentBusinessUnitID))
            {
                var GetParentLevelQuery = new QueryExpression("businessunit") { Criteria = new FilterExpression(), ColumnSet = new ColumnSet(new[] { "isp_businessunitlevel" }), PageInfo = new PagingInfo { Count = 1, PageNumber = 1 } };
                GetParentLevelQuery.Criteria.AddCondition("businessunitid", ConditionOperator.Equal, ParentBusinessUnitID);

                var RetrieveParentBusinessResult = service.RetrieveMultiple(GetParentLevelQuery);
                if (RetrieveParentBusinessResult.Entities.Count > 0 && RetrieveParentBusinessResult.Entities[0].Attributes.ContainsKey("isp_businessunitlevel"))
                    TreeDepth = RetrieveParentBusinessResult.Entities[0].GetAttributeValue<Int32>("isp_businessunitlevel");
                if (TreeDepth == null)
                    TreeDepth = 1;
                else
                    TreeDepth++;
            }

            //used for the precreate
            if (!target.Attributes.ContainsKey("isp_businessunitlevel"))
                target.Attributes.Add("isp_businessunitlevel", TreeDepth);
            else
                target.Attributes["isp_businessunitlevel"] = TreeDepth;
        }
    }
}
