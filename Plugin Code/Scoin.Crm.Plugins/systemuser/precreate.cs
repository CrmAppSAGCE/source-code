﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Scoin.Crm.Plugins.systemuser
{
    public class precreate : IPlugin
    {

      public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            if (target.Attributes.ContainsKey("isp_salescode"))
            {
                if (target.Attributes["isp_salescode"] != null && !string.IsNullOrWhiteSpace(target.Attributes["isp_salescode"].ToString()))
                {
                    return;
                }
            }

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);


          
            string LastName = target.GetAttributeValue<string>("lastname");
            if (!string.IsNullOrEmpty(LastName))
                LastName = LastName.Substring(0, Math.Min(3, LastName.Length));
            else
                LastName = "---";

            string BusinessUnitId = target.GetAttributeValue<EntityReference>("businessunitid").Id.ToString();

            if (!string.IsNullOrEmpty(BusinessUnitId))
            {

                var GetBusinessUnitQuery = new QueryExpression("businessunit") { Criteria = new FilterExpression(), ColumnSet = new ColumnSet(new[] { "name" }), PageInfo = new PagingInfo { Count = 1, PageNumber = 1 } };
                GetBusinessUnitQuery.Criteria.AddCondition("name", ConditionOperator.NotNull);
                GetBusinessUnitQuery.Criteria.AddCondition("businessunitid", ConditionOperator.Equal, BusinessUnitId);

                var RetrieveBusinessResult = service.RetrieveMultiple(GetBusinessUnitQuery);
                if (RetrieveBusinessResult.Entities.Count > 0 && RetrieveBusinessResult.Entities[0].Attributes.ContainsKey("businessunitid"))
                    BusinessUnitId = RetrieveBusinessResult.Entities[0].GetAttributeValue<string>("name");

                BusinessUnitId = BusinessUnitId.Substring(0, Math.Min(1, BusinessUnitId.Length));

            }
            else
                BusinessUnitId = "-";

            string newRefNumber = (BusinessUnitId + LastName).ToLower();

          


            var getUsersQuery = new QueryExpression("systemuser") { Criteria = new FilterExpression(), ColumnSet = new ColumnSet(new[] { "isp_salescode" }), PageInfo = new PagingInfo { Count = 1, PageNumber = 1 } };
            getUsersQuery.Criteria.AddCondition("isp_salescode", ConditionOperator.NotNull);
            getUsersQuery.Criteria.AddCondition("isp_salescode", ConditionOperator.BeginsWith, newRefNumber.Substring(0, Math.Min(3, newRefNumber.Length)));
            getUsersQuery.AddOrder("isp_salescode", OrderType.Descending);

            int maxRefNum = 0;

            var RetrieveResult = service.RetrieveMultiple(getUsersQuery);
            if (RetrieveResult.Entities.Count > 0 && RetrieveResult.Entities[0].Attributes.ContainsKey("systemuserid"))
                maxRefNum = Convert.ToInt32(RetrieveResult.Entities[0].GetAttributeValue<string>("systemuserid").Substring(4, 3));
            maxRefNum++;


           newRefNumber += maxRefNum.ToString().PadLeft(3, '0');

            //used for the precreate
            if (!target.Attributes.ContainsKey("isp_salescode"))
                target.Attributes.Add("isp_salescode", newRefNumber);
            else
                target.Attributes["isp_salescode"] = newRefNumber;

        }
    }
}
