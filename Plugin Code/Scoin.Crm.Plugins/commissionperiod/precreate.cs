﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.commissionperiod
{
    public class precreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            var target = (Entity)context.InputParameters["Target"];
            string userType = string.Empty;

            if (target.GetAttributeValue<DateTime>("isp_begindate") != null && target.GetAttributeValue<DateTime>("isp_enddate") != null)
            {
                if (target.GetAttributeValue<OptionSetValue>("isp_commissionfor").Value == 1)
                {
                    userType = "Broker ";
                }
                if (target.GetAttributeValue<OptionSetValue>("isp_commissionfor").Value == 2)
                {
                    userType = "Shop ";
                }
                if (target.Contains("isp_name"))
                {
                    target.Attributes["isp_name"] = userType + "Commission Period: " + target.GetAttributeValue<DateTime>("isp_begindate").ToLocalTime().ToString("dd MMMM yyyy") + "-" + target.GetAttributeValue<DateTime>("isp_enddate").ToLocalTime().ToString("dd MMMM yyyy");
                }
                else
                {
                    target.Attributes.Add("isp_name", userType + "Commission Period: " + target.GetAttributeValue<DateTime>("isp_begindate").ToLocalTime().ToString("dd MMMM yyyy") + "-" + target.GetAttributeValue<DateTime>("isp_enddate").ToLocalTime().ToString("dd MMMM yyyy"));
                }
            }
        }
    }
}
