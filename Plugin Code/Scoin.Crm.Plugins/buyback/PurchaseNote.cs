﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Scoin.Crm.Plugins.PastelService;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Crm.Sdk.Messages;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.buyback
{
    public class PurchaseNote : IPlugin
    {
        // Create the invoice in Pastel
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                var target = (Entity)context.InputParameters["Target"];
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);

                var BBQuery = new QueryExpression("isp_buyback") { ColumnSet = new ColumnSet(true) };
                trace.Trace("trace log 1");
                BBQuery.Criteria.AddCondition("isp_buybackid", ConditionOperator.Equal, target.Id);
                trace.Trace("trace log 2");
                var systemuserLink = BBQuery.AddLink("systemuser", "ownerid", "systemuserid");
                trace.Trace("trace log 3");
                systemuserLink.EntityAlias = "su";
                trace.Trace("trace log 4");
                systemuserLink.Columns.AddColumn("isp_salescode");
                trace.Trace("trace log 5");
                systemuserLink.Columns.AddColumn("isp_pasteluser");
                trace.Trace("trace log 6");
                systemuserLink.Columns.AddColumn("isp_customercode");
                trace.Trace("trace log 7");
                var customerLink = BBQuery.AddLink("contact", "isp_customer", "contactid");
                trace.Trace("trace log 8");
                customerLink.EntityAlias = "c";
                trace.Trace("trace log 9");
                customerLink.Columns.AddColumn("lastname");
                trace.Trace("trace log 10");
                customerLink.Columns.AddColumn("fullname");

                var oBuyBack = service.RetrieveMultiple(BBQuery).Entities[0];
                trace.Trace("trace log 11");
                QueryExpression DefaultsQE = new QueryExpression("isp_default");
                trace.Trace("trace log 12");
                DefaultsQE.ColumnSet = new ColumnSet(true);
                var Defaults = service.RetrieveMultiple(DefaultsQE).Entities[0];
                trace.Trace("trace log 13");

                // Get the buyback products
                var lineItems = (from bbp in orgContext.CreateQuery<isp_buybackproducts>()
                                 join p in orgContext.CreateQuery<Product>()
                                 on bbp.isp_ExistingProducts.Id equals p.ProductId.Value
                                 join bb in orgContext.CreateQuery<isp_buyback>()
                                 on bbp.isp_BuyBackProductsIdRelationship.Id equals bb.isp_buybackId.Value
                                 where bb.isp_buybackId.Value == context.PrimaryEntityId
                                 where bbp.statecode.Value == isp_buybackproductsState.Active
                                 select new
                                 {
                                     isp_buybackproductsid = bbp.isp_buybackproductsId,
                                     quantity = bbp.isp_Quantity,
                                     productsbuybackprice = bbp.isp_ProductsBuyBackPrice,
                                     isp_existingproducts = bbp.isp_ExistingProducts,
                                     productid = p.ProductId,
                                     productnumber = p.ProductNumber,
                                     isp_vatcode = p.isp_vatcode,
                                     isp_buybackid = bb.isp_buybackId,
                                     isp_total = bb.isp_total
                                 }).ToList();

                trace.Trace("trace log 17");
                #region Delivery methods
                string Delivery = "";
                if (oBuyBack.GetAttributeValue<OptionSetValue>("isp_collectionpreference").Value == 863300000)
                    Delivery = "Drop Off";
                if (oBuyBack.GetAttributeValue<OptionSetValue>("isp_collectionpreference").Value == 863300001)
                    Delivery = "Courier";
                if (oBuyBack.GetAttributeValue<OptionSetValue>("isp_collectionpreference").Value == 863300002)
                    Delivery = "Safe Custody";
                #endregion
                trace.Trace("trace log 18");
                string SalesCode = (string)oBuyBack.GetAttributeValue<AliasedValue>("su.isp_salescode").Value;
                trace.Trace("trace log 19");
                #region Order Number
                string sLastName = (string)oBuyBack.GetAttributeValue<AliasedValue>("c.lastname").Value;
                trace.Trace("trace log 20");
                string sOrderNumber = oBuyBack.GetAttributeValue<string>("isp_buybacknumber");
                if (sLastName.Length > 5)
                {
                    sLastName = sLastName.Substring(0, 6);
                }
                sOrderNumber = sOrderNumber + "-" + sLastName;
                #endregion
                trace.Trace("trace log 21");
                var PurchaseNote = new CreateDocumentRq
                {
                    DocumentType = EnumDocumentType.SupplierInvoice,
                    Header = new DocumentHeader
                    {
                        Date = DateTime.Today,
                        ClosingDate = DateTime.Today,
                        CustomerCode = "DUMP",
                        IncExcl = false,
                        DeliveryAddress = new ArrayOfString(),
                        Address = new ArrayOfString(),
                        InvoiceMessage = new ArrayOfString(),
                        OnHold = false,
                        OrderNumber = sOrderNumber,// numbers of the buyback number & excl BB " - " surname oBuyBack.GetAttributeValue<string>("isp_customerreference"),
                        Contact = target.Attributes.Contains("isp_contactid") ? target.GetAttributeValue<EntityReference>("isp_contactid").Name : "",
                        SalesAnalysisCode = SalesCode,
                        ShipDeliver = Delivery

                    },
                    Line = new DocumentLine[lineItems.Count]
                };
                trace.Trace("trace log 22");
                #region Address
                String ContactName = "";
                String VatIdNumber = "";
                if (oBuyBack.Attributes.Contains("isp_customer"))
                    ContactName = (oBuyBack.GetAttributeValue<EntityReference>("isp_customer")).Name;
                trace.Trace("trace log 23");
                if (oBuyBack.Attributes.Contains("isp_companyname") && !string.IsNullOrWhiteSpace(oBuyBack.GetAttributeValue<string>("isp_companyname")))
                    ContactName = oBuyBack.GetAttributeValue<string>("isp_companyname");
                trace.Trace("trace log 24");
                PurchaseNote.Header.DeliveryAddress.Add(ContactName);
                if (oBuyBack.Attributes.Contains("isp_idnumber") && !string.IsNullOrWhiteSpace(oBuyBack.GetAttributeValue<string>("isp_idnumber")))
                    VatIdNumber = oBuyBack.GetAttributeValue<string>("isp_idnumber");
                trace.Trace("trace log 25");
                if (oBuyBack.Attributes.Contains("isp_vatnumber") && !string.IsNullOrWhiteSpace(oBuyBack.GetAttributeValue<string>("isp_idnumber")))
                    VatIdNumber = oBuyBack.GetAttributeValue<string>("isp_vatnumber");
                trace.Trace("trace log 26");
                PurchaseNote.Header.DeliveryAddress.Add(VatIdNumber);
                if (oBuyBack.Attributes.Contains("isp_line1") && !string.IsNullOrWhiteSpace(oBuyBack.GetAttributeValue<string>("isp_line1")))
                    PurchaseNote.Header.DeliveryAddress.Add(oBuyBack.GetAttributeValue<string>("isp_line1"));
                trace.Trace("trace log 27");
                if (oBuyBack.Attributes.Contains("isp_line2") && !string.IsNullOrWhiteSpace(oBuyBack.GetAttributeValue<string>("isp_line2")))
                    PurchaseNote.Header.DeliveryAddress.Add(oBuyBack.GetAttributeValue<string>("isp_line2"));
                trace.Trace("trace log 28");
                if (oBuyBack.Attributes.Contains("isp_line3") && !string.IsNullOrWhiteSpace(oBuyBack.GetAttributeValue<string>("isp_line3")))
                    PurchaseNote.Header.DeliveryAddress.Add(oBuyBack.GetAttributeValue<string>("isp_line3"));
                String CityCode = "";
                trace.Trace("trace log 29");
                if (oBuyBack.Attributes.Contains("isp_city") && !string.IsNullOrWhiteSpace(oBuyBack.GetAttributeValue<string>("isp_city")))
                    CityCode += oBuyBack.GetAttributeValue<string>("isp_city");
                if (oBuyBack.Attributes.Contains("isp_postalcode") && !string.IsNullOrWhiteSpace(oBuyBack.GetAttributeValue<string>("isp_postalcode")))
                    CityCode += "-" + oBuyBack.GetAttributeValue<string>("isp_postalcode");
                PurchaseNote.Header.DeliveryAddress.Add(CityCode);
                trace.Trace("trace log 30");
                if (oBuyBack.Attributes.Contains("isp_telephone") && !string.IsNullOrWhiteSpace(oBuyBack.GetAttributeValue<string>("isp_telephone")))
                    PurchaseNote.Header.Telephone = oBuyBack.GetAttributeValue<string>("isp_telephone");
                trace.Trace("trace log 31");
                if (oBuyBack.Attributes.Contains("isp_fax") && !string.IsNullOrWhiteSpace(oBuyBack.GetAttributeValue<string>("isp_fax")))
                    PurchaseNote.Header.Fax = oBuyBack.GetAttributeValue<string>("isp_fax");
                if (oBuyBack.Attributes.Contains("discountpercentage"))
                    PurchaseNote.Header.Discount = oBuyBack.GetAttributeValue<decimal>("discountpercentage");
                trace.Trace("trace log 32");
                #endregion
                trace.Trace("trace log 33");
                string allTokens = Defaults.GetAttributeValue<string>("isp_serviceitemproductcode");
                String[] token = allTokens.ToLower().Split('|');
                Boolean serviceItem = false;
                trace.Trace(lineItems.Count.ToString());
                for (var i = 0; i < lineItems.Count; i++)
                {
                    #region check if it's a service item
                    serviceItem = false;
                    foreach (string t in token)
                    {
                        if (lineItems[i].productnumber.ToLower().StartsWith(t))
                        {
                            serviceItem = true;
                        }
                    }
                    #endregion
                    trace.Trace("trace log 34");
                    #region some quaity checks
                    Decimal quantity = new Decimal(1);
                    if (lineItems[i].quantity == null)
                    {
                        throw new Exception("Please ensure that all Buy Back product Items have a Quantity");
                    }
                    if (lineItems[i].productsbuybackprice == null)
                    {
                        throw new Exception("Ensure that all products have a Buy Back Product Price filled in");
                    }
                    #endregion
                    trace.Trace("trace log 35");
                    quantity = lineItems[i].quantity.Value;

                    #region productnumber
                    string sCode = "";
                    if (lineItems[i].productnumber != null)
                    {
                        sCode = lineItems[i].productnumber;
                    }
                    #endregion
                    #region Multistore
                    string sMultiStore = "";
                    if (Defaults.Attributes.Contains("isp_storenumber"))
                    {
                        sMultiStore = Defaults.GetAttributeValue<string>("isp_storenumber");
                    }
                    #endregion
                    #region Description
                    string sDescription = "";
                    if (lineItems[i].isp_existingproducts != null)
                    {
                        sDescription = lineItems[i].isp_existingproducts.Name;
                    }
                    #endregion
                    #region InclusivePrice
                    decimal dInclusivePrice = 0;
                    if (lineItems[i].productsbuybackprice != null)
                    {
                        dInclusivePrice = lineItems[i].productsbuybackprice.Value;
                    }
                    trace.Trace("trace log 36");
                    #endregion
                    #region UnitSellingPrice
                    decimal dUnitSellingPrice = 0;
                    if (lineItems[i].productsbuybackprice != null)
                    {
                        dUnitSellingPrice = lineItems[i].productsbuybackprice.Value;
                    }
                    #endregion
                    trace.Trace("trace log 38");
                    PurchaseNote.Line[i] = new DocumentLine
                    {
                        Code = sCode,
                        MultiStore = sMultiStore,
                        Description = sDescription,
                        InclusivePrice = dInclusivePrice,
                        Unit = "",
                        Quantity = serviceItem ? quantity * 1 : quantity,
                        UnitSellingPrice = dUnitSellingPrice,
                        TaxType = short.Parse((lineItems[i].isp_vatcode.Value.ToString())),
                        LineType = EnumLineType.Inventory,
                        CostCode = SalesCode
                    };
                }

                trace.Trace("trace log 39");
                string PastelWS = "http://sagcesql01/{0}";
                var binding = new System.ServiceModel.BasicHttpBinding(System.ServiceModel.BasicHttpSecurityMode.None) { CloseTimeout = new TimeSpan(0, 0, 25), OpenTimeout = new TimeSpan(0, 0, 25), SendTimeout = new TimeSpan(0, 0, 25), ReceiveTimeout = new TimeSpan(0, 0, 25) };
                PastelServiceSoapClient client = new PastelServiceSoapClient(binding, new System.ServiceModel.EndpointAddress(string.Format(PastelWS, "PastelService.asmx")));
                string PastelCompany = (string)Defaults.GetAttributeValue<EntityReference>("isp_pastelcompany").Name;
                trace.Trace("trace log 40");
                short PastelUser = 38;
                trace.Trace("Got doc exists");
                string purchaseNoteOrderNumber = PurchaseNote.Header.OrderNumber;
                if (PurchaseNote.Header.OrderNumber.Length > 15)
                {
                    purchaseNoteOrderNumber = PurchaseNote.Header.OrderNumber.Substring(0, 15);
                }
                var DocExists = client.GetRecord("acchisth", 11, purchaseNoteOrderNumber, PastelCompany, PastelUser, 8);
                trace.Trace("Got doc exists");
                if (DocExists != null)
                {
                    trace.Trace(DocExists.ToString());
                    if (DocExists.Substring(0, 1) == "0")
                    {
                        var ExistingDoc = DocExists.Substring(2);
                        Entity PurchaseNoteEntity = new Entity("isp_purchasenote");
                        trace.Trace("trace log 41");
                        PurchaseNoteEntity.Attributes.Add("isp_purchasenotenumber", ExistingDoc);
                        trace.Trace("trace log 42");
                        PurchaseNoteEntity.Attributes.Add("isp_buybacknumber", new EntityReference("isp_buyback", target.Id));
                        trace.Trace(target.Id.ToString());
                        PurchaseNoteEntity.Attributes.Add("ownerid", oBuyBack.GetAttributeValue<EntityReference>("ownerid"));
                        trace.Trace(oBuyBack.GetAttributeValue<EntityReference>("ownerid").ToString());
                        trace.Trace(oBuyBack.GetAttributeValue<Money>("isp_total").ToString());
                        PurchaseNoteEntity.Attributes.Add("isp_total", oBuyBack.GetAttributeValue<Money>("isp_total"));
                        trace.Trace((string)oBuyBack.GetAttributeValue<AliasedValue>("c.fullname").Value.ToString());
                        PurchaseNoteEntity.Attributes.Add("isp_contactname", (string)oBuyBack.GetAttributeValue<AliasedValue>("c.fullname").Value);
                        trace.Trace("trace log 46");
                        if (oBuyBack.GetAttributeValue<EntityReference>("isp_atp") != null)
                        {
                            trace.Trace(oBuyBack.GetAttributeValue<EntityReference>("isp_atp").ToString());
                            PurchaseNoteEntity.Attributes.Add("isp_atp", oBuyBack.GetAttributeValue<EntityReference>("isp_atp"));
                        }
                        if (oBuyBack.GetAttributeValue<string>("isp_idnumber") != null)
                        {
                            trace.Trace(oBuyBack.GetAttributeValue<string>("isp_idnumber").ToString());
                            PurchaseNoteEntity.Attributes.Add("isp_idnumber", oBuyBack.GetAttributeValue<string>("isp_idnumber"));
                        }
                        var PNote = service.Create(PurchaseNoteEntity);
                        target.Attributes["isp_purchasenote"] = new EntityReference("isp_purchasenote", PNote);
                        target.Attributes["isp_purchasenotecreatedby"] = new EntityReference("systemuser", context.UserId);
                        target.Attributes["isp_purchasenotecreatedon"] = DateTime.Now;
                        service.Execute(new Microsoft.Crm.Sdk.Messages.SetStateRequest { EntityMoniker = new EntityReference(isp_buyback.EntityLogicalName, context.PrimaryEntityId), Status = new OptionSetValue(863300003), State = new OptionSetValue(1) });
                        return;
                    }
                }

                trace.Trace("trace log 41");
                var rs = client.CreateDocument(PurchaseNote, PastelCompany, PastelUser);
                trace.Trace("trace log 42");
                if (rs.Success)
                {
                    var BuyBackNumber = rs.Output;
                    Entity PurchaseNoteEntity = new Entity("isp_purchasenote");
                    PurchaseNoteEntity.Attributes.Add("isp_purchasenotenumber", BuyBackNumber);
                    PurchaseNoteEntity.Attributes.Add("isp_buybacknumber", new EntityReference("isp_buyback", target.Id));
                    trace.Trace(target.Id.ToString());
                    PurchaseNoteEntity.Attributes.Add("ownerid", oBuyBack.GetAttributeValue<EntityReference>("ownerid"));
                    trace.Trace(oBuyBack.GetAttributeValue<EntityReference>("ownerid").ToString());
                    trace.Trace(oBuyBack.GetAttributeValue<Money>("isp_total").ToString());
                    PurchaseNoteEntity.Attributes.Add("isp_total", oBuyBack.GetAttributeValue<Money>("isp_total"));
                    trace.Trace((string)oBuyBack.GetAttributeValue<AliasedValue>("c.fullname").Value.ToString());
                    PurchaseNoteEntity.Attributes.Add("isp_contactname", (string)oBuyBack.GetAttributeValue<AliasedValue>("c.fullname").Value);
                    trace.Trace("trace log 46");
                    if (oBuyBack.GetAttributeValue<EntityReference>("isp_atp") != null)
                    {
                        trace.Trace(oBuyBack.GetAttributeValue<EntityReference>("isp_atp").ToString());
                        PurchaseNoteEntity.Attributes.Add("isp_atp", oBuyBack.GetAttributeValue<EntityReference>("isp_atp"));
                    }
                    if (oBuyBack.GetAttributeValue<string>("isp_idnumber") != null)
                    {
                        trace.Trace(oBuyBack.GetAttributeValue<string>("isp_idnumber").ToString());
                        PurchaseNoteEntity.Attributes.Add("isp_idnumber", oBuyBack.GetAttributeValue<string>("isp_idnumber"));
                    }
                    PurchaseNoteEntity.Attributes.Add("isp_paymentoption", oBuyBack.GetAttributeValue<OptionSetValue>("isp_paymentoptions"));
                    var PNote = service.Create(PurchaseNoteEntity);
                    target.Attributes["isp_purchasenote"] = new EntityReference("isp_purchasenote", PNote);
                    target.Attributes["isp_purchasenotecreatedby"] = new EntityReference("systemuser", context.UserId);
                    target.Attributes["isp_purchasenotecreatedon"] = DateTime.Now;
                    SetStateRequest setStatRequest = new SetStateRequest()
                    {
                        EntityMoniker = new EntityReference
                        {
                            Id = lineItems[0].isp_buybackid.Value,
                            LogicalName = isp_buyback.EntityLogicalName,
                        },
                        State = new OptionSetValue(1),
                        Status = new OptionSetValue(863300003)

                    };
                    service.Execute(setStatRequest);
                    trace.Trace("trace log 43");
                }
                else
                    throw new Exception("Error from Pastel: \r\n" + rs.ErrorMessage);
            }
        }
    }
}
