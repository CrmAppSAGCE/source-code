﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

namespace Scoin.Crm.Plugins
{
        public static class Util
        {

            public static Guid FindId(Entity Target, Entity PreEntity, string AttributeName)
            {
                Guid Id = Guid.Empty;

                if (Target != null && Target.Attributes.Contains(AttributeName) == true)
                {
                    if (Target.Attributes[AttributeName] != null)
                    {
                        Id = Target.GetAttributeValue<EntityReference>(AttributeName).Id;
                    }
                }
                else
                {
                    if (PreEntity != null)
                    {
                        if (PreEntity.Attributes.Contains(AttributeName) == true)
                        {
                            if (PreEntity.Attributes[AttributeName] != null)
                            {

                                Id = PreEntity.GetAttributeValue<EntityReference>(AttributeName).Id;
                            }
                        }
                    }
                }

                return Id;
            }

            public static OptionSetValue FindSetValue(Entity Target, Entity PreEntity, string AttributeName)
            {
                OptionSetValue Num = null;

                if (Target != null && Target.Attributes.Contains(AttributeName) == true)
                {
                    if (Target.Attributes[AttributeName] != null)
                    {
                        Num = Target.GetAttributeValue<OptionSetValue>(AttributeName);
                    }
                }
                else
                {
                    if (PreEntity != null)
                    {
                        if (PreEntity.Attributes.Contains(AttributeName) == true)
                        {
                            if (PreEntity.Attributes[AttributeName] != null)
                            {
                                Num = PreEntity.GetAttributeValue<OptionSetValue>(AttributeName);
                            }
                        }
                    }
                }
                return Num;
            }

            public static string GetOptionsSetTextOnValue(IOrganizationService service, string entityName, string attributeName, int selectedValue)
            {

                RetrieveAttributeRequest retrieveAttributeRequest = new RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = attributeName,
                    RetrieveAsIfPublished = true
                };
                // Execute the request.
                RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
                // Access the retrieved attribute.
                Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata)
                retrieveAttributeResponse.AttributeMetadata;// Get the current options list for the retrieved attribute.
                OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
                string selectedOptionLabel = string.Empty;
                foreach (OptionMetadata oMD in optionList)
                {
                    if (oMD.Value == selectedValue)
                    {
                        selectedOptionLabel = oMD.Label.UserLocalizedLabel.Label;
                    }
                }
                return selectedOptionLabel;
            }

            public static int FindOptionSetValue(Entity Target, Entity PreEntity, string AttributeName)
            {
                int Num = 0;

                if (Target != null && Target.Attributes.Contains(AttributeName) == true)
                {
                    if (Target.Attributes[AttributeName] != null)
                    {
                        Num = Target.GetAttributeValue<OptionSetValue>(AttributeName).Value;
                    }
                }
                else
                {
                    if (PreEntity != null)
                    {
                        if (PreEntity.Attributes.Contains(AttributeName) == true)
                        {
                            if (PreEntity.Attributes[AttributeName] != null)
                            {
                                Num = (PreEntity.GetAttributeValue<OptionSetValue>(AttributeName)).Value;
                            }
                        }
                    }
                }
                return Num;
            }

            public static int FindNumber(Entity Target, Entity PreEntity, string AttributeName)
            {
                int Num = 0;

                if (Target != null && Target.Attributes.Contains(AttributeName) == true)
                {
                    if (Target.Attributes[AttributeName] != null)
                    {
                        Num = Target.GetAttributeValue<int>(AttributeName);
                    }
                }
                else
                {
                    if (PreEntity != null)
                    {
                        if (PreEntity.Attributes.Contains(AttributeName) == true)
                        {
                            if (PreEntity.Attributes[AttributeName] != null)
                            {
                                Num = PreEntity.GetAttributeValue<int>(AttributeName);
                            }
                        }
                    }
                }
                return Num;
            }

            public static Decimal FindDecimal(Entity Target, Entity PreEntity, string AttributeName)
            {
                Decimal Num = 0;

                if (Target != null && Target.Attributes.Contains(AttributeName) == true)
                {
                    if (Target.Attributes[AttributeName] != null)
                    {
                        Num = Target.GetAttributeValue<decimal>(AttributeName);
                    }
                }
                else
                {
                    if (PreEntity != null)
                    {
                        if (PreEntity.Attributes.Contains(AttributeName) == true)
                        {
                            if (PreEntity.Attributes[AttributeName] != null)
                            {
                                Num = PreEntity.GetAttributeValue<decimal>(AttributeName);
                            }
                        }
                    }
                }
                return Num;
            }



            public static string FindString(Entity Target, Entity PreEntity, string AttributeName)
            {
                String Str = String.Empty;

                if (Target != null && Target.Attributes.Contains(AttributeName) == true)
                {
                    if (Target.Attributes[AttributeName] != null)
                    {
                        Str = Target.GetAttributeValue<String>(AttributeName);
                    }
                }
                else
                {
                    if (PreEntity != null)
                    {
                        if (PreEntity.Attributes.Contains(AttributeName) == true)
                        {
                            if (PreEntity.Attributes[AttributeName] != null)
                            {
                                Str = PreEntity.GetAttributeValue<String>(AttributeName);
                            }
                        }
                    }
                }
                return Str;
            }
        }


    }

