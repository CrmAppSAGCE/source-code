﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;



namespace Scoin.Crm.Plugins.isp_smsactivity
{
    public class postcreate : IPlugin
    {
        private readonly ProviderSettings providerSettings;
        public postcreate(string unsecure)
        {
            providerSettings = new ProviderSettings(unsecure);
        }

        public postcreate(string unsecure, string secure) : this(unsecure) { }

        private string ReturnEncodedHTML(string ToEncode)
        {
            ToEncode = ToEncode.Replace(" ", "%20");
            ToEncode = ToEncode.Replace("!", "%21");
            ToEncode = ToEncode.Replace("\"", "%5C");
            ToEncode = ToEncode.Replace("+", "%2B");
            ToEncode = ToEncode.Replace("&", "%26");
            ToEncode = ToEncode.Replace("?", "%3F");
            ToEncode = ToEncode.Replace(".", "%2E");
            ToEncode = ToEncode.Replace(",", "%2C");
            ToEncode = ToEncode.Replace("=", "%3D");
            return ToEncode;
        }
        private string ParseCellNumber(string To)
        {
            To = To.Replace("-", "");
            To = To.Replace("(", "");
            To = To.Replace(")", "");
            To = To.Replace(" ", "");
            return To;
        }
        private string SendSMS(string UserName, string Password, string Account, string id, string To, string Description)
        {
            Uri uri = new Uri("http://bulk.connet-systems.com/submit/");
            To = ParseCellNumber(To);
            if (To.IndexOf('0', 0) == 0)
                To = "%2b27" + To.Substring(1, 9);
            else if (To.IndexOf("+27", 0) == 0)
                To = "%2b" + To.Substring(1, 11);

            Description = ReturnEncodedHTML(Description);

            //UserName = "gcexchange";
            //Password = "gcexmsdksmpwha62yhj";
            //Account = "sagold";
            string vars = "?username=" + UserName + "&password=" + Password + "&account=" + Account + "&da=" + To + "&ud=" + Description + "&id=" + id;

            var WebReq = System.Net.HttpWebRequest.Create(uri + vars);
            WebReq.Method = "GET";
            HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

            Stream DeliveryResponse = WebResp.GetResponseStream();
            StreamReader SReader = new StreamReader(DeliveryResponse);
            string StatusCode = SReader.ReadLine();
            StatusCode = SReader.ReadLine().Remove(0, 6); //remove Code:_ 
            string MessageDescription = SReader.ReadLine().Substring(13);

            return MessageDescription;
        }
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            Entity smsactivity = (Entity)context.InputParameters["Target"];
            Guid EntityGuid = Guid.NewGuid();

            var description = smsactivity.GetAttributeValue<string>("description");
            var to = smsactivity.GetAttributeValue<EntityCollection>("to");
            bool failed = false;
            string response = String.Empty;

            if (!string.IsNullOrWhiteSpace(description) && to != null)
            {
                IList<string> numbers = new List<string>();
                foreach (var recipient in to.Entities)
                {
                    var partyref = (EntityReference)recipient.Attributes["partyid"];
                    const string numberfield = "mobilephone";
                    var entity = service.Retrieve(partyref.LogicalName.ToLower(), partyref.Id, new ColumnSet(new[] { numberfield }));
                    if (entity.Contains(numberfield))
                    {
                        var number = entity.GetAttributeValue<string>(numberfield);
                        if (!string.IsNullOrEmpty(number) && !numbers.Contains(number))
                            numbers.Add(number);
                    }
                }
                if (numbers.Count == 0)
                {
                    response = "No numbers to send to";
                    failed = true;
                }
                foreach (var number in numbers.Distinct())
                {
                    try
                    {
                        //need to get the id of the entity instance
                        string id = smsactivity["activityid"] + "_" + number.ToString();
                        response = id + SendSMS(providerSettings.Username, providerSettings.Password, providerSettings.Account, id, number, description);
                        //response = id + SendSMS(providerSettings.Username, providerSettings.Password, providerSettings.Account, id, number, smsactivity["activityid"].ToString());
                        if (response != "Accepted")
                        {
                            failed = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        failed = true;
                        response = ex.Message;
                    }
                }
            }
        }
    }
}
