﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins.purchasenote
{
    public class postupdatehistorycalculations : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.ParentContext != null)
            {
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                Entity target = (Entity)context.InputParameters["Target"];
                OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
                EntityHelper helper = new EntityHelper(context, service);
                ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                Entity preMessageImage = null;
                Entity postMessageImage = null;

                if (context.PreEntityImages.Contains("PreImage") && context.PreEntityImages["PreImage"] is Entity)
                {
                    preMessageImage = (Entity)context.PreEntityImages["PreImage"];
                }
                if (context.PostEntityImages.Contains("PostImage") && context.PostEntityImages["PostImage"] is Entity)
                {
                    postMessageImage = (Entity)context.PostEntityImages["PostImage"];
                }
                if (postMessageImage != null && preMessageImage != null)
                {
                    if (helper.HasValueChanged(preMessageImage, postMessageImage, "isp_runpaymentdatacalculator"))
                    {
                        int bullionQuantity = 0;
                        int rareQuantity = 0;
                        decimal rareValue = 0;
                        decimal bullionValue = 0;

                        GeneratedCode.isp_payment purchaseNotePayment = new GeneratedCode.isp_payment();

                        GeneratedCode.isp_purchasenote purchaseNote = (from pn in orgContext.CreateQuery<GeneratedCode.isp_purchasenote>()
                                                                       where pn.isp_purchasenoteId.Value == context.PrimaryEntityId
                                                                       select new GeneratedCode.isp_purchasenote
                                                                       {
                                                                           isp_purchasenoteId = pn.isp_purchasenoteId,
                                                                           isp_purchasenotenumber = pn.isp_purchasenotenumber,
                                                                           isp_buybacknumber = pn.isp_buybacknumber,
                                                                           isp_Total = pn.isp_Total
                                                                       }).FirstOrDefault();
                        isp_buyback buyBack = (from bb in orgContext.CreateQuery<isp_buyback>()
                                               where bb.isp_buybackId.Value == purchaseNote.isp_buybacknumber.Id
                                               select new isp_buyback
                                               {
                                                   isp_buybackId = bb.isp_buybackId
                                               }).FirstOrDefault();

                        List<isp_buybackproducts> buyBackProducts = (from bbp in orgContext.CreateQuery<isp_buybackproducts>()
                                                                     where bbp.isp_BuyBackProductsIdRelationship.Id == buyBack.isp_buybackId.Value
                                                                     && bbp.statecode.Value == isp_buybackproductsState.Active
                                                                     select new isp_buybackproducts
                                                                     {
                                                                         isp_buybackproductsId = bbp.isp_buybackproductsId,
                                                                         isp_BuyBackProductsIdRelationship = bbp.isp_BuyBackProductsIdRelationship,
                                                                         isp_Quantity = bbp.isp_Quantity,
                                                                         isp_ProductsBuyBackPrice = bbp.isp_ProductsBuyBackPrice,
                                                                         isp_ExistingProducts = bbp.isp_ExistingProducts
                                                                     }).ToList();
                        foreach (isp_buybackproducts bbp in buyBackProducts)
                        {
                            Product product = (from p in orgContext.CreateQuery<Product>()
                                               where p.ProductId.Value == bbp.isp_ExistingProducts.Id
                                               select new Product
                                               {
                                                   ProductId = p.ProductId,
                                                   isp_ProductType = p.isp_ProductType
                                               }).FirstOrDefault();

                            if (product.isp_ProductType.Value == 1)
                            {
                                bullionValue += bbp.isp_ProductsBuyBackPrice.Value * Convert.ToDecimal(bbp.isp_Quantity);
                                bullionQuantity += bbp.isp_Quantity.Value;
                            }
                            if (product.isp_ProductType.Value == 2)
                            {
                                rareValue += bbp.isp_ProductsBuyBackPrice.Value * Convert.ToDecimal(bbp.isp_Quantity);
                                rareQuantity += bbp.isp_Quantity.Value;
                            }
                        }
                        purchaseNotePayment.isp_PurchaseNoteId = new EntityReference(GeneratedCode.isp_purchasenote.EntityLogicalName, context.PrimaryEntityId);
                        purchaseNotePayment.isp_Amount = purchaseNote.isp_Total;
                        purchaseNotePayment.isp_PurchaseNoteBullionQuantity = bullionQuantity;
                        purchaseNotePayment.isp_PurchaseNoteBullionValue = new Money(bullionValue);
                        purchaseNotePayment.isp_PurchaseNoteRareQuantity = rareQuantity;
                        purchaseNotePayment.isp_PurchaseNoteRareValue = new Money(rareValue);
                        purchaseNotePayment.statuscode = new OptionSetValue(863300003);
                        purchaseNotePayment.isp_PaymentreferenceNumber = "Purchase Note: " + purchaseNote.isp_purchasenotenumber;
                        orgContext.AddObject(purchaseNotePayment);
                        orgContext.SaveChanges();
                    }
                }
            }
        }
    }
}
