﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Scoin.Crm.Plugins.GeneratedCode;

namespace Scoin.Crm.Plugins
{
    internal class CommissionHelper
    {
        /// <summary>
        /// Creates a new Broker Commission
        /// </summary>
        /// <param name="orgContext">The OrganizationServiceContext</param>
        /// <param name="commissionName">Name of the Commission</param>
        /// <param name="ownerId">The Broker</param>
        /// <param name="bullionCount">Number of bullion coins</param>
        /// <param name="bullionTotal">Total value of bullion coins</param>
        /// <param name="CommissionDeducedTotal">The amount that the broker's commission is worked out from</param>
        /// <param name="commissionBeginDate">The commission period begin date</param>
        /// <param name="commissionEndDate">The commission period end date</param>
        /// <param name="purchaseNoteItemQuantity">The number of purchase note items</param>
        /// <param name="purchaseNoteCommission">The purchase note value</param>
        /// <param name="commissionPeriodNumber">The commission period number</param>
        /// <returns>The Guid of the new broker</returns>
        public Guid createBrokerCommission(OrganizationServiceContext orgContext, string commissionName, EntityReference ownerId, int bullionCount, Money bullionTotal, Money CommissionDeducedTotal, DateTime commissionBeginDate, DateTime commissionEndDate, int purchaseNoteItemQuantity, int bullionQuantity, int commissionPeriodNumber)
        {
            isp_brokercommission brokerCommission = new isp_brokercommission();
            Guid brokerCommissionId = Guid.NewGuid();
            brokerCommission.isp_brokercommissionId = brokerCommissionId;
            brokerCommission.isp_name = commissionName;
            brokerCommission.OwnerId = ownerId;
            brokerCommission.isp_BullionCount = bullionCount;
            brokerCommission.isp_BullionValue = bullionTotal;
            brokerCommission.isp_CommissionableAmount = CommissionDeducedTotal;
            brokerCommission.isp_CommissionPeriodStartDate = commissionBeginDate;
            brokerCommission.isp_CommissionPeriodEndDate = commissionEndDate;
            brokerCommission.isp_PurchaseNoteItemQuantity = purchaseNoteItemQuantity;
            brokerCommission.isp_PurchaseNoteBullionQuantity = bullionQuantity;
            brokerCommission.isp_CommissionPeriodNumber = commissionPeriodNumber;
            orgContext.AddObject(brokerCommission);
            orgContext.SaveChanges();

            return brokerCommissionId;
        }

        /// <summary>
        /// Get the Commission linked to the broker at this current time to determine whether to update or create a new broker commission
        /// </summary>
        /// <param name="orgContext">The OrganizationServiceContext</param>
        /// <param name="ownerId">The broker</param>
        /// <param name="date">The date used to find the commission</param>
        /// <returns>A broker's commission if it exists</returns>
        public isp_brokercommission getBrokerCommission(OrganizationServiceContext orgContext, Guid ownerId, DateTime date)
        {
            isp_brokercommission brokerCommissionPeriod = null;

            List<isp_brokercommission> brokerCommission = (from bc in orgContext.CreateQuery<isp_brokercommission>()
                                                           where bc.OwnerId.Id == ownerId
                                                           select new isp_brokercommission
                                                           {
                                                               isp_brokercommissionId = bc.isp_brokercommissionId,
                                                               isp_CommissionableAmount = bc.isp_CommissionableAmount,
                                                               isp_BullionValue = bc.isp_BullionValue,
                                                               isp_BullionCount = bc.isp_BullionCount,
                                                               isp_CommissionPeriodStartDate = bc.isp_CommissionPeriodStartDate,
                                                               isp_CommissionPeriodEndDate = bc.isp_CommissionPeriodEndDate,
                                                               isp_PurchaseNoteCommission = bc.isp_PurchaseNoteCommission,
                                                               isp_PurchaseNoteItemQuantity = bc.isp_PurchaseNoteItemQuantity,
                                                               isp_PurchaseNoteBullionQuantity = bc.isp_PurchaseNoteBullionQuantity,
                                                               isp_CommissionPeriodNumber = bc.isp_CommissionPeriodNumber,
                                                               OwnerId = bc.OwnerId
                                                           }).ToList();

            foreach (isp_brokercommission bc in brokerCommission)
            {
                if (bc.isp_CommissionPeriodStartDate.Value.ToLocalTime().Date <= date.ToLocalTime().Date && bc.isp_CommissionPeriodEndDate.Value.ToLocalTime().Date >= date.ToLocalTime().Date)
                {
                    brokerCommissionPeriod = bc;
                }
            }
            return brokerCommissionPeriod;
        }

        /// <summary>
        /// Gets the Commission Period being used
        /// </summary>
        /// <param name="orgContext">The OrganizationServiceContext</param>
        /// <param name="date">The date used to find the Commission Period</param>
        /// <returns>The Commission Period being used</returns>
        public isp_commissionperiod getCommissionPeriod(OrganizationServiceContext orgContext, DateTime date, int commissionFor, ITracingService trace)
        {
            isp_commissionperiod commission = null;
            List<isp_commissionperiod> commissionPeriod = (from cp in orgContext.CreateQuery<isp_commissionperiod>()
                                                           where cp.isp_CommissionFor.Value == commissionFor
                                                           && cp.statecode.Value == isp_commissionperiodState.Active
                                                           select new isp_commissionperiod
                                                           {
                                                               isp_commissionperiodId = cp.isp_commissionperiodId,
                                                               isp_BeginDate = cp.isp_BeginDate,
                                                               isp_EndDate = cp.isp_EndDate,
                                                               isp_CommissionFor = cp.isp_CommissionFor,
                                                               isp_CommissionPeriodNumber = cp.isp_CommissionPeriodNumber
                                                           }).ToList();
            foreach (isp_commissionperiod cp in commissionPeriod)
            {
                trace.Trace("Start Date: " + cp.isp_BeginDate.Value.ToLocalTime().Date.ToString());
                trace.Trace("Date: " + date.ToLocalTime().Date.ToString());
                trace.Trace("End Date: " + cp.isp_EndDate.Value.ToLocalTime().Date.ToString());
                if (cp.isp_BeginDate.Value.ToLocalTime().Date <= date.ToLocalTime().Date && cp.isp_EndDate.Value.ToLocalTime().Date >= date.ToLocalTime().Date)
                {
                    commission = cp;
                    trace.Trace("Got commission");
                }
            }
            if (commission == null)
            {
                throw new InvalidPluginExecutionException("A Commission Period does not exist. ");
            }
            return commission;
        }

        /// <summary>
        /// Creates a new Shop Commission
        /// </summary>
        /// <param name="orgContext">The OrganizationServiceContext</param>
        /// <param name="commissionName">Name of the Commission</param>
        /// <param name="ownerId">The Broker</param>
        /// <param name="bullionCount">Number of bullion coins</param>
        /// <param name="bullionTotal">Total value of bullion coins</param>
        /// <param name="CommissionDeducedTotal">The amount that the broker's commission is worked out from</param>
        /// <param name="commissionBeginDate">The commission period begin date</param>
        /// <param name="commissionEndDate">The commission period end date</param>
        /// <param name="purchaseNoteItemQuantity">The number of purchase note items</param>
        /// <param name="purchaseNoteCommission">The purchase note value</param>
        /// <param name="commissionPeriodNumber">The commission period number</param>
        /// <returns>The Guid of the new shop commission</returns>
        public Guid createShopCommission(OrganizationServiceContext orgContext, string commissionName, EntityReference ownerId, int bullionCount, Money bullionTotal, Money CommissionDeducedTotal, Money doubleCommission, Money rareCommission, DateTime commissionBeginDate, DateTime commissionEndDate, int commissionPeriodNumber)
        {
           decimal doubleCommissionValue = doubleCommission != null ? doubleCommission.Value : 0;
            isp_shopcommission shopCommission = new isp_shopcommission();
            Guid shopCommisionId = Guid.NewGuid();
            shopCommission.isp_shopcommissionId = shopCommisionId;
            shopCommission.isp_name = commissionName;
            shopCommission.isp_ShopId = ownerId;
            shopCommission.OwnerId = ownerId;
            shopCommission.isp_BullionCount = bullionCount;
            shopCommission.isp_BullionValue = bullionTotal;
            shopCommission.isp_CommissionableAmount = new Money(CommissionDeducedTotal.Value + doubleCommissionValue);
            shopCommission.isp_DoubleCommission = doubleCommission;
            shopCommission.isp_RareCommission = rareCommission;
            shopCommission.isp_CommissionPeriodStartDate = commissionBeginDate.ToLocalTime();
            shopCommission.isp_CommissionPeriodEndDate = commissionEndDate.ToLocalTime();
            shopCommission.isp_CommissionPeriodNumber = commissionPeriodNumber;
            orgContext.AddObject(shopCommission);
            orgContext.SaveChanges();

            return shopCommisionId;
        }

        /// <summary>
        /// Get the Commission linked to the broker at this current time to determine whether to update or create a new shop commission
        /// </summary>
        /// <param name="orgContext">The OrganizationServiceContext</param>
        /// <param name="ownerId">The Shop</param>
        /// <param name="date">The date</param>
        /// <returns></returns>
        public isp_shopcommission getShopCommission(OrganizationServiceContext orgContext, Guid ownerId, DateTime date)
        {
            isp_shopcommission shopCommissionPeriod = null;

            List<isp_shopcommission> shopCommission = (from sc in orgContext.CreateQuery<isp_shopcommission>()
                                                       where sc.OwnerId.Id == ownerId
                                                       select new isp_shopcommission
                                                         {
                                                             isp_shopcommissionId = sc.isp_shopcommissionId,
                                                             isp_CommissionableAmount = sc.isp_CommissionableAmount,
                                                             isp_BullionValue = sc.isp_BullionValue,
                                                             isp_BullionCount = sc.isp_BullionCount,
                                                             isp_DoubleCommission = sc.isp_DoubleCommission,
                                                             isp_RareCommission = sc.isp_RareCommission,
                                                             isp_CommissionPeriodStartDate = sc.isp_CommissionPeriodStartDate,
                                                             isp_CommissionPeriodEndDate = sc.isp_CommissionPeriodEndDate,
                                                             isp_CommissionPeriodNumber = sc.isp_CommissionPeriodNumber,
                                                             OwnerId = sc.OwnerId
                                                         }).ToList();

            foreach (isp_shopcommission sc in shopCommission)
            {
                if (sc.isp_CommissionPeriodStartDate.Value.ToLocalTime().Date <= date.ToLocalTime().Date && sc.isp_CommissionPeriodEndDate.Value.ToLocalTime().Date >= date.ToLocalTime().Date)
                {
                    shopCommissionPeriod = sc;
                }
            }
            return shopCommissionPeriod;
        }

        public CommissionHelper()
        {
        }
    }
}
