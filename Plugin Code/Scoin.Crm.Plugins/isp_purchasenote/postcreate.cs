﻿using System;
using Microsoft.Xrm.Sdk;

namespace Scoin.Crm.Plugins.isp_purchasenote
{
    public class postcreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            Entity target = (Entity)context.InputParameters["Target"];

            target.Attributes.Add("isp_purchasenoteguid", context.PrimaryEntityId.ToString());
            service.Update(target);
        }
    }
}
