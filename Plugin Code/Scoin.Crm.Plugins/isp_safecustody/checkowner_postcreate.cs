﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;

namespace Scoin.Crm.Plugins.isp_safecustody
{
    public class checkowner_postcreate : IPlugin
    {

        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            var target = (Entity)context.InputParameters["Target"];

            if (target.Contains("isp_atpnumber"))
            {
                var orderRef = target.GetAttributeValue<EntityReference>("isp_atpnumber");
                var order = service.Retrieve("salesorder", orderRef.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(new [] {"ownerid"}));

                var owner = order.GetAttributeValue<EntityReference>("ownerid");
                if (target.Contains("ownerid"))
                {
                    if (target.GetAttributeValue<EntityReference>("ownerid").Id != owner.Id)
                        service.Execute(new Microsoft.Crm.Sdk.Messages.AssignRequest { Target = new EntityReference("isp_safecustody", target.Id), Assignee = owner });
                } else if (context.UserId != owner.Id)
                    service.Execute(new Microsoft.Crm.Sdk.Messages.AssignRequest { Target = new EntityReference("isp_safecustody", target.Id), Assignee = owner });
            }

        }
    }
}
