﻿using System;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace StockAllocationPlugin
{
    public class PreUpdate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var Target = (Entity)context.InputParameters["Target"];
            var PreImage = (context.PreEntityImages.Contains("PreImage") ? (Entity)context.PreEntityImages["PreImage"] : null);
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(service);
            ITracingService trace = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            if (context.Depth > 1)
            {
                return;
            }
            if (Target.Attributes.Contains("isp_stockfrompastel"))
            {
                #region Get details from record we're on

                isp_stockcheck parentStockCheck = (from sc in orgContext.CreateQuery<isp_stockcheck>()
                                                   where sc.isp_PastelWarehouse.Id == ((EntityReference)Target["isp_pastelwarehouse"]).Id
                                                   && sc.isp_ProductCode == (string)Target["isp_productcode"]
                                                   && sc.isp_parentstock == null
                                                   select new isp_stockcheck
                                                   {
                                                       isp_stockcheckId = sc.isp_stockcheckId,
                                                       isp_StockFromPastel = sc.isp_StockFromPastel,
                                                       isp_StockLevel = sc.isp_StockLevel,
                                                       isp_ProductCode = sc.isp_ProductCode,
                                                       isp_PastelWarehouse = sc.isp_PastelWarehouse
                                                   }).FirstOrDefault();
                trace.Trace("starting");
                int iPrePastelStockLevel = 0;
                trace.Trace("got pre stock");
                int iPostPastelLevel = (int)Target["isp_stockfrompastel"];
                Guid sPastelWarehouse = Guid.Empty;
                trace.Trace("about to get warehouse");
                if (parentStockCheck.isp_PastelWarehouse.Id != null)
                {
                    sPastelWarehouse = parentStockCheck.isp_PastelWarehouse.Id;
                }
                trace.Trace("got warehouse");
                String sProduct = string.Empty;
                if (parentStockCheck.isp_ProductCode != null)
                {
                    sProduct = parentStockCheck.isp_ProductCode;
                }
                trace.Trace("got product");
                #endregion

                var childStock = (from cs in orgContext.CreateQuery<isp_stockcheck>()
                                  where cs.isp_ProductCode == sProduct
                                  && cs.isp_PastelWarehouse.Id == sPastelWarehouse
                                  && cs.isp_parentstock != null
                                  select new
                                  {
                                      stockcheckid = cs.isp_stockcheckId,
                                      stocklevel = cs.isp_StockLevel
                                  }).ToList();
                int iChildLevel = 0;
                if (childStock.Count > 0)
                {
                    foreach (var c in childStock)
                    {
                        iChildLevel += c.stocklevel.Value;
                    }
                }
                iPrePastelStockLevel = iPostPastelLevel - iChildLevel;
                parentStockCheck.isp_StockLevel = iPrePastelStockLevel;
                orgContext.UpdateObject(parentStockCheck);
                orgContext.SaveChanges();
            }
        }
    }
}
