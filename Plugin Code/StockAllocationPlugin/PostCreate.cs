﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlClient;

namespace StockAllocationPlugin
{
    public class PostCreate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var target = (Entity)context.InputParameters["Target"];

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            if (target.Attributes.Contains("isp_stockfrompastel"))
            {
                Int32 iStockFromPastel = (Int32)target["isp_stockfrompastel"];
                if (iStockFromPastel != 0)
                {
                    Entity oStockCheck = new Entity("isp_stockcheck");
                    oStockCheck.Attributes.Add("isp_stockcheckid", target.Id);
                    target.Attributes.Add("isp_stocklevel", iStockFromPastel);
                    service.Update(target);
                }
            }         
        }
    }
}
