﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Crm.Plugins;
using Microsoft.Xrm;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk;

/// <summary>
/// Updates the Stock History from the data contained in the stock history staging table
/// </summary>
/// <author>Kieran Petrie</author>
/// <company>iSPartners</company>
namespace InsertStockHistory
{
    class EntityUpdater
    {
        #region "SQL Setup"
        string sqlQuery = "SELECT * FROM [Scoin].[Staging].[StockHistory] "+
                          "where StoreCode != '' "+
                          "ORDER BY [ProductCode], [StoreCode],[DocumentDate] desc , DocumentNumber desc, LineNumber asc";
        string stockLevelQuery = "select isp_productstocklevelId, ProductNumber, Scoin_Test_MSCRM.dbo.isp_pastelwarehouse.isp_pastelwarehousecode " +
                                 "from Scoin_Test_MSCRM.dbo.isp_productstocklevel, Scoin_Test_MSCRM.dbo.Product, Scoin_Test_MSCRM.dbo.isp_pastelwarehouse " +
                                 "where Scoin_Test_MSCRM.dbo.isp_productstocklevel.isp_pastelwarehouse = Scoin_Test_MSCRM.dbo.isp_pastelwarehouse.isp_pastelwarehouseId " +
                                 "and Scoin_Test_MSCRM.dbo.isp_productstocklevel.isp_product = Scoin_Test_MSCRM.dbo.Product.ProductId";
        string stockHistoryQuery = "select isp_stockhistoryid, isp_linenumber, isp_documentdate, isp_documentnumber, isp_quantity, isp_runningtotal, isp_productstocklevelid, isp_sequence from Scoin_Test_MSCRM..isp_stockhistory;";
        SqlConnection thisConnection;
        SqlCommand command, productStockCommand, stockHistoryCommand;
        SqlDataReader reader, productStockReader, stockHistoryReader;
        bool hasConnection = false;
        #endregion

        #region "CRM Setup"
        private string[] _columns = { "isp_sequence", "isp_documentdate", "isp_documentnumber", "isp_quantity", "isp_runningtotal", "isp_productstocklevelid", "isp_linenumber" };
        private string userguid = "CD41E1E6-6DEB-E011-868E-000E0C3BF634";
        #endregion

        #region "Dictionary Setup"
        Dictionary<String, String> stockMap = new Dictionary<String, String>();
        Dictionary<String, Entity> stockHistoryMap = new Dictionary<String, Entity>();
        Dictionary<String, Int32> sequenceMap = new Dictionary<String, Int32>();
        Dictionary<String, Int32> runningTotalMap = new Dictionary<String, Int32>();
        #endregion

        #region "setSequence"
        /// <summary>
        /// This will check and set the next sequence number for the given Entity via the isp_productstocklevelid
        /// </summary>
        /// <param name="entity">Entity to check/update</param>
        /// <returns>true if the entity was altered</returns>
        private bool setSequence(Entity entity)
        {
            int mapNumber = 1;
            string key = ((EntityReference)entity["isp_productstocklevelid"]).Id.ToString();
            if (sequenceMap.ContainsKey(key))
            {
                //The sequence number in the map was the last one issued so get it and add 1 to get this entities sequence number
                mapNumber = sequenceMap[key];
                mapNumber++;
                sequenceMap[key] = mapNumber;
            }
            else
            {
                sequenceMap.Add(key, mapNumber);
            }

            //Check if the sequence number matches
            if (entity.Contains("isp_sequence"))
            {
                if (entity["isp_sequence"].ToString() != mapNumber.ToString().PadLeft(7, '0'))
                {
                    entity["isp_sequence"] = mapNumber.ToString().PadLeft(7, '0');
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                //Must be an insert so set the sequence
                entity.Attributes.Add("isp_sequence", mapNumber.ToString().PadLeft(7, '0'));
                return true;
            }
        }
        #endregion

        #region "setRunningTotal"
        /// <summary>
        /// This will check and set the running total for the given Entity via the isp_productstocklevelid
        /// </summary>
        /// <param name="entity">Entity to check/update</param>
        /// <returns>true if the entity was altered</returns>
        private bool setRunningTotal(Entity entity)
        {
            int runningTotal = 0;
            string key = ((EntityReference)entity["isp_productstocklevelid"]).Id.ToString();
            if (runningTotalMap.ContainsKey(key))
            {
                //The total in the map should be this entities total
                runningTotal = runningTotalMap[key];
            }
            else
            {
                runningTotalMap.Add(key, runningTotal);
            }

            //Check if the running total matches
            if (entity.Contains("isp_runningtotal"))
            {
                if (int.Parse(entity["isp_runningtotal"].ToString()) != runningTotal)
                {
                    entity["isp_runningtotal"] = runningTotal;
                    runningTotalMap[key] = runningTotal + int.Parse(entity["isp_quantity"].ToString());
                    return true;
                }
                else
                {
                    //Set what the next running total should be;
                    runningTotalMap[key] = runningTotal + int.Parse(entity["isp_quantity"].ToString());
                    return false;
                }
            }
            else
            {
                //Must be an insert so set the runningtotal
                entity.Attributes.Add("isp_runningtotal", runningTotal);
                runningTotal = runningTotal + int.Parse(entity["isp_quantity"].ToString());
                runningTotalMap[key] = runningTotal;
                return true;
            }
        }
        #endregion

        #region "establishCommand"
        /// <summary>
        /// Established a connection to the db and executes the command prepared in the SQL Setup section
        /// </summary>
        /// <returns>SQLDataReader with the results of command</returns>
        private SqlDataReader establishCommand()
        {
            try
            {
                if(!hasConnection)
                {
                    getConnection();
                }

                if(hasConnection)
                {
                    command = thisConnection.CreateCommand();
                    command.CommandText = sqlQuery;
                    reader = command.ExecuteReader();
                    return reader;
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }
        #endregion

        #region "closeConnection"
        /// <summary>
        /// Cleans up the db reader and connection
        /// </summary>
        private void closeConnection()
        {
            try
            {
                reader.Close();
                thisConnection.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        #endregion

        #region "createCRMProvider"
        /// <summary>
        /// Creates a provider to CRM
        /// </summary>
        /// <returns>Provider</returns>
        private TestServiceProvider createCRMProvider()
        {
            try
            {
                //Establish the connection to CRM
                TestPluginContext pluginContext = new TestPluginContext("sagcecrm", "Coinnect", "crmapp", "i$Partner$", "sagoldcoin");
                TestServiceProvider provider = new TestServiceProvider(pluginContext);
                return provider;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }
        #endregion

        #region "getConnection"
        /// <summary></summary>
        /// 
        private void getConnection()
        {
            if (!hasConnection)
            {
                try
                {
                    thisConnection = new SqlConnection("Server=sagcesql;Database=Scoin;User ID=EntityUpdater;Password=pass@word1;Trusted_Connection=False;");
                    thisConnection.Open();
                    hasConnection = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }
            }

        }
        #endregion

        #region "getStock"
        /// <summary>
        /// Sets up the stockMap with the following KVP <key>warehosuecode+productcode</key><value>isp_productstocklevelid</value>
        /// As the values are read from the staging table they must be matched to the correct isp_productstocklevelid. As the only variables in the staging table
        /// to do this are warehousecode and productcode the stock map uses this as its key.
        /// </summary>
        private void getStock()
        {
            try
            {
                if (!hasConnection)
                {
                    getConnection();
                }

                if (hasConnection)
                {
                    productStockCommand = thisConnection.CreateCommand();
                    productStockCommand.CommandText = stockLevelQuery;
                    productStockReader = productStockCommand.ExecuteReader();

                    while (productStockReader.Read())
                    {
                        string key = productStockReader["isp_pastelwarehousecode"].ToString() + productStockReader["ProductNumber"].ToString();
                        string value = productStockReader["isp_productstocklevelId"].ToString();
                        stockMap.Add(key, value);
                    }
                    productStockReader.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
        #endregion

        #region "getStockHistory"
        /// <summary>
        /// Sets up the stockHistoryMap with the following KVP <key>documentnumber+linenumber</key><value>entity</value>
        /// This will fetch all of the current stock history in CRM and add them to the stockHistoryMap. As the only variables in the staging table
        /// which can be uniquely mapped to current stock history are documentnumber and linenumber this is used as the key. These are fetched using the SDK 
        /// as this will make updating easier
        /// </summary>
        /// <param name="provider">CRM provider to make the request through</param>
        private void getStockHistory()
        {
            try
            {
                if (!hasConnection)
                {
                    getConnection();
                }

                if (hasConnection)
                {
                    stockHistoryCommand = thisConnection.CreateCommand();
                    stockHistoryCommand.CommandText = stockHistoryQuery;
                    stockHistoryReader = stockHistoryCommand.ExecuteReader();

                    while (stockHistoryReader.Read())
                    {
                        Entity temp = new Entity("isp_stockhistory");
                        temp.Attributes.Add("isp_stockhistoryid", new Guid(stockHistoryReader["isp_stockhistoryid"].ToString()));
                        temp.Attributes.Add("isp_linenumber",stockHistoryReader["isp_linenumber"]);
                        temp.Attributes.Add("isp_documentdate",stockHistoryReader["isp_documentdate"]);
                        temp.Attributes.Add("isp_documentnumber", stockHistoryReader["isp_documentnumber"]);
                        temp.Attributes.Add("isp_quantity",stockHistoryReader["isp_quantity"]);
                        temp.Attributes.Add("isp_runningtotal", stockHistoryReader["isp_runningtotal"]);
                        temp.Attributes.Add("isp_productstocklevelid", new EntityReference("isp_productstocklevel", new Guid(stockHistoryReader["isp_productstocklevelid"].ToString())));
                        temp.Attributes.Add("isp_sequence", stockHistoryReader["isp_sequence"].ToString());
                        string key = temp["isp_documentnumber"].ToString() + temp["isp_linenumber"].ToString();
                        stockHistoryMap.Add(key, temp);
                    }
                    stockHistoryReader.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }

        }
        #endregion

        #region "insertStockHistory"
        /// <summary>
        /// This will setup the given entity and try and insert it into CRM
        /// </summary>
        /// <param name="service">CRM service to call the create on</param>
        /// <param name="toInsert">Entity to create in CRM</param>
        /// <returns>true if successful false otherwise</returns>
        private bool insertStockHistory(IOrganizationService service, Entity toInsert)
        {
            try
            {
                string key = reader["StoreCode"].ToString() + reader["ProductCode"].ToString();
                //check for the product stock level id
                if (stockMap.ContainsKey(key))
                {
                    EntityReference reference = new EntityReference();
                    reference.Id = new Guid(stockMap[key].ToString());
                    toInsert.Attributes.Add("isp_productstocklevelid", reference);
                }
                else
                {
                    //Cant find this product store comobo therefore cannot insert
                    Console.WriteLine("Failed to find Stock Id for {0},{1}", reader["StoreCode"].ToString(), reader["ProductCode"].ToString());
                    return false;
                }

                //Set the sequence number for this entity
                setSequence(toInsert);
                //Set the running total for this entity
                setRunningTotal(toInsert);
                //Create the entity
                service.Create(toInsert);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }
        #endregion

        #region "updateStockHistory"
        /// <summary>
        /// This will check the given entity for update and if required update the entity in CRM
        /// <param name="service">CRM service to call the create on</param>
        /// <param name="retrieved">CRM Entity retrievevd and stored in stockHistoryMap</param>
        /// <param name="toUpdate">Entity to update in CRM</param>
        /// <returns>true if successful false otherwise</returns>
        private bool updateStockHistory(IOrganizationService service, Entity toUpdate)
        {
            try
            {
                bool update = false;

                if (setSequence(toUpdate))
                {
                    update = true;
                }

                if (setRunningTotal(toUpdate))
                {
                    update = true;
                }

                if (update)
                {
                    service.Update(toUpdate);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }
        #endregion

        #region "updateCRM"
        /// <summary>
        /// This will decide if the given entity needs to be updated or inserted.
        /// </summary>
        /// <param name="entity">Entity to check from the Stagging Table</param>
        /// <param name="provider">CRM provider to do the update/insert</param>
        /// <returns>true if successfully processed</returns>
        private bool updateCRM(IServiceProvider provider, Entity entity)
        {
            bool success = false;
            //Setup the CRM service
            IOrganizationService service = (IOrganizationService)provider.GetService(typeof(IOrganizationServiceFactory));

            try
            {
                String key = entity["isp_documentnumber"].ToString()+ entity["isp_linenumber"].ToString();
                if (stockHistoryMap.ContainsKey(key))
                {
                    //This history item is in CRM so check for update. Only the sequence no and running total may be updated
                    //due to a more recent transaction therefore work with the retrieved
                    success = updateStockHistory(service, stockHistoryMap[key]);
                }
                else
                {
                    //We have not found the history item and should therefore create it
                    success = insertStockHistory(service, entity);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }
            return success;
        }
        #endregion

        static int Main(string[] args)
        {
            
            Console.WriteLine("***** Starting @{0}*****", DateTime.Now);
            //Create the updater
            EntityUpdater updater = new EntityUpdater();
            //Setup the required data
            updater.getStock();
            updater.getStockHistory();
            SqlDataReader reader = updater.establishCommand();
            

            int successCount = 0;
            int failureCount = 0;

            if (reader != null)
            {
                IServiceProvider crmProvider = (IServiceProvider)updater.createCRMProvider();
                if (crmProvider != null)
                {
                    //updater.getStockHistory(crmProvider);
                    //We have a reader with the data from the db
                    while (reader.Read())
                    {
                        try
                        {
                            Entity entityToUpdate = new Entity("isp_stockhistory");
                            entityToUpdate.Attributes.Add("isp_linenumber", reader["LineNumber"]);
                            entityToUpdate.Attributes.Add("isp_documentdate", reader["DocumentDate"]);
                            entityToUpdate.Attributes.Add("isp_documentnumber", reader["DocumentNumber"]);
                            entityToUpdate.Attributes.Add("isp_quantity", int.Parse(reader["Quantity"].ToString()));

                            if (updater.updateCRM(crmProvider, entityToUpdate))
                            {
                                successCount++;
                            }
                            else
                            {
                                failureCount++;
                            }

                            if ((successCount + failureCount) % 2000 == 0)
                            {
                                Console.WriteLine("***Count = {0}***", successCount + failureCount);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            failureCount++;
                            Console.WriteLine("\t{0}\t{1}\t{2}", reader["StoreCode"], reader["ProductCode"], reader["QuantityOnHand"]);
                        }

                    }
                    updater.closeConnection();
                    Console.WriteLine("*****Success = {0} ********", successCount);
                    Console.WriteLine("*****Failure = {0} ********", failureCount);
                    Console.WriteLine("***** Finished @{0}*****", DateTime.Now);
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            return 1;
        }
    }
}

