﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Crm.Plugins;
using Microsoft.Xrm;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk;
using Microsoft.Win32;  

namespace UpdatePastelWareHouse
{
    class EntityUpdater
    {
        #region "SQL setup"
        string sqlQuery = "SELECT * FROM [Coinnect].[Scoin].[PastelWareHouse] ";
        SqlConnection thisConnection;
        SqlCommand command;
        SqlDataReader reader;
        #endregion

        #region "CRM Setup"
        private string[] _columns = { "isp_pastelwarehousecode", "isp_pastelwarehousename" };
        private string userguid = "CD41E1E6-6DEB-E011-868E-000E0C3BF634";
        Guid companyGuid = new Guid("E80361E3-000A-E111-A61C-78ACC08839C9");
        #endregion

        #region "Entity Ref Setup"
        EntityReference companyRef = new EntityReference();

        public EntityUpdater()
        {
            companyRef.Id = companyGuid;
            //defaultCurrencyRef.LogicalName = "Currency";
            companyRef.Name = "Scoin";
        }
        #endregion

        #region "establishCommand"
        /// <summary>
        /// Established a connection to the db and executes the command prepared in the SQL Setup section
        /// </summary>
        /// <returns>SQLDataReader with the results of command</returns>
        private SqlDataReader establishCommand()
        {
            try
            {
                string sConnStr = ReadReg("CRMDB");
                thisConnection = new SqlConnection(sConnStr);
                thisConnection.Open();
                command = thisConnection.CreateCommand();
                command.CommandText = sqlQuery;
                reader = command.ExecuteReader();
                return reader;
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }
        #endregion

        #region "closeConnection"
        /// <summary>
        /// Cleans up the db reader and connection
        /// </summary>
        private void closeConnection()
        {
            try
            {
                reader.Close();
                thisConnection.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        #endregion

        #region "createCRMProvider"
        /// <summary>
        /// Creates a provider to CRM
        /// </summary>
        /// <returns>Provider</returns>
        private TestServiceProvider createCRMProvider()
        {
            try
            {
                //Establish the connection to CRM
                TestPluginContext pluginContext = new TestPluginContext("sagcecrm", "Coinnect", "crmapp", "i$Partner$", "sagoldcoin");
                TestServiceProvider provider = new TestServiceProvider(pluginContext);
                return provider;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }
        #endregion

        #region "updateCRM"
        /// <summary>
        /// This will decide if the given entity needs to be updated or inserted.
        /// </summary>
        /// <param name="entity">Entity to check from the Stagging Table</param>
        /// <param name="provider">CRM provider to do the update/insert</param>
        /// <returns>true if successfully processed</returns>
        private bool updateCRM(IServiceProvider provider, Entity entity)
        {
            bool success = false;
            //Setup the CRM service
            IOrganizationService service = (IOrganizationService)provider.GetService(typeof(IOrganizationServiceFactory));

            //Create the CRM query
            var refNumQuery = new QueryExpression("isp_pastelwarehouse") { Criteria = new FilterExpression(), PageInfo = new PagingInfo { Count = 1, PageNumber = 1 } };
            refNumQuery.Criteria.AddCondition("isp_pastelwarehousecode", ConditionOperator.Equal, entity["isp_pastelwarehousecode"]);
            refNumQuery.Criteria.AddCondition("isp_pastelcompanyidname", ConditionOperator.Equal, "SCOIN");
            //refNumQuery.ColumnSet = new ColumnSet(true);
            refNumQuery.ColumnSet.AddColumns(_columns);
            var retrieveResult = new EntityCollection();
            try
            {
                //Fetch from CRM
                retrieveResult = service.RetrieveMultiple(refNumQuery);

                if (retrieveResult.Entities.Count == 1)
                {
                    //We have a match so check if it is the same and update
                    success = updateWareHouse(service, retrieveResult.Entities[0], entity);
                }
                else if (retrieveResult.Entities.Count == 0)
                {
                    success = insertWareHouse(service, entity);
                    //We have not found the product and should therefore create it
                }
                else
                {
                    //Got more than one result abort
                    return false;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            return success;
        }
        #endregion

        #region "updateWareHouse"
        /// <summary>
        /// This will check the given entity for update and if required update the entity in CRM
        /// <param name="service">CRM service to call the create on</param>
        /// <param name="retrieved">CRM Entity retrievevd and stored in stockHistoryMap</param>
        /// <param name="toUpdate">Entity to update in CRM</param>
        /// <returns>true if successful false otherwise</returns>
        private bool updateWareHouse(IOrganizationService provider, Entity retrieved, Entity toUpdate)
        {
            try
            {
                if (retrieved["isp_pastelwarehousename"].ToString() != toUpdate["isp_pastelwarehousename"].ToString())
                {
                    retrieved["isp_pastelwarehousename"] = toUpdate["isp_pastelwarehousename"];
                }

                provider.Update(retrieved);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }

            return true;
        }
        #endregion

        #region "insertStockHistory"
        /// <summary>
        /// This will setup the given entity and try and insert it into CRM
        /// </summary>
        /// <param name="service">CRM service to call the create on</param>
        /// <param name="toInsert">Entity to create in CRM</param>
        /// <returns>true if successful false otherwise</returns>
        private bool insertWareHouse(IOrganizationService provider, Entity toInsert)
        {
            try
            {
                toInsert.Attributes.Add("isp_pastelcompanyid", companyRef);
                provider.Create(toInsert);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }
        #endregion

        static int Main(string[] args)
        {
            Console.WriteLine("***** Starting @{0}*****", DateTime.Now);
            EntityUpdater updater = new EntityUpdater();
            //Get a connection to the db
            SqlDataReader reader = updater.establishCommand();

            int successCount = 0;
            int failureCount = 0;

            if (reader != null)
            {
                IServiceProvider crmProvider = (IServiceProvider)updater.createCRMProvider();
                if (crmProvider != null)
                {
                    //We have a reader with the data from the db
                    while (reader.Read())
                    {
                        try
                        {
                            Entity entityToUpdate = new Entity("isp_pastelwarehouse");
                            entityToUpdate.Attributes.Add("isp_pastelwarehousecode", reader["PastelWareHouseCode"]);
                            entityToUpdate.Attributes.Add("isp_pastelwarehousename", reader["isp_pastelwarehousename"]);
                            if (updater.updateCRM(crmProvider, entityToUpdate))
                            {
                                successCount++;
                            }
                            else
                            {
                                failureCount++;
                                Console.WriteLine("\t{0}\t{1}", reader["PastelWareHouseCode"], reader["isp_pastelwarehousename"]);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            failureCount++;
                            Console.WriteLine("\t{0}\t{1}", reader["PastelWareHouseCode"], reader["isp_pastelwarehousename"]);
                        }

                    }
                    updater.closeConnection();
                    Console.WriteLine("*****Success = {0} ********", successCount);
                    Console.WriteLine("*****Failure = {0} ********", failureCount);
                    Console.WriteLine("***** Finished @{0}*****", DateTime.Now);
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            return 1;
        }

        public string ReadReg(string KeyName)
        {
            string sReturn = String.Empty;
            RegistryKey rk = Registry.LocalMachine;
            RegistryKey sk1 = rk.OpenSubKey("SOFTWARE\\ISPartners");
            if (sk1 != null)
            {
                sReturn = (string)sk1.GetValue(KeyName.ToUpper());
            }
            return sReturn;
        }


    }
}

