﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Crm.Plugins;
using Microsoft.Xrm;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk;
using Microsoft.Win32;  

namespace UpdateProductStockLevel
{
    class EntityUpdater
    {
        #region "SQL Setup
        string sqlQuery = "SELECT * FROM [Coinnect].[Scoin].[ProductStockLevel]";
        string stockQuery = "select isp_productstocklevelId,isp_name,isp_pastelwarehouse, isp_product,isp_productcode, isp_stocklevel, isp_price, isp_vat from Coinnect..isp_productstocklevel";
        SqlConnection thisConnection;
        SqlCommand command, stockCommand;
        SqlDataReader reader, stockReader;
        bool hasConnection = false;
        #endregion

        #region "CRM Setup"
        private string[] _columns = { "isp_pastelwarehouse", "isp_product", "isp_name", "isp_stocklevel" };
        private string userguid = "CD41E1E6-6DEB-E011-868E-000E0C3BF634";
        Guid companyGuid = new Guid("AE755E20-AABD-E011-8120-78ACC08839C9");
        EntityReference currencyRef = new EntityReference("transactioncurrency", new Guid("FC83003D-6C07-E111-838C-0800272BC48D"));
        #endregion

        #region "Dictionary Setup"
        Dictionary<String, String> warehouseMap = new Dictionary<String, String>();
        Dictionary<String, Entity> productMap = new Dictionary<String, Entity>();
        Dictionary<String, Entity> stockMap = new Dictionary<String, Entity>();
        Dictionary<String, Int32> globalStockMap = new Dictionary<String, Int32>();
        #endregion

        #region "Entity Ref Setup"
        EntityReference companyRef = new EntityReference();
        EntityReference warehouseRef = new EntityReference();
        EntityReference productRef = new EntityReference();


        public EntityUpdater()
        {
            companyRef.Id = companyGuid;
            //defaultCurrencyRef.LogicalName = "Currency";
            companyRef.Name = "Scoin";
        }
        #endregion

        private SqlDataReader establishCommand()
        {
            try
            {
                if (!hasConnection)
                {
                    getConnection();
                }

                if (hasConnection)
                {
                    command = thisConnection.CreateCommand();
                    command.CommandText = sqlQuery;
                    reader = command.ExecuteReader();
                    return reader;
                }
                else
                {
                    return null;
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        private void getConnection()
        {
            try
            {
                if (!hasConnection)
                {
                    string sConnStr = ReadReg("CRMDB");
                    thisConnection = new SqlConnection(sConnStr);
                    thisConnection.Open();
                    hasConnection = true;
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void closeConnection()
        {
            try
            {
                reader.Close();
                thisConnection.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        #region "createCRMProvider"
        /// <summary>
        /// Creates a provider to CRM
        /// </summary>
        /// <returns>Provider</returns>
        private TestServiceProvider createCRMProvider()
        {
            try
            {
                //Establish the connection to CRM
                TestPluginContext pluginContext = new TestPluginContext("sagcecrm", "Coinnect", "crmapp", "i$Partner$", "sagoldcoin");
                TestServiceProvider provider = new TestServiceProvider(pluginContext);
                return provider;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }
        #endregion

        #region "getWarehouses"
        /// <summary>
        /// Gets the current Pastel Warehouses from CRM and adds them to a dictionary so the Warehouse Code can be translated into its Guid for insert
        /// </summary>
        private void getWarehouses(IServiceProvider provider)
        {
            var wareHouseQuery = new QueryExpression("isp_pastelwarehouse") { Criteria = new FilterExpression() };
            wareHouseQuery.ColumnSet.AddColumns("isp_pastelwarehouseid");
            wareHouseQuery.ColumnSet.AddColumns("isp_pastelwarehousecode");
            wareHouseQuery.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
            wareHouseQuery.Criteria.AddCondition("isp_pastelcompanyidname", ConditionOperator.Equal, "SCOIN");

            var wareHouseQueryResult = new EntityCollection();

            IOrganizationService service = (IOrganizationService)provider.GetService(typeof(IOrganizationServiceFactory));
            wareHouseQueryResult = service.RetrieveMultiple(wareHouseQuery);

            for (int x = 0; x < wareHouseQueryResult.Entities.Count; x++)
            {
                warehouseMap.Add(wareHouseQueryResult.Entities[x]["isp_pastelwarehousecode"].ToString(), wareHouseQueryResult.Entities[x]["isp_pastelwarehouseid"].ToString());
            }
            Console.WriteLine("Warehouses Added : " + wareHouseQueryResult.Entities.Count);
        }
        #endregion

        #region "getStock"
        /// <summary>
        /// Gets the current Product Stock from CRM and adds them to a dictionary so they can be checked for changes
        /// </summary>
        private void getStock(/*IServiceProvider provider*/)
        {
            if (!hasConnection)
            {
                getConnection();
            }

            if (hasConnection)
            {
                stockCommand = thisConnection.CreateCommand();
                stockCommand.CommandText = stockQuery;
                stockReader = stockCommand.ExecuteReader();

                while (stockReader.Read())
                {
                    //Convert to Entity
                   
                    Entity temp = new Entity("isp_productstocklevel");
                    temp.Attributes.Add("isp_product", new EntityReference("product", new Guid(stockReader["isp_product"].ToString())));
                    temp.Attributes.Add("isp_stocklevel", int.Parse(stockReader["isp_stocklevel"].ToString()));
                    temp.Attributes.Add("isp_name", stockReader["isp_name"].ToString());
                    temp.Attributes.Add("isp_productstocklevelid", new Guid(stockReader["isp_productstocklevelid"].ToString()));
                    temp.Attributes.Add("isp_pastelwarehouse", new EntityReference("isp_pastelwarehouse", new Guid(stockReader["isp_pastelwarehouse"].ToString())));
                    temp.Attributes.Add("isp_productcode", stockReader["isp_productcode"].ToString());
                    
                    Money price = new Money(decimal.Parse(stockReader["isp_price"].ToString()));
                    temp.Attributes.Add("isp_price", price);
                    string dec = stockReader["isp_vat"].ToString();
                     if (dec == null) {
                         dec = ("0.00000");
                     } 
                    if (dec == "")
                     {
                         dec = ("0.00000");
                     }
                    if(dec.Contains('.')){
                        dec = dec.Substring(0,4);
                    } temp.Attributes.Add("isp_vat", decimal.Parse(dec)); 




                    string key = ((EntityReference)temp["isp_pastelwarehouse"]).Id.ToString() + ((EntityReference)temp["isp_product"]).Id.ToString();
                    if(!stockMap.ContainsKey(key))
                    stockMap.Add(key, temp);
                }
                stockReader.Close();
            }
        }
        #endregion

        #region "getProducts"
        /// <summary>
        /// Gets the current Products from CRM and adds them to a dictionary so the Product NUmber can be translated into its Guid for insert
        /// </summary>
        private void getProducts(IServiceProvider provider)
        {
            var productQuery = new QueryExpression("product") { Criteria = new FilterExpression() };
            productQuery.ColumnSet.AddColumns("productid");
            productQuery.ColumnSet.AddColumns("name");
            productQuery.ColumnSet.AddColumns("productnumber");
            productQuery.ColumnSet.AddColumns("quantityonhand");
            var productResult = new EntityCollection();

            IOrganizationService service = (IOrganizationService)provider.GetService(typeof(IOrganizationServiceFactory));
            productResult = service.RetrieveMultiple(productQuery);

            for (int x = 0; x < productResult.Entities.Count; x++)
            {
                String temp = productResult.Entities[x]["productnumber"].ToString();
                productMap.Add(temp, productResult.Entities[x]);
            }
        }
        #endregion

        #region "updateCRM"
        /// <summary>
        /// This will decide if the given entity needs to be updated or inserted.
        /// </summary>
        /// <param name="entity">Entity to check from the Stagging Table</param>
        /// <param name="provider">CRM provider to do the update/insert</param>
        /// <returns>true if successfully processed</returns>
        private bool updateCRM(IServiceProvider provider, Entity entity)
        {
            bool success = false;
            //Setup the CRM service
            IOrganizationService service = (IOrganizationService)provider.GetService(typeof(IOrganizationServiceFactory));

            try
            {
                if (warehouseMap.ContainsKey(entity["isp_pastelwarehouse"].ToString()))
                {
                    //Found the warehouse
                    warehouseRef.Id = new Guid(warehouseMap[entity["isp_pastelwarehouse"].ToString()]);
                    entity["isp_pastelwarehouse"] = warehouseRef;

                    if (productMap.ContainsKey(entity["isp_product"].ToString()))
                    {
                        Entity temp = productMap[entity["isp_product"].ToString()];
                        //Found the product
                        productRef.Id = new Guid(temp["productid"].ToString());
                        entity["isp_product"] = productRef;
                        entity.Attributes.Add("isp_name", temp["name"]);
                    }
                    else
                    {
                        Console.WriteLine("Failed To Find Product - {0}", entity["isp_product"].ToString());
                        return false;
                    }
                }
                else
                {
                    Console.WriteLine("Failed To Find WareHouse - {0}", entity["isp_pastelwarehouse"].ToString());
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }

            try
            {
                String key = ((EntityReference)entity["isp_pastelwarehouse"]).Id.ToString() + ((EntityReference)entity["isp_product"]).Id.ToString();
                if (stockMap.ContainsKey(key))
                {
                    //We have a match so check if it is the same and update
                    success = updateProductStock(service, stockMap[key], entity);
                }
                else
                {
                    success = insertProductStock(service, entity);
                    //We have not found the product and should therefore create it
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }
            return success;
        }
        #endregion

        #region "updateProductStock"
        /// <summary>
        /// This will check the given entity for update and if required update the entity in CRM
        /// <param name="service">CRM service to call the create on</param>
        /// <param name="retrieved">CRM Entity retrievevd and stored in stockHistoryMap</param>
        /// <param name="toUpdate">Entity to update in CRM</param>
        /// <returns>true if successful false otherwise</returns>
        private bool updateProductStock(IOrganizationService provider, Entity retrieved, Entity toUpdate)
        {
            bool update = false;
            try
            {
                decimal retrievedPrice = ((Money)retrieved["isp_price"]).Value;
                decimal entityPrice = ((Money)toUpdate["isp_price"]).Value;
                if (retrievedPrice != entityPrice)
                {
                    retrieved["isp_price"] = toUpdate["isp_price"];
                    update = true;
                }

                if (retrieved["isp_vat"].ToString() != toUpdate["isp_vat"].ToString())
                {
                    retrieved["isp_vat"] = toUpdate["isp_vat"];
                    update = true;
                }

                if (retrieved["isp_name"].ToString() != toUpdate["isp_name"].ToString())
                {
                    retrieved["isp_name"] = toUpdate["isp_name"];
                    update = true;
                }

                if (retrieved["isp_stocklevel"].ToString() != toUpdate["isp_stocklevel"].ToString())
                {
                    retrieved["isp_stocklevel"] = toUpdate["isp_stocklevel"];
                    update = true;
                }

                if (update)
                {
                    provider.Update(retrieved);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }

            return true;
        }
        #endregion

        private bool insertProductStock(IOrganizationService provider, Entity toInsert)
        {
            try
            {
                provider.Create(toInsert);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }

        static int Main(string[] args)
        {
            Console.WriteLine("***** Starting @{0}*****", DateTime.Now);
            EntityUpdater updater = new EntityUpdater();
            //get the stock from db
            updater.getStock();
            //Get a connection to the db
            SqlDataReader reader = updater.establishCommand();

            int successCount = 0;
            int failureCount = 0;

            if (reader != null)
            {
                IServiceProvider crmProvider = (IServiceProvider)updater.createCRMProvider();
                if (crmProvider != null)
                {
                    updater.getWarehouses(crmProvider);
                    updater.getProducts(crmProvider);
                    //updater.getStock(crmProvider);
                    //We have a reader with the data from the db
                    while (reader.Read())
                    {
                        try
                        {
                            Entity entityToUpdate = new Entity("isp_productstocklevel");
                            entityToUpdate.Attributes.Add("isp_pastelwarehouse", reader["StoreCode"]);
                            entityToUpdate.Attributes.Add("isp_product", reader["ProductCode"]);
                            entityToUpdate.Attributes.Add("isp_productcode", reader["ProductCode"]);

                            string dec = reader["VATPercentage"].ToString();
                            if (dec.Contains('.'))
                            {
                                dec = dec.Substring(0, 4);
                            }
                            else
                            {
                                dec = dec + ".00";
                            }
                            if (reader["VATPercentage"].ToString() == "-1")
                            {
                                entityToUpdate.Attributes.Add("isp_vat", decimal.Parse("1.00"));
                            }
                            else
                            {
                                entityToUpdate.Attributes.Add("isp_vat", decimal.Parse(dec));
                            }
                            //Values must not have cents should be whole numbers
                            String value = reader["ListPriceIncl"].ToString();

                            if (value.Contains('.'))
                            {
                                value = value.Substring(0, value.IndexOf('.'));
                            }

                            Money price = new Money(decimal.Parse(value));
                            entityToUpdate.Attributes.Add("isp_price", price);

                            entityToUpdate.Attributes.Add("transactioncurrencyid", updater.currencyRef);

                            if (int.Parse(reader["QuantityOnHand"].ToString()) < 0)
                            {
                                entityToUpdate.Attributes.Add("isp_stocklevel", 0);
                            }
                            else
                            {
                                entityToUpdate.Attributes.Add("isp_stocklevel", int.Parse(reader["QuantityOnHand"].ToString()));
                            }

                            if (updater.updateCRM(crmProvider, entityToUpdate))
                            {
                                successCount++;
                                if (successCount % 100 == 0)
                                    Console.WriteLine(successCount);
                            }
                            else
                            {
                                failureCount++;
                                // Console.WriteLine("\t{0}\t{1}\t{2}",  reader["StoreCode"], reader["ProductCode"], reader["QuantityOnHand"]);
                            }
                            if ((successCount + failureCount) % 2000 == 0)
                            {
                                Console.WriteLine("***Count = {0}***", successCount + failureCount);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            failureCount++;
                            Console.WriteLine("\t{0}\t{1}\t{2}", reader["StoreCode"], reader["ProductCode"], reader["QuantityOnHand"]);
                           
                        }

                    }
                    updater.closeConnection();
                    Console.WriteLine("*****Success = {0} ********", successCount);
                    Console.WriteLine("*****Failure = {0} ********", failureCount);
                    Console.WriteLine("***** Finished @{0}*****", DateTime.Now);
                   
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            return 1;
        }

        public string ReadReg(string KeyName)
        {
            string sReturn = String.Empty;
            RegistryKey rk = Registry.LocalMachine;
            RegistryKey sk1 = rk.OpenSubKey("SOFTWARE\\ISPartners");
            if (sk1 != null)
            {
                sReturn = (string)sk1.GetValue(KeyName.ToUpper());
            }
            return sReturn;
        }

    }
}

