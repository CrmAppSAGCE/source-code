﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Crm.Plugins;
using Microsoft.Win32;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace UpdateProductStockLevel
{
    class EntityUpdater
    {
        #region "SQL Setup
        string sqlQuery = "SELECT * FROM [Coinnect].[Scoin].[ProductStockLevel]  ";
        string stockQuery = "select S.isp_stockcheckid, S.isp_name, S.isp_pastelwarehouse, S.isp_product, S.isp_productcode, " +
            "  isnull(S.isp_stockfrompastel,0) as isp_stockfrompastel, P.price as isp_price, S.isp_vat " +
            " from Coinnect.dbo.isp_stockcheck S inner join Coinnect.dbo.Product P on S.isp_product = P.ProductId and s.isp_parentstock IS NULL ";
        SqlConnection thisConnection, thisLocalConnection;
        SqlCommand command, stockCommand;
        SqlDataReader reader, stockReader;
        bool hasConnection = false;
        bool hasLocalConnection = false;
        #endregion

        #region "CRM Setup"
        private string[] _columns = { "isp_pastelwarehouse", "isp_product", "isp_name", "isp_stockfrompastel" };
        private string userguid = "CD41E1E6-6DEB-E011-868E-000E0C3BF634";
        Guid companyGuid = new Guid("AE755E20-AABD-E011-8120-78ACC08839C9");
        EntityReference currencyRef = new EntityReference("transactioncurrency", new Guid("FC83003D-6C07-E111-838C-0800272BC48D"));
        #endregion

        #region "Dictionary Setup"
        Dictionary<String, String> warehouseMap = new Dictionary<String, String>();
        Dictionary<String, Entity> productMap = new Dictionary<String, Entity>();
        Dictionary<String, Entity> stockMap = new Dictionary<String, Entity>();
        Dictionary<String, Int32> globalStockMap = new Dictionary<String, Int32>();
        Dictionary<String, EntityReference> warehouseTeamMap = new Dictionary<String, EntityReference>();
        #endregion

        #region "Entity Ref Setup"
        EntityReference companyRef = new EntityReference();
        EntityReference warehouseRef = new EntityReference();
        EntityReference productRef = new EntityReference();


        public EntityUpdater()
        {
            companyRef.Id = companyGuid;
            //defaultCurrencyRef.LogicalName = "Currency";
            companyRef.Name = "Scoin";
        }
        #endregion

        #region "establishCommand"
        /// <summary>
        /// Established a connection to the db and executes the command prepared in the SQL Setup section
        /// </summary>
        /// <returns>SQLDataReader with the results of command</returns>
        private SqlDataReader establishCommand()
        {
            try
            {
                if (!hasConnection)
                {
                    getConnection();
                }

                if (hasConnection)
                {
                    command = thisConnection.CreateCommand();
                    command.CommandText = sqlQuery;
                    reader = command.ExecuteReader();
                    return reader;
                }
                else
                {
                    return null;
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }
        #endregion

        #region "getConnection"
        /// <summary>
        /// Established a connection to the db and executes the command prepared in the SQL Setup section
        /// </summary>
        /// <returns>SQLDataReader with the results of command</returns>
        private void getConnection()
        {
            try
            {
                if (!hasConnection)
                {
                    String sSQLConn = sGetRegSettings("STAGINGDB");
                    thisConnection = new SqlConnection(sSQLConn);
                    thisConnection.Open();
                    hasConnection = true;
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        #endregion

        #region "getLocalConnection"
        /// <summary>
        /// Established a connection to the db and executes the command prepared in the SQL Setup section
        /// </summary>
        /// <returns>SQLDataReader with the results of command</returns>
        private void getLocalConnection()
        {
            try
            {
                if (!hasLocalConnection)
                {
                    string sConnStr = sGetRegSettings("CRMDB");
                    thisLocalConnection = new SqlConnection(sConnStr);
                    thisLocalConnection.Open();
                    hasLocalConnection = true;
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        #endregion

        #region "closeConnection"
        /// <summary>
        /// Cleans up the db reader and connection
        /// </summary>
        private void closeConnection()
        {
            try
            {
                reader.Close();
                thisConnection.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        #endregion

        #region "createCRMProvider"
        /// <summary>
        /// Creates a provider to CRM
        /// </summary>
        /// <returns>Provider</returns>
        private TestServiceProvider createCRMProvider()
        {
            try
            {
                //Establish the connection to CRM
                TestPluginContext pluginContext = new TestPluginContext("sagcecrm", "Coinnect", "crmapp", "i$Partner$", "sagoldcoin");
                TestServiceProvider provider = new TestServiceProvider(pluginContext);
                return provider;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }
        #endregion

        #region "getWarehouses"
        /// <summary>
        /// Gets the current Pastel Warehouses from CRM and adds them to a dictionary so the Warehouse Code can be translated into its Guid for insert
        /// </summary>
        private void getWarehouses(IServiceProvider provider)
        {
            var wareHouseQuery = new QueryExpression("isp_pastelwarehouse") { Criteria = new FilterExpression() };
            wareHouseQuery.ColumnSet.AddColumns("isp_pastelwarehouseid");
            wareHouseQuery.ColumnSet.AddColumns("isp_pastelwarehousecode");
            wareHouseQuery.ColumnSet.AddColumns("isp_team");
            wareHouseQuery.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
            wareHouseQuery.Criteria.AddCondition("isp_pastelcompanyidname", ConditionOperator.Equal, "SCOIN");

            var wareHouseQueryResult = new EntityCollection();

            IOrganizationService service = (IOrganizationService)provider.GetService(typeof(IOrganizationServiceFactory));
            wareHouseQueryResult = service.RetrieveMultiple(wareHouseQuery);
            string warehouses = "...";
            for (int x = 0; x < wareHouseQueryResult.Entities.Count; x++)
            {
                warehouses += wareHouseQueryResult.Entities[x]["isp_pastelwarehousecode"].ToString() + "|";
                warehouseMap.Add(wareHouseQueryResult.Entities[x]["isp_pastelwarehousecode"].ToString(), wareHouseQueryResult.Entities[x]["isp_pastelwarehouseid"].ToString());
                warehouseTeamMap.Add(wareHouseQueryResult.Entities[x]["isp_pastelwarehouseid"].ToString(), wareHouseQueryResult.Entities[x].GetAttributeValue<EntityReference>("isp_team"));
            }
            Console.WriteLine("Warehouses Added : " + " " + warehouses);
            Console.WriteLine("Warehouses Added : " + wareHouseQueryResult.Entities.Count);
        }
        #endregion

        #region "getStock"
        /// <summary>
        /// Gets the current Product Stock from CRM and adds them to a dictionary so they can be checked for changes
        /// </summary>
        private void getStock()
        {
            if (!hasConnection)
            {
                getConnection();
            }

            if (hasConnection)
            {
                stockCommand = thisConnection.CreateCommand();
                stockCommand.CommandText = stockQuery;
                stockReader = stockCommand.ExecuteReader();

                while (stockReader.Read())
                {
                    Entity temp = new Entity("isp_stockcheck");
                    temp.Attributes.Add("isp_product", new EntityReference("product", new Guid(stockReader["isp_product"].ToString())));
                    temp.Attributes.Add("isp_stockfrompastel", int.Parse(stockReader["isp_stockfrompastel"].ToString()));
                    temp.Attributes.Add("isp_name", stockReader["isp_name"].ToString());
                    temp.Attributes.Add("isp_stockcheckid", new Guid(stockReader["isp_stockcheckid"].ToString()));
                    temp.Attributes.Add("isp_pastelwarehouse", new EntityReference("isp_pastelwarehouse", new Guid(stockReader["isp_pastelwarehouse"].ToString())));
                    temp.Attributes.Add("isp_productcode", stockReader["isp_productcode"].ToString());
                    string stockReaderPrice = "";
                    if (string.IsNullOrEmpty(stockReader["isp_price"].ToString()))
                    {
                        stockReaderPrice = "0.00";
                    }
                    else
                    {
                        stockReaderPrice = stockReader["isp_price"].ToString();
                    }
                    Money price = new Money(decimal.Parse(stockReaderPrice));
                    temp.Attributes.Add("isp_price", price);
                    string dec = stockReader["isp_vat"].ToString();
                    if (dec == null)
                    {
                        dec = ("0.00000");
                    }
                    if (dec == "")
                    {
                        dec = ("0.00000");
                    }
                    if (dec.Contains('.'))
                    {
                        dec = dec.Substring(0, 4);
                    } temp.Attributes.Add("isp_vat", decimal.Parse(dec));

                    string key = ((EntityReference)temp["isp_pastelwarehouse"]).Id.ToString() + ((EntityReference)temp["isp_product"]).Id.ToString();
                    if (!stockMap.ContainsKey(key))
                        stockMap.Add(key, temp);
                }
                stockReader.Close();
            }
        }
        #endregion

        #region "getProducts"
        /// <summary>
        /// Gets the current Products from CRM and adds them to a dictionary so the Product NUmber can be translated into its Guid for insert
        /// </summary>
        private void getProducts(IServiceProvider provider)
        {
            var productQuery = new QueryExpression("product") { Criteria = new FilterExpression() };
            productQuery.ColumnSet.AddColumns("productid");
            productQuery.ColumnSet.AddColumns("name");
            productQuery.ColumnSet.AddColumns("productnumber");
            productQuery.ColumnSet.AddColumns("quantityonhand");
            var productResult = new EntityCollection();

            IOrganizationService service = (IOrganizationService)provider.GetService(typeof(IOrganizationServiceFactory));
            productResult = service.RetrieveMultiple(productQuery);

            for (int x = 0; x < productResult.Entities.Count; x++)
            {
                String temp = productResult.Entities[x]["productnumber"].ToString();
                productMap.Add(temp, productResult.Entities[x]);
            }
        }
        #endregion

        #region "updateCRM"
        /// <summary>
        /// This will decide if the given entity needs to be updated or inserted.
        /// </summary>
        /// <param name="entity">Entity to check from the Stagging Table</param>
        /// <param name="provider">CRM provider to do the update/insert</param>
        /// <returns>true if successfully processed</returns>
        private bool updateCRM(IServiceProvider provider, Entity entity, bool mustUpdate)
        {
            bool success = false;
            if (mustUpdate)
            {
                IOrganizationService service = (IOrganizationService)provider.GetService(typeof(IOrganizationServiceFactory));
                try
                {
                    if (warehouseMap.ContainsKey(entity["isp_pastelwarehouse"].ToString()))
                    {
                        //Found the warehouse
                        warehouseRef.Id = new Guid(warehouseMap[entity["isp_pastelwarehouse"].ToString()]);
                        warehouseRef.LogicalName = "isp_pastelwarehouse";
                        entity["isp_pastelwarehouse"] = warehouseRef;
                        if (warehouseTeamMap.ContainsKey(warehouseRef.Id.ToString()))
                        {
                            if (warehouseTeamMap[warehouseRef.Id.ToString()] != null)
                            {
                                entity["ownerid"] = warehouseTeamMap[warehouseRef.Id.ToString()];
                            }

                        }
                        if (productMap.ContainsKey(entity["isp_product"].ToString()))
                        {
                            Entity temp = productMap[entity["isp_product"].ToString()];
                            //Found the product
                            productRef.Id = new Guid(temp["productid"].ToString());
                            productRef.LogicalName = "product";
                            entity["isp_product"] = productRef;
                            entity.Attributes.Add("isp_name", temp["name"]);

                        }
                        else
                        {
                            Console.WriteLine("Failed To Find Product - {0}", entity["isp_product"].ToString());
                            return false;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Failed To Find WareHouse - {0}", entity["isp_pastelwarehouse"].ToString());
                        return false;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    return false;
                }

                try
                {
                    if (((EntityReference)entity["isp_pastelwarehouse"]).Id.ToString() == "AF3E4A79-6885-E211-B2C8-00155D04F907")
                    {

                    }
                    String key = ((EntityReference)entity["isp_pastelwarehouse"]).Id.ToString() + ((EntityReference)entity["isp_product"]).Id.ToString();
                    if (stockMap.ContainsKey(key))
                    {
                        //We have a match so check if it is the same and update
                        success = updateProductStock(service, stockMap[key], entity);
                    }
                    else
                    {
                        success = insertProductStock(service, entity);
                        //We have not found the product and should therefore create it
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    return false;
                }
                return success;
            }
            return true;
        }
        #endregion

        #region "updateProductStock"
        /// <summary>
        /// This will check the given entity for update and if required update the entity in CRM
        /// <param name="service">CRM service to call the create on</param>
        /// <param name="retrieved">CRM Entity retrievevd and stored in stockHistoryMap</param>
        /// <param name="toUpdate">Entity to update in CRM</param>
        /// <returns>true if successful false otherwise</returns>
        private bool updateProductStock(IOrganizationService provider, Entity retrieved, Entity toUpdate)
        {
            bool update = false;
            try
            {
                decimal retrievedPrice = ((Money)retrieved["isp_price"]).Value;
                decimal entityPrice = ((Money)toUpdate["isp_price"]).Value;
                if (retrievedPrice != entityPrice)
                {
                    // toUpdate["isp_price"] = retrieved["isp_price"];
                    retrieved["isp_price"] = toUpdate["isp_price"];
                    update = true;
                }

                if (retrieved["isp_vat"].ToString() != toUpdate["isp_vat"].ToString())
                {
                    // toUpdate["isp_vat"] = retrieved["isp_vat"];
                    retrieved["isp_vat"] = toUpdate["isp_vat"];
                    update = true;
                }

                if (retrieved["isp_name"].ToString() != toUpdate["isp_name"].ToString())
                {
                    // toUpdate["isp_name"] = retrieved["isp_name"];
                    retrieved["isp_name"] = toUpdate["isp_name"];
                    update = true;
                }

                if (retrieved["isp_stockfrompastel"].ToString() != toUpdate["isp_stockfrompastel"].ToString())
                {
                    //toUpdate["isp_stockfrompastel"] = retrieved["isp_stockfrompastel"];
                    retrieved["isp_stockfrompastel"] = toUpdate["isp_stockfrompastel"];
                    update = true;
                }


                if (update)
                {
                    // provider.Update(toUpdate);
                    provider.Update(retrieved);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }

            return true;
        }
        #endregion

        #region "insertStockHistory"
        /// <summary>
        /// This will setup the given entity and try and insert it into CRM
        /// </summary>
        /// <param name="service">CRM service to call the create on</param>
        /// <param name="toInsert">Entity to create in CRM</param>
        /// <returns>true if successful false otherwise</returns>
        private bool insertProductStock(IOrganizationService provider, Entity toInsert)
        {
            try
            {
                provider.Create(toInsert);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }
        #endregion

        static int Main(string[] args)
        {
            Console.WriteLine("***** Starting @{0}*****", DateTime.Now);
            EntityUpdater updater = new EntityUpdater();
            //get the stock from db
            updater.getStock();
            //Get a connection to the db
            SqlDataReader reader = updater.establishCommand();

            int successCount = 0;
            int failureCount = 0;

            if (reader != null)
            {
                IServiceProvider crmProvider = (IServiceProvider)updater.createCRMProvider();
                if (crmProvider != null)
                {
                    updater.getWarehouses(crmProvider);
                    updater.getProducts(crmProvider);
                    //updater.getStock(crmProvider);
                    //We have a reader with the data from the db
                    while (reader.Read())
                    {
                        try
                        {
                            Entity entityToUpdate = new Entity("isp_stockcheck");
                            entityToUpdate.Attributes.Add("isp_pastelwarehouse", reader["StoreCode"]);
                            entityToUpdate.Attributes.Add("isp_product", reader["ProductCode"]);
                            entityToUpdate.Attributes.Add("isp_productcode", reader["ProductCode"]);

                            string dec = reader["VATPercentage"].ToString();
                            if (dec.Contains('.'))
                            {
                                dec = dec.Substring(0, 4);
                            }
                            else
                            {
                                dec = dec + ".00";
                            }
                            if (reader["VATPercentage"].ToString() == "-1")
                            {
                                entityToUpdate.Attributes.Add("isp_vat", decimal.Parse("1.00"));
                            }
                            else
                            {
                                entityToUpdate.Attributes.Add("isp_vat", decimal.Parse(dec));
                            }
                            //Values must not have cents should be whole numbers
                            String value = reader["ListPriceIncl"].ToString();

                            if (value.Contains('.'))
                            {
                                value = value.Substring(0, value.IndexOf('.'));
                            }

                            Money price = new Money(decimal.Parse(value));
                            entityToUpdate.Attributes.Add("isp_price", price);

                            entityToUpdate.Attributes.Add("transactioncurrencyid", updater.currencyRef);

                            int quantity = Convert.ToInt32((double)reader["QuantityOnHand"]); ;
                            bool mustUpdate = false;
                            if (quantity != null)
                            {
                                entityToUpdate.Attributes.Add("isp_stockfrompastel", quantity);
                                mustUpdate = true;
                            }

                            if (updater.updateCRM(crmProvider, entityToUpdate, mustUpdate))
                            {
                                successCount++;
                                if (successCount % 100 == 0)
                                    Console.WriteLine(successCount);
                            }
                            else
                            {
                                failureCount++;
                                // Console.WriteLine("\t{0}\t{1}\t{2}",  reader["StoreCode"], reader["ProductCode"], reader["QuantityOnHand"]);
                            }
                            if ((successCount + failureCount) % 2000 == 0)
                            {
                                Console.WriteLine("***Count = {0}***", successCount + failureCount);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            failureCount++;
                            Console.WriteLine("\t{0}\t{1}\t{2}", reader["StoreCode"], reader["ProductCode"], reader["QuantityOnHand"]);

                        }

                    }
                    updater.closeConnection();
                    Console.WriteLine("*****Success = {0} ********", successCount);
                    Console.WriteLine("*****Failure = {0} ********", failureCount);
                    Console.WriteLine("***** Finished @{0}*****", DateTime.Now);

                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            return 1;
        }

        private string sGetRegSettings(string sRegKey)
        {
            string result = String.Empty;
            RegistryKey rk = Registry.LocalMachine;
            RegistryKey sk1 = rk.OpenSubKey("Software\\ISPartners");
            if (sk1 != null)
            {
                result = (String)sk1.GetValue(sRegKey.ToUpper());
            }
            return result;
        }

    }


}

