﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Crm.Plugins;
using Microsoft.Xrm;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk;
using Microsoft.Win32;  

namespace UpdateProduct
{
    class EntityUpdater
    {
        #region "SQL setup"
        string sqlQuery = "SELECT * FROM [Coinnect].[Scoin].[Product]";
        SqlConnection thisConnection;
        SqlCommand command;
        SqlDataReader reader;
        #endregion

        #region "CRM Setup"
        private string[] _columns = { "name", "productnumber", "quantityonhand", "price", "standardcost", "isp_producttype", "isp_vatincl", "isp_vatexcl", "isp_vatcode", "isp_serviceitem", "isp_vat" };
        private string userguid = "CD41E1E6-6DEB-E011-868E-000E0C3BF634";
        Guid defaultPriceListGuid = new Guid("148DBD52-7908-E111-8B71-0800272BC48D");
        Guid defaultUnitGroup = new Guid("EB0898E1-FB13-4B64-A51F-2BFADDF3C0AF");
        Guid defaultUnitOfMeasure = new Guid("06A0E1CF-913B-44E9-BD3E-A6C04FB08DBC");
        Guid defaultCurrency = new Guid("FC83003D-6C07-E111-838C-0800272BC48D");
        #endregion

        #region "Entity Ref Setup"
        EntityReference defaultPriceListRef = new EntityReference();
        EntityReference defaultUnitGroupRef = new EntityReference();
        EntityReference defaultUOMRef = new EntityReference();
        EntityReference defaultCurrencyRef = new EntityReference();
        EntityReference productRef = new EntityReference();



        public EntityUpdater()
        {
            defaultPriceListRef.Id = defaultPriceListGuid;
            //defaultPriceListRef.LogicalName = "Price List";
            defaultPriceListRef.Name = "SA Gold Coin Pastel Integrated Pricelist";

            defaultUnitGroupRef.Id = defaultUnitGroup;
            //defaultUnitGroupRef.LogicalName = "UoMSchedule";
            defaultUnitGroupRef.Name = "Default Unit";

            defaultUOMRef.Id = defaultUnitOfMeasure;
            //defaultUOMRef.LogicalName = "UoM";
            defaultUOMRef.Name = "Primary Unit";

            defaultCurrencyRef.Id = defaultCurrency;
            //defaultCurrencyRef.LogicalName = "Currency";
            defaultCurrencyRef.Name = "Rand";
        }
        #endregion

        #region "establishCommand"
        /// <summary>
        /// Established a connection to the db and executes the command prepared in the SQL Setup section
        /// </summary>
        /// <returns>SQLDataReader with the results of command</returns>
        private SqlDataReader establishCommand()
        {
            try
            {
                string sConnStr = ReadReg("CRMDB");
                thisConnection = new SqlConnection(sConnStr);
                thisConnection.Open();
                command = thisConnection.CreateCommand();
                command.CommandText = sqlQuery;
                reader = command.ExecuteReader();
                return reader;
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);

            }
            return null;
        }
        #endregion

        #region "closeConnection"
        /// <summary>
        /// Cleans up the db reader and connection
        /// </summary>
        private void closeConnection()
        {
            try
            {
                reader.Close();
                thisConnection.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);

            }
        }
        #endregion

        #region "createCRMProvider"
        /// <summary>
        /// Creates a provider to CRM
        /// </summary>
        /// <returns>Provider</returns>
        private TestServiceProvider createCRMProvider()
        {
            try
            {
                //Establish the connection to CRM
                TestPluginContext pluginContext = new TestPluginContext("sagcecrm", "Coinnect", "crmapp", "i$Partner$", "sagoldcoin");
                //IOrganizationService service = (IOrganizationService)connectToCRM("https://nbcrfli.ispartnerscloud.co.za/XRMServices/2011/Organization.svc", "ruank@ispcld", "Milenko01");
                TestServiceProvider provider = new TestServiceProvider(pluginContext);
                //TestServiceProvider prov = new TestServiceProvider();
                return provider;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);

                return null;
            }
        }
        #endregion

        #region "updateCRM"
        /// <summary>
        /// This will decide if the given entity needs to be updated or inserted.
        /// </summary>
        /// <param name="entity">Entity to check from the Stagging Table</param>
        /// <param name="provider">CRM provider to do the update/insert</param>
        /// <returns>true if successfully processed</returns>
        private bool updateCRM(IServiceProvider provider, Entity entity)
        {
            bool success = false;
            //Setup the CRM service
            IOrganizationService service = (IOrganizationService)provider.GetService(typeof(IOrganizationServiceFactory));

            //Create the CRM query
            var refNumQuery = new QueryExpression("product") { Criteria = new FilterExpression(), PageInfo = new PagingInfo { Count = 1, PageNumber = 1 } };
            refNumQuery.Criteria.AddCondition("productnumber", ConditionOperator.Equal, entity["productnumber"]);
            //refNumQuery.ColumnSet = new ColumnSet(true);
            refNumQuery.ColumnSet.AddColumns(_columns);
            var retrieveResult = new EntityCollection();
            try
            {
                //Fetch from CRM
                retrieveResult = service.RetrieveMultiple(refNumQuery);

                if (retrieveResult.Entities.Count == 1)
                {
                    //We have a match so check if it is the same and update
                    success = updateProduct(service, retrieveResult.Entities[0], entity);
                }
                else if (retrieveResult.Entities.Count == 0)
                {
                    success = insertProduct(service, entity);
                    //We have not found the product and should therefore create it
                }
                else
                {
                    //Got more than one result abort
                    return false;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);

            }
            return success;
        }
        #endregion

        #region "updateProduct"
        /// <summary>
        /// This will check the given entity for update and if required update the entity in CRM
        /// <param name="service">CRM service to call the create on</param>
        /// <param name="retrieved">CRM Entity retrievevd and stored in stockHistoryMap</param>
        /// <param name="toUpdate">Entity to update in CRM</param>
        /// <returns>true if successful false otherwise</returns>
        private bool updateProduct(IOrganizationService provider, Entity retrieved, Entity toUpdate)
        {
            bool updateProduct = false;
            bool updatePriceListItem = false;

            if (retrieved["name"].ToString() != toUpdate["name"].ToString())
            {
                retrieved["name"] = toUpdate["name"];
                updateProduct = true;
            }

            OptionSetValue re = (OptionSetValue)retrieved["isp_producttype"];
            OptionSetValue tu = (OptionSetValue)toUpdate["isp_producttype"];
            if (re.Value != tu.Value)
            {
                retrieved["isp_producttype"] = toUpdate["isp_producttype"];
                updateProduct = true;
            }

            if (retrieved["quantityonhand"].ToString().Substring(0, retrieved["quantityonhand"].ToString().IndexOf(".")) != toUpdate["quantityonhand"].ToString())
            {
                retrieved["quantityonhand"] = decimal.Parse(toUpdate["quantityonhand"].ToString());
                updateProduct = true;
            }

            Microsoft.Xrm.Sdk.Money value = (Microsoft.Xrm.Sdk.Money)retrieved["price"];
            Microsoft.Xrm.Sdk.Money value1 = (Microsoft.Xrm.Sdk.Money)toUpdate["price"];
            if (value.Value != value1.Value)
            {
                retrieved["price"] = value1;
                updateProduct = true;
                updatePriceListItem = true;
            }

            value = (Microsoft.Xrm.Sdk.Money)retrieved["standardcost"];
            value1 = (Microsoft.Xrm.Sdk.Money)toUpdate["standardcost"];
            if (value.Value != value1.Value)
            {
                retrieved["standardcost"] = value1;
                updateProduct = true;
            }

            value = (Microsoft.Xrm.Sdk.Money)retrieved["isp_vatincl"];
            value1 = (Microsoft.Xrm.Sdk.Money)toUpdate["isp_vatincl"];
            if (value.Value != value1.Value)
            {
                retrieved["isp_vatincl"] = value1;
                updateProduct = true;
            }

            value = (Microsoft.Xrm.Sdk.Money)retrieved["isp_vatexcl"];
            value1 = (Microsoft.Xrm.Sdk.Money)toUpdate["isp_vatexcl"];
            if (value.Value != value1.Value)
            {
                retrieved["isp_vatexcl"] = value1;
                updateProduct = true;
            }

            value = (Microsoft.Xrm.Sdk.Money)retrieved["isp_vat"];
            value1 = (Microsoft.Xrm.Sdk.Money)toUpdate["isp_vat"];
            if (value.Value != value1.Value)
            {
                retrieved["isp_vat"] = value1;
                updateProduct = true;
            }

            int OldInt = Int32.Parse(retrieved["isp_vatcode"].ToString());
            int NewInt = Int32.Parse(toUpdate["isp_vatcode"].ToString());
            if (OldInt != NewInt)
            {
                retrieved["isp_vatcode"] = NewInt;
                updateProduct = true;
            }


            Microsoft.Xrm.Sdk.OptionSetValue opvalue = (Microsoft.Xrm.Sdk.OptionSetValue)retrieved["isp_producttype"];
            Microsoft.Xrm.Sdk.OptionSetValue opvalue1 = (Microsoft.Xrm.Sdk.OptionSetValue)toUpdate["isp_producttype"];
            if (opvalue.Value != opvalue1.Value)
            {
                retrieved["isp_producttype"] = opvalue1;
                updateProduct = true;
            }

            Boolean ServiceItem1 = (Boolean)retrieved["isp_serviceitem"];
            Boolean ServiceItem2 = (Boolean)toUpdate["isp_serviceitem"];
            if (ServiceItem1 != ServiceItem2)
            {
                retrieved["isp_serviceitem"] = ServiceItem2;
                updateProduct = true;
            }

            if (updateProduct)
            {
                try
                {
                    provider.Update(retrieved);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);

                    return false;
                }
            }

            if (updatePriceListItem)
            {
                try
                {
                    //Have to find the pricelist item to update
                    var refNumQuery = new QueryExpression("productpricelevel") { Criteria = new FilterExpression(), PageInfo = new PagingInfo { Count = 1, PageNumber = 1 } };
                    refNumQuery.Criteria.AddCondition("productid", ConditionOperator.Equal, retrieved["productid"]);
                    //refNumQuery.ColumnSet = new ColumnSet(true);
                    refNumQuery.ColumnSet = new ColumnSet(true);

                    var retrieveResult = provider.RetrieveMultiple(refNumQuery);

                    if (retrieveResult.Entities.Count == 1)
                    {
                        Entity priceListItem = retrieveResult.Entities[0];
                        priceListItem["amount"] = toUpdate["price"];
                        provider.Update(priceListItem);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);

                    return false;
                }

            }

            return true;
        }
        #endregion

        #region "insertProduct"
        /// <summary>
        /// This will setup the given entity and try and insert it into CRM
        /// </summary>
        /// <param name="service">CRM service to call the create on</param>
        /// <param name="toInsert">Entity to create in CRM</param>
        /// <returns>true if successful false otherwise</returns>
        private bool insertProduct(IOrganizationService provider, Entity toInsert)
        {
            try
            {
                //Need to insert the Product First then the PriceList
                //link to default unit of measure
                toInsert.Attributes.Add("defaultuomid", defaultUOMRef);
                //link to default unit group 
                toInsert.Attributes.Add("defaultuomscheduleid", defaultUnitGroupRef);
                //link to default pricelist
                toInsert.Attributes.Add("pricelevelid", defaultPriceListRef);
                Guid productGuid = provider.Create(toInsert);
                Entity priceList = new Entity("productpricelevel");
                priceList.Attributes.Add("pricelevelid", defaultPriceListRef);
                productRef.Id = productGuid;
                productRef.Name = toInsert["name"].ToString();
                priceList.Attributes.Add("productid", productRef);
                priceList.Attributes.Add("transactioncurrencyid", defaultCurrencyRef);
                priceList.Attributes.Add("uomid", defaultUOMRef);
                Microsoft.Xrm.Sdk.OptionSetValue values = new Microsoft.Xrm.Sdk.OptionSetValue();
                values.Value = 2;
                priceList.Attributes.Add("quantitysellingcode", values);
                values = new Microsoft.Xrm.Sdk.OptionSetValue();
                values.Value = 1;
                priceList.Attributes.Add("pricingmethodcode", values);
                priceList.Attributes.Add("amount", toInsert["price"]);
                Guid newPriceList = provider.Create(priceList);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }
        #endregion

        static int Main(string[] args)
        {
            Console.WriteLine("***** Starting @{0}*****", DateTime.Now);
            EntityUpdater updater = new EntityUpdater();
            //Get a connection to the db
            SqlDataReader reader = updater.establishCommand();

            int successCount = 0;
            int failureCount = 0;

            if (reader != null)
            {
                IServiceProvider crmProvider = (IServiceProvider)updater.createCRMProvider();
                if (crmProvider != null)
                {
                    //We have a reader with the data from the db
                    while (reader.Read())
                    {
                        try
                        {
                            Entity entityToUpdate = new Entity("product");
                            if (reader["Description"] != DBNull.Value && reader["Description"].ToString() != "")
                            {
                                entityToUpdate.Attributes.Add("name", reader["Description"]);
                            }
                            else
                            {
                                entityToUpdate.Attributes.Add("name", reader["ProductNumber"]);
                            }

                            entityToUpdate.Attributes.Add("productnumber", reader["ProductNumber"]);

                            if (decimal.Parse(reader["QuantityOnHand"].ToString()) < 0)
                            {
                                entityToUpdate.Attributes.Add("quantityonhand", decimal.Parse("0"));
                            }
                            else
                            {
                                entityToUpdate.Attributes.Add("quantityonhand", decimal.Parse(reader["QuantityOnHand"].ToString()));
                            }

                            Microsoft.Xrm.Sdk.Money money = new Microsoft.Xrm.Sdk.Money();
                            if (reader["ListPrice"] != DBNull.Value)
                            {
                                money = new Microsoft.Xrm.Sdk.Money();
                                money.Value = decimal.Parse(reader["ListPrice"].ToString());
                                entityToUpdate.Attributes.Add("price", money);
                            }
                            else
                            {

                                money.Value = decimal.Parse("0");
                                entityToUpdate.Attributes.Add("price", money);
                            }

                            if (reader["StandardCost"] != DBNull.Value)
                            {
                                money = new Microsoft.Xrm.Sdk.Money();
                                money.Value = decimal.Parse(reader["StandardCost"].ToString());
                                entityToUpdate.Attributes.Add("standardcost", money);
                            }
                            else
                            {
                                money = new Microsoft.Xrm.Sdk.Money();
                                money.Value = decimal.Parse("0");
                                entityToUpdate.Attributes.Add("standardcost", money);
                            }
                            if (reader["VatIncl"] != DBNull.Value)
                            {
                                money = new Microsoft.Xrm.Sdk.Money();
                                money.Value = decimal.Parse(reader["VatIncl"].ToString());
                                entityToUpdate.Attributes.Add("isp_vatincl", money);
                            }
                            else
                            {
                                money = new Microsoft.Xrm.Sdk.Money();
                                money.Value = decimal.Parse("0");
                                entityToUpdate.Attributes.Add("isp_vatincl", money);
                            }
                            if (reader["VatExcl"] != DBNull.Value)
                            {
                                money = new Microsoft.Xrm.Sdk.Money();
                                money.Value = decimal.Parse(reader["VatExcl"].ToString());
                                entityToUpdate.Attributes.Add("isp_vatexcl", money);
                            }
                            else
                            {
                                money = new Microsoft.Xrm.Sdk.Money();
                                money.Value = decimal.Parse("0");
                                entityToUpdate.Attributes.Add("isp_vatexcl", money);
                            }

                            ///jean tampering
                            //if (reader["VatIncl"] != DBNull.Value)
                            //{
                            //    money = new Microsoft.Xrm.Sdk.Money();
                            //    money.Value = decimal.Parse(reader["VatIncl"].ToString()) - decimal.Parse(reader["VatExcl"].ToString());
                            //    entityToUpdate.Attributes.Add("isp_vat", money);
                            //}
                            if (reader["ListPrice"] != DBNull.Value)
                            {

                                money = new Microsoft.Xrm.Sdk.Money();
                                money.Value = decimal.Parse(reader["ListPrice"].ToString());
                                if (reader["VatExcl"] != DBNull.Value)
                                {
                                    money.Value = money.Value - decimal.Parse(reader["VatExcl"].ToString());
                                }
                                if (reader["Description"].ToString().Contains("Para")) 
                                { 
                                    int t = 0; }
                                entityToUpdate.Attributes.Add("isp_vat", money);
                            }
                            else
                            {
                                money = new Microsoft.Xrm.Sdk.Money();
                                money.Value = decimal.Parse("0");
                                entityToUpdate.Attributes.Add("isp_vat", money);
                            }
                            Int32 IntValue = 0;
                            if (reader["VatCode"] != DBNull.Value)
                            {
                                IntValue = Int32.Parse(reader["VatCode"].ToString());
                                entityToUpdate.Attributes.Add("isp_vatcode", IntValue);
                            }
                            else
                            {
                                IntValue = 0;
                                entityToUpdate.Attributes.Add("isp_vatcode", IntValue);
                            }

                            Microsoft.Xrm.Sdk.OptionSetValue values = new Microsoft.Xrm.Sdk.OptionSetValue();
                            values.Value = int.Parse(reader["Type"].ToString());

                            if (values.Value == 0)
                            {
                                int x = 0;
                            }
                            if (values.Value > 2)
                            {
                                values.Value = 2;
                            }
                            entityToUpdate.Attributes.Add("isp_producttype", values);
                            bool NewVal = true;
                            string val = reader["Physical"].ToString();
                            if (val == "1")
                                NewVal = false;


                            entityToUpdate.Attributes.Add("isp_serviceitem", NewVal);

                            if (updater.updateCRM(crmProvider, entityToUpdate))
                            {
                                successCount++;
                                if (successCount % 50 == 0)
                                    Console.WriteLine(successCount);

                            }
                            else
                            {
                                //Console.ReadLine();
                                failureCount++;
                                Console.WriteLine("\t{0}\t{1}\t{2}\t{3}\t{4}\t{5}", reader["Description"], reader["ProductNumber"], reader["QuantityOnHand"], reader["ListPrice"], reader["StandardCost"], reader["Type"]);


                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            failureCount++;
                            Console.WriteLine("\t{0}\t{1}\t{2}\t{3}\t{4}\t{5}", reader["Description"], reader["ProductNumber"], reader["QuantityOnHand"], reader["ListPrice"], reader["StandardCost"], reader["Type"]);

                        }

                    }
                    updater.closeConnection();
                    Console.WriteLine("*****Success = {0} ********", successCount);
                    Console.WriteLine("*****Failure = {0} ********", failureCount);
                    Console.WriteLine("***** Finished @{0}*****", DateTime.Now);

                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            return 1;
        }

        public string ReadReg(string KeyName)
        {
            string sReturn = String.Empty;
            RegistryKey rk = Registry.LocalMachine;
            RegistryKey sk1 = rk.OpenSubKey("SOFTWARE\\ISPartners");
            if (sk1 != null)
            {
                    sReturn = (string)sk1.GetValue(KeyName.ToUpper());
            }
            return sReturn;
        }

    }
}

