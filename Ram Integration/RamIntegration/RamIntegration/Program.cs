﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Crm.Plugins;
using Microsoft.Win32;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace RamIntegration
{
    class InvoiceUpdater
    {
        #region "SQL Setup
        string sqlQuery = "SELECT * FROM [Coinnect].[dbo].[RAMMessages]";
        SqlConnection thisConnection;
        SqlCommand command;
        SqlDataReader reader;
        bool hasConnection = false;
        #endregion

        #region "Dictionary Setup"
        Dictionary<String, Entity> invoiceMap = new Dictionary<String, Entity>();
        #endregion

        #region "Connect to DB"
        /// <summary>
        /// Establish a command to the DB based on a query
        /// </summary>
        /// <returns>The reader of rows found in the DB</returns>
        private SqlDataReader establishCommand()
        {
            try
            {
                if (!hasConnection)
                {
                    getConnection();
                }
                if (hasConnection)
                {
                    command = thisConnection.CreateCommand();
                    command.CommandText = sqlQuery;
                    reader = command.ExecuteReader();
                    return reader;
                }
                else
                {
                    return null;
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// Gets a connection
        /// </summary>
        private void getConnection()
        {
            try
            {
                if (!hasConnection)
                {
                    string sConnStr = ReadReg("RAMDB");
                    thisConnection = new SqlConnection(sConnStr);
                    thisConnection.Open();
                    hasConnection = true;
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Closes a connection
        /// </summary>
        private void closeConnection()
        {
            try
            {
                reader.Close();
                thisConnection.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        #endregion

        #region "createCRMProvider"
        /// <summary>
        /// Creates a provider to CRM
        /// </summary>
        /// <returns>Provider</returns>
        private TestServiceProvider createCRMProvider()
        {
            try
            {
                //Establish the connection to CRM
                TestPluginContext pluginContext = new TestPluginContext("sagcecrm", "Coinnect", "crmapp", "i$Partner$", "sagoldcoin");
                TestServiceProvider provider = new TestServiceProvider(pluginContext);
                return provider;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }
        #endregion

        #region "getInvoices"
        /// <summary>
        /// Gets the current Invoices from CRM and adds them to a dictionary so the Invoice Number can be translated into its Guid for insert
        /// </summary>
        private void getInvoices(IServiceProvider provider, SqlDataReader reader)
        {
            string shipperReference = string.Empty;
            string temp = string.Empty;
            var invoiceResult = new EntityCollection();
            var invoiceQuery = new QueryExpression("invoice") { Criteria = new FilterExpression() };
            invoiceQuery.Criteria.AddCondition("name", ConditionOperator.Like, "IN%");
            invoiceQuery.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
            invoiceQuery.Criteria.AddCondition("shippingmethodcode", ConditionOperator.Equal, 100000000);
            invoiceQuery.ColumnSet.AddColumns("invoiceid");
            invoiceQuery.ColumnSet.AddColumns("name");
            invoiceQuery.ColumnSet.AddColumns("isp_deliverystatus");
            invoiceQuery.ColumnSet.AddColumns("isp_deliverymessage");

            IOrganizationService service = (IOrganizationService)provider.GetService(typeof(IOrganizationServiceFactory));
            invoiceResult = service.RetrieveMultiple(invoiceQuery);
            for (int x = 0; x < invoiceResult.Entities.Count; x++)
            {
                temp = invoiceResult.Entities[x]["name"].ToString();
                if (!invoiceMap.ContainsKey(temp))
                {
                    invoiceMap.Add(temp, invoiceResult.Entities[x]);
                }
            }
        }
        #endregion

        #region "updateCRM"
        /// <summary>
        /// This will decide if the given entity needs to be updated or inserted.
        /// </summary>
        /// <param name="entity">Entity to check from the RAM Table</param>
        /// <param name="provider">CRM provider to do the update</param>
        /// <returns>true if successfully processed</returns>
        private bool updateCRM(IServiceProvider provider, Entity entity)
        {
            bool success = false;
            //Setup the CRM service
            IOrganizationService service = (IOrganizationService)provider.GetService(typeof(IOrganizationServiceFactory));

            try
            {
                if (invoiceMap.ContainsKey(entity["name"].ToString()))
                {
                    try
                    {
                        success = updateInvoice(service, invoiceMap[entity["name"].ToString()], entity);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        return false;
                    }
                }
                else
                {
                    Console.WriteLine("Failed To Invoice - {0}", entity["name"].ToString());
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }
            return success;
        }
        #endregion

        #region "updateInvoice"
        /// <summary>
        /// This will check the given entity for update and if required update the entity in CRM
        /// <param name="service">CRM service to call the create on</param>
        /// <param name="retrieved">CRM Entity retrievevd and stored in InvoiceyMap</param>
        /// <param name="toUpdate">Entity to update in CRM</param>
        /// <returns>true if successful false otherwise</returns>
        private bool updateInvoice(IOrganizationService provider, Entity retrieved, Entity toUpdate)
        {
            bool update = false;
            try
            {
                if (!retrieved.Contains("isp_deliverystatus"))
                {
                    retrieved.Attributes.Add("isp_deliverystatus", new OptionSetValue(0));
                }
                if (((OptionSetValue)retrieved["isp_deliverystatus"]).Value != new OptionSetValue((int)toUpdate["isp_deliverystatus"]).Value)
                {
                    retrieved["isp_deliverystatus"] = new OptionSetValue((int)toUpdate["isp_deliverystatus"]);
                    update = true;
                }
                if (!retrieved.Contains("isp_deliverymessage"))
                {
                    retrieved.Attributes.Add("isp_deliverymessage", "");
                }
                if (retrieved["isp_deliverymessage"].ToString() != toUpdate["isp_deliverymessage"].ToString())
                {
                    retrieved["isp_deliverymessage"] = toUpdate["isp_deliverymessage"];
                    update = true;
                }
                if (update)
                {
                    provider.Update(retrieved);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return false;
            }

            return true;
        }
        #endregion

        /// <summary>
        /// Run the program
        /// </summary>
        /// <param name="args">args</param>
        /// <returns>1 or 0</returns>
        static int Main(string[] args)
        {
            Console.WriteLine("***** Starting @{0}*****", DateTime.Now);
            InvoiceUpdater updater = new InvoiceUpdater();

            //Get a connection to the db
            SqlDataReader reader = updater.establishCommand();
            if (reader != null)
            {
                IServiceProvider crmProvider = (IServiceProvider)updater.createCRMProvider();
                if (crmProvider != null)
                {
                    int successCount = 0;
                    int failureCount = 0;
                    int deliveryStatus;
                    string shipperReference = string.Empty;
                    updater.getInvoices(crmProvider, reader);
                    while (reader.Read())
                    {
                        try
                        {
                            switch ((string)reader["MovementType"])
                            {
                                case "DelVehArrCollected":
                                case "Consigned":
                                    deliveryStatus = 1; //Dispatched to courier
                                    break;
                                case "LineHaulArrDelHub":
                                    deliveryStatus = 2; //Arrived at Depot
                                    break;
                                case "DelVehDep":
                                    deliveryStatus = 3; //En Route
                                    break;
                                case "DelVehArrNonDel":
                                    deliveryStatus = 4; //Delivery attempt failed
                                    break;
                                case "DelVehArrPOD":
                                case "PODCapturePOD":
                                case "InOculus":
                                    deliveryStatus = 5; //Delivered
                                    break;
                                case "MiscClosure":
                                    deliveryStatus = 6; //Delivery Failed
                                    break;
                                default:
                                    deliveryStatus = 0;
                                    break;
                            }
                            if (Regex.IsMatch(reader["ShipperReference"].ToString(), @"^\d+"))
                            {
                                shipperReference = "IN" + reader["ShipperReference"].ToString();
                            }
                            else
                            {
                                shipperReference = reader["ShipperReference"].ToString();
                            }
                            Entity entityToUpdate = new Entity("invoice");
                            entityToUpdate.Attributes.Add("name", reader["ShipperReference"]);
                            entityToUpdate.Attributes.Add("isp_deliverystatus", deliveryStatus);
                            entityToUpdate.Attributes.Add("isp_deliverymessage", reader["MsgText"]);

                            if (updater.updateCRM(crmProvider, entityToUpdate))
                            {
                                successCount++;
                                if (successCount % 100 == 0)
                                    Console.WriteLine(successCount);
                            }
                            else
                            {
                                failureCount++;
                            }
                            if ((successCount + failureCount) % 2000 == 0)
                            {
                                Console.WriteLine("***Count = {0}***", successCount + failureCount);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            failureCount++;
                            Console.WriteLine("\t{0}\t{1}\t{2}", reader["ShipperReference"], reader["MovementType"], reader["MsgText"]);
                        }
                    }
                    updater.closeConnection();
                    Console.WriteLine("*****Success = {0} ********", successCount);
                    Console.WriteLine("*****Failure = {0} ********", failureCount);
                    Console.WriteLine("*****Finished @{0}*****", DateTime.Now);
                    //  Console.Read();
                    return 0;
                }
                else
                {
                    return 1;
                }
            }

            return 1;
        }

        /// <summary>
        /// Read the registry key
        /// </summary>
        /// <param name="KeyName">The Key name</param>
        /// <returns>The connection string found in the reg key</returns>
        public string ReadReg(string KeyName)
        {
            string sReturn = String.Empty;
            RegistryKey rk = Registry.LocalMachine;
            RegistryKey sk1 = rk.OpenSubKey("SOFTWARE\\ISPartners");
            if (sk1 != null)
            {
                sReturn = (string)sk1.GetValue(KeyName.ToUpper());
            }
            return sReturn;
        }
    }
}
